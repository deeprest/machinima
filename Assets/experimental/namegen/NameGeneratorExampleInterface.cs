﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

[CreateAssetMenu]
public class NameGeneratorExampleInterface : ScriptableObject
{
  [SerializeField] NameGenerator generator;

  [Header( "Text Object" )]
  [SerializeField] Font[] fonts;
  [SerializeField] GameObject NamePrefab;
  [SerializeField] Color[] colors = new[] {Color.white, Color.red, Color.yellow};
  [SerializeField] float yStride = 1.4f;

  [SerializeField] int Setting_GenCount = 10;
  [SerializeField] int Setting_TrainingMultiplier = 1;
  [SerializeField] bool Setting_GenTextObjects;

  List<GameObject> gens = new List<GameObject>();

  [ExposeMethod] void Seed() { generator.SeedGenerator(); }
  [ExposeMethod] void Clear() { generator.Clear(); }
  [ExposeMethod] void Dump() { generator.Dump(); }
  [ExposeMethod] void TrainDefault() { generator.TrainDefault( Setting_TrainingMultiplier ); }

  [ExposeMethod] void Generate()
  {
    if( Setting_GenTextObjects )
    {
      DeleteAll();
      GenerateList( 0, Setting_GenCount, null );
    }
    else
    {
      string output = "";
      for( int i = 0; i < Setting_GenCount; i++ )
        output += generator.Generate() + ".\n";
      Debug.Log( output );
    }
  }

  [ExposeMethod]
  public void DeleteAll()
  {
    generator.Clear();
    foreach( var gameObject in gens )
      Util.Destroy( gameObject );
    gens.Clear();
  }

  public GameObject GenerateGameObject( GameObject prefab, string text, Font font, bool setColor, Color color )
  {
    GameObject go = Instantiate( prefab );
    if( go.TryGetComponent( out TextMesh textMesh ) )
    {
      textMesh.text = text;
      textMesh.font = font;
      if( setColor )
        textMesh.color = color;
      go.GetComponent<MeshRenderer>().sharedMaterial = textMesh.font.material;
    }
    else if( go.TryGetComponent( out Text txt ) )
    {
      txt.text = text;
      txt.font = font;
      if( setColor )
        txt.color = color;
    }
    return go;
  }

  public void GenerateList( int startIndex, int count, Transform parent )
  {
    for( int i = 0; i < count; i++ )
    {
      GameObject go = GenerateGameObject( NamePrefab, generator.Generate(), fonts[Random.Range( 0, fonts.Length )], true, colors[Random.Range( 0, colors.Length )] );
      gens.Add( go );
      go.transform.SetParent( parent );
      go.transform.localPosition = new Vector3( 0, -yStride * (startIndex + i), 0 );
    }
  }
}