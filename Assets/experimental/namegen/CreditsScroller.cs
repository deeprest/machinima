﻿using UnityEngine;
using System.Collections.Generic;

public class CreditsScroller : MonoBehaviour
{
  public CreditsGenerator creditsGenerator;
  public NameGenerator nameGenerator;
  [SerializeField] GameObject NamePrefab;
  [SerializeField] TMPro.TMP_FontAsset NameFont;
  [SerializeField] GameObject CreditsTitlePrefab;
  [SerializeField] TMPro.TMP_FontAsset CreditFont;
  [SerializeField] float speed = 100;
  [SerializeField] float XOffset;
  [SerializeField] int ActiveCount = 10;
  [SerializeField] float KillHeight = 500;
  [SerializeField] float yStride = 200;
  [SerializeField] float yTitleNameOffset = -50;

  List<GameObject> gens = new List<GameObject>();
  bool active;

  [ExposeMethod]
  public void StartScroll()
  {
    active = true;
    CreateSingle();
  }

  [ExposeMethod]
  public void StopScroll()
  {
    active = false;
    nameGenerator.Clear();
    for( int i = 0; i < gens.Count; i++ )
      Destroy( gens[i].gameObject );
    gens.Clear();
  }

  public void Update()
  {
    if( !active )
      return;
    for( int i = 0; i < gens.Count; i++ )
    {
      if( gens[i].transform.localPosition.y > transform.localPosition.y + KillHeight )
      {
        Destroy( gens[i].gameObject );
        gens.RemoveAt( i-- );
      }
    }
    for( int i = 0; i < gens.Count; i++ )
      gens[i].transform.localPosition = gens[i].transform.localPosition + new Vector3( 0, Time.deltaTime * speed, 0 );
    for( int i = gens.Count; i < ActiveCount; i++ )
      CreateSingle();
  }

  public void CreateSingle()
  {
    Vector3 pos = Vector3.down * KillHeight;
    // middle align list, title on top

    if( gens.Count > 0 )
      pos = new Vector3( XOffset, gens[gens.Count - 1].transform.localPosition.y - yStride, 0 );
    GameObject go = Instantiate( CreditsTitlePrefab, transform );
    // scope
    {
      if( go.TryGetComponent( out TMPro.TextMeshProUGUI txt ) )
      {
        txt.text = creditsGenerator.GenName();
        txt.font = CreditFont;
      }
      gens.Add( go );
      go.transform.localScale = Vector3.one;
      go.transform.localPosition = pos;
    }

    // scope
    {
      GameObject nameGO = Instantiate( NamePrefab, go.transform );
      if( nameGO.TryGetComponent( out TMPro.TextMeshProUGUI txt ) )
      {
        txt.text = nameGenerator.Generate();
        txt.font = NameFont;
      }
      nameGO.transform.localScale = Vector3.one;
      nameGO.transform.localPosition = new Vector3( 0, yTitleNameOffset, 0 );
    }
  }
}