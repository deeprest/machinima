using UnityEngine;
using UnityEngine.UI;

public class Mushroom : MonoBehaviour
{
  public int dev_index;

  public Camera MushCamera;
  public RawImage MushImage;
  public Canvas MushCanvas;
  public RenderTexture MushRenderTexture;
  Timer timer = new Timer();
  [SerializeField] MushroomSettings current_active_settings;
  public MushroomSettings[] MushSettings;

  [System.Serializable]
  public struct MushroomSettings
  {
    public string name;
    // 228
    public int alpha;
    // 9+...    -1, -.5, 
    public float zoom;
    // .125  .25 .5 .75 1
    public float rot;
    // Default, behind, front
    public string layer;
    public int order;

    public float image_scale_x;
    public float image_scale_y;
    public bool realtime_offset;
  }

  MushroomSettings default_settings = new MushroomSettings() { name = "default", alpha = 0, layer = "Default", order = 0, rot = 0, zoom = 0, image_scale_x = 0, image_scale_y = 0};

  float btof( int a ) { return ((float)Mathf.Clamp( a, 0, 255 )) / 255f; }

  public MushroomSettings LerpValues( float a, MushroomSettings i, MushroomSettings t )
  {
    MushroomSettings r = new MushroomSettings();
    r.alpha = (int)(255f * Mathf.Lerp( btof( i.alpha ), btof( t.alpha ), a ));
    r.zoom = Mathf.Lerp( i.zoom, t.zoom, a );
    r.rot = Mathf.Lerp( i.rot, t.rot, a );
    // just assign values that cannot be lerped
    r.layer = t.layer;
    r.order = t.order;
    r.image_scale_x = Mathf.Lerp( i.image_scale_x, t.image_scale_x, a );
    r.image_scale_y = Mathf.Lerp( i.image_scale_y, t.image_scale_y, a );
    r.realtime_offset = t.realtime_offset;
    return r;
  }

  public void Enable( int index ) { Enable( MushSettings[Mathf.Min( MushSettings.Length - 1, index )] ); }

  public void Enable( MushroomSettings t )
  {
    timer.Stop( false );
    MushCanvas.enabled = true;
    MushCamera.enabled = true;
    MushCamera.Render();
    UpdateMushroomDisplay( t );
  }

  public void Disable()
  {
    timer.Stop( false );
    current_active_settings = default_settings;
    UpdateMushroomDisplay( current_active_settings );
    MushCanvas.enabled = false;
    MushCamera.enabled = false;
  }

  public Timer EnableLerp( float duration, int index ) { return EnableLerp( duration, MushSettings[Mathf.Min( MushSettings.Length - 1, index )] ); }

  public Timer EnableLerp( float duration, MushroomSettings t )
  {
    Enable( current_active_settings );
    MushroomSettings i = current_active_settings;
    timer.Start( duration, ( timer ) =>
    {
      MushroomSettings temp = LerpValues( timer.ProgressNormalized, i, t );
      UpdateMushroomDisplay( temp );
    }, () => { UpdateMushroomDisplay( t ); } );
    return timer;
  }

  public void DisableLerp( float duration )
  {
    MushroomSettings temp = current_active_settings;
    float initial_alpha = btof( temp.alpha );
    timer.Stop( false );
    timer.Start( duration, ( timer ) =>
    {
      temp.alpha = (int)(255 * Mathf.Lerp( initial_alpha, 0, timer.ProgressNormalized ));
      UpdateMushroomDisplay( temp );
    }, () => { Disable(); } );
  }

  public void UpdateMushroomDisplay( MushroomSettings ms )
  {
    current_active_settings = ms;
    // +zoom means smaller camera size, so it's negated here.
    MushCamera.orthographicSize = (MushImage.rectTransform.sizeDelta.y*0.5f) - ms.zoom;
    MushCamera.transform.rotation = Quaternion.Euler( 0, 0, ms.rot );
    /*MushCamera.transform.localPosition = new Vector3( ms.offset_x, ms.offset_y, -10 );*/
    Color ugh = MushImage.color;
    ugh.a = (float)ms.alpha / 255f;
    MushImage.color = ugh;
    MushImage.transform.localScale = new Vector3( 1f + ms.image_scale_x, 1f + ms.image_scale_y, 1 );
    MushCanvas.sortingLayerName = ms.layer;
    MushCanvas.sortingOrder = ms.order;
  }

  public bool EnableRealtimeOffset = true;
  public float offset_factor = 0.01f;

  public void LateUpdate()
  {
    if( EnableRealtimeOffset && current_active_settings.realtime_offset && Global.instance.CurrentPlayer )
    {
      MushCamera.transform.localPosition = new Vector3( Global.instance.CurrentPlayer.velocity.x * offset_factor, Global.instance.CurrentPlayer.velocity.y * offset_factor, -10 );
    }
  }

  [ExposeMethod()]
  public void Dev_Enable() { Enable( MushSettings[Mathf.Min( MushSettings.Length - 1, dev_index )] ); }

  [ExposeMethod()]
  public void Dev_EnableLerp() { EnableLerp( 2, MushSettings[Mathf.Min( MushSettings.Length - 1, dev_index )] ); }

  [ExposeMethod()]
  public void Dev_Disable() { Disable(); }

  [ExposeMethod()]
  public void Dev_DisableLerp() { DisableLerp( 2 ); }
}