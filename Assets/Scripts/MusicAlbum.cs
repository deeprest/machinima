using UnityEngine;
/*using UnityEngine.Localization;*/

[CreateAssetMenu( fileName = "FILENAME", menuName = "MENUNAME", order = 0 )]
public class MusicAlbum : ScriptableObject
{
  public string URL;
  public string Artist;
  public string Title;
  /*public LocalizedString Artist;
  public LocalizedString Title;*/
  public Sprite Art;
}