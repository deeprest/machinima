using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;


[CustomEditor( typeof(SpriteLibraryTemplate) )]
public class SpriteLibraryTemplateEditor : Editor
{
  public override void OnInspectorGUI()
  {
    SpriteLibraryTemplate slt = target as SpriteLibraryTemplate;

    if( GUI.Button( EditorGUILayout.GetControlRect( false, 30 ), "Generate Sprite Libraries" ) )
    {
      foreach( var libSprite in slt.LibrarySprites )
      {
        // preserve asset references to the given library by overwriting all fields instead of replacing the asset.
        UnityEngine.U2D.Animation.SpriteLibraryAsset sla = libSprite.LibraryAsset;
        List<string> categoryNames = new List<string>( sla.GetCategoryNames() );
        for( int i = 0; i < categoryNames.Count; i++ )
        {
          List<string> labelNames = new List<string>( sla.GetCategoryLabelNames( categoryNames[i] ) );
          for( int j = 0; j < labelNames.Count; j++ )
          {
            sla.RemoveCategoryLabel( categoryNames[i], labelNames[j], true );
          }
        }

        string path = AssetDatabase.GetAssetPath( libSprite.Spritesheet );
        // add +1 to sprites index below because the first given object is the texture asset
        Object[] sprites = AssetDatabase.LoadAllAssetsAtPath( path );
        
        foreach( var category in slt.Categories )
          for( int i = 0; i < category.indices.Length; i++ )
            sla.AddCategoryLabel( (Sprite) sprites[1+category.indices[i]], category.name, i.ToString() );

        EditorUtility.SetDirty( sla );
      }
    }

    DrawDefaultInspector();
  }
}
#endif

[System.Serializable]
public class Category
{
  public string name;
  public int[] indices;
}

[System.Serializable]
public class SpriteLibraryAndSprites
{
  //name is just for the editor list
  public string name;
  public UnityEngine.U2D.Animation.SpriteLibraryAsset LibraryAsset;
  public Texture2D Spritesheet;
}

[CreateAssetMenu]
public class SpriteLibraryTemplate : ScriptableObject
{
  public SpriteLibraryAndSprites[] LibrarySprites;
  public Category[] Categories;
}