﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.U2D.Animation;
#if UNITY_EDITOR
using UnityEditor;

public class SpriteAnimEditor : MonoBehaviourInspector
{
  public override void OnInspectorGUI()
  {
    DrawTheExposed();
    /*if( EditorGUILayout.DropdownButton( new GUIContent( "dropper" ), FocusType.Keyboard ) ) { }
    EditorGUILayout.BeginFadeGroup( 0 );
    EditorGUILayout.TextField( "hello" );
    EditorGUILayout.EndFadeGroup();*/
    DrawDefaultInspector();
  }
}
#endif

[ExecuteAlways]
public class SpriteAnimation : MonoBehaviour
{
  public SpriteResolver resolver;
  public bool USING_RESOLVER;

  public Renderer mr;
  /*public MeshFilter mf;*/
  public AnimSequence startAnim;

  public bool playing;
  [SerializeField, ReadOnly]
  AnimSequence CurrentSequence;
  [FormerlySerializedAs( "playAtAStart" )]
  public bool playAtStart = true;
  public AnimSequence[] anims = new AnimSequence[1];

  /*Rect frame;*/
  /*Vector2[] uvs = new Vector2[4];*/
  float animStart;
  int CurrentFrameIndex;

  int hash_Anim;
  Dictionary<string, AnimSequence> seqDictionary = new Dictionary<string, AnimSequence>();

#if UNITY_EDITOR
  void OnDrawGizmos()
  {
    // Ensure continuous Update calls.
    if( !Application.isPlaying )
    {
      EditorApplication.QueuePlayerLoopUpdate();
      SceneView.RepaintAll();
    }
  }
#endif

  void Awake()
  {
    if( mr == null )
      mr = GetComponent<MeshRenderer>();
    /*if( mf == null )
      mf = GetComponent<MeshFilter>();*/

    if( playAtStart )
      Play( startAnim );
  }

  public void PlayNoRestart( AnimSequence sequence )
  {
    if( CurrentSequence != sequence )
      Play( sequence );
  }

  public void Stop()
  {
    playing = false;
    mr.enabled = false;
    CurrentSequence = null;
  }

  [ExposeMethod]
  public void Play( AnimSequence sequence )
  {
    if( sequence == null || sequence.sprites.Length == 0 )
      return;
    if( !mr.enabled )
      mr.enabled = true;
    playing = true;
    CurrentSequence = sequence;
    CurrentFrameIndex = 0;
    if( Application.isPlaying )
      animStart = Time.time;
#if UNITY_EDITOR
    else
      animStart = (float) EditorApplication.timeSinceStartup;
#endif

    if( Application.isPlaying )
      mr.material.mainTexture = CurrentSequence.sprites[0].texture;
    else
      mr.sharedMaterial.mainTexture = CurrentSequence.sprites[0].texture;
  }

  public void Play( string seqName, bool restart = false )
  {
    if( seqDictionary.ContainsKey( seqName ) )
      Play( seqDictionary[seqName] );
    else
      Debug.LogWarning( "AnimSequence hash value not found." );
  }

  void UpdateFrame( float time )
  {
    if( playing && CurrentSequence != null )
    {
      if( resolver && CurrentSequence.labels.Length > 0 )
      {
        CurrentFrameIndex = Mathf.FloorToInt( (Mathf.Max( 0, time - animStart )) * (float) CurrentSequence.fps ) % CurrentSequence.labels.Length;
        resolver.SetCategoryAndLabel( CurrentSequence.category, CurrentSequence.labels[CurrentFrameIndex] );
        resolver.ResolveSpriteToSpriteRenderer();
      }
      else if( CurrentSequence.sprites.Length > 0 )
      {
        CurrentFrameIndex = Mathf.FloorToInt( (Mathf.Max( 0, time - animStart )) * (float) CurrentSequence.fps ) % CurrentSequence.sprites.Length;
        Sprite sprite = CurrentSequence.sprites[CurrentFrameIndex];
        if( mr is SpriteRenderer spriteRenderer )
        {
          spriteRenderer.sprite = sprite;
        }
      }
      /*else if( mr is MeshRenderer )
      {
        if( sprite != null )
          frame = new Rect( sprite.rect.x, (sprite.texture.height - sprite.rect.y - sprite.rect.height), sprite.rect.width, sprite.rect.height );

        if( mr.sharedMaterial.mainTexture != null )
        {
          float w = sprite.texture.width;
          float h = sprite.texture.height;

          uvs[0].x = frame.x / w;
          uvs[0].y = (h - frame.y) / h;
          uvs[1].x = (frame.x + frame.width) / w;
          uvs[1].y = (h - frame.y) / h;
          uvs[2].x = frame.x / w;
          uvs[2].y = ((h - frame.y) - frame.height) / h;
          uvs[3].x = (frame.x + frame.width) / w;
          uvs[3].y = ((h - frame.y) - frame.height) / h;

          if( Application.isPlaying )
          {
            mf.mesh.uv = uvs;
          }
          else
          {
            mf.sharedMesh.uv = uvs;
          }
        }
      }*/
    }
  }

  void LateUpdate()
  {
    if( Application.isPlaying )
      UpdateFrame( Time.time );
#if UNITY_EDITOR
    else
    {
      if( hash_Anim != anims.GetHashCode() )
      {
        hash_Anim = anims.GetHashCode();
        BuildDictionary();
      }
      UpdateFrame( (float) EditorApplication.timeSinceStartup );
    }
#endif
  }

  void BuildDictionary()
  {
    seqDictionary.Clear();
    foreach( var anim in anims )
    {
      // anims could be null in the list
      if( anim )
        seqDictionary.Add( anim.name, anim );
    }
  }
}