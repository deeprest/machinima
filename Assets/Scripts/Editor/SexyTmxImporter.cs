using UnityEngine;
using SuperTiled2Unity;
using SuperTiled2Unity.Editor;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine.UIElements;
using System.Collections.Generic;
using UnityEngine.Serialization;

namespace ugh
{
  [System.Serializable]
  public class NamePrefabPair
  {
    public string m_TypeName;
    public GameObject m_Prefab;
  }

  [FilePath( "ProjectSettings/UghSettings.asset", FilePathAttribute.Location.ProjectFolder )]
  public class UghSettings : ScriptableSingleton<UghSettings>
  {
    public List<TypePrefabReplacement> m_PrefabReplacements = new List<TypePrefabReplacement>();
    internal void SaveSettings() { Save( true ); }
  }

  public class UghSettingsProvider : SettingsProvider
  {
    private SerializedObject m_SerializedObject;
    private bool m_ShowPrefabReplacements;
    private ReorderableList m_PrefabReplacementList;

    public UghSettingsProvider() : base( "Project/PROJECT SETTINGS", SettingsScope.Project ) { }

    [SettingsProvider]
    public static SettingsProvider CreateCustomSettingsProvider()
    {
      var provider = new UghSettingsProvider();
      return provider;
    }

    public override void OnActivate( string searchContext, VisualElement rootElement )
    {
      base.OnActivate( searchContext, rootElement );
      /*UghSettings.instance.SaveSettings();*/
      m_SerializedObject = new SerializedObject( UghSettings.instance );

      // Prepare our list of prefab replacements
      var replacements = m_SerializedObject.FindProperty( nameof(UghSettings.m_PrefabReplacements) );
      m_PrefabReplacementList = new ReorderableList( m_SerializedObject, replacements, true, false, true, true )
      {
        headerHeight = 0,
        drawElementCallback = OnDrawPrefabReplacementElement,
      };
    }


    public override void OnGUI( string searchContext )
    {
      m_SerializedObject.Update();
      EditorGUI.BeginChangeCheck();

      EditorGUILayout.LabelField( "Prefabs", EditorStyles.boldLabel );
      m_PrefabReplacementList.DoLayoutList();
      if( EditorGUI.EndChangeCheck() )
      {
        if( m_SerializedObject.ApplyModifiedProperties() )
        {
          UghSettings.instance.SaveSettings();
        }
      }
    }

    private void OnDrawPrefabReplacementElement( Rect rect, int index, bool isActive, bool isFocused )
    {
      using( new GuiScopedIndent() )
      {
        const int kMargin = 20;
        float fieldWidth = (rect.width - kMargin) / 2;
        var element = m_PrefabReplacementList.serializedProperty.GetArrayElementAtIndex( index );
        var nameProperty = element.FindPropertyRelative( "m_TypeName" );
        var prefabProperty = element.FindPropertyRelative( "m_Prefab" );
        rect.y += 2;
        nameProperty.stringValue = EditorGUI.TextField( new Rect( rect.x, rect.y, fieldWidth, EditorGUIUtility.singleLineHeight ), nameProperty.stringValue );
        prefabProperty.objectReferenceValue = EditorGUI.ObjectField( new Rect( rect.x + fieldWidth + kMargin, rect.y, fieldWidth, EditorGUIUtility.singleLineHeight ), GUIContent.none, prefabProperty.objectReferenceValue, typeof(GameObject), false );
      }
    }
  }

  // The AutoCustomTmxImporterAttribute will force this importer to always be applied.
  // Leave this attribute off if you want to choose a custom importer from a drop-down list instead.
  [AutoCustomTmxImporter()]
  public class SexyTmxImporter : CustomTmxImporter
  {
    public override void TmxAssetImported( TmxAssetImportedArgs args )
    {
      // Note: args.ImportedSuperMap is the root of the imported prefab
      // You can modify the gameobjects and components any way you wish here
      // Howerver, the results must be deterministic (i.e. the same prefab is created each time)
      SuperMap map = args.ImportedSuperMap;
      //Debug.Log( $"Map '{map.name}' has been imported." );
      
      SuperColliderComponent[] sccs = map.gameObject.GetComponentsInChildren<SuperColliderComponent>();
      foreach( var scc in sccs )
        if( scc.gameObject.layer == LayerMask.NameToLayer( "pit" ) )
          scc.gameObject.AddComponent<Pit>();
      
      SuperLayer[] superLayers = map.GetComponentsInChildren<SuperLayer>();
      for( int i = 0; i < superLayers.Length; i++ )
      {
        var obj = superLayers[i];
        if( !Mathf.Approximately( obj.m_ParallaxX, 1 ) || !Mathf.Approximately( obj.m_ParallaxY, 1 ) )
        {
          ParallaxLayer lax = obj.gameObject.AddComponent<ParallaxLayer>();
          lax.Scale.x = obj.m_ParallaxX;
          lax.Scale.y = obj.m_ParallaxY;
          lax.offset.x = obj.m_OffsetX / args.AssetImporter.PixelsPerUnit;
          lax.offset.y = obj.m_OffsetY / -args.AssetImporter.PixelsPerUnit;

          /*if( obj is SuperTileLayer )
          {
            //SuperTileLayer stl = obj as SuperTileLayer;
            if( (obj as SuperTileLayer).TryGetComponent<Tilemap>( out Tilemap tmap ))
            {
              tmap.tileAnchor = Vector3.down;
            }
          }*/
        }
        if( obj is SuperImageLayer )
        {
          obj.gameObject.transform.position += Vector3.up;
        }
      }

      SuperObject[] objs = map.GetComponentsInChildren<SuperObject>();
      for( int j = 0; j < objs.Length; j++ )
      {
        var obj = objs[j];
        SuperCustomProperties props = obj.GetComponent<SuperCustomProperties>();
        // temp "out" object
        CustomProperty prop;
        if( obj.m_Type.Equals( "CameraZone" ) )
        {
          obj.gameObject.layer = LayerMask.NameToLayer( "trigger" );
          CameraZone zone = obj.gameObject.AddComponent<CameraZone>();
          zone.AffectX = false;
          zone.colliders = new Collider2D[0];
          if( obj.gameObject.TryGetComponent( out Collider2D cld ) )
          {
            cld.isTrigger = true;
            ArrayUtility.Add( ref zone.colliders, cld );
          }

          if( props.TryGetCustomProperty( "set_ortho", out prop ) )
            zone.SetOrtho = prop.GetValueAsBool();
          if( props.TryGetCustomProperty( "ortho_target", out prop ) )
            zone.orthoTarget = prop.GetValueAsFloat();
          if( props.TryGetCustomProperty( "encompass_bounds", out prop ) )
            zone.EncompassBounds = prop.GetValueAsBool();
          if( props.TryGetCustomProperty( "priority", out prop ) )
            zone.priority = prop.GetValueAsInt();
          if( props.TryGetCustomProperty( "affect_x", out prop ) )
            zone.AffectX = prop.GetValueAsBool();
        }
        else if( obj.m_Type.Equals( "SpawnPoint" ) )
        {
          TypePrefabReplacement npp = UghSettings.instance.m_PrefabReplacements.Find( x => x.m_TypeName == "SPAWN_POINT" );
          if( npp == null )
          {
            Debug.LogError( "SPAWN_POINT is missing from Ugh Settings -> Prefab" );
            return;
          }
          obj.gameObject.SetActive( false );
          GameObject go = Object.Instantiate( npp.m_Prefab, obj.transform.position, Quaternion.identity, obj.transform.parent );
          if( props.TryGetCustomProperty( "first_spawn", out prop ) )
            if( prop.GetValueAsBool() )
            {
              go.tag = "FirstSpawn";
            }
        }
        
        /*else if( obj.m_Type.Equals( "Pit" ) )
        {
          TypePrefabReplacement npp = UghSettings.instance.m_PrefabReplacements.Find( x => x.m_TypeName == "PIT" );
          if( npp == null )
          {
            Debug.LogError( "PIT is missing from Ugh Settings -> Prefab" );
            return;
          }
          GameObject pitGO = Object.Instantiate( npp.m_Prefab, obj.transform.position, Quaternion.identity, obj.transform.parent );

          {
            if( obj.gameObject.TryGetComponent( out PolygonCollider2D poly ) )
            {
              PolygonCollider2D p2 = pitGO.AddComponent<PolygonCollider2D>();
              p2.SetPath( 0, poly.points );
            }
            else if( obj.gameObject.TryGetComponent( out BoxCollider2D box ) )
            {
              BoxCollider2D b2 = pitGO.AddComponent<BoxCollider2D>();
              b2.offset = box.offset;
              b2.size = box.size;
            }
          }

          Pit pit = pitGO.GetComponent<Pit>();
          if( props.TryGetCustomProperty( "line", out prop ) )
          {
            if( prop.m_Type == "object" )
            {
              int id = prop.GetValueAsInt();
              GameObject line = args.AssetImporter.GetObject( id );
              if( !line )
              {
                Debug.LogError( "Pit line object reference did not resolve." );
              }
              else if( line.TryGetComponent( out EdgeCollider2D edge ) )
              {
                pit.localPath = new Vector2[edge.points.Length];
                Vector2 offset = line.transform.position - pit.transform.position;
                for( int i = 0; i < edge.points.Length; i++ )
                  pit.localPath[i] = offset + edge.points[i];
              }
              else if( line.TryGetComponent( out PolygonCollider2D poly ) )
              {
                pit.localPath = new Vector2[poly.points.Length];
                Vector2 offset = line.transform.position - pit.transform.position;
                for( int i = 0; i < poly.points.Length; i++ )
                  pit.localPath[i] = offset + poly.points[i];
              }
              else if( line.TryGetComponent( out BoxCollider2D box ) )
              {
                pit.localPath = new Vector2[4];
                Vector2 offset = line.transform.position - pit.transform.position;

                float bx = box.size.x * 0.5f;
                float by = box.size.y * 0.5f;
                pit.localPath[0] = offset + box.offset + new Vector2( -bx, -by );
                pit.localPath[1] = offset + box.offset + new Vector2( bx, -by );
                pit.localPath[2] = offset + box.offset + new Vector2( bx, by );
                pit.localPath[3] = offset + box.offset + new Vector2( -bx, by );
              }
              line.SetActive( false );
            }
            else
            {
              Debug.LogError( "Pit property named \"line\" must be an object reference" );
            }
          }
          else
          {
            Debug.LogError( "Pit needs an object property named \"line\" in Tiled" );
          }
          obj.gameObject.SetActive( false );
        }*/
      }
    }
  }

  // Use DisplayNameAttribute to control how class appears in the list
  /*[DisplayName("My Other Importer")]
  public class MyOtherTmxImporter : CustomTmxImporter
  {
    public override void TmxAssetImported(TmxAssetImportedArgs args)
    {
      // Just log the number of layers in our tiled map
      var map = args.ImportedSuperMap;
      var layers = map.GetComponentsInChildren<SuperLayer>();
      Debug.LogFormat("Map '{0}' has {1} layers.", map.name, layers.Length);
    }
  }*/
}