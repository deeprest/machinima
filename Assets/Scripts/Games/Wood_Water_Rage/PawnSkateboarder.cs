using System.Collections.Generic;
using UnityEngine;
using SuperTiled2Unity;
using UnityEngine.Serialization;
using UnityEngine.Tilemaps;

[SelectionBase]
public class PawnSkateboarder : Pawn
{
  [Header( "References" )]
  [SerializeField] SpriteMask mask;

  [Header( "Sound" )]
  public new AudioSource audio;
  public AudioClip soundJump;
  public AudioClip soundDamage;
  public AudioClip soundPickup;

  public Transform Board;
  public Transform Rider;

  [Header( "Settings" )]
  public float speedFactorNormalized = 0;
  [SerializeField]
  public float Acceleration = 10;
  public float BrakeCoef = 1.5f;
  public float MaxSpeed = 10;
  public float MinSpeed = .1f;
  public float VerticalSpeed = 4;
  public float TurnAnimDelayFactor = 1;

  public float jumpHeight = 3;
  /*public float jumpVelMin = 5;
  public float jumpVelMax = 6f;
  public float jumpSpeed { get { return (jumpVelMin + (jumpVelMax - jumpVelMin) * speedFactorNormalized); } }*/
  public float jumpDuration = .4f;
  public float fallDuration = 3;


  public new Vector2 Velocity
  {
    get
    {
      /*if( carryEntity != null )
        return velocity + ((IVelocity)carryEntity).GetVelocityAtPoint( transform.position );
      else if( carryConveyorBelt != null )
        return velocity + conveyorMove;
      else*/
      return velocity; /* + inertia;*/
    }
  }

  public bool IsPlayer { get { return Global.instance.CurrentPlayer == this; } }

  public bool jumpStart { get { return input.Jump && !pinput.Jump; } }

  public bool jumpStop { get { return !input.Jump && pinput.Jump; } }


  [Header( "State" )]
  public bool facingRight = true;

  public enum State
  {
    COAST,
    ACCEL,
    BRAKE,
    TURN_AWAY,
    TURN_TOWARD,
    JUMPING,
    FALLING,
    FACEDOWN,
    HOP,
    GRIND,
    FAST
  }

  public State state;

  Timer jumpTimer = new Timer();
  Timer fallTimer = new Timer();

  // COLLISION
  ContactFilter2D filter2D;
  // compare with y coord.
  const float collisionCornerY = 0.658504546f;
  // compare with x coord.
  const float collisionCornerX = 0.752576649f;
  // Collision: cached for optimization
  const int BipedDirectionalRaycastHits = 8;
  int HitLayers;
  RaycastHit2D hitTop;
  RaycastHit2D hitBottom;
  RaycastHit2D hitRight;
  RaycastHit2D hitLeft;

  [Header( "Player Damage" )]
  [SerializeField] float damageDuration = 0.5f;
  public bool takingDamage;
  Timer damageTimer = new Timer();
  bool damagePulseOn;
  Timer damagePulseTimer = new Timer();
  public float damagePulseInterval = .1f;
  public float damageBlinkDuration = 1f;
  [SerializeField] private AnimationCurve damageShakeCurve;

  protected override void Awake()
  {
    base.Awake();
    /*InitializeParts();*/
    bottomHits = new RaycastHit2D[BipedDirectionalRaycastHits];
    topHits = new RaycastHit2D[BipedDirectionalRaycastHits];
    leftHits = new RaycastHit2D[BipedDirectionalRaycastHits];
    rightHits = new RaycastHit2D[BipedDirectionalRaycastHits];
    filter2D = new ContactFilter2D() { layerMask = Global.CharacterCollideLayers | Global.TriggerLayers, useLayerMask = true, useTriggers = false };

    if( controller == null && assignedController != null )
    {
      controller = assignedController.GetNewController();
      // assign pawn later. gives Global a chance to assign CurrentPlayer/PlayerController
      // controller.AssignPawn( this );
    }
  }

  protected override void Start()
  {
    HitLayers = Global.TriggerLayers | Global.CharacterDamageLayers;
    if( AutoPopulateLists )
      PopulateLists();

    // unpack
    InteractIndicator.SetActive( false );
    InteractIndicator.transform.SetParent( null );

    // acquire abilities serialized into the prefab
    /*abilities = new List<Ability>( abilityDefs.Count );
    for( int i = 0; i < abilityDefs.Count; i++ )
    {
      Ability alt = abilityDefs[i].GetNewAbility();
      abilities.Add( alt );
      alt.OnAcquire( this );
    }
    if( abilities.Count > 0 && CurrentAbilityIndex < abilities.Count )
      AssignAbility( abilities[CurrentAbilityIndex] );

    if( weaponDefs.Count > 0 && CurrentWeaponIndex < weaponDefs.Count )
      AssignWeapon( weaponDefs[CurrentWeaponIndex] );*/

    if( controller != null )
    {
      controller.AssignPawn( this );
    }

    if( controller == Global.instance.PlayerController )
    {
      Global.instance.OnPlayerHealthChange( Health, MaxHealth );
    }
  }

  public override void OnControllerAssigned()
  {
    // settings are read before player is created, so set player settings here.
    /*speedFactorNormalized = Global.instance.FloatSetting["PlayerSpeedFactor"].Value;*/
    state = State.COAST;
    velocity = Vector2.zero;
    Rider.localPosition = Vector3.zero;
    Board.parent = transform;
    Board.localPosition = Vector3.zero;
    lastKnownSafePosition = transform.position;
  }

  public override void PreSceneTransition()
  {
    velocity = Vector2.zero;
    InteractIndicator.transform.SetParent( gameObject.transform );
    /*StopCharge();
    Reticle.transform.SetParent( gameObject.transform );
    if( ability != null )
      ability.PreSceneTransition();
    if( weapon != null )
      weapon.PreSceneTransition();*/
    UnselectWorldSelection();
  }

  public override void PostSceneTransition()
  {
    InteractIndicator.SetActive( false );
    InteractIndicator.transform.SetParent( null );
    /*Reticle.transform.SetParent( null );
    if( ability != null )
      ability.PostSceneTransition();
    if( weapon != null )
      weapon.PreSceneTransition();*/
    // prevent latent state variables from affecting logic (through state variables, such as inertia)
    collideBottom = collideLeft = collideRight = collideTop = false;
  }

  protected override void OnDestroy()
  {
    if( Global.IsQuiting )
      return;
    base.OnDestroy();
    damageTimer.Stop( false );
    damagePulseTimer.Stop( false );
    /*climbStartTimer.Stop( false );
    if( chargeEffect != null )
    {
      audio2.Stop();
      Destroy( chargeEffectGO );
    }
    chargeStartDelay.Stop( false );
    chargePulse.Stop( false );
    if( weapon != null )
      weapon.Deactivate();
    if( ability != null )
      ability.Deactivate();

    Destroy( Reticle.gameObject );*/
    Destroy( InteractIndicator );
  }

  [SerializeField] public Animator animatorBoard;

  [SerializeField] string AnimRider = "debug";
  private string pAnimRider = "idle";
  [SerializeField] string AnimBoard = "debug";
  private string pAnimBoard = "idle";

  Timer turnTimer = new Timer();
  Timer turnToCoastTimer = new Timer();
  public float turnToCoastTimeout = 2;
  // factor of vertical speed (turn speed)
  [ReadOnly] public float turnAnimDelay = 0;
  bool turnComplete;

  /*public bool OverrideAnimation;
  public string OverrideAnimationName;*/

  // speed when hit occurs
  float hitSpeed;
  Vector2 hitPoint;
  public float fallHeight = 2;
  public Vector3 lastKnownSafePosition;
  float safeLeadDistance = 4f;
  /*
NEED:
damage blink
grinding
*/

  public override void EntityUpdate()
  {
    if( !enabled )
      return;
    float DT = Time.deltaTime;
    velocity.y = 0;

    // conditions for normal input
    if( state != State.JUMPING && state != State.HOP && state != State.FALLING && state != State.FACEDOWN )
    {
      if( Vector2.SqrMagnitude( transform.position - lastKnownSafePosition ) > safeLeadDistance * safeLeadDistance )
      {
        hitCount = Physics2D.BoxCast( (Vector2)transform.position + boxBoard.offset, boxBoard.size, 0, Vector2.right * safeLeadDistance, filter2D, Global.RaycastHits, velocity.magnitude * DT );
        if( hitCount == 0 )
        {
          lastKnownSafePosition = transform.position;
        }
      }
      if( jumpStart && input.HopModifier )
      {
        StartJump();
      }
      else if( jumpStart )
      {
        StartHop();
      }
      else if( input.MoveUp )
      {
        if( state != State.TURN_AWAY )
        {
          turnComplete = false;
          turnAnimDelay = 1f / (TurnAnimDelayFactor * 4f * VerticalSpeed);
          turnTimer.Start( turnAnimDelay, null, () => { turnComplete = true; } );
        }
        if( collideTop )
          state = State.COAST;
        else
          state = State.TURN_AWAY;
        velocity.y = VerticalSpeed;
        // allow braking while turning
        if( input.MoveLeft )
          velocity.x -= Acceleration * BrakeCoef * DT;
      }
      else if( input.MoveDown )
      {
        if( state != State.TURN_TOWARD )
        {
          turnComplete = false;
          turnTimer.Start( turnAnimDelay, null, () => { turnComplete = true; } );
        }
        if( collideBottom )
          state = State.COAST;
        else
          state = State.TURN_TOWARD;
        velocity.y = -VerticalSpeed;
        // allow braking while turning
        if( input.MoveLeft )
          velocity.x -= Acceleration * BrakeCoef * DT;
      }
      else if( input.MoveRight )
      {
        if( velocity.x >= MaxSpeed )
          state = State.FAST;
        else
          state = State.ACCEL;
        velocity += Vector2.right * Acceleration * DT;
      }
      else if( input.MoveLeft )
      {
        state = State.BRAKE;
        velocity.x -= Acceleration * BrakeCoef * DT;
      }
      else
      {
        if( velocity.x >= MaxSpeed || turnToCoastTimer.IsActive )
          state = State.FAST;
        else
          state = State.COAST;
      }

      // todo split state logic from velocity calculation. There is only one state, but pawn should be able to move (for instance) right and down.

      if( state == State.TURN_AWAY || state == State.TURN_TOWARD || state == State.BRAKE )
      {
        turnToCoastTimer.Start( turnToCoastTimeout );
      }

      velocity.x = Mathf.Min( MaxSpeed, Mathf.Max( MinSpeed, velocity.x ) );
      /*if( !input.MoveUp && !input.MoveDown )
        velocity.y -= Mathf.Sign( velocity.y ) * Mathf.Min( Mathf.Abs( velocity.y * DecelCoef ), Mathf.Abs( velocity.y ) );*/
    }

    // COLLISION!! collide, fall and faceplant
    if( collideRight && state != State.FACEDOWN && state != State.FALLING && !takingDamage )
    {
      Faceplant( () =>
      {
        RespawnWithGracePeriod( lastKnownSafePosition );
      } );
    }

    if( state == State.FALLING )
    {
      velocity = Vector2.zero;
    }
    if( state == State.FACEDOWN )
    {
      velocity = Vector2.zero;
    }
    if( state != State.TURN_AWAY && state != State.TURN_TOWARD )
    {
      turnTimer.Stop( false );
    }

    collideRight = false;
    collideLeft = false;
    collideTop = false;
    collideBottom = false;
    bottomHitCount = 0;
    topHitCount = 0;
    rightHitCount = 0;
    leftHitCount = 0;

    Vector2 boffset = Vector2.zero;
    Vector2 vpos = (Vector2)transform.position;

    if( state != State.HOP && state != State.JUMPING && state != State.GRIND && state != State.FALLING && state != State.FACEDOWN )
    {
      boffset = boxBoard.offset;
      /*boffset.x *= Mathf.Sign( transform.localScale.x );*/
      vpos = (Vector2)boxBoard.transform.position + boffset;
      /*vpos += boffset;*/
      Vector2 bsiz = boxBoard.size;
      // Respawn (etc) might set the position explicitly. If it's further away than the snap distance then adjust it here.
      /*if( Vector2.SqrMagnitude( (Vector2)transform.position - pos ) > 1f / 16f )
          pos = transform.position;*/

      /*int twowayLayer = LayerMask.NameToLayer( "twowayPlatform" );
int downMask = Global.CharacterCollideLayers;*/
      /*if( ignoreTwowayPlatform || flying )
        downMask = Global.CharacterCollideLayers & ~LayerMask.GetMask( "twowayPlatform" );*/

#if UNITY_EDITOR || DEVELOPMENT_BUILD
      if( Popcron.Gizmos.Enabled )
      {
        Popcron.Gizmos.Square( vpos, bsiz, Color.green );
        /*Popcron.Gizmos.Square( vpos + velocity * DT, bsiz, Color.cyan );*/
      }
#endif

      // vertical. adjust before testing again below.
      Vector2 vely = new Vector2( 0, velocity.y );
      Vector2 boxy = new Vector2( bsiz.x - contactSeparation, bsiz.y );
      hitCount = Physics2D.BoxCast( vpos, boxy, 0, vely, filter2D, Global.RaycastHits, Mathf.Abs( velocity.y * DT ) );
      for( int i = 0; i < hitCount; i++ )
      {
        hit = Global.RaycastHits[i];
        if( hit.transform.IsChildOf( transform ) )
          continue;
        if( ((1 << hit.transform.gameObject.layer) & Global.TriggerLayers) == 0 )
        {
          if( hit.normal.y < -collisionCornerY )
          {
            collideTop = true;
            // velocity.y = Mathf.Min( velocity.y, 0 );
            vpos.y = hit.point.y - (bsiz.y * 0.5f) - contactSeparation;

            // prevent clipping through angled walls.
            velocity.y -= (Util.Project2D( velocity.normalized, hit.normal ) * velocity.magnitude).y;
          }
          if( hit.normal.y > collisionCornerY )
          {
            collideBottom = true;
            // velocity.y = Mathf.Max( velocity.y, 0 );
            vpos.y = hit.point.y + (bsiz.y * 0.5f) + contactSeparation;

            // prevent clipping through angled walls.
            velocity.y -= (Util.Project2D( velocity.normalized, hit.normal ) * velocity.magnitude).y;
          }
        }
      }

#if UNITY_EDITOR || DEVELOPMENT_BUILD
      if( Popcron.Gizmos.Enabled )
      {
        Popcron.Gizmos.Square( vpos, boxy, Color.blue );
      }
#endif

      // horizontal
      Vector2 velx = new Vector2( velocity.x, 0 );
      Vector2 boxx = new Vector2( bsiz.x, bsiz.y - contactSeparation );
      hitCount = Physics2D.BoxCast( vpos, boxx, 0, velx, filter2D, Global.RaycastHits, Mathf.Abs( velocity.x * DT ) );
      for( int i = 0; i < hitCount; i++ )
      {
        hit = Global.RaycastHits[i];
        if( hit.transform.IsChildOf( transform ) )
          continue;
        ITrigger tri = hit.transform.GetComponent<ITrigger>();
        if( tri != null )
        {
          tri.Trigger( hit, transform );
        }
        IDamage dam = hit.transform.GetComponent<IDamage>();
        if( dam != null )
        {
          // maybe only damage other if charging weapon or has active powerup?
          if( ContactDamageDefinition != null )
          {
            Damage dmg = new Damage( ContactDamageDefinition ) { instigator = this, damageSource = transform, hit = hit };
            dam.TakeDamage( dmg );
          }
        }
        Pickup pickup = hit.transform.GetComponent<Pickup>();
        if( pickup != null && pickup.SelectOnContact )
        {
          if( pickup.unique != UniquePickupType.None )
          {
            if( pickup.unique == UniquePickupType.Health )
              AddHealth( pickup.uniqueInt0 );
            /*else if( pickup.unique == UniquePickupType.HealthMax )
              AddMaxHealth( pickup.uniqueInt0 );
            else if( pickup.unique == UniquePickupType.SpeedFactorNormalized )
              speedFactorNormalized += pickup.uniqueFloat0;
            else if( pickup.unique == UniquePickupType.DashDuration )
              dashDuration += pickup.uniqueFloat0;*/
          }
          else
          {
            /*if( pickup.abilityDefinition != null )
            {
              if( AddAbility( pickup.abilityDefinition ) )
                pickup.Select( this );
            }*/
          }
          // this should destroy the pickup
          pickup.Select( this );
        }

        if( ((1 << hit.transform.gameObject.layer) & Global.TriggerLayers) == 0 )
        {
          if( hit.normal.x < -collisionCornerX )
          {
            collideRight = true;
            hitSpeed = Mathf.Abs( velocity.x );
            // velocity.x = Mathf.Min( velocity.x, 0 );
            vpos.x = hit.point.x - (bsiz.x * 0.5f) - contactSeparation;

            // prevent clipping through angled walls.
            velocity.x -= (Util.Project2D( velocity.normalized, hit.normal ) * velocity.magnitude).x;
          }
          if( hit.normal.x > collisionCornerX )
          {
            collideLeft = true;
            hitSpeed = Mathf.Abs( velocity.x );
            // velocity.x = Mathf.Max( velocity.x, 0 );
            vpos.x = hit.point.x + (bsiz.x * 0.5f) + contactSeparation;

            // prevent clipping through angled walls.
            velocity.x -= (Util.Project2D( velocity.normalized, hit.normal ) * velocity.magnitude).x;
          }
          /*if( hit.normal.y < -collisionCornerY )
          {
            collideTop = true;
            velocity.y = Mathf.Min( velocity.y, 0 );
          }
          if( hit.normal.y > collisionCornerY )
          {
            collideBottom = true;
            velocity.y = Mathf.Max( velocity.y, 0 );
          }*/
        }
      }
#if UNITY_EDITOR || DEVELOPMENT_BUILD
      if( Popcron.Gizmos.Enabled )
      {
        Popcron.Gizmos.Square( vpos, bsiz, Color.cyan );
      }
#endif
    }
    transform.position = vpos - boffset + velocity * DT;
    /*else
    {
      transform.position = (Vector2)transform.position + velocity * DT;
    }*/
    /*transform.position = Util.Snap( pos, 16 );*/

    /*// preserve inertia when jumping from moving platforms
    if( previousCarry != null && carryEntity == null )
      inertia = ((IVelocity) previousCarry).GetVelocityAtPoint( pos );*/
    /*UpdateHit( DT );*/

    /*if( OverrideAnimation )
      anim = OverrideAnimationName;
    else*/
    if( state == State.FACEDOWN )
      AnimRider = "faceplant";
    else if( state == State.HOP )
    {
      AnimRider = "hop";
      AnimBoard = "hop";
    }
    else if( state == State.JUMPING )
      AnimRider = "jump";
    else if( state == State.FALLING )
      AnimRider = "fall";
    else
    {
      if( state == State.TURN_AWAY )
      {
        if( turnComplete )
        {
          AnimRider = "turn_away";
          AnimBoard = "turn_away";
        }
        else
        {
          AnimRider = "turn_slight";
          AnimBoard = "roll";
        }
      }

      if( state == State.TURN_TOWARD )
      {
        if( turnComplete )
        {
          AnimRider = "turn_toward";
          AnimBoard = "turn_toward";
        }
        else
        {
          AnimRider = "turn_slight";
          AnimBoard = "roll";
        }
      }

      if( state == State.BRAKE )
      {
        AnimRider = "brake";
        AnimBoard = "brake";
      }

      if( state == State.ACCEL )
      {
        AnimRider = "accel";
        AnimBoard = "accel";
      }

      if( state == State.FAST )
      {
        AnimRider = "fast";
        AnimBoard = "roll";
      }

      if( state == State.COAST )
      {
        AnimRider = "coast";
        AnimBoard = "roll";
      }
    }
    /*else if( !jumping && flying )
    {
      // propellerfly anim has the biped's front arm holding up the propeller
      if( ability is AirbotAbility )
        anim = "propellerfly";
      else
        anim = "fly";
    }*/

    // todo Animator.StringToHash
    if( !AnimRider.Equals( pAnimRider ) )
      animator.Play( AnimRider );
    pAnimRider = AnimRider;

    // todo Animator.StringToHash
    if( !AnimBoard.Equals( pAnimBoard ) )
      animatorBoard.Play( AnimBoard );
    pAnimBoard = AnimBoard;

    transform.localScale = (new Vector3( facingRight ? 1 : -1, 1, 1 ));
    ResetInput();

    foreach( var dl in adlines )
      Popcron.Gizmos.Line( dl.a, dl.b, Color.red );
    adlines.Clear();
  }


  [SerializeField] protected BoxCollider2D boxBoard;

  public struct dline
  {
    public Vector2 a;
    public Vector2 b;
    public Color c;
  }

  List<dline> adlines = new List<dline>();
  public float pointSize = .01f;
  public float pointSizeRed = 0.02f;
  public int maxDebugPoints = 10;


  Vector2 riderPos;
  Vector2 boardPos;

  void StartJump()
  {
    state = State.JUMPING;
    riderPos = Vector2.zero;
    jumpTimer.Start( jumpDuration, ( timer ) =>
    {
      if( timer.ProgressNormalized < 0.5f )
        riderPos.y = timer.ProgressNormalized * 2f * jumpHeight;
      else
        riderPos.y = (1 - (timer.ProgressNormalized - 0.5f) * 2f) * jumpHeight;
      Rider.localPosition = riderPos;
    }, () =>
    {
      state = State.COAST;
      jumpTimer.Stop( false );
      Rider.localPosition = Vector3.zero;
    } );
    /*velocity.y = jumpSpeed;*/
    if( Global.instance.CurrentPlayer == this )
      audio.PlayOneShot( soundJump );
  }

  void StartHop()
  {
    state = State.HOP;
    riderPos = Vector2.zero;
    boardPos = Vector2.zero;
    jumpTimer.Start( jumpDuration, ( timer ) =>
    {
      if( timer.ProgressNormalized < 0.5f )
      {
        riderPos.y = timer.ProgressNormalized * 2f * jumpHeight;
        boardPos.y = timer.ProgressNormalized * 2f * jumpHeight;
      }
      else
      {
        riderPos.y = (1 - (timer.ProgressNormalized - 0.5f) * 2f) * jumpHeight;
        boardPos.y = (1 - (timer.ProgressNormalized - 0.5f) * 2f) * jumpHeight;
      }
      Rider.localPosition = riderPos;
      Board.localPosition = boardPos;
    }, () =>
    {
      state = State.COAST;
      jumpTimer.Stop( false );
      Rider.localPosition = Vector3.zero;
      Board.localPosition = Vector3.zero;
    } );
    /*velocity.y = jumpSpeed;*/
    if( Global.instance.CurrentPlayer == this )
      audio.PlayOneShot( soundJump );
  }

  void DamagePulseFlip()
  {
    damagePulseTimer.Start( damagePulseInterval, null, delegate
    {
      damagePulseOn = !damagePulseOn;
      if( damagePulseOn )
        foreach( var sr in spriteRenderers )
          sr.enabled = true;
      else
        foreach( var sr in spriteRenderers )
          sr.enabled = false;
      DamagePulseFlip();
    } );
  }

  public void RespawnWithGracePeriod( Vector2 pos )
  {
    if( Global.instance.CurrentPlayer == this )
      Global.instance.RespawnAt( pos );
    else
      transform.position = pos;

    damageGraceActive = true;
    damageTimer.Start( damageDuration, null, delegate()
    {
      takingDamage = false;
      damagePulseOn = true;
      DamagePulseFlip();
      damageTimer.Start( damageBlinkDuration, null, delegate()
      {
        foreach( var sr in spriteRenderers )
          sr.enabled = true;
        damageGraceActive = false;
        damagePulseTimer.Stop( false );
      } );
    } );
  }


  public Sprite panicSprite;
  [FormerlySerializedAs( "fallwhooseSprite" )]
  public Sprite fallwhoosh;
  public Sprite hitSprite;
  Timer fallIntoPitTimer = new Timer();
  public float pitapproach = 0.2f;
  public float pitboardhoverdur = 0.2f;
  [FormerlySerializedAs( "pitdelay0" )]
  public float pitboardfalldur = 0.2f;
  public float pitfalldur = 0.75f;
  public float pitdelaybeforerespawn = 3;
  public float boardfallspeed = 1;

  public void FallIntoPit( Pit pit, Vector2 location )
  {
    Vector2 lerpt = location; //(Vector2)pit.transform.position + Util.FindClosestPointOnPath( transform.position - pit.transform.position, pit.localPath );
    Vector2 startpos = transform.position;
    // lerp to fall position
    fallIntoPitTimer.Start( pitapproach, timer => { transform.position = Vector2.Lerp( startpos, lerpt, timer.ProgressNormalized ); }, () =>
    {
      // lerp camera to fall point
      Global.instance.CameraController.LerpAuto( lerpt, 0.5f, () => { Global.instance.CameraController.LerpAuto( lerpt, -0.5f + pitboardhoverdur + 3 * 0.2f + pitboardfalldur + pitfalldur + pitdelaybeforerespawn ); } );

      mask.transform.parent = null;
      // this must be kept current with the asset object mask size
      mask.transform.position = lerpt + new Vector2( -2, -6.375f ); //-6.5f );
      mask.gameObject.SetActive( true );
      foreach( var sr in spriteRenderers )
        sr.maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
      state = State.FALLING;

      Board.parent = null;

      // player hovers for a moment.
      fallIntoPitTimer.Start( pitboardhoverdur, null, () =>
      {
        // start panic blinking animation
        Global.instance.SpriteOneShotBlinking( panicSprite, Rider.position + Vector3.up * 3f, 3, 0.15f );
        // delay here should equal blinking animation duration above
        new Timer( 3 * 0.2f, null, () =>
        {
          // board falls first
          SpriteRenderer bsr = Board.GetComponent<SpriteRenderer>();
          Color boardColor = bsr.color;
          fallIntoPitTimer.Start( pitboardfalldur, timer =>
          {
            boardColor.a = 1f - timer.ProgressNormalized;
            bsr.color = boardColor;
            Board.position += Vector3.down * timer.ProgressSeconds * boardfallspeed;
          }, () =>
          {
            boardColor.a = 0;
            bsr.color = boardColor;

            // falls into pit
            SpriteRenderer psr = Rider.GetComponent<SpriteRenderer>();
            Color pawnColor = psr.color;
            fallIntoPitTimer.Start( pitfalldur,
              timer1 =>
              {
                pawnColor.a = 1f - timer1.ProgressNormalized;
                psr.color = pawnColor;
                transform.position = Vector2.Lerp( lerpt, lerpt + Vector2.down * 4, timer1.ProgressNormalized );
              }, () =>
              {
                pawnColor.a = 0;
                psr.color = pawnColor;

                Global.instance.SpriteOneShot( fallwhoosh, lerpt, 1f );
                fallIntoPitTimer.Start( pitdelaybeforerespawn, null, () =>
                {
                  mask.transform.parent = transform;
                  mask.gameObject.SetActive( false );
                  RespawnWithGracePeriod( lastKnownSafePosition );
                  foreach( var sr in spriteRenderers )
                  {
                    sr.maskInteraction = SpriteMaskInteraction.None;
                    Color src = sr.color;
                    src.a = 1;
                    sr.color = src;
                  }
                  pit.triggered = false;
                } );
              } );
          } );
        } );
      } );
    } );
  }


  public void Faceplant( System.Action callback )
  {
    takingDamage = true;
    jumpTimer.Stop( false );
    state = State.FALLING;
    Board.parent = null;
    hitPoint = transform.position;

    // play smack anim
    if( Global.instance.CurrentPlayer == this && soundHit )
      audio.PlayOneShot( soundHit );
    /*if( controller == Global.instance.PlayerController )
    {
      CameraShake shaker = Global.instance.CameraController.GetComponent<CameraShake>();
      shaker.amplitude = 0.3f;
      shaker.duration = 0.5f;
      shaker.rate = 100;
      shaker.intensityCurve = damageShakeCurve;
      shaker.enabled = true;
    }*/

    float localStartY = Rider.localPosition.y;
    float localStartX = 0;
    fallTimer.Start( fallDuration, ( timer ) =>
    {
      // fall to ground
      if( timer.ProgressNormalized * 0.8f < 0.25f )
        riderPos.y = Mathf.Lerp( localStartY, localStartY + fallHeight, timer.ProgressNormalized * (1f / 0.25f) );
      else
        riderPos.y = Mathf.Lerp( localStartY + fallHeight, -0.5f, (timer.ProgressNormalized - 0.25f) * (1f / 0.75f) );
      Rider.localPosition = riderPos;
      transform.position = hitPoint + Vector2.right * (localStartX + timer.ProgressSeconds * Mathf.Max( 1, Mathf.Min( 5, hitSpeed ) ));
    }, () =>
    {
      state = State.FACEDOWN;
      if( Global.instance.CurrentPlayer == this )
        audio.PlayOneShot( soundDamage );
      
      if( controller == Global.instance.PlayerController )
      {
        CameraShake shaker = Global.instance.CameraController.GetComponent<CameraShake>();
        shaker.amplitude = 0.3f;
        shaker.duration = 0.5f;
        shaker.rate = 100;
        shaker.intensityCurve = damageShakeCurve;
        shaker.enabled = true;
      }
      new Timer( 2, null, () => { callback?.Invoke(); } );
      
    } );
  }
#if false
  public override void TakeDamage( Damage damage )
  {
    if( dead || damageGraceActive )
      return;
    AddHealth( -damage.def.amount );
    if( Health <= 0 )
    {
      flashTimer.Stop( false );
      Die( damage );
      return;
    }
    //partHead.transform.localScale = Vector3.one * (1 + (Health / MaxHealth) * 10);
    // audio.PlayOneShot( soundDamage );
    if( controller == Global.instance.PlayerController )
    {
      CameraShake shaker = Global.instance.CameraController.GetComponent<CameraShake>();
      shaker.amplitude = 0.3f;
      shaker.duration = 0.5f;
      shaker.rate = 100;
      shaker.intensityCurve = damageShakeCurve;
      shaker.enabled = true;
    }
    /*Play( "damage" );
    float sign = 0;
    if( damage.damageSource != null )
      sign = Mathf.Sign( damage.damageSource.position.x - transform.position.x );
    facingRight = sign > 0;
    velocity.y = 0;
    OverrideVelocity( new Vector2( -sign * damagePushAmount, damageLift ), damageDuration );
    */
    takingDamage = true;
    damageGraceActive = true;
    /*if( ability != null )
      ability.Deactivate();
    if( weapon != null )
      weapon.Deactivate();*/
    damageTimer.Start( damageDuration, null, delegate()
    {
      takingDamage = false;
      DamagePulseFlip();
      damageTimer.Start( damageBlinkDuration, null, delegate()
      {
        foreach( var sr in spriteRenderers )
          sr.enabled = true;
        damageGraceActive = false;
        damagePulseTimer.Stop( false );
      } );
    } );
  }
#endif


#if false
  List<Component> pups = new List<Component>();

  new void UpdateHit( float dT )
  {
    /*ClimbableBackground = null;*/

    pups.Clear();
    hitCount = Physics2D.BoxCastNonAlloc( transform.position, box.size + Vector2.up * DownOffset * 2 + Vector2.right * 0.1f, 0,
      velocity, Global.RaycastHits, Mathf.Max( raylength, velocity.magnitude * dT ),
      HitLayers );
    for( int i = 0; i < hitCount; i++ )
    {
      hit = Global.RaycastHits[i];
      if( hit.transform.IsChildOf( transform ) )
        continue;
      ITrigger tri = hit.transform.GetComponent<ITrigger>();
      if( tri != null )
      {
        tri.Trigger( hit, transform );
      }
      IDamage dam = hit.transform.GetComponent<IDamage>();
      if( dam != null )
      {
        // maybe only damage other if charging weapon or has active powerup?
        if( ContactDamageDefinition != null )
        {
          Damage dmg = new Damage( ContactDamageDefinition ) { instigator = this, damageSource = transform, hit = hit };
          dam.TakeDamage( dmg );
        }
      }
      Pickup pickup = hit.transform.GetComponent<Pickup>();
      if( pickup != null && pickup.SelectOnContact )
      {
        if( pickup.unique != UniquePickupType.None )
        {
          if( pickup.unique == UniquePickupType.Health )
            AddHealth( pickup.uniqueInt0 );
          /*else if( pickup.unique == UniquePickupType.HealthMax )
            AddMaxHealth( pickup.uniqueInt0 );
          else if( pickup.unique == UniquePickupType.SpeedFactorNormalized )
            speedFactorNormalized += pickup.uniqueFloat0;
          else if( pickup.unique == UniquePickupType.DashDuration )
            dashDuration += pickup.uniqueFloat0;*/
        }
        else
        {
          /*if( pickup.weaponDefinition != null )
          {
            if( AddWeapon( pickup.weaponDefinition ) )
              pickup.Select( this );
          }
          else if( pickup.abilityDefinition != null )
          {
            if( AddAbility( pickup.abilityDefinition ) )
              pickup.Select( this );
          }*/
        }
        // this should destroy the pickup
        pickup.Select( this );
      }
      /*WarpDoor warpd = hit.transform.GetComponent<WarpDoor>();
      if( warpd != null )
        warpDoor = warpd;*/

      /*ClimbableBackground = hit.transform.GetComponent<ClimbableBackground>();*/
    }
    /*if( IsPlayer )
    {
      pups.Clear();
      hitCount = Physics2D.CircleCastNonAlloc( transform.position, selectRange, Vector3.zero, Global.RaycastHits, 0,
        Global.WorldSelectableLayers );
      for( int i = 0; i < hitCount; i++ )
      {
        hit = Global.RaycastHits[i];
        if( hit.transform.gameObject == null || hit.transform == transform || hit.transform.root == transform.root )
          continue;
        IWorldSelectable pup = hit.transform.GetComponent<IWorldSelectable>();
        if( pup != null )
        {
          pups.Add( (Component) pup );
          /*if( !highlightedPickups.Contains( pup ) )
          {
            pup.Highlight();
            highlightedPickups.Add( pup );
          }#1#
        }
      }

      IWorldSelectable closest = (IWorldSelectable) Util.FindSmallestAngle( transform.position, shoot, pups.ToArray() );
      if( closest == null )
      {
        if( closestISelect != null )
        {
          closestISelect.Unhighlight();
          closestISelect = null;
          /*InteractIndicator.SetActive( false );#1#
        }
        if( WorldSelection != null )
        {
          WorldSelection.Unselect();
          WorldSelection = null;
        }
      }
      else if( closest != closestISelect )
      {
        if( closestISelect != null )
        {
          closestISelect.Unhighlight();
          /*InteractIndicator.SetActive( false );#1#
        }
        closestISelect = closest;
        closestISelect.Highlight();
        // Not everything is indicated the same way
        /*InteractIndicator.SetActive( true );
        InteractIndicator.transform.position = closestISelect.GetPosition();#1#
      }
      else
      {
        InteractIndicator.transform.position = closestISelect.GetSelectablePosition();
      }
      /*highlightedPickupsRemove.Clear();
      foreach( var pup in highlightedPickups )
        if( !pups.Contains( pup ) )
        {
          pup.Unhighlight();
          highlightedPickupsRemove.Add( pup );
        }
      foreach( var pup in highlightedPickupsRemove )
        highlightedPickups.Remove( pup );
      
    }
    }*/
  }

#endif
}