﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


[CreateAssetMenu]
public class AIControllerDefinition : ControllerDefinition
{
  public override Controller GetNewController() { return new AIController( this ); }

  /*public float sightRange = 5;
  public float SightPulseInterval = 0.5f;
  public float sightStartRadius = 0.5f;*/

  public float RandomMoveIntervalMin = 1;
  public float RandomMoveIntervalMax = 5;
  public float RandomMoveTargetDistance = 10;
  public float RandomMoveTargetRadius = 10;

  public float small = 0.1f;

  public float AvoidanceRaycastDistance = 3;
  public float jumpMinimum = .2f;
  public float jumpCheckDistanceRight = 1;
  public float jumpCheckOriginUp = .3f;
  public float jumpHoldDuration = 0.2f;


  /*public float idealDistance = 0.5f;*/
  /*public float AimSpeed = 20;*/
  /*public bool EnableLead;*/
  // If the projectile does not come closer than this, then reposition.
  /*public float AimErrorMaximum = 2;
  public float AimAdjustAmount = 1;
  public float AimAdjustSteps = 10;*/
}


public class AIController : Controller
{
  public string AgentTypeName = "Small";
  public PathAgent pathAgent;

  /*PlayerBiped biped;*/
  public Transform sightOrigin;
  /*public Entity VisibleTarget;*/

  Timer RandomMoveTimer = new Timer();
  /*Timer SightPulseTimer = new Timer();*/
  int hitCount;
  /*bool toggleShoot;*/
  Timer jumpHoldTimer = new Timer();
  bool holdLeft;
  bool holdRight;

  Vector2 targetPosition;
  /*Vector2 aimPos;
  Vector2 aimVector;*/
  /*Vector3[] trajectory = new Vector3[100];*/
  /*List<Entity> enemies = new List<Entity>();
  List<Entity> friendlies = new List<Entity>();*/
  List<Pickup> pickups = new List<Pickup>();
  /*List<TeamCaptureGoal> goals = new List<TeamCaptureGoal>();*/
  int InterestLayers;

  public AIController( AIControllerDefinition newDef ) : base( newDef ) { def = newDef; }
  public AIControllerDefinition def;
  ~AIController() { Destroy(); }

  void Destroy()
  {
    //SightPulseTimer.Stop( false );
    RandomMoveTimer.Stop( false );
  }

  public override void PreSceneTransition()
  {
    // SightPulseTimer.Stop( false );
    RandomMoveTimer.Stop( false );
  }
  /*public override void PostSceneTransition() { }*/


  public override void AssignPawn( Pawn pwn )
  {
    base.AssignPawn( pwn );
    /*if( pwn is PlayerBiped playerBiped )
      biped = playerBiped;*/

    sightOrigin = pawn.transform;

    pathAgent = new PathAgent();
    pathAgent.Client = pawn;
    pathAgent.transform = pawn.transform;
    pathAgent.AgentTypeID = Global.instance.AgentType[AgentTypeName];
    pathAgent.AllowOffMesh = false;

    InterestLayers = LayerMask.GetMask( "entity", "trigger", "destructible", "projectileCollide" );
  }

  public override void Update()
  {
    if( pawn == null )
      return;

    if( playback )
    {
      if( pawn == null || playbackIndex >= playbackFrameBuffer.Count )
        PlaybackEnd();
      else
      {
        pawn.transform.position = playbackFrameBuffer[playbackIndex].position;
        pawn.ApplyInput( playbackFrameBuffer[playbackIndex++].input );
      }
    }
    else
    {
      // cached for optimization
      Vector2 pawnPos = pawn.transform.position;
#if false
      if( !SightPulseTimer.IsActive )
      {
        SightPulseTimer.Start( def.SightPulseInterval );
        // reaffirm target
        /*VisibleTarget = null;*/
        /*enemies.Clear();
        friendlies.Clear();*/
        pickups.Clear();
        /*goals.Clear();*/
        int count = Physics2D.OverlapCircleNonAlloc( pawnPos, def.sightRange, Global.ColliderResults, InterestLayers );
        for( int i = 0; i < count; i++ )
        {
          Collider2D cld = Global.ColliderResults[i];
          if( cld.transform.IsChildOf( pawn.transform ) )
            continue;
          if( cld.TryGetComponent( out Entity entity ) )
          {
            // when would this happen?
            if( entity == entity.parentEntity )
            {
              Debug.LogWarning( "entity.parentEntity is itself", entity );
              continue;
            }
            // Get the topmost parent entity. 
            Entity parentEntity = entity;
            while( parentEntity.parentEntity )
              parentEntity = parentEntity.parentEntity;

            /*if( pawn.IsEnemyTeam( parentEntity.TeamFlags ) )
              enemies.Add( entity );
            else if( pawn.IsSameTeam( parentEntity.TeamFlags ) )
              friendlies.Add( entity );
            else */
            if( parentEntity is Pickup pickup )
              pickups.Add( pickup );
          }
          /*if( cld.TryGetComponent( out TeamCaptureGoal goal ) && (pawn.TeamFlags & goal.TeamFlags) == 0 )
            goals.Add( goal );*/
        }
        /*if( Global.instance.sceneScript is BattleMode battleMode )
          goals = battleMode.Goals;*/
        
        /*bool ValidPath = false;
        if( enemies.Count > 0 )
        {
          enemies.Sort( ( a, b ) =>
          {
            float distA = Vector3.SqrMagnitude( a.transform.position - pawnPos );
            float distB = Vector3.SqrMagnitude( b.transform.position - pawnPos );
            if( distA < distB )
              return -1;
            if( distA > distB )
              return 1;
            return 0;
          } );

          // engage the enemies that can be seen from here.
          for( int i = 0; i < enemies.Count; i++ )
          {
            Vector2 delta = enemies[i].transform.position - pawnPos;
            hitCount = Physics2D.LinecastNonAlloc( (Vector2) sightOrigin.position + delta.normalized * def.sightStartRadius, enemies[i].transform.position, Global.RaycastHits, Global.SightObstructionLayers | LayerMask.GetMask( "entity" ) );
            bool sightObstructed = true;
            if( hitCount == 0 )
            {
              sightObstructed = false;
            }
            else
            {
              for( int j = 0; j < hitCount; j++ )
              {
                if( Global.RaycastHits[j].collider.transform.IsChildOf( pawn.transform ) )
                  continue;
                if( Global.RaycastHits[j].collider.transform.IsChildOf( enemies[i].transform ) )
                  sightObstructed = false;
                break;
              }
            }
            if( !sightObstructed )
            {
              VisibleTarget = enemies[i];
              targetPosition = enemies[i].transform.position;
              break;
            }
          }

          if( !VisibleTarget )
          {
            // enemies are around but I cannot see them.
            Vector2 dest = enemies[0].transform.position;
            ValidPath = pathAgent.SetPath( dest + new Vector2( Mathf.Sign( pawnPos.x - dest.x ) * 0.5f * Random.value, 0 ) );
          }
        }*/

        /*if( !ValidPath && pickups.Count > 0 && pawn.Health < pawn.MaxHealth && (!Global.instance.CurrentPlayer || pawn.Health < Global.instance.CurrentPlayer.Health) )
        {
          foreach( var pickup in pickups )
          {
            if( pickup && pickup.unique == UniquePickupType.Health )
            {
              ValidPath = pathAgent.SetPath( pickup.transform.position );
              if( ValidPath )
                break;
            }
          }
        }*/

        /*if( !ValidPath && goals != null && goals.Count > 0 )
        {
          List<TeamCaptureGoal> enemyGoals = new List<TeamCaptureGoal>();
          Dictionary<TeamCaptureGoal, PathAgent.Path> paths = new Dictionary<TeamCaptureGoal, PathAgent.Path>();
          foreach( var goal in goals )
            if( pawn.TeamFlags != goal.TeamFlags )
            {
              PathAgent.Path path = pathAgent.GetPath( pawnPos, goal.SpawnPointLocal.position );
              if( path.Complete )
              {
                enemyGoals.Add( goal );
                paths.Add( goal, path );
              }
            }

          if( enemyGoals.Count > 0 )
          {
            // sort by path length
            enemyGoals.Sort( ( a, b ) =>
            {
              float distA = paths[a].PathLength;
              float distB = paths[b].PathLength;
              if( distA < distB || (paths[a].Complete && !paths[b].Complete) )
                return -1;
              if( distA > distB || (paths[b].Complete && !paths[a].Complete) )
                return 1;
              return 0;
            } );

            ValidPath = true;
            pathAgent.SetPath( paths[enemyGoals[0]] );
            () =>
            {
              if( Vector2.SqrMagnitude( pawnPos - enemyGoals[0].transform.position ) < biped.selectRange * biped.selectRange )
                enemyGoals[0].Select( pawn );
            }
          }
        }
*/
        /*if( !ValidPath && friendlies.Count > 0 )
        {
          // Follow a friend.
          Entity follow = friendlies.Find( x => x == Global.instance.CurrentPlayer );
          if( !follow )
            follow = friendlies[0];
          Vector2 followDest = follow.transform.position;
          ValidPath = pathAgent.SetPath( followDest + new Vector2( Mathf.Sign( pawnPos.x - followDest.x ) * def.idealDistance, 0 ) );
        }*/
      } // SightPulseTimer

      /*Vector2 wpos = targetPosition;
      if( VisibleTarget )
      {
        toggleShoot = !toggleShoot;
        if( toggleShoot )
          input.Fire = true;

        if( pawn is PlayerBiped biped && biped.weapon != null )
        {
          if( def.EnableLead )
          {
            if( biped.weapon.def.weaponCategory == WeaponDefinition.WeaponCategory.Projectile )
              wpos += VisibleTarget.velocity * (((Vector2) VisibleTarget.transform.position - pawn.GetShotOriginPosition()).magnitude / biped.weapon.def.data.Speed);
          }
          if( biped.weapon.def.data.UseGravity )
          {
            float finalSqrMag = Single.MaxValue;
            int bestStep = 0;
            for( int step = 0; step < def.AimAdjustSteps; step++ )
            {
              // use trajectory for prjectiles that use gravity.
              int count = biped.weapon.GetTrajectory( ref trajectory, biped.GetShotOriginPosition(), (wpos + Vector2.up * step * def.AimAdjustAmount) - pawn.GetShotOriginPosition() );
              float sqrMag = Single.MaxValue;
              for( int i = 0; i < count; i++ )
              {
                float sqrmag = Vector2.SqrMagnitude( wpos - (Vector2) trajectory[i] );
                if( sqrmag < sqrMag )
                {
                  sqrMag = sqrmag;
                }
              }
              if( sqrMag < finalSqrMag )
              {
                finalSqrMag = sqrMag;
                bestStep = step;
              }
            }
            wpos += Vector2.up * bestStep * def.AimAdjustAmount;

            if( finalSqrMag > def.AimErrorMaximum * def.AimErrorMaximum )
            {
              // shot will not get close enough, get closer.
              Vector2 dest = VisibleTarget.transform.position;
              pathAgent.SetPath( dest + new Vector2( Mathf.Sign( pawnPos.x - dest.x ) * 0.5f * Random.value, 0 ) );
            }
          }
        }
      }
      aimPos = Vector2.MoveTowards( aimPos, wpos, Time.deltaTime * def.AimSpeed );
      aimVector = aimPos - pawn.GetShotOriginPosition();
      input.Aim = aimVector;
    */
#endif

      if( !RandomMoveTimer.IsActive )
      {
        RandomMoveTimer.Start( Random.Range( def.RandomMoveIntervalMin, def.RandomMoveIntervalMax ) );
        // random cruise
        pathAgent.SetPath( (Vector2)pawn.transform.position + Vector2.right * def.RandomMoveTargetDistance + Random.insideUnitCircle * def.RandomMoveTargetRadius, null );
      }

      pathAgent.UpdatePath();
      pathAgent.DebugRenderPath();

      Vector2 moveDelta = pathAgent.DeltaToCurrentWaypoint;
      if( pathAgent.HasPath && moveDelta.magnitude > def.small )
      {
        if( moveDelta.x > def.small )
          input.MoveRight = true;
        // move left is used to brake
        /*if( moveDelta.x < -def.small )
          input.MoveLeft = true;*/
        if( moveDelta.y < -def.small )
          input.MoveDown = true;
        if( moveDelta.y > def.small )
          input.MoveUp = true;
      }
      else
      {
        // if no path then raycast to avoid hitting things
        int count = Physics2D.RaycastNonAlloc( pawnPos, Vector2.right, Global.RaycastHits, def.AvoidanceRaycastDistance, Global.CharacterCollideLayers );
        if( count == 0 )
          input.MoveRight = true;
        else
        {
          count = Physics2D.RaycastNonAlloc( pawnPos, Vector2.right + Vector2.up, Global.RaycastHits, def.AvoidanceRaycastDistance, Global.CharacterCollideLayers );
          if( count == 0 )
          {
            input.MoveRight = true;
            input.MoveUp = true;
          }
          else
          {
            count = Physics2D.RaycastNonAlloc( pawnPos, Vector2.right + Vector2.down, Global.RaycastHits, def.AvoidanceRaycastDistance, Global.CharacterCollideLayers );
            if( count == 0 )
            {
              input.MoveRight = true;
              input.MoveDown = true;
            }
            else
            {
              // trapped!

              input.Jump = jumpHoldTimer.IsActive;
              if( input.Jump )
              {
                input.MoveRight = holdRight;
                /*input.MoveLeft = holdLeft;*/
              }
              else
                // todo if( object is directly in front, and there is clearance on other side (valid navmesh position) )
              {
                jumpHoldTimer.Start( def.jumpHoldDuration );
                input.Jump = false;
                input.MoveRight = true;
                holdRight = true;

                /*holdRight = false;
                holdLeft = false;
                if( pathAgent.DeltaToNextWaypoint.x >= 0 &&
                   Physics2D.RaycastNonAlloc( (Vector2) pawnPos + Vector2.up * def.jumpCheckOriginUp, Vector2.right, Global.RaycastHits, def.jumpCheckDistanceRight, Global.CharacterCollideLayers ) > 0 )
                {
                  jumpHoldTimer.Start( def.jumpHoldDuration );
                  input.Jump = false;
                  input.MoveRight = true;
                  holdRight = true;
                }
                else if( pathAgent.DeltaToNextWaypoint.x < 0 &&
                        Physics2D.RaycastNonAlloc( (Vector2) pawnPos + Vector2.up * def.jumpCheckOriginUp, Vector2.left, Global.RaycastHits, def.jumpCheckDistanceRight, Global.CharacterCollideLayers ) > 0 )
                {
                  jumpHoldTimer.Start( def.jumpHoldDuration );
                  input.Jump = false;
                  input.MoveLeft = true;
                  holdLeft = true;
                }*/
              }
            }
          }
        }
      }

      if( pawn != null )
        pawn.ApplyInput( input );
    }

    input = default;
  }

#region Playback

  bool playback;
  int playbackIndex = 0;
  float playbackStartTime;
  List<RecordFrame> playbackFrameBuffer;

  public void PlaybackBegin( RecordHeader header, List<RecordFrame> evt )
  {
    playback = true;
    playbackIndex = 0;
    playbackStartTime = Time.unscaledTime;
    playbackFrameBuffer = evt;
    //header 
    pawn.transform.position = header.initialposition;
  }

  public void PlaybackEnd() { playback = false; }

#endregion

  /*static Vector3[] trajectoryPoints = new Vector3[100];

  public static bool GetAimVectorSolveForTarget( ref Vector2 outVector, Vector2 target, Weapon weapon, Vector2 shotOrigin, int AimAdjustSteps = 5, float AimAdjustAmount = 1, float AimErrorMaximum = 2 )
  {
    Vector2 wpos = target;
    float finalSqrMag = Single.MaxValue;
    int bestStep = 0;
    for( int step = 0; step < AimAdjustSteps; step++ )
    {
      // use trajectory for prjectiles that use gravity.
      int count = weapon.GetTrajectory( ref trajectoryPoints, shotOrigin, (wpos + Vector2.up * step * AimAdjustAmount) - shotOrigin );
      float sqrMag = Single.MaxValue;
      for( int i = 0; i < count; i++ )
      {
        float sqrmag = Vector2.SqrMagnitude( wpos - (Vector2) trajectoryPoints[i] );
        if( sqrmag < sqrMag )
        {
          sqrMag = sqrmag;
        }
      }
      if( sqrMag < finalSqrMag )
      {
        finalSqrMag = sqrMag;
        bestStep = step;
      }
    }
    wpos += Vector2.up * bestStep * AimAdjustAmount;
    if( finalSqrMag > AimErrorMaximum * AimErrorMaximum )
    {
      // shot will not get close enough.
      return false;
    }
    outVector = wpos - shotOrigin;
    return true;
  }*/
}