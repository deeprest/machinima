using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

/*#if UNITY_EDITOR
using UnityEditor;

[CustomEditor( typeof(Pit) )]
public class PitEdtitor : MonoBehaviourInspector
{
  void OnSceneGUI()
  {
    Handles.color = Color.yellow;
    Pit bot = target as Pit;
    if( bot.localPath != null && bot.localPath.Length > 0 )
    {
      Vector2[] points = bot.localPath;
      // open polygon, do not draw last segment
      for( int i = 0; i < points.Length - 1; i++ )
        Handles.DrawLine( (Vector2)bot.transform.position + bot.localPath[i], (Vector2)bot.transform.position + bot.localPath[i + 1] );
    }
  }
}
#endif*/

public class Pit : MonoBehaviour, ITrigger
{
  [FormerlySerializedAs( "path" )]
  /*public Vector2[] localPath;*/
  public bool triggered;

  public void Trigger( RaycastHit2D hit, Transform instigator )
  {
    if( triggered )
      return;
    triggered = true;
    if( instigator.TryGetComponent( out PawnSkateboarder rider ) )
    {
      // if pit tile collision shapes are within a single polygon collider, find the closest shape centroid
      PhysicsShapeGroup2D physicsShapeGroup2D = new PhysicsShapeGroup2D( 1000, 4 );
      int count = hit.collider.GetShapes( physicsShapeGroup2D );
      Vector2 center = Vector2.zero;
      List<Vector2> centers = new List<Vector2>( count * 4 );
      int vcount = 0;
      for( int i = 0; i < count; i++ )
      {
        vcount = physicsShapeGroup2D.GetShape( i ).vertexCount;
        for( int v = 0; v < vcount; ++v )
          center += physicsShapeGroup2D.GetShapeVertex( i, v );
        center.x /= vcount;
        center.y /= vcount;
        centers.Add( center );
        center = Vector2.zero;
      }
      Vector2 target_location = Util.FindClosest( instigator.position, centers.ToArray() );

      rider.FallIntoPit( this, target_location );
    }
  }
}