﻿using System.Collections;
using UnityEngine;
using Dungbeetle;
using System.Threading;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class BugReportUI : UIScreen
{
  [SerializeField] Text ProgressMessage;
  [SerializeField] Text ErrorMessage;

  [SerializeField] Button SendButton;
  [SerializeField] RawImage Screenshot;
  [SerializeField] InputField Email;
  [SerializeField] InputField Description;
  string email { get { return Email.text; } }
  string bugDescription { get { return Description.text; } }

  bool launched;
  ScreenshotSource screenshotSource;
  string sceneName;
  string buildName;
  AttachmentCollection attachments;
  bool fullAttachments;
  bool showing;
  TimeoutMessage errorMessage;
  LayoutMessage progressMessage;
  LayoutMessage closingMessage;
  static Thread mainThread;

  void Awake()
  {
    SendButton.onClick.AddListener( () =>
    {
      PlayerPrefs.SetString( "bugreportemail", Email.text );
      //EditorReporterConfig.DefaultReporter = email;

      attachments.AddLog( !fullAttachments );
      var e = attachments.Wait();
      while( e.MoveNext() )
        ; // Wait
      progressMessage = new LayoutMessage( "Uploading: 0%", true );
      BugReportSender.Send( screenshotSource.Bytes, bugDescription, email, sceneName, buildName, HandleProgressMessage, HandleBugProgress, HandleAttachmentProgress, progressMessage.CancellationToken, PostSendResult, attachments.ToStringArray() );
    } );
  }

  public override void Unselect()
  {
    base.Unselect();
    if( progressMessage != null )
      progressMessage.Cancel();
    if( screenshotSource != null )
      screenshotSource.Dispose();
    screenshotSource = null;
    showing = false;
  }

  public void Launch( string scene_name, string build_name, AttachmentCollection _attachments, ScreenshotSource screenshot )
  {
    launched = true;
    screenshotSource = screenshot;
    if( PlayerPrefs.HasKey( "bugreportemail" ) )
      Email.text = PlayerPrefs.GetString( "bugreportemail" );
    //Email.text = EditorReporterConfig.DefaultReporter ?? "";
    sceneName = scene_name;
    buildName = build_name;
    attachments = _attachments;

    mainThread = Thread.CurrentThread;
    Screenshot.texture = screenshotSource.FullTexture;

    showing = true;
    Description.text = "";
    
    progressMessage = null;
    closingMessage = null;
    errorMessage = null;
    ProgressMessage.text = "";
    ErrorMessage.text = "";
    Select();
  }

  void Update()
  {
    if( !showing )
      return;
    if( progressMessage != null && progressMessage.Ready )
      ProgressMessage.text = progressMessage;
    else if( closingMessage != null && closingMessage.Ready )
      ProgressMessage.text = closingMessage;
    else if( errorMessage != null && !errorMessage.EndReached )
      ErrorMessage.text = errorMessage;
  }

  private void CloseWithMessage( bool isServerResponse, string message, bool waitForLayout = true )
  {
    string result;
    if( isServerResponse )
    {
      if( message.Equals( "Accepted" ) )
        result = "Report received.\nThank you!";
      else
        result = string.Format( "Server: {0}", message );
    }
    else
    {
      result = string.Format( "Error: {0}", message );
    }
    closingMessage = new LayoutMessage( result, !waitForLayout );
  }


  private void HandleProgressMessage( string value, bool socketFailed, string socketFailMsg )
  {
    if( progressMessage != null && !SocketFailure( socketFailed, socketFailMsg ) )
      progressMessage.content = value;
  }

  private void HandleBugProgress( float value, bool socketFailed, string socketFailMsg )
  {
    if( progressMessage != null && !SocketFailure( socketFailed, socketFailMsg ) )
      progressMessage.content = string.Format( "Uploading report: {0}%", (int) (value * 100f) );
  }

  private void HandleAttachmentProgress( float value, bool socketFailed, string socketFailMsg )
  {
    if( progressMessage != null && !SocketFailure( socketFailed, socketFailMsg ) )
      progressMessage.content = string.Format( "Uploading attachments: {0}%", (int) (value * 100f) );
  }

  private void PostSendResult( string resultMessage, bool socketFailed, string socketFailMsg )
  {
    if( !SocketFailure( socketFailed, socketFailMsg ) )
    {
      CloseWithMessage( true, resultMessage, false );
      progressMessage = null;
    }
  }

  private bool SocketFailure( bool socketFailed, string socketFailMsg )
  {
    if( socketFailed )
    {
      errorMessage = "Connection failed";
      Debug.LogErrorFormat( "Reporter connection failed\n{0}", socketFailMsg );
      progressMessage = null;
    }
    return socketFailed;
  }

  private class LayoutMessage
  {
    public string content;
    private bool ready;

    private CancellationTokenSource source;
    private CancellationToken token;

    public CancellationToken CancellationToken => token;

    public void Cancel()
    {
      source.Cancel();
    }

    public LayoutMessage( string content, bool ready )
    {
      this.source = new CancellationTokenSource();
      this.token = source.Token;
      this.content = content;
      this.ready = ready;
    }

    public bool Ready { get { return ready /*|| (ready = UnityEngine.Event.current.type == EventType.Layout)*/; } }

    public static implicit operator string( LayoutMessage m )
    {
      return m.content;
    }
  }

  private class TimeoutMessage
  {
    private string content;
    private float endTime;
    private bool endTimeInitialized;

    private TimeoutMessage( string content )
    {
      this.content = content;
      if( mainThread.Equals( Thread.CurrentThread ) )
      {
        endTime = Time.realtimeSinceStartup + 3f;
        endTimeInitialized = true;
      }
      else
      {
        endTime = float.PositiveInfinity;
      }
    }

    public bool EndReached
    {
      get
      {
        if( endTimeInitialized )
        {
          return Time.realtimeSinceStartup > endTime;
        }
        else if( mainThread.Equals( Thread.CurrentThread ) )
        {
          endTime = Time.realtimeSinceStartup + 3f;
          endTimeInitialized = true;
        }
        return false;
      }
    }

    public static implicit operator string( TimeoutMessage m )
    {
      return m.content;
    }

    public static implicit operator TimeoutMessage( string m )
    {
      return new TimeoutMessage( m );
    }
  }
}