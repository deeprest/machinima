using System;
using UnityEngine;
using UnityEngine.UI;

public class MusicPlayer : MonoBehaviour
{
  public TMPro.TMP_Text ArtistTitle;
  public TMPro.TMP_Text Album;
  // "Go To Song's Webpage" 
  public Button AttributionLink;
  public Image AlbumArt;
  // Defaults
  public Sprite DefaultSprite;

  public Animator animator;

  void OnEnable()
  {
    if( animator )
      animator.SetBool( "show", false );
  }

  public void ShowMusicInfo( AudioLoop audio )
  {
    AttributionLink.onClick.RemoveAllListeners();

    if( !audio )
      return;

    if( string.IsNullOrEmpty( audio.TrackTitle ) )
      /*if( audio.TrackTitle.IsEmpty )*/
      ArtistTitle.text = "...";
    else
      ArtistTitle.text = audio.TrackTitle;
    /*ArtistTitle.text = audio.TrackTitle.GetLocalizedString();*/

    if( audio.Album )
    {
      if( !string.IsNullOrWhiteSpace( audio.Album.URL ) )
        AttributionLink.onClick.AddListener( () => { Application.OpenURL( audio.Album.URL ); } );
      Album.text = audio.Album.Title;
      /*Album.text = audio.Album.Title.GetLocalizedString();*/
      if( audio.Album.Art )
        AlbumArt.sprite = audio.Album.Art;
      else
        AlbumArt.sprite = DefaultSprite;
      ArtistTitle.text = audio.Album.Artist + " - " + audio.TrackTitle;
      /*ArtistTitle.text = audio.Album.Artist.GetLocalizedString() + " - " + audio.TrackTitle.GetLocalizedString();*/
    }
    else
    {
      AlbumArt.sprite = DefaultSprite;
      Album.text = "";
      ArtistTitle.text = audio.OptionArtist + " - " + audio.TrackTitle;
      /*ArtistTitle.text = audio.OptionArtist.GetLocalizedString() + " - " + audio.TrackTitle.GetLocalizedString();*/
    }

    if( animator )
    {
      animator.SetBool( "show", true );
      new Timer( 2, null, () => { animator.SetBool( "show", false ); } );
    }
  }
}