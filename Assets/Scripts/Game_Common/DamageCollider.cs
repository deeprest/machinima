using UnityEngine;

public class DamageCollider : MonoBehaviour
{
  public DamageDefinition contactDamageDefinition;
  public BoxCollider2D box;
  const float raylength = 0;
  RaycastHit2D hit;

  void Update()
  {
    if( contactDamageDefinition != null )
    {
      int hitCount = Physics2D.BoxCastNonAlloc( box.transform.position + (box.transform.rotation * box.offset), box.size, box.transform.eulerAngles.z, Vector2.zero, Global.RaycastHits, raylength, Global.CharacterDamageLayers );
      for( int i = 0; i < hitCount; i++ )
      {
        hit = Global.RaycastHits[i];

        IDamage dam = hit.transform.GetComponent<IDamage>();
        if( dam != null )
        {
          Damage dmg = new Damage( contactDamageDefinition )
          {
            instigator = null,
            damageSource = transform,
            hit = hit
          };
          DamageResult dmr = dam.CalculateDamageResult( dmg );
          if( !dmr.ignore )
            dmr.entity.TakeDamage( dmg );
        }
      }
    }
  }
}