﻿using UnityEngine;

public enum UniquePickupType
{
  None,
  SpeedFactorNormalized,
  DashDuration,
  Health,
  HealthMax
}

public class Pickup : Entity, IWorldSelectable
{
  public override bool IsUnderLimit() { return Limit.IsUnderLimit(); }
  public new static Limit<Pickup> Limit = new Limit<Pickup>();

  static Pickup() { Limit.EnforceUpper = true; }

  [SerializeField] AudioClip soundPickup;

  /*public WeaponDefinition weaponDefinition;
  public AbilityDefinition abilityDefinition;*/
  public bool SelectOnContact;
  public bool EnableHighlight;
  public UniquePickupType unique;
  public float uniqueFloat0;
  public int uniqueInt0;

  protected override void Awake()
  {
    if( Pickup.Limit.OnCreate( this ) )
    {
      base.Awake();
    }
  }
  
  public void Highlight()
  {
    if( EnableHighlight )
    {
      if( Global.instance.CurrentPlayer != null )
      {
        Global.instance.CurrentPlayer.InteractIndicator.SetActive( true );
        Global.instance.CurrentPlayer.InteractIndicator.transform.position = GetSelectablePosition();
      }
      if( animator != null )
        animator.Play( "highlight" );
    }
  }

  public void Unhighlight()
  {
    if( EnableHighlight )
    {
      if( Global.instance.CurrentPlayer != null )
        Global.instance.CurrentPlayer.InteractIndicator.SetActive( false );
      if( animator != null )
        animator.Play( "idle" );
    }
  }

  public void Select() { Select( null ); }

  public void Select( Entity instigator )
  {
    Global.instance.AudioOneShot( soundPickup, transform.position );
    Destroy( gameObject );
  }

  public void Unselect() { }
  public Vector2 GetSelectablePosition() { return transform.position; }

  protected override void Start()
  {
    base.Start();
    if( animator != null )
      animator.Play( "idle" );
    /*if( !SelectOnContact )
    {
      collideMask |= LayerMask.GetMask( "entity" );
      collideMask2 |= LayerMask.GetMask( "entity" );
    }*/
  }
}