﻿using UnityEngine;
using UnityEngine.U2D;

#if UNITY_EDITOR
using UnityEditor;

/*[CustomEditor( typeof(ParallaxLayer) )]
public class ParallaxLayerEditor : Editor
{
  public override void OnInspectorGUI()
  {
    // DIRTY HACK
    EditorUtility.SetDirty( target );
    //(target as ParallaxLayer).LateUpdate();
    DrawDefaultInspector();
  }
}*/
#endif

[ExecuteInEditMode]
public class ParallaxLayer : MonoBehaviour
{
  public bool ScaleGameObject;
  public Vector2 Scale;
  [ReadOnly]
  public Vector2 offset;
  private Transform cam;

  [Header( "Experiment" )]
  const float pixelsPerUnit = 16;
  /*public bool snap;*/

  Vector3 pos;
  void Start()
  {
    if( Application.isPlaying )
      cam = Global.instance.CameraController.transform;
    pos = transform.position;
  }

  void LateUpdate()
  {
#if UNITY_EDITOR
    if( ScaleGameObject )
      transform.localScale = Vector2.one - Scale;
    if( !Application.isPlaying )
    {
      // avoid changing the transform unnecessarily, which makes the scene dirty.
      if( cam == null && SceneView.GetAllSceneCameras() != null && SceneView.GetAllSceneCameras()[0] != null )
        cam = SceneView.GetAllSceneCameras()[0].transform;
      /*if( Camera.main && Camera.main.isActiveAndEnabled )
        cam = Camera.main.transform;*/
    }
#endif
    if( cam != null )
    {
      offset = Snap( offset );
      Vector2 parent_position = Vector2.zero;
      if( transform.parent )
        parent_position = Snap(transform.parent.position);
      pos = Snap( offset + parent_position + Vector2.Scale( Snap(cam.position) - parent_position, new Vector2( 1f - Scale.x, 1f - Scale.y ) ) );
    }
    transform.position = pos;
  }

  Vector2 Snap( Vector2 vector )
  {
    return new Vector2( Mathf.Round( vector.x * pixelsPerUnit ) / pixelsPerUnit, Mathf.Round( vector.y * pixelsPerUnit ) / pixelsPerUnit );
  }
}
