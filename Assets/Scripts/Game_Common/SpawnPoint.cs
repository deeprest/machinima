﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Serialization;

[SelectionBase]
public class SpawnPoint : MonoBehaviour //SerializedComponent
{
  //[Serialize] public string TeamName;
  //public Team MyTeam;

  public Transform SpawnPointLocal;
  public Animation Animation;
  public AudioSource AudioSource;
  public AudioClip SpawnSound;
  public ParticleSystem SpawnEffect;

  public float CannonSpeed = 10;
  //[Serialize] 
  public float SpawnRadius = 0f;
  //[Serialize] 
  public bool AutoSpawn = true;
  public int TargetQuota = 1;
  [FormerlySerializedAs( "StartDelay" )]
  public float MaxStartDelay = 1f;
  [FormerlySerializedAs( "UnderRepeatRate" )]
  public float UnderRepeatInterval = 1f;
  [FormerlySerializedAs( "MaxedRepeatRate" )]
  public float MaxedRepeatInterval = 5f;
  public bool ChooseRandomPrefab = false;
  public List<GameObject> SpawnPrefab;
  public System.Action<GameObject> OnSpawn;
  public List<GameObject> SpawnedCharacters = new List<GameObject>();

  float lastTime = 0;
  int index = 0;

#if SERIALIZED
  [HideInInspector]
  [Serialize] public List<int> SpawnedCharactersID = new List<int>();

  public override void BeforeSerialize()
  {
    SpawnedCharactersID.Clear();
    // serialize the SO ids for SpawnedCharacters, and resolve after deserialization.
    foreach( var obj in SpawnedCharacters )
      if( obj != null )
        SpawnedCharactersID.Add( obj.GetComponent<SerializedObject>().id );
  }

  public override void AfterDeserialize()
  {
    SpawnedCharacters.Clear();
    foreach( var sf in SpawnedCharactersID )
    {
      SerializedObject so = SerializedObject.ResolveObjectFromId( sf );
      if( so != null )
        SpawnedCharacters.Add( so.gameObject );
    }
    SpawnedCharactersID.Clear();
  }
#endif

  void OnEnable()
  {
    if( SpawnPrefab.Count == 0 )
      return;
    /*if( ChooseRandomPrefab )
      randy = new System.Random( GetInstanceID() );*/
    lastTime = Random.Range( 0, MaxStartDelay );
  }

  void Update()
  {
    if( AutoSpawn )
    {
      if( SpawnedCharacters.Count < TargetQuota )
      {
        if( Time.time - lastTime > UnderRepeatInterval )
        {
          lastTime = Time.time;
          SpawnLimited();
        }
      }
      else
      {
        if( Time.time - lastTime > MaxedRepeatInterval )
        {
          lastTime = Time.time;
          SpawnLimited();
        }
      }
    }
  }

  public void SpawnMultiple( int count )
  {
    // ignore quota
    for( int i = 0; i < count; i++ )
      SpawnThatThing();
  }

  public void SpawnLimited() { SpawnLimitedGameObject(); }

  public GameObject SpawnLimitedGameObject()
  {
    if( SpawnPrefab.Count == 0 )
      return null;
    SpawnedCharacters.RemoveAll( x => x == null );
    if( SpawnedCharacters.Count < TargetQuota )
      return SpawnThatThing();
    return null;
  }

  GameObject SpawnThatThing()
  {
    if( ChooseRandomPrefab )
      index = Random.Range( 0, SpawnPrefab.Count );
    GameObject prefab = SpawnPrefab[index];
    if( !ChooseRandomPrefab )
      index = (index + 1 >= SpawnPrefab.Count) ? 0 : index + 1;
    if( prefab == null )
    {
      Debug.LogWarning( "Spawn point has null objects in list " + name, gameObject );
    }
    else
    {
      /*if( prefab.GetComponent<SerializedObject>() == null )
      {
        Debug.LogWarning( "SpawnPoint can only spawn prefabs with a SerializedObject component" );
        return;
      }*/

      // obey limits by doing a check before spawn
      ILimit[] limits = prefab.GetComponentsInChildren<ILimit>();
      foreach( var cmp in limits )
      {
        if( !cmp.IsUnderLimit() )
          return null;
      }

      Vector3 pos = transform.position;
      Quaternion rot = transform.rotation;
      if( SpawnPointLocal )
      {
        pos = SpawnPointLocal.position;
        rot = SpawnPointLocal.rotation;
      }
      pos += Random.insideUnitSphere * SpawnRadius;
      pos.z = 0;
      GameObject go = null;
      if( transform.root != transform && transform.root.tag == "Zone" )
        go = Global.instance.Spawn( prefab, pos, rot, transform.root, true, true );
      else
        go = Global.instance.Spawn( prefab, pos, rot, null, true, true );

      /*if( MyTeam != null )
        Global.Instance.AssignTeam( go, MyTeam );
*/
      if( Animation != null )
      {
        Animation.Stop();
        Animation.Play();
      }
      if( SpawnEffect != null )
        SpawnEffect.Play();
      if( AudioSource != null && SpawnSound != null )
        AudioSource.PlayOneShot( SpawnSound );

      /*Entity entity = go.GetComponent<Entity>();
      if( entity != null ) entity.velocity = transform.up * CannonSpeed;*/

      SpawnedCharacters.Add( go );
      OnSpawn?.Invoke( go );
      return go;
    }
    return null;
  }


  /*
  public void SetTeam( Team team )
  {
    MyTeam = team;
    TeamName = MyTeam.name;
    Color tc = Color.white;
    if( team != null )
      tc = team.Color;
    TextMesh tm = GetComponentInChildren<TextMesh>();
    if( tm != null )
      tm.color = tc;
  }
  */
}