﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

#endif

[RequireComponent( typeof(SpriteRenderer) )]
[ExecuteAlways]
public class TextureScroller : MonoBehaviour
{
#region Dynamic Editor Material

#if UNITY_EDITOR
  bool cachedValue;
#endif
  public bool ScrollerActive;
  [SerializeField] Material runtimeMaterial;
  [SerializeField] SpriteRenderer renderer;

#endregion

  [SerializeField, ReadOnly] Vector2 offset;
  [SerializeField, ReadOnly] float wrapValue = 1;
  public float ScrollSpeed { get { return scrollSpeed; } set { scrollSpeed = value; } }
  public float scrollSpeed;

#region Dynamic Editor Material

  void OnEnable()
  {
#if UNITY_EDITOR
    if( ScrollerActive && !EditorApplication.isPlaying )
      renderer.sharedMaterial = Instantiate( runtimeMaterial );
#else
    renderer.material = runtimeMaterial;
#endif
  }

  void OnDisable()
  {
#if UNITY_EDITOR
    renderer.sharedMaterial = runtimeMaterial;
#endif
  }

#endregion

  void Update()
  {
#region Dynamic Editor Material

#if UNITY_EDITOR
    if( ScrollerActive != cachedValue )
    {
      if( ScrollerActive )
        renderer.sharedMaterial = Instantiate( runtimeMaterial );
      else
        renderer.sharedMaterial = runtimeMaterial;
      cachedValue = ScrollerActive;
    }
#endif
    if( ScrollerActive )
    {
      offset.x += scrollSpeed * Time.smoothDeltaTime;
      if( offset.x > wrapValue )
        offset.x -= wrapValue;
      if( offset.x < -wrapValue )
        offset.x += wrapValue;
#if UNITY_EDITOR
      renderer.sharedMaterial.mainTextureOffset = offset;
#else
    renderer.material.mainTextureOffset = offset;
#endif
    }

#endregion
  }
}