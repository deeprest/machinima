﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

#if UNITY_EDITOR
using UnityEditor;
#endif
// NOTE Only supports horizontally-chained zone chunks. Chunks must be uniform in size.

// to do particle systems' transform setting should be local to origin root transform
// We can use ParticleSystem's Simulation Space option "Custom", and assign the relevent zone transform.

// DONE move dynamic objects, too
// DONE support layers for navmesh in citywrap
// DONE: Walkers segments
// DONE: navmesh needs to regenerate after each chunk change
// DONE: Nodes need to update based on focus position, not if it hits the borders.

/*[System.Serializable]
public class ZoneChunk
{
  public string name;
  public GameObject Parent;
}*/

/*[System.Serializable]
public class ZoneChunkList : ReorderableArray<ZoneChunk> { }*/

public class CityWrap : SceneScript
{
  [SerializeField] Transform focus;
  [SerializeField] float FocusWidth = 50;
  [SerializeField] Vector2 chunkSize = new Vector2( 30, 10 );
  [SerializeField] GameObject[] chunks;

  Bounds bo;
  [System.NonSerialized]
  public GameObject navmeshObject;

  const int MaxColliderCountPerQuery = 1000;
  Collider2D[] colliders = new Collider2D[MaxColliderCountPerQuery];
#if UNITY_EDITOR
  List<Bounds> debugBounds = new List<Bounds>();
#endif


  public override void StartScene()
  {
    base.StartScene();
    // navmesh
    /*GenerateAllLayerNavSurfaces( Vector2.zero, new Vector2( chunkSize.x * 3, chunkSize.y ) );*/
    focus = Global.instance.CameraController.transform;
    // navmesh
    // Get the navmesh surface object to move around as the city wraps
    /*navmeshObject = Global.instance.GetNavMeshObject();*/

    Global.instance.StartMusicLoop();
  }

  int ModIndex( int index )
  {
    int ugh = index;
    while( ugh < 0 )
      ugh += chunks.Length;
    return ugh % chunks.Length;
  }

  [ExposeMethod]
  public override void UpdateScene()
  {
    if( DynamicSpawnEnabled && Global.instance.CurrentPlayer != null )
    {
      Vector2 ppos = (Vector2)Global.instance.CurrentPlayer.transform.position;
      if( Vector2.SqrMagnitude( ppos - DSpoint ) > DSTravelDistance * DSTravelDistance )
      {
        DSpoint = ppos;

        Vector2 spawnVector = Global.instance.CurrentPlayer.velocity.normalized * DSpawnDistance;
        spawnVector += new Vector2( -spawnVector.y, spawnVector.x ).normalized * (Random.value * 2 - 1);
        if( spawnVector.sqrMagnitude < 1 )
          spawnVector = Random.insideUnitCircle * Global.instance.UpdateCullDistance;

        Vector2 SpawnPos = ppos + spawnVector;
        NavMeshHit navhit;
        if( NavMesh.SamplePosition( SpawnPos, out navhit, Global.instance.UpdateCullDistance, NavMesh.AllAreas ) )
          SpawnPos = navhit.position;

        /*Camera cam = Global.instance.CameraController.cam;
        Vector2 cameraExtents = new Vector2( cam.orthographicSize * cam.aspect, cam.orthographicSize );
        if( Vector2.SqrMagnitude( SpawnPos - (Vector2)cam.transform.position ) > Mathf.Max( cameraExtents.sqrMagnitude, 2 ) )*/
        GameObject parentChunk = chunks[ModIndex( Mathf.FloorToInt( SpawnPos.x / chunkSize.x ) )];
        if( parentChunk )
          Global.instance.Spawn( DynamicSpawnObject, SpawnPos, Quaternion.identity, parentChunk.transform, true );
      }
    }
    
#if UNITY_EDITOR
    if( !Application.isPlaying && SceneView.GetAllSceneCameras() != null && SceneView.GetAllSceneCameras()[0] != null )
        focus = SceneView.GetAllSceneCameras()[0].transform;
#endif
    if( !focus )
      return;
    bo = new Bounds( new Vector3( focus.position.x, focus.position.y, 0 ), new Vector3( FocusWidth, FocusWidth, 0 ) );
#if UNITY_EDITOR
    Popcron.Gizmos.Square( bo.center, bo.size, Color.yellow );
#endif
    // relocate a range of chunks
    bool relocate = false;
    int minx = Mathf.FloorToInt( bo.min.x / chunkSize.x );
    int maxx = Mathf.FloorToInt( bo.max.x / chunkSize.x );
    for( int i = minx; i <= maxx; i++ )
    {
      int modIndex = ModIndex( i );
      Vector3 oldPos = chunks[modIndex].transform.position;
      Vector3 newPos = new Vector3( chunkSize.x * i, 0, 0 );
      if( Vector3.SqrMagnitude( oldPos - newPos ) > 1 )
      {
        chunks[modIndex].transform.position = newPos;
        chunks[modIndex].SetActive( true );
        relocate = true;
        // move all dynamic/root objects with a rectangle to the new location
        MoveObjectsToNewZone( oldPos, newPos );
      }
      // nvamesh
      /*if( relocate )
        navmeshObject.transform.position = new Vector3( chunkSize.x * (Mathf.Floor( focus.position.x / chunkSize.x ) - 1), 0, 0 );*/
    }
#if UNITY_EDITOR
    foreach( var bound in debugBounds )
      Popcron.Gizmos.Square( bound.center, bound.size, Color.blue );
#endif

    // background
    /*{
      float backgroundWidth = background.size.x * background.transform.localScale.x;
      int fox = Mathf.FloorToInt( focus.transform.position.x / (backgroundWidth / backgroundParallax.Scale.x) );
      Vector3 oldPos = background.transform.localPosition;
      Vector3 newPos = new Vector3( backgroundWidth * fox, 0, 0 );
      if( Vector3.SqrMagnitude( oldPos - newPos ) > 1 )
      {
        background.transform.localPosition = newPos + Vector3.right * (0.5f * backgroundWidth);
      }
    }*/
    
  }

  void MoveObjectsToNewZone( Vector2 oldPos, Vector2 newPos )
  {
    Bounds moveBounds = new Bounds();
    moveBounds.SetMinMax( new Vector3( oldPos.x, -Global.GameplayLayerOffset * 0.5f, 0 ), new Vector3( oldPos.x + chunkSize.x, Global.GameplayLayerOffset * 0.5f, 0 ) );
#if UNITY_EDITOR
    debugBounds.Add( moveBounds );
    new Timer( 2, null, () => { debugBounds.Remove( moveBounds ); } );
#endif
    Vector3 delta = newPos - oldPos;
    int count = Physics2D.OverlapAreaNonAlloc( oldPos, oldPos + Vector2.right * chunkSize.x + Vector2.up * chunkSize.y, colliders, LayerMask.GetMask( "entity", "entityCollisionOnly", "destructible", "worldselect", "triggerAndCollision" ) );

    // Move only root transforms. Ignore duplicates.
    List<Transform> roots = new List<Transform>();
    for( int i = 0; i < count; i++ )
      roots.Add( colliders[i].transform.root );
    roots.Sort( ( a, b ) =>
    {
      int ida = a.GetInstanceID();
      int idb = b.GetInstanceID();
      if( ida < idb )
        return -1;
      if( ida > idb )
        return 1;
      return 0;
    } );
    Transform previous = null;
    for( int i = 0; i < roots.Count; i++ )
    {
      if( roots[i] != previous )
        roots[i].transform.position = roots[i].transform.position + delta;
      previous = roots[i];
    }

    if( count == MaxColliderCountPerQuery )
      MoveObjectsToNewZone( oldPos, newPos );
  }

  public override Vector3 FindSpawnPosition()
  {
    // wrap spawn position to avoid "walking" the city around
    Vector3 pos = base.FindSpawnPosition();
    int xIndex = Mathf.FloorToInt( pos.x / chunkSize.x );
    Vector3 offset = pos - new Vector3( chunkSize.x * xIndex, 0, 0 );
    return new Vector3( chunkSize.x * ModIndex( xIndex ), 0, 0 ) + offset;
  }
}
