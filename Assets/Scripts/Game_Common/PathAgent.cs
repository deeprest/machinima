﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.AI;

public class PathAgent
{
  public Transform transform;
  /*RaycastHit2D[] RaycastHits;*/
  public Entity Client;

  public bool HasPath;
  public bool AllowOffMesh;
  const float WaypointRadii = 0.5f;
  Vector2 DestinationPosition;
  System.Action OnPathEnd;
  /*System.Action OnPathCancel;*/
  NavMeshPath nvp;
  float PathEventTime;
  int index;
  List<Vector3> waypoint = new List<Vector3>();
#if UNITY_EDITOR || DEVELOPMENT_BUILD
  public List<LineSegment> debugPath = new List<LineSegment>();
#endif
  public int AgentTypeID;

  public Vector2 DeltaToCurrentWaypoint { get; set; }
  public Vector2 DeltaToNextWaypoint { get; set; }

  // sidestep
  /*public bool SidestepAvoidance;
  float SidestepLast;
  Vector2 Sidestep;*/

  public PathAgent() { nvp = new NavMeshPath(); }

  /*public Vector2 GetCurrentWaypoint()
  {
    return waypointEnu.Current;
  }*/

  public void UpdatePath()
  {
    Vector2 pos = transform.position;
    if( HasPath )
    {
      if( waypoint.Count > 0 )
      {
        if( Time.time - PathEventTime > Global.instance.RepathInterval )
        {
          PathEventTime = Time.time;
          SetPath( DestinationPosition, OnPathEnd );
        }
        Vector2 current = waypoint[index];
        Vector2 nextWaypoint = DestinationPosition;
        DeltaToNextWaypoint = nextWaypoint - current;
        if( index + 1 < waypoint.Count )
        {
          nextWaypoint = waypoint[index + 1];
          DeltaToNextWaypoint = nextWaypoint - current;
        }

        Vector2 projected = current;
        /*Vector2 asdf = Util.Project2D( pos - current, nextWaypoint - current );
        if( Vector2.Dot( (nextWaypoint - current).normalized, asdf.normalized ) > 0 )
          projected = current + asdf;*/
        Vector2 DeltaToProjected = projected - pos;

        if( Vector3.SqrMagnitude( DeltaToProjected ) > WaypointRadii * WaypointRadii )
        {
          DeltaToCurrentWaypoint = DeltaToProjected;
        }
        else if( index + 1 < waypoint.Count )
        {
          index++;
          DeltaToCurrentWaypoint = (Vector2) waypoint[index] - pos;
          /*if( index + 1 < waypoint.Count )
          {
            nextWaypoint = waypoint[index + 1];
            DeltaToNextWaypoint = nextWaypoint - current;
          }*/
        }
        else
        {
          // destination reached
          // clear the waypoints before calling the callback because it may set another path and you do not want them to accumulate
          waypoint.Clear();
#if UNITY_EDITOR
          debugPath.Clear();
#endif
          HasPath = false;
          DestinationPosition = pos;
          // do this to allow OnPathEnd to become null because the callback may set another path without a callback.
          System.Action temp = OnPathEnd;
          OnPathEnd = null;
          if( temp != null )
            temp.Invoke();
        }
      }
    }
    else
    {
      // no path
      DeltaToCurrentWaypoint = Vector2.zero;
    }
    /*
        if( Global.instance.GlobalSidestepping && SidestepAvoidance )
        {
          if( Time.time - SidestepLast > Global.instance.SidestepInterval )
          {
            Sidestep = Vector3.zero;
            SidestepLast = Time.time;
            if( MoveDirection.magnitude > 0.001f )
            {
              float distanceToWaypoint = Vector3.Distance( waypointEnu.Current, pos );
              if( distanceToWaypoint > Global.instance.SidestepIgnoreWithinDistanceToGoal )
              {
                float raycastDistance = Mathf.Min( distanceToWaypoint, Global.instance.SidestepRaycastDistance );
                int count = Physics2D.CircleCastNonAlloc( pos, 0.5f, MoveDirection.normalized,  Global.RaycastHits, raycastDistance, Global.CharacterSidestepLayers );
                for( int i = 0; i < count; i++ )
                {
                  Entity other =  Global.RaycastHits[i].transform.root.GetComponent<Entity>();
                  if( other != null && other != Client )
                  {
                    Vector3 delta = other.pos - pos;
                    Sidestep = ((pos + Vector3.Project( delta, MoveDirection.normalized )) - other.pos).normalized * Global.instance.SidestepDistance;
                    break;
                  }
                }
              }
            }
          }
          MoveDirection += Sidestep;
        }
    */
  }


  public void DebugRenderPath()
  {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
    if( debugPath.Count > 0 )
    {
      Color pathColor = Color.white;
      if( nvp.status == NavMeshPathStatus.PathInvalid )
        pathColor = Color.red;
      if( nvp.status == NavMeshPathStatus.PathPartial )
        pathColor = Color.gray;
      foreach( var ls in debugPath )
        Popcron.Gizmos.Line( ls.a, ls.b, pathColor );
    }
    Popcron.Gizmos.Line( transform.position, (Vector2) transform.position + DeltaToCurrentWaypoint, Color.blue );
#endif
  }

  public void ClearPath()
  {
    HasPath = false;
    waypoint.Clear();
#if UNITY_EDITOR || DEVELOPMENT_BUILD
    debugPath.Clear();
#endif
    OnPathEnd = null;
    DestinationPosition = transform.position;
  }

  public bool SetPath( Vector3 TargetPosition, System.Action onArrival = null )
  {
    // WARNING!! DO NOT set path from within Start(). The nav meshes are not guaranteed to exist during Start()
    OnPathEnd = onArrival;
    DestinationPosition = TargetPosition;

    NavMeshHit navhit;
    /*if( !AllowOffMesh )
    {*/
    if( NavMesh.SamplePosition( TargetPosition, out navhit, 1.0f, NavMesh.AllAreas ) )
      DestinationPosition = navhit.position;
    /*}*/
    Vector3 StartPosition = transform.position;
    if( NavMesh.SamplePosition( StartPosition, out navhit, 1.0f, NavMesh.AllAreas ) )
      StartPosition = navhit.position;

    NavMeshQueryFilter filter = new NavMeshQueryFilter();
    filter.agentTypeID = AgentTypeID;
    filter.areaMask = NavMesh.AllAreas;
    nvp.ClearCorners();
    if( NavMesh.CalculatePath( StartPosition, DestinationPosition, filter, nvp ) )
    {
      if( nvp.status == NavMeshPathStatus.PathComplete || nvp.status == NavMeshPathStatus.PathPartial )
      {
        if( nvp.corners.Length > 0 )
        {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
          Vector3 prev = StartPosition;
          debugPath.Clear();
          foreach( var p in nvp.corners )
          {
            LineSegment seg = new LineSegment();
            seg.a = prev;
            seg.b = p;
            debugPath.Add( seg );
            prev = p;
          }
#endif
          waypoint.Clear();
          waypoint.AddRange( nvp.corners );
          // add final position in case it's off the nav mesh
          waypoint.Add( TargetPosition );
          index = 0;
          PathEventTime = Time.time;
          HasPath = true;
          return true;
        }
        /*else
        {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
          Debug.Log( "corners is zero path to: " + TargetPosition );
#endif
        }*/
      }
      else
      {
        waypoint.Clear();
        if( NavMesh.SamplePosition( TargetPosition, out navhit, 50.0f, NavMesh.AllAreas ) )
          DestinationPosition = navhit.position;
        waypoint.Add( DestinationPosition );
        
/*#if UNITY_EDITOR || DEVELOPMENT_BUILD
        Debug.Log( "invalid path to: " + TargetPosition );
#endif*/
      }
    }
    return false;
  }

  public class Path
  {
    public bool Complete;
    public float PathLength;
    public Vector3[] Points;
  }

  public void SetPath( Path path, System.Action onArrival = null )
  {
    OnPathEnd = onArrival;
    DestinationPosition = path.Points[path.Points.Length - 1];
    waypoint.Clear();
    waypoint.AddRange( path.Points );
    index = 0;
    PathEventTime = Time.time;
    HasPath = true;
  }

  public Path GetPath( Vector3 StartPosition, Vector3 TargetPosition )
  {
    // WARNING!! DO NOT call from within Start(). The nav meshes are not guaranteed to exist during Start()
    Path path = new Path();
    List<Vector3> points = new List<Vector3>();

    NavMeshHit navhit;
    /*if( !AllowOffMesh )
    {*/
    if( NavMesh.SamplePosition( TargetPosition, out navhit, 1.0f, NavMesh.AllAreas ) )
      TargetPosition = navhit.position;
    /*}*/
    if( NavMesh.SamplePosition( StartPosition, out navhit, 1.0f, NavMesh.AllAreas ) )
      StartPosition = navhit.position;

    NavMeshQueryFilter filter = new NavMeshQueryFilter();
    filter.agentTypeID = AgentTypeID;
    filter.areaMask = NavMesh.AllAreas;
    nvp.ClearCorners();
    if( NavMesh.CalculatePath( StartPosition, DestinationPosition, filter, nvp ) )
    {
      if( nvp.status == NavMeshPathStatus.PathComplete || nvp.status == NavMeshPathStatus.PathPartial )
      {
        if( nvp.corners.Length > 0 )
        {
          points.AddRange( nvp.corners );
          // add final position in case it's off the nav mesh
          points.Add( TargetPosition );
          path.Complete = nvp.status == NavMeshPathStatus.PathComplete;
          path.Points = points.ToArray();
          // calc length
          float length = 0;
          for( int i = 0; i < points.Count; i++ )
            length += (points[(i + 1) % points.Count] - points[i]).magnitude;
          path.PathLength = length;
        }
      }
    }
    return path;
  }
}