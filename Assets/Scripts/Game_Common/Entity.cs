﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;
#if UNITY_EDITOR
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor( typeof(Entity), true )]
public class EntityEditor : MonoBehaviourInspector
{
  public override void OnInspectorGUI()
  {
    DrawTheExposed();
    Entity obj = target as Entity;
    if( GUI.Button( EditorGUILayout.GetControlRect(), "Manually Populate Lists in Hierarchy" ) )
    {
      Entity[] entities = obj.GetComponentsInChildren<Entity>();
      foreach( var entity in entities )
      {
        entity.AutoPopulateLists = false;
        entity.ClearLists();
        entity.PopulateLists();
      }
      EditorUtility.SetDirty( target );
    }
    if( GUI.Button( EditorGUILayout.GetControlRect(), "Set to Auto" ) )
    {
      Entity[] entities = obj.GetComponentsInChildren<Entity>();
      foreach( var entity in entities )
      {
        entity.AutoPopulateLists = true;
        entity.ClearLists();
      }
      EditorUtility.SetDirty( target );
    }

    if( GUI.Button( EditorGUILayout.GetControlRect(), "Experimental (Assign to All)" ) )
    {
      obj.AutoPopulateLists = false;
      obj.spriteRenderers.Clear();
      obj.spriteRenderers.AddRange( obj.GetComponentsInChildren<SpriteRenderer>() );
      EditorUtility.SetDirty( obj );
      List<Entity> entities = new List<Entity>( obj.GetComponentsInChildren<Entity>() );
      entities.Remove( obj );
      foreach( var entity in entities )
      {
        entity.AutoPopulateLists = false;
        entity.spriteRenderers.Clear();
        entity.spriteRenderers.AddRange( entity.GetComponentsInChildren<SpriteRenderer>() );
        EditorUtility.SetDirty( entity );
      }
    }

    DrawDefaultInspector();
  }
}
#endif

public class Entity : MonoBehaviour, IVelocity, IDamage, ILimit
{
  public virtual bool IsUnderLimit() { return Limit.IsUnderLimit(); }
  public static Limit<Entity> Limit = new Limit<Entity>();
  
  public bool IgnoreLimit { get; set; }

  public bool IgnoreCull;
  public bool culled;

  /*[EnumFlag]
  public TeamFlags TeamFlags;*/

  // The most recent entity from which damage was received.
  public Entity Instigator { get; set; }

  public new Transform transform;
  [SerializeField] public BoxCollider2D box;
  /*[SerializeField] protected new Renderer renderer;*/
  [SerializeField] public Animator animator;
  [SerializeField] public Rigidbody2D body;

  public bool AutoPopulateLists = true;
  public List<SpriteRenderer> spriteRenderers = new List<SpriteRenderer>();
  // composite entities
  [System.NonSerialized]
  public Entity parentEntity;

  public bool UseGravity;
  public bool IsStatic;

  // "local velocity"  ...without inertia or carryCharacter velocity 
  public Vector2 velocity;

  // calculated velocity
  public Vector2 Velocity
  {
    get
    {
      if( carryEntity != null )
      {
        if( carryEntity.GetInstanceID() == GetInstanceID() )
        {
          Debug.LogWarning( $"{name}: carryEntity is self" );
          return velocity;
        }
        return velocity + ((IVelocity) carryEntity).GetVelocityAtPoint( transform.position );
      }
      /*else if( carryConveyorBelt != null )
      {
        return velocity + conveyorMove;
      }*/
      else if( IsStatic && parentEntity != null )
      {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        if( parentEntity == this )
        {
          Debug.LogWarning( "parentEntity is self", gameObject );
          return velocity;
        }
#endif
        return parentEntity.Velocity;
      }

      return velocity + inertia;
    }
  }

  public Vector2 GetVelocityAtPoint( Vector2 point )
  {
    if( body )
      return body.GetPointVelocity( point );
    return Velocity;
  }

  // velocity when character is in the air
  public Vector2 inertia;
  // used for walljumps
  public Vector2 overrideVelocity;
  public Timer overrideVelocityTimer = new Timer();

  public bool hanging { get; set; }
  // Moving platforms / stacking game objects.
  // Only types that implement IVelocity should be assigned to this reference.
  [ReadOnly] public Object carryEntity;
  /*public ConveyorBelt carryConveyorBelt;*/
  public Vector2 conveyorMove;

  public float friction = 10f;
  public float raylength = 0.025f;
  public float contactSeparation = 0.02f;
  public float DownOffset;
  public float Raydown { get { return DownOffset; } } //{ get { return DownOffset + 0.02f; } }

  protected int collideMask;
  protected int collideMask2;

  // collision flags
  public bool collideRight;
  public bool collideLeft;
  public bool collideTop;
  public bool collideBottom;
  public const float corner = 0.707106769f;
  // cache raycast hits to process afterward, for things like crush detection.
  public const int DirectionalRaycastHits = 4;
  public RaycastHit2D[] bottomHits;
  public RaycastHit2D[] topHits;
  public RaycastHit2D[] leftHits;
  public RaycastHit2D[] rightHits;
  protected int bottomHitCount;
  protected int topHitCount;
  protected int leftHitCount;
  protected int rightHitCount;
  // cached
  protected RaycastHit2D hit;
  protected int hitCount;
  public Vector2 boxOffset;

  [Header( "Damage" )]
  [ReadOnly] public bool damageGraceActive;
  public bool CanTakeDamage = true;
  [Tooltip( "ignore damage below this value" )]
  public int DamageThreshold = 1;
  public bool ReflectIfBelowThreshold;
  /*[deeprest.Serialize]*/
  public int Health = 3;
  public int MaxHealth = 5;
  public AudioClip soundHit;

  // FLASH
  public static int Shader_FlashAmount;
  public static int Shader_FlashColor;

  static Entity()
  {
    Shader_FlashAmount = Shader.PropertyToID( "_FlashAmount" );
    Shader_FlashColor = Shader.PropertyToID( "_FlashColor" );
  }

  protected Timer flashTimer = new Timer();
  public float flashInterval = 0.05f;
  public int flashCount = 5;
  [System.NonSerialized] bool flip;
  protected readonly float flashOn = 1f;
  [Tooltip( "deal this damage on collision" )]
  public DamageDefinition ContactDamageDefinition;
  [Tooltip( "deal this damage when crushing another entity" )]
  public DamageDefinition CrushDamageDefinition;

  [Header( "Death" )]
  public bool EnableDeathSequence;
  public float boomDuration = 3;
  public const float boomInterval = 0.15f;
  public float boomRadius = 0.3f;
  protected Timer destructionTimer;
  protected Timer deathSequenceBoomTimer;
  public Color boomFadeColor = new Color( 0.9926471f, 0.3533663f, 0.2627595f, 1 );
  public bool UseGlobalExplosion = true;

  public bool dead;
  public AudioClip soundDeath;
  public SpawnChance[] SpawnOnDeath;
  public UnityEvent EventDeath;
  public UnityEvent EventDestroyed;
  public UnityEvent GetDestructionEvent() { return EventDestroyed; }

  [System.Serializable]
  public class SpawnChance
  {
    public bool alwaysSpawn;
    public int weight = 1;
    public GameObject prefab;
  }

  // "collision" impedes *this* object's movement
  public System.Action UpdateCollision;
  // "hit" inflicts damage on others
  public System.Action UpdateHit;
  // brains!!
  public System.Action UpdateLogic;

  public virtual void PreSceneTransition() { }
  public virtual void PostSceneTransition() { }

  protected virtual void Awake()
  {
    if( Limit.OnCreate( this ) )
    {
      transform = GetComponent<Transform>();
      if( box )
        boxOffset = box.offset;
    }
  }

  protected virtual void OnDestroy()
  {
    if( Global.IsQuiting )
      return;
    Limit.OnDestroy( this );

    flashTimer?.Stop( false );
    overrideVelocityTimer?.Stop( false );
    destructionTimer?.Stop( false );
    deathSequenceBoomTimer?.Stop( false );

    if( parentEntity )
      parentEntity.Remove( GetComponentsInChildren<SpriteRenderer>() );
    parentEntity = null;
    if( !Global.instance.LoadingScene )
      EventDestroyed?.Invoke();
  }

  public void ClearLists() { spriteRenderers.Clear(); }

  public void PopulateLists() { spriteRenderers.AddRange( GetComponentsInChildren<SpriteRenderer>() ); }

  protected virtual void Start()
  {
    // Do not override if the parentEntity was set in the editor. 
    if( transform.parent != null )
      parentEntity = transform.parent.GetComponentInParent<Entity>();

    // Keep this call in Start() to avoid calling GetComponents*() unnecessarily
    if( AutoPopulateLists )
      PopulateLists();

    UpdateHit = BoxHit;
    if( !IsStatic )
    {
      if( box )
        UpdateCollision = BoxCollision;
    }

    collideMask = Global.CharacterCollideLayers;
    collideMask2 = Global.CharacterCollideLayersNoTwoWayPlatform;
    bottomHits = new RaycastHit2D[DirectionalRaycastHits];
    topHits = new RaycastHit2D[DirectionalRaycastHits];
    leftHits = new RaycastHit2D[DirectionalRaycastHits];
    rightHits = new RaycastHit2D[DirectionalRaycastHits];
  }

  public virtual void EntityUpdate()
  {
    if( !dead && UpdateLogic != null )
      UpdateLogic.Invoke();

    if( !dead && UpdateHit != null )
      UpdateHit.Invoke();

    if( !IsStatic )
      UpdatePosition();

    if( UpdateCollision != null )
      UpdateCollision.Invoke();

    if( !damageGraceActive )
      DetectCrushDamage();

    UpdateParts();
  }

  protected void BoxHit()
  {
    if( ContactDamageDefinition != null )
    {
      hitCount = Physics2D.BoxCastNonAlloc( box.transform.position, box.size, 0, velocity, Global.RaycastHits, raylength, Global.CharacterDamageLayers );
      for( int i = 0; i < hitCount; i++ )
      {
        hit = Global.RaycastHits[i];
        if( hit.transform.IsChildOf( transform ) )
          continue;
        IDamage dam = hit.transform.GetComponent<IDamage>();
        if( dam != null )
        {
          Damage dmg = new Damage( ContactDamageDefinition )
          {
            instigator = this,
            damageSource = transform,
            hit = hit
          };
          DamageResult dmr = dam.CalculateDamageResult( dmg );
          if( !dmr.ignore )
            dmr.entity.TakeDamage( dmg );
        }
      }
    }
  }

  public void OverrideVelocity( Vector2 pVelocity, float duration )
  {
    overrideVelocity = pVelocity;
    overrideVelocityTimer.Start( duration,
      delegate( Timer timer ) { overrideVelocity = pVelocity; },
      delegate
      {
        /////// EXPERIMENTAL
        // [this would give inertia when taking damage. If this happens in midair, you simply keep moving/falling in that direction.]
        /*inertia.x = overrideVelocity.x;*/

        overrideVelocity = Vector2.zero;
      } );
  }

  void UpdatePosition()
  {
    if( overrideVelocityTimer.IsActive )
      velocity = overrideVelocity;
    if( UseGravity )
      velocity.y += -Global.Gravity * Time.deltaTime;
    velocity.y = Mathf.Max( velocity.y, -Global.MaxVelocity );

    if( collideTop )
    {
      velocity.y = Mathf.Min( velocity.y, 0 );
    }
    if( collideBottom )
    {
      velocity.x -= (velocity.x * friction) * Time.deltaTime;
      velocity.y = Mathf.Max( velocity.y, 0 );
    }
    if( collideRight )
    {
      velocity.x = Mathf.Min( velocity.x, 0 );
    }
    if( collideLeft )
    {
      velocity.x = Mathf.Max( velocity.x, 0 );
    }

    transform.position += (Vector3) Velocity * Time.deltaTime;
  }


  protected void BoxCollision()
  {
    carryEntity = null;
    /*carryConveyorBelt = null;*/

    float dT = Time.deltaTime;
    collideRight = false;
    collideLeft = false;
    collideTop = false;
    collideBottom = false;
    bottomHitCount = 0;
    topHitCount = 0;
    rightHitCount = 0;
    leftHitCount = 0;

    boxOffset.x = boxOffset.x * Mathf.Sign( transform.localScale.x );
    Vector2 adjust = (Vector2) transform.position + boxOffset;
    float down = Mathf.Max( velocity.y > 0 ? 0 : Raydown, -velocity.y * dT );

    Vector2 boxOrigin = adjust;
    Vector2 boxSize = box.size;
    Bounds bounds = new Bounds( boxOrigin, boxSize );
#if UNITY_EDITOR || DEVELOPMENT_BUILD
    Popcron.Gizmos.Square( bounds.center, bounds.size, Color.red );
#endif

    hitCount = Physics2D.BoxCastNonAlloc( boxOrigin, boxSize, 0, Vector2.down, Global.RaycastHits, down, collideMask );
    float prefer = float.MinValue;
    for( int i = 0; i < hitCount; i++ )
    {
      hit = Global.RaycastHits[i];
      if( hit.transform.IsChildOf( transform ) )
        continue;

      if( hit.normal.y > corner && hit.point.y > prefer )
      {
        collideBottom = true;
        prefer = hit.point.y;
        bottomHits[bottomHitCount++] = hit;
        velocity.y = Mathf.Max( velocity.y, 0 );
        inertia.x = 0;
        adjust.y = hit.point.y + boxSize.y * 0.5f + Raydown;

        // moving platforms
        if( hit.transform.TryGetComponent( out IVelocity cha ) )
          carryEntity = (Object) cha;

        /*if( hit.transform.TryGetComponent( out ConveyorBelt belt ) && belt.enabled )
        {
          carryConveyorBelt = belt;
          if( carryConveyorBelt.CW )
            conveyorMove = new Vector2( hit.normal.y, -hit.normal.x ) * carryConveyorBelt.Speed;
          else
            conveyorMove = new Vector2( -hit.normal.y, hit.normal.x ) * carryConveyorBelt.Speed;
        }*/
      }
    }

    // Do a separate raycast to avoid hitting oneway platforms while moving upwards.
    hitCount = Physics2D.BoxCastNonAlloc( boxOrigin, boxSize, 0, Vector2.down, Global.RaycastHits, Mathf.Max( Raydown, -velocity.y * dT ), collideMask2 );
    for( int i = 0; i < hitCount; i++ )
    {
      hit = Global.RaycastHits[i];
      if( hit.transform.IsChildOf( transform ) )
        continue;

      if( hit.normal.y < -corner && topHitCount < DirectionalRaycastHits )
      {
        collideTop = true;
        topHits[topHitCount++] = hit;
        // EXPERIMENTAL: remove the ability for objects to push one another downward.
        /*velocity.y = Mathf.Min( velocity.y, 0 );
        adjust.y = hit.point.y - boxSize.y * 0.5f - contactSeparation;*/
      }

      if( hit.normal.x > corner && leftHitCount < DirectionalRaycastHits )
      {
        collideLeft = true;
        leftHits[leftHitCount++] = hit;
        velocity.x = Mathf.Max( velocity.x, 0 );
        inertia.x = Mathf.Max( inertia.x, 0 );
        adjust.x = hit.point.x + boxSize.x * 0.5f + contactSeparation;
        // prevent clipping through angled walls when falling fast.
        /*velocity.y -= Util.Project2D( velocity, hit.normal ).y;*/
      }

      if( hit.normal.x < -corner && rightHitCount < DirectionalRaycastHits )
      {
        collideRight = true;
        rightHits[rightHitCount++] = hit;
        velocity.x = Mathf.Min( velocity.x, 0 );
        inertia.x = Mathf.Min( inertia.x, 0 );
        adjust.x = hit.point.x - boxSize.x * 0.5f - contactSeparation;
        // prevent clipping through angled walls when falling fast.
        /*velocity.y -= Util.Project2D( velocity, hit.normal ).y;*/
      }
    }

    transform.position = adjust - boxOffset;
    box.offset = boxOffset + velocity * Time.deltaTime;
#if UNITY_EDITOR || DEVELOPMENT_BUILD
    Popcron.Gizmos.Square( (Vector2) transform.position + box.offset, box.size, Color.cyan );
#endif
  }

  [ExposeMethod]
  public virtual void Die() { Die( default ); }

  [ExposeMethod]
  public virtual void Die( Damage damage )
  {
    if( dead )
      return;
    dead = true;
    velocity = Vector2.zero;

    if( EnableDeathSequence )
    {
      StartDeathSequence();
    }
    else
    {
      if( soundDeath != null )
        Global.instance.AudioOneShot( soundDeath, transform.position );

      if( UseGlobalExplosion )
        Instantiate( Global.instance.explosion, transform.position, Quaternion.identity );

      GameObject[] prefab = GetDeathSpawnPrefabs();
      for( int i = 0; i < prefab.Length; i++ )
        Instantiate( prefab[i], transform.position, Quaternion.identity );
      EventDeath?.Invoke();
      Destroy( gameObject );
    }
  }

  protected void StartDeathSequence()
  {
    deathSequenceBoomTimer = new Timer( (int) (boomDuration / boomInterval), boomInterval,
      delegate( Timer timer ) { Instantiate( Global.instance.explosion, (Vector2) transform.position + Random.insideUnitCircle * boomRadius, Quaternion.identity ); }, null );

    for( int i = 0; i < spriteRenderers.Count; i++ )
    {
      spriteRenderers[i].material.SetFloat( Shader_FlashAmount, 0 );
      spriteRenderers[i].material.SetColor( Shader_FlashColor, boomFadeColor );
    }
    destructionTimer = new Timer( boomDuration, delegate( Timer timer )
    {
      for( int i = 0; i < spriteRenderers.Count; i++ )
      {
        if( spriteRenderers[i] == null )
          continue;
        spriteRenderers[i].material.SetFloat( Shader_FlashAmount, timer.ProgressNormalized );
      }
    }, () =>
    {
      deathSequenceBoomTimer.Stop( true );
      GameObject[] prefab = GetDeathSpawnPrefabs();
      for( int i = 0; i < prefab.Length; i++ )
        Instantiate( prefab[i], transform.position, Quaternion.identity );
      Destroy( gameObject );
      EventDeath?.Invoke();
    } );
  }


  public virtual GameObject[] GetDeathSpawnPrefabs()
  {
    List<GameObject> gos = new List<GameObject>();
    if( SpawnOnDeath.Length > 0 )
    {
      int spawnChanceWeightTotal = 0;
      for( int i = 0; i < SpawnOnDeath.Length; i++ )
        spawnChanceWeightTotal += SpawnOnDeath[i].weight;
      int index = 0;
      int randy = Random.Range( 0, spawnChanceWeightTotal + 1 );
      int runningTotal = 0;
      for( index = 0; index < SpawnOnDeath.Length; index++ )
      {
        if( SpawnOnDeath[index].alwaysSpawn )
        {
          if( SpawnOnDeath[index].prefab != null )
            gos.Add( SpawnOnDeath[index].prefab );
          continue;
        }
        runningTotal += SpawnOnDeath[index].weight;
        if( runningTotal > randy )
        {
          if( SpawnOnDeath[index].prefab != null )
            gos.Add( SpawnOnDeath[index].prefab );
          break;
        }
      }
    }
    return gos.ToArray();
  }

  public virtual Vector2 GetShotOriginPosition() { return transform.position; }
  public virtual Vector2 GetAimVector() { return Vector2.up; }

  public virtual void AddHealth( int amount ) { Health = Mathf.Clamp( Health + amount, 0, MaxHealth ); }


  public virtual DamageResult CalculateDamageResult( Damage damage )
  {
    DamageResult dmr = new DamageResult( this );

    dmr.ignore = !CanTakeDamage || damageGraceActive;

    if( dead /*|| (damage.instigator && IsSameTeam( damage.instigator.TeamFlags ))*/ )
    {
      dmr.ignore = true;
      dmr.pass = true;
      return dmr;
    }
    // not known yet
    /*Instigator = damage.instigator;*/

    // pass to sub-entity if applicable
    if( damage.hit && damage.hit.collider && damage.hit.collider.transform.IsChildOf( transform ) )
    {
      IDamage subentity = damage.hit.collider.GetComponent<IDamage>();
      if( subentity != null && !ReferenceEquals( subentity, this ) )
      {
        dmr = subentity.CalculateDamageResult( damage );
        dmr.entity = subentity;
        return dmr;
      }
    }

    if( !dmr.ignore && damage.def && damage.def.amount < DamageThreshold )
    {
      dmr.ignore = true;
      if( ReflectIfBelowThreshold )
        dmr.reflect = true;
    }

    return dmr;
  }

  [ExposeMethod]
  public void TakeDamage() { TakeDamage( new Damage( Global.instance.CrushDamageDefinition ) ); }

  void AssignInstigator( Entity instigator )
  {
    Instigator = instigator;
    if( parentEntity )
      parentEntity.AssignInstigator( instigator );
  }

  public virtual void TakeDamage( Damage damage )
  {
    if( damage.instigator )
      AssignInstigator( damage.instigator );
    if( dead || damageGraceActive )
      return;
    AddHealth( -damage.def.amount );
    if( Health <= 0 )
    {
      flashTimer.Stop( false );
      Die( damage );
    }
    else
    {
      // hit sound
      if( soundHit != null )
        Global.instance.AudioOneShot( soundHit, transform.position );
      else if( Global.instance.SoundHit != null )
        Global.instance.AudioOneShot( Global.instance.SoundHit, transform.position );

#if DEBUG_CHECKS
      for( int i = 0; i < spriteRenderers.Count; i++ )
        if( spriteRenderers[i] == null )
          Debug.LogWarning( "TakeDamage(): renderer in spriteRenderers null", this );
#endif

      // color pulse
      flip = true;
      damageGraceActive = true;

      for( int i = 0; i < spriteRenderers.Count; i++ )
        spriteRenderers[i].material.SetFloat( Shader_FlashAmount, flashOn );
      flashTimer.Start( flashCount * 2, flashInterval, delegate( Timer t )
      {
        flip = !flip;
        for( int i = 0; i < spriteRenderers.Count; i++ )
          spriteRenderers[i].material.SetFloat( Shader_FlashAmount, flip ? flashOn : 0 );
      }, delegate
      {
        damageGraceActive = false;
        for( int i = 0; i < spriteRenderers.Count; i++ )
          spriteRenderers[i].material.SetFloat( Shader_FlashAmount, 0 );
      } );
    }
  }

  public Vector2 GetExplosionDetectionPoint() { return transform.position; }

  // Utility function for dealing damage with a single line. Does not handle reflections, passthrough, etc.
  public bool DamageOther( IDamage dam, DamageDefinition def, Transform source, RaycastHit2D hitSource )
  {
    if( dam == null )
      return false;
    Damage dmg = new Damage( def )
    {
      instigator = this,
      damageSource = source,
      hit = hitSource
    };
    DamageResult dmr = dam.CalculateDamageResult( dmg );
    if( !dmr.ignore )
    {
      dmr.entity.TakeDamage( dmg );
      return true;
    }
    return false;
  }


  // This is needed for Unity Events
  public void SetCanTakeDamage( bool value ) { CanTakeDamage = value; }

  void DetectCrushDamage()
  {
    if( collideRight && collideLeft && rightHitCount > 0 && leftHitCount > 0 && rightHits[0].point.x - leftHits[0].point.x < box.size.x * 0.8f )
      TakeDamage( new Damage( Global.instance.CrushDamageDefinition ) );
    if( collideTop && collideBottom && topHitCount > 0 && bottomHitCount > 0 && topHits[0].point.y - bottomHits[0].point.y < box.size.y * 0.8f )
      TakeDamage( new Damage( Global.instance.CrushDamageDefinition ) );
  }

  public void TakeCrushDamage( Entity Crusher )
  {
    Damage crushDamage;
    if( Crusher.CrushDamageDefinition )
      crushDamage = new Damage( Crusher.CrushDamageDefinition );
    else
      crushDamage = new Damage( Global.instance.CrushDamageDefinition );
    crushDamage.damageSource = Crusher.transform;
    // crush damage should not need an instigator.
    TakeDamage( crushDamage );
  }

  public void Remove( SpriteRenderer[] srs )
  {
    foreach( var sr in srs )
      spriteRenderers.Remove( sr );
    if( parentEntity != null )
      parentEntity.Remove( srs );
  }

  // for EventDestroyed unity events
  public void DestroyGameObject( GameObject go ) { Destroy( go ); }

  /*public virtual bool IsEnemyTeam( TeamFlags other ) { return TeamFlags != TeamFlags.None && other != TeamFlags.None && other != TeamFlags; }
  public virtual bool IsSameTeam( TeamFlags other ) { return (TeamFlags & other) > 0; }*/


  [Header( "Animated Sorting Order" )]
  public int CharacterLayer;

  [System.Serializable]
  public struct CharacterPart
  {
    /*public bool enabled;*/
    public SpriteRenderer renderer;
    public int layerAnimated;
    [Tooltip( "Optional" )]
    public Animator animator;
  }

  [System.NonSerialized]
  public List<CharacterPart> AnimatedCharacterParts;

  // Call this from Awake()
  public virtual void InitializeParts()
  {
    // intentionally empty
  }

  // Call this from LateUpdate()
  public void UpdateParts()
  {
    // Most entities will not need this, let the implementors instantiate the list as-needed.
    if( AnimatedCharacterParts == null )
      return;

    for( int i = 0; i < AnimatedCharacterParts.Count; i++ )
      if( AnimatedCharacterParts[i].renderer == null )
        AnimatedCharacterParts.RemoveAt( i-- );

    foreach( var part in AnimatedCharacterParts )
      part.renderer.sortingOrder = CharacterLayer + part.layerAnimated;
  }

  public virtual void Teleport( Vector2 newPos ) { transform.position = newPos; }

  public void SetProgressFlag( string flag ) { Global.instance.ProgressFlag[flag].Value = true; }


#region AnimationEventFunctions

  public void PlaySound( AudioClip clip ) { Global.instance.AudioOneShot( clip, transform.position ); }

#endregion
}