﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;


[System.Serializable]
public struct InputState
{
  // 1 byte
  public bool MoveLeft;
  public bool MoveRight;
  public bool MoveUp;
  public bool MoveDown;
  public bool Jump;
  //
  public bool HopModifier;
  //
  public bool NOTUSED_B;
  //
  public bool NOTUSED_C;
  // 1 byte
  public bool NOTUSED_D;
  public bool NOTUSED_E;
  public bool NOTUSED_F;
  public bool NOTUSED_G;
  public bool NOTUSED0;
  public bool NOTUSED1;
  public bool NOTUSED2;
  public bool NOTUSED3;
  // 8 byte
  public Vector2 Aim;
}

// todo record the initial state of all objects in the scene
// store random seed / generation seed
// store the scene index or sname

// replace using frame index with timestamps. during playback, look ahead to the
// state in the next keyframe and interpolate to it.

// add chop drop event
// store slo-motion begin/end events
// store pause, menu active events (or ignore them during capture)

public static class RecordConstants
{
  public const int HEADER_SIZE = 8;
  public const int FRAME_SIZE = 4 + (2 + 8) + 8;
}

public struct RecordHeader
{
  public Vector2 initialposition;
  // todo need all relevent pawn state
  // scene ref / name to load
  // load serialized scene state
}

public struct RecordFrame
{
  public float timestamp;
  public InputState input;
  public Vector2 position;
}


[CreateAssetMenu]
public class PlayerControllerDefinition : ControllerDefinition
{
  public override Controller GetNewController() { return new PlayerController( this ); }
  public float DirectionalCursorDistance = 3;

  public float AutoPlayTimeout = 5;
  public float RandomMoveIntervalMin = 1;
  public float RandomMoveIntervalMax = 5;
  public float RandomMoveTargetDistance = 10;
  public float RandomMoveTargetRadius = 10;
  public float small = 0.1f;
  public float AvoidanceRaycastDistance = 3;
  
  /*public float jumpMinimum = .2f;
  public float jumpCheckDistanceRight = 1;
  public float jumpCheckOriginUp = .3f;*/
  public float jumpHoldDuration = 0.2f;
}

public class PlayerController : Controller
{
  PlayerControllerDefinition def;
  Controls.BipedActionsActions BA;
  // any persistent input variables
  /*Vector2 aimPosition = Vector2.zero;
  Vector2 aimDeltaSinceLastFrame;*/
  /*Timer RespawnTimer = new Timer();*/

  public string AgentTypeName = "Small";
  PathAgent pathAgent;
  bool AutoPlay;
  Timer AutoPlayTimer = new Timer();
  Timer RandomMoveTimer = new Timer();
  int hitCount;
  Timer jumpHoldTimer = new Timer();
  bool holdLeft;
  bool holdRight;

  // init bindings to modify input struct
  public PlayerController( PlayerControllerDefinition newDef ) : base( newDef )
  {
    def = newDef;
    BindControls();
    recordpath = Application.persistentDataPath + "/input.dat";
  }

  public void HACKSetSpeed( float speed )
  {
    if( pawn != null )
      pawn.OnControllerAssigned();
  }

  public override void AssignPawn( Pawn pwn )
  {
    /*if( Global.instance.PlayerController == this )
    {
      Global.instance.CameraController.LookTarget = null;
      DisableBipedControls();
    }*/

    base.AssignPawn( pwn );
    if( Global.instance.PlayerController == this )
    {
      Global.instance.CameraController.LookTarget = this;
      EnableBipedControls();
    }
    
    pathAgent = new PathAgent();
    pathAgent.Client = pawn;
    pathAgent.transform = pawn.transform;
    pathAgent.AgentTypeID = Global.instance.AgentType[AgentTypeName];
    pathAgent.AllowOffMesh = false;
  }

  public void EnableBipedControls()
  {
    Global.instance.Controls.BipedActions.Enable();
    // reset the aim vector to avoid jumps
    /*aimPosition = Vector2.zero;*/
  }

  public void DisableBipedControls() { Global.instance.Controls.BipedActions.Disable(); }

  private void BindControls()
  {
    BA = Global.instance.Controls.BipedActions;

    // BA.HopModifier.started += ( obj ) => input.HopModifier = true;
    // BA.NextWeapon.performed += ( obj ) => input.NextWeapon = true;
    //BA.Charge.started += ( obj ) => input.Charge = true;
    // BA.Interact.performed += ( obj ) => { input.Interact = true; };
    /*BA.Aim.performed += ( obj ) => { aimDeltaSinceLastFrame = obj.ReadValue<Vector2>(); };*/

    /*BA.Minimap.performed += ( obj ) => { Global.instance.ToggleMinimap(); };*/
  }

  // apply input to pawn
  public void FireOnlyInputMode()
  {
    Global.instance.Controls.BipedActions.Disable();
    /*Global.instance.Controls.BipedActions.Aim.Enable();
    Global.instance.Controls.BipedActions.Fire.Enable();*/
  }

  public override void Update()
  {
    if( pawn == null )
      return;

    if( playback )
    {
      if( pawn == null || playbackIndex >= playbackFrameBuffer.Count )
        PlaybackEnd();
      else
      {
        pawn.transform.position = playbackFrameBuffer[playbackIndex].position;
        pawn.ApplyInput( playbackFrameBuffer[playbackIndex++].input );
      }
    }
    else
    {
      /*if( Cursor.lockState != CursorLockMode.None )
      {
        if( Global.instance.UsingGamepad )
        {
          Vector2 newposition = BA.Aim.ReadValue<Vector2>() * playerDef.DirectionalCursorDistance;
          Vector2 clipped = ClipAimPositionToEdgeOfScreen( newposition );
          if( clipped.sqrMagnitude > 0.5f )
            aimPosition = clipped;
          else
            aimPosition = newposition;
        }
        else
        {
          Vector2 newposition = aimPosition + aimDeltaSinceLastFrame * Global.instance.CursorSensitivity;
          if( Camera.main.orthographic )
          {
            Vector2 clipped = ClipAimPositionToEdgeOfScreen( newposition );
            // only clip to edge of screen when not close to the edge. This is to avoid letting the aim vector diminish to nothing
            // while running into the edge of the screen
            if( clipped.sqrMagnitude > 0.5f )
              aimPosition = clipped;
            else
              aimPosition = newposition;
            // keep cursor within CursorOuter radius
            aimPosition = aimPosition.normalized * Mathf.Max( Mathf.Min( aimPosition.magnitude, Camera.main.orthographicSize * Camera.main.aspect * Global.instance.CursorOuterOrtho ), 0.01f );
          }
          else
          {
            aimPosition = newposition;
            aimPosition = aimPosition.normalized * Mathf.Max( Mathf.Min( aimPosition.magnitude, Global.instance.CursorOuterPerspective ), 0.01f );
          }
          aimDeltaSinceLastFrame = Vector2.zero;
        }
      }
      input.Aim = aimPosition;*/
      input.Jump = BA.Jump.IsPressed();
      input.HopModifier = BA.HopModifier.IsPressed();

      Vector2 move = BA.Move.ReadValue<Vector2>();
      if( move.x > 0.5f )
        input.MoveRight = true;
      if( move.x < -0.5f )
        input.MoveLeft = true;
      if( move.y > 0.5f )
        input.MoveUp = true;
      if( move.y < -0.5f )
        input.MoveDown = true;

      UpdateAutoPlay();

      pawn.ApplyInput( input );

      if( recording )
      {
        recordFrameBuffer.Add( new RecordFrame
        {
          input = input,
          position = pawn.transform.position,
          timestamp = Time.time - recordStartTime
        } );
      }
    }

    input = default;
  }

  void UpdateAutoPlay()
  {
    if( !(input.Jump || input.MoveDown || input.MoveUp || input.MoveRight || input.MoveLeft) )
    {
      if( !AutoPlayTimer.IsActive )
        AutoPlayTimer.Start( def.AutoPlayTimeout, null, () => { AutoPlay = true; } );
    }
    else
    {
      AutoPlayTimer.Stop( false );
      AutoPlay = false;
    }

    if( AutoPlay )
    {
      // cached for optimization
      Vector2 pawnPos = pawn.transform.position;

      // NOTE Player can only move to the right.
      if( !RandomMoveTimer.IsActive || pathAgent.DeltaToCurrentWaypoint.x < 0 || !pathAgent.HasPath )
      {
        RandomMoveTimer.Start( Random.Range( def.RandomMoveIntervalMin, def.RandomMoveIntervalMax ) );
        // random cruise
        pathAgent.SetPath( (Vector2)pawn.transform.position + Vector2.right * def.RandomMoveTargetDistance + Random.insideUnitCircle * def.RandomMoveTargetRadius, null );
      }

      pathAgent.UpdatePath();
      pathAgent.DebugRenderPath();

      Vector2 moveDelta = pathAgent.DeltaToCurrentWaypoint;
      if( pathAgent.HasPath && moveDelta.magnitude > def.small )
      {
        if( moveDelta.x > def.small )
          input.MoveRight = true;
        // move left is used to brake
        /*if( moveDelta.x < -def.small )
          input.MoveLeft = true;*/
        if( moveDelta.y < -def.small )
          input.MoveDown = true;
        if( moveDelta.y > def.small )
          input.MoveUp = true;
      }
      else
      {
        // if no path then raycast to avoid hitting things
        int count = Physics2D.RaycastNonAlloc( pawnPos, Vector2.right, Global.RaycastHits, def.AvoidanceRaycastDistance, Global.CharacterCollideLayers );
        if( count == 0 )
          input.MoveRight = true;
        else
        {
          count = Physics2D.RaycastNonAlloc( pawnPos, Vector2.right + Vector2.up, Global.RaycastHits, def.AvoidanceRaycastDistance, Global.CharacterCollideLayers );
          if( count == 0 )
          {
            input.MoveRight = true;
            input.MoveUp = true;
          }
          else
          {
            count = Physics2D.RaycastNonAlloc( pawnPos, Vector2.right + Vector2.down, Global.RaycastHits, def.AvoidanceRaycastDistance, Global.CharacterCollideLayers );
            if( count == 0 )
            {
              input.MoveRight = true;
              input.MoveDown = true;
            }
            else
            {
              // trapped!

              input.Jump = jumpHoldTimer.IsActive;
              if( input.Jump )
              {
                input.MoveRight = holdRight;
                /*input.MoveLeft = holdLeft;*/
              }
              else
                // todo if( object is directly in front, and there is clearance on other side (valid navmesh position) )
              {
                jumpHoldTimer.Start( def.jumpHoldDuration );
                input.Jump = false;
                input.MoveRight = true;
                holdRight = true;

                /*holdRight = false;
                holdLeft = false;
                if( pathAgent.DeltaToNextWaypoint.x >= 0 &&
                   Physics2D.RaycastNonAlloc( (Vector2) pawnPos + Vector2.up * def.jumpCheckOriginUp, Vector2.right, Global.RaycastHits, def.jumpCheckDistanceRight, Global.CharacterCollideLayers ) > 0 )
                {
                  jumpHoldTimer.Start( def.jumpHoldDuration );
                  input.Jump = false;
                  input.MoveRight = true;
                  holdRight = true;
                }
                else if( pathAgent.DeltaToNextWaypoint.x < 0 &&
                        Physics2D.RaycastNonAlloc( (Vector2) pawnPos + Vector2.up * def.jumpCheckOriginUp, Vector2.left, Global.RaycastHits, def.jumpCheckDistanceRight, Global.CharacterCollideLayers ) > 0 )
                {
                  jumpHoldTimer.Start( def.jumpHoldDuration );
                  input.Jump = false;
                  input.MoveLeft = true;
                  holdLeft = true;
                }*/
              }
            }
          }
        }
      }
    }
  }

  public void NormalInputMode() { Global.instance.Controls.BipedActions.Enable(); }

  Vector2 ClipAimPositionToEdgeOfScreen( Vector2 newPlayerLocalAimPosition )
  {
    // clip cursor to edge of screen
    Vector2 cpos = Camera.main.gameObject.transform.position;
    float chsy = Camera.main.orthographicSize;
    float chsx = Camera.main.orthographicSize * Camera.main.aspect;
    Vector2 UL = new Vector2( cpos.x - chsx, cpos.y + chsy );
    Vector2 UR = new Vector2( cpos.x + chsx, cpos.y + chsy );
    Vector2 LR = new Vector2( cpos.x + chsx, cpos.y - chsy );
    Vector2 LL = new Vector2( cpos.x - chsx, cpos.y - chsy );
#if UNITY_EDITOR || DEVELOPMENT_BUILD
    Popcron.Gizmos.Line( UL, UR, Color.red );
    Popcron.Gizmos.Line( UR, LR, Color.red );
    Popcron.Gizmos.Line( LR, LL, Color.red );
    Popcron.Gizmos.Line( LL, UL, Color.red );
#endif
    Vector2 cursorOrigin = pawn.transform.position;
    Vector2 NewAimPosition = cursorOrigin + newPlayerLocalAimPosition;
    Vector2 result = NewAimPosition;
    bool intersect = false;
    if( Util.LineSegementsIntersect( UL, UR, cursorOrigin, NewAimPosition, ref result ) )
      intersect = true;
    if( Util.LineSegementsIntersect( UR, LR, cursorOrigin, NewAimPosition, ref result ) )
      intersect = true;
    if( Util.LineSegementsIntersect( LR, LL, cursorOrigin, NewAimPosition, ref result ) )
      intersect = true;
    if( Util.LineSegementsIntersect( LL, UL, cursorOrigin, NewAimPosition, ref result ) )
      intersect = true;
    if( intersect )
    {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
      Popcron.Gizmos.Circle( result, .2f, Quaternion.identity, Color.red );
      Popcron.Gizmos.Line( cursorOrigin, result, Color.red );
#endif
      return result - cursorOrigin;
    }
#if UNITY_EDITOR || DEVELOPMENT_BUILD
    Popcron.Gizmos.Line( cursorOrigin, result, Color.cyan );
#endif
    return newPlayerLocalAimPosition;
  }

#region Record

  // Playback
  bool playback;
  float playbackStartTime;
  int playbackIndex = 0;
  RecordHeader playbackHeader;
  List<RecordFrame> playbackFrameBuffer;

  // Record
  bool recording;
  float recordStartTime;
  public RecordHeader recordHeader;
  public List<RecordFrame> recordFrameBuffer;

  // testing file path
  string recordpath;

  public void RecordToggle()
  {
    if( recording )
    {
      recording = false;
      /*SerializeRecordBufferToFile( recordpath );*/
    }
    else
      RecordBegin();
  }

  public bool IsRecording() { return recording; }

  public void RecordBegin()
  {
    recording = true;
    recordFrameBuffer = new List<RecordFrame>();
    recordHeader = new RecordHeader
    {
      initialposition = pawn.transform.position
    };
    recordStartTime = Time.time;
  }

  public void SerializeRecordBufferToFile( string filepath )
  {
    FileStream fs = File.Open( filepath, FileMode.Create );
    byte[] x;
    int p = 0;
    byte[] buffer = new byte[RecordConstants.HEADER_SIZE + recordFrameBuffer.Count * RecordConstants.FRAME_SIZE];

    x = System.BitConverter.GetBytes( recordHeader.initialposition.x );
    buffer[p] = x[0];
    buffer[p + 1] = x[1];
    buffer[p + 2] = x[2];
    buffer[p + 3] = x[3];
    p += 4;

    x = System.BitConverter.GetBytes( recordHeader.initialposition.y );
    buffer[p] = x[0];
    buffer[p + 1] = x[1];
    buffer[p + 2] = x[2];
    buffer[p + 3] = x[3];
    p += 4;

    for( int i = 0; i < recordFrameBuffer.Count; i++ )
    {
      RecordFrame frame = recordFrameBuffer[i];
      InputState input = frame.input;
      // 2 byte
      int value = (input.MoveLeft ? 0x1 : 0x0) << 0 | (input.MoveRight ? 0x1 : 0x0) << 1 | (input.MoveUp ? 0x1 : 0x0) << 2 | (input.MoveDown ? 0x1 : 0x0) << 3 | (input.Jump ? 0x1 : 0x0) << 4 | (input.HopModifier ? 0x1 : 0x0) << 5 | (input.NOTUSED_B ? 0x1 : 0x0) << 6 | (input.NOTUSED_C ? 0x1 : 0x0) << 7 | (input.NOTUSED_D ? 0x1 : 0x0) << 8 | (input.NOTUSED_E ? 0x1 : 0x0) << 9 | (input.NOTUSED_E ? 0x1 : 0x0) << 10 | (input.NOTUSED_F ? 0x1 : 0x0) << 11 | (input.NOTUSED0 ? 0x1 : 0x0) << 12 | (input.NOTUSED0 ? 0x1 : 0x0) << 13 | (input.NOTUSED0 ? 0x1 : 0x0) << 14 | (input.NOTUSED0 ? 0x1 : 0x0) << 15;

      buffer[p] = (byte)value;
      buffer[p + 1] = (byte)(value >> 8);
      p += 2;

      x = System.BitConverter.GetBytes( input.Aim.x );
      buffer[p] = x[0];
      buffer[p + 1] = x[1];
      buffer[p + 2] = x[2];
      buffer[p + 3] = x[3];
      p += 4;

      x = System.BitConverter.GetBytes( input.Aim.y );
      buffer[p] = x[0];
      buffer[p + 1] = x[1];
      buffer[p + 2] = x[2];
      buffer[p + 3] = x[3];
      p += 4;

      // position
      x = System.BitConverter.GetBytes( frame.position.x );
      buffer[p] = x[0];
      buffer[p + 1] = x[1];
      buffer[p + 2] = x[2];
      buffer[p + 3] = x[3];
      p += 4;

      x = System.BitConverter.GetBytes( frame.position.y );
      buffer[p] = x[0];
      buffer[p + 1] = x[1];
      buffer[p + 2] = x[2];
      buffer[p + 3] = x[3];
      p += 4;

      x = System.BitConverter.GetBytes( frame.timestamp );
      buffer[p] = x[0];
      buffer[p + 1] = x[1];
      buffer[p + 2] = x[2];
      buffer[p + 3] = x[3];
      p += 4;
    }

    fs.Write( buffer, 0, buffer.Length );
    fs.Flush();
    fs.Close();
  }

  public void PlaybackToggle()
  {
    playback = !playback;
    if( playback )
    {
      DeserializePlaybackBuffer( recordpath, out playbackHeader, out playbackFrameBuffer );
      playback = true;
      playbackIndex = 0;
      playbackStartTime = Time.time;

      pawn.transform.position = recordHeader.initialposition;
    }
    else
      PlaybackEnd();
  }

  public void DeserializePlaybackBuffer( string filepath, out RecordHeader header, out List<RecordFrame> frames )
  {
    List<RecordFrame> recordFrames = new List<RecordFrame>();
    byte[] buffer = File.ReadAllBytes( filepath );
    // header
    RecordHeader recordHeader = new RecordHeader();
    recordHeader.initialposition = new Vector3( System.BitConverter.ToSingle( buffer, 0 ), System.BitConverter.ToSingle( buffer, 4 ), 0 );
    // frames
    for( int i = RecordConstants.HEADER_SIZE; i < buffer.Length; i += RecordConstants.FRAME_SIZE )
    {
      InputState state = new InputState
      {
        MoveLeft = ((buffer[i] >> 0) & 0x1) > 0,
        MoveRight = ((buffer[i] >> 1) & 0x1) > 0,
        MoveUp = ((buffer[i] >> 2) & 0x1) > 0,
        MoveDown = ((buffer[i] >> 3) & 0x1) > 0,
        Jump = ((buffer[i] >> 4) & 0x1) > 0,
        HopModifier = ((buffer[i] >> 5) & 0x1) > 0,
        NOTUSED_B = ((buffer[i] >> 6) & 0x1) > 0,
        NOTUSED_C = ((buffer[i] >> 7) & 0x1) > 0,

        NOTUSED_D = ((buffer[i + 1] >> 0) & 0x1) > 0,
        NOTUSED_E = ((buffer[i + 1] >> 1) & 0x1) > 0,
        NOTUSED_F = ((buffer[i + 1] >> 2) & 0x1) > 0,
        NOTUSED_G = ((buffer[i + 1] >> 3) & 0x1) > 0,
        NOTUSED0 = ((buffer[i + 1] >> 4) & 0x1) > 0,
        NOTUSED1 = ((buffer[i + 1] >> 5) & 0x1) > 0,
        NOTUSED2 = ((buffer[i + 1] >> 6) & 0x1) > 0,
        NOTUSED3 = ((buffer[i + 1] >> 7) & 0x1) > 0,

        Aim = new Vector2( System.BitConverter.ToSingle( buffer, i + 2 ), System.BitConverter.ToSingle( buffer, i + 6 ) )
      };

      Vector2 pos = new Vector2( System.BitConverter.ToSingle( buffer, i + 10 ), System.BitConverter.ToSingle( buffer, i + 14 ) );
      float timestamp = System.BitConverter.ToSingle( buffer, i + 18 );

      recordFrames.Add( new RecordFrame { timestamp = timestamp, input = state, position = pos } );
    }
    frames = recordFrames;
    header = recordHeader;
  }

  public void PlaybackEnd() { playback = false; }

#endregion
}