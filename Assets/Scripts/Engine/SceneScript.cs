﻿using System.Collections.Generic;
using SuperTiled2Unity;
using Unity.AI.Navigation;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering;
using UnityEngine.Serialization;
using UnityEngine.Tilemaps;
using UnityEngine.U2D.Animation;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

#if UNITY_EDITOR
using SuperTiled2Unity.Editor;
#endif

public class SceneScript : MonoBehaviour
{
  public GameObject NavMeshPrefab;
  public GameObject NavMeshModifierPrefab;

  [Header( "On Start" )]
  public bool RenderMinimap;
  public bool GenerateVariants;
  /*public bool GenerateNavMesh;*/

  [Header( "Settings" )]
  public Bounds bounds;
  public Color backgroundColor;
  public AudioLoop music;
  public bool RandomMusicCycle = false;
  public Tilemap tilemap;
  public CameraZone ForceCameraZone;
  /*public Light2D ambientLight;
  public Rain rain;*/
  /*public RestaurantNamegen RestaurantNamegen;*/
  [Header( "DynamicSpawn" )]
  public bool DynamicSpawnEnabled;
  [FormerlySerializedAs( "DSDistance" )]
  public float DSTravelDistance;
  public float DSpawnDistance = 20;
  public Vector2 DSpoint;
  public GameObject DynamicSpawnObject;
  public UnityEvent OnSceneStart;

  public SpriteLibraryAsset SpriteLibraryAsset;

  public virtual void StartScene()
  {
    if( ForceCameraZone != null )
      Global.instance.OverrideCameraZone( ForceCameraZone );

    // set the LookTarget before calling this
    Global.instance.CameraController.TeleportToLookTarget();

    Global.instance.CameraController.cam.backgroundColor = backgroundColor;

    /*if( RandomMusicCycle )
      Global.instance.PlayRandomSongFromAlbum();
    else if( music != null )
      Global.instance.StartMusicLoop( music );*/
    /*Global.instance.MusicCrossFadeTo( music, Global.instance.MusicTransitionDuration );*/

    if( SpriteLibraryAsset )
    {
      SpriteLibrary[] comps = FindObjectsByType<SpriteLibrary>( FindObjectsInactive.Include, FindObjectsSortMode.None );
      foreach( var cmp in comps )
        cmp.spriteLibraryAsset = SpriteLibraryAsset;
    }

    /*if( ambientLight != null )
    {
      float targetIntensity = ambientLight.intensity;
      new Timer( 3, delegate( Timer timer ) { ambientLight.intensity = timer.ProgressNormalized * targetIntensity; }, null );
    }*/

    // Optional
    /*if( rain != null )
      rain.Initialize( bounds );*/

    /*WalkerSystem[] WalkerSystems = FindObjectsOfType<WalkerSystem>();
    if( WalkerSystems != null )
      for( int i = 0; i < WalkerSystems.Length; i++ )
        WalkerSystems[i].Generate();*/

    /*RestaurantTitleTrigger[] triggers = FindObjectsOfType<RestaurantTitleTrigger>();
    // temp: generate a new business name for each one registered
    foreach( var rtiTrigger in triggers )
      rtiTrigger.AssignInfo( RestaurantNamegen.GenerateRestaurantTitle() );*/

    /*if( RenderMinimap )
      Global.instance.MinimapRender( bounds );*/

    // spawn or position the player.
    if( Global.instance.CurrentPlayer == null )
      Global.instance.SpawnPlayer();
    else
    {
      Global.instance.CurrentPlayer.transform.position = Global.instance.FindBestSpawnPosition();
      Global.instance.CurrentPlayer.velocity = Vector2.zero;
    }

    /*if( GenerateVariants )
    {
      foreach( var gv in generationVariants )
        gv.Generate();
    }*/

    /*if( GenerateNavMesh )
    {
      GenerateNavSurfaces();
    }*/

    OnSceneStart?.Invoke();
  }

  public virtual void UpdateScene()
  {
    /*if( DynamicSpawnEnabled && Global.instance.CurrentPlayer != null )
    {
      Vector2 ppos = (Vector2)Global.instance.CurrentPlayer.transform.position;
      if( Vector2.SqrMagnitude( ppos - DSpoint ) > DSDistance * DSDistance )
      {
        DSpoint = ppos;

        Vector2 spawnVector = Global.instance.CurrentPlayer.velocity.normalized * (Global.instance.CameraController.orthoTarget * 2 + 2);
        spawnVector += new Vector2( -spawnVector.y, spawnVector.x ).normalized * (Random.value * 2 - 1);
        if( spawnVector.sqrMagnitude < 1 )
          spawnVector = Random.insideUnitCircle * Global.instance.UpdateCullDistance;

        Vector2 SpawnPos = ppos + spawnVector;
        /*NavMeshHit navhit;
        if( NavMesh.SamplePosition( SpawnPos, out navhit, Global.instance.UpdateCullDistance, NavMesh.AllAreas ) )
          SpawnPos = navhit.position;#1#

        /*Camera cam = Global.instance.CameraController.cam;
        Vector2 cameraExtents = new Vector2( cam.orthographicSize * cam.aspect, cam.orthographicSize );
        if( Vector2.SqrMagnitude( SpawnPos - (Vector2)cam.transform.position ) > Mathf.Max( cameraExtents.sqrMagnitude, 2 ) )#1#
          Global.instance.Spawn( DynamicSpawnObject, SpawnPos, Quaternion.identity, null, true );
      }
    }*/
  }

  public virtual Vector3 FindSpawnPosition() { return Global.instance.CycleSpawnPosition(); }

  public void PlayerInputOff() { Global.instance.Controls.BipedActions.Disable(); }

  public void CameraZoom( float value ) { Global.instance.CameraController.orthoTarget = value; }

  public void AssignCameraZone( CameraZone zone ) { Global.instance.OverrideCameraZone( zone ); }

#if UNITY_EDITOR
  [ExposeMethod]
  public void EncapsulateBounds_Editor()
  {
    EncapsulateBounds();
    EditorUtility.SetDirty( this );
  }


  public void EncapsulateBounds()
  {
    // todo establish a sensible bounds default without crawling through every object in the scene
    bounds = new Bounds();
    bounds.size = new Vector3( 1, 1, 1 );
    // encapsulate existing objects in scene
    GameObject[] gos = Object.FindObjectsOfType<GameObject>();
    foreach( var go in gos )
    {
      if( go == gameObject )
        continue;
      Collider2D[] cld = go.GetComponentsInChildren<Collider2D>();
      foreach( var c in cld )
        bounds.Encapsulate( c.bounds );
    }
  }

  const string NavMeshBoxName = "NavMeshBox_";
  const string NavMeshSurfaceName = "NavMesh";
  /*const int MaxLayerCount = 1;
  MeshCollider[] layerNavmeshCollider = new MeshCollider[MaxLayerCount];
  // leave a little extra room on the outside so flying enemies can get around.
  const float BoundsPadding = 5;*/

  [ExposeMethod]
  public void RemoveNavSurfaces()
  {
    List<GameObject> destroy = new List<GameObject>();
    SuperMap[] supermaps = FindObjectsByType<SuperMap>( FindObjectsInactive.Include, FindObjectsSortMode.None );
    for( int i = 0; i < supermaps.Length; ++i )
    {
      SuperMap smap = supermaps[i];
      Transform[] transforms = smap.transform.GetComponentsInChildren<Transform>( true );
      for( int asdf = 0; asdf < transforms.Length; asdf++ )
      {
        if( transforms[asdf].name.StartsWith( NavMeshBoxName ) || transforms[asdf].name.StartsWith( NavMeshSurfaceName ) )
          destroy.Add( transforms[asdf].gameObject );
      }
    }
    foreach( var ugh in destroy )
      if( ugh != null )
        Util.Destroy( ugh );
  }

  public void GenerateNavSurfaces()
  {
    RemoveNavSurfaces();

    SuperMap[] supermaps = FindObjectsByType<SuperMap>( FindObjectsInactive.Include, FindObjectsSortMode.None );
    for( int i = 0; i < supermaps.Length; ++i )
    {
      SuperMap smap = supermaps[i];
      if( smap.gameObject.layer != LayerMask.NameToLayer( "Default" ) )
        continue;

      GameObject nmb = new GameObject( NavMeshBoxName + smap.name );
      nmb.transform.parent = smap.transform;
      nmb.transform.localPosition = Vector3.back * 0.1f;
      MeshCollider meshCollider = nmb.AddComponent<MeshCollider>();
      meshCollider.convex = true;

      GameObject navSurfaceGO = Instantiate( NavMeshPrefab, smap.transform );
      navSurfaceGO.name = NavMeshSurfaceName;
      NavMeshSurface navMeshSurface = navSurfaceGO.GetComponent<NavMeshSurface>();

      Mesh mesh = new Mesh();
      mesh.indexFormat = IndexFormat.UInt32;
      List<Vector3> verts = new List<Vector3>();
      List<int> indices = new List<int>();
      int idx = 0;

      SuperTileLayer[] stls = smap.GetComponentsInChildren<SuperTileLayer>();
      Tilemap[] tilemaps = new Tilemap[stls.Length];
      for( int j = 0; j < stls.Length; j++ )
        tilemaps[j] = stls[j].GetComponent<Tilemap>();

      Tilemap ground_tilemap = System.Array.Find( tilemaps, x => { return x.gameObject.name == "ground"; } );
      if( ground_tilemap == null )
        ground_tilemap = tilemaps[0];
      for( int y = ground_tilemap.cellBounds.yMin; y < ground_tilemap.cellBounds.yMin + smap.m_Height; y++ )
      {
        for( int x = ground_tilemap.cellBounds.xMin; x < ground_tilemap.cellBounds.xMin + smap.m_Width; x++ )
        {
          Vector3Int pos = new Vector3Int( x, y, 0 );
          bool valid = false;

          for( int t = 0; t < tilemaps.Length; ++t )
          {
            if( tilemaps[t].HasTile( pos ) )
            {
              /*valid = true;
              break;*/
              TileBase tilebase = tilemaps[t].GetTile( pos );
              SuperTile tile = tilebase as SuperTile;
              if( tile != null )
              {
                CustomProperty prop = tile.m_CustomProperties.Find( ugh => ugh.m_Name == "ground" );
                if( prop != null && prop.GetValueAsBool() )
                {
                  valid = true;
                  break;
                }
              }
            }
          }

          if( valid )
          {
            verts.AddRange( new[] { new Vector3( pos.x, pos.y, 0 ), new Vector3( pos.x, pos.y + 1, 0 ), new Vector3( pos.x + 1, pos.y + 1, 0 ), new Vector3( pos.x + 1, pos.y, 0 ) } );
            indices.AddRange( new[] { idx + 0, idx + 1, idx + 3, idx + 1, idx + 2, idx + 3 } );
            idx += 4;
          }
        }
      }
      
      SuperColliderComponent[] sccs = smap.GetComponentsInChildren<SuperColliderComponent>();
      List<CombineInstance> comb = new List<CombineInstance>();
      for( int j = 0; j < sccs.Length; j++ )
      {
        SuperColliderComponent scc = sccs[j];
        if( (1 << scc.gameObject.layer & LayerMask.GetMask( "Default", "pit" )) == 0 )
          continue;
        if( scc.TryGetComponent( out Collider2D cldr ) )
        {
          Mesh tri = cldr.CreateMesh( false, false, true );
          if( tri )
          {
            CombineInstance combineInstance = new CombineInstance();
            Vector3[] v = tri.vertices;
            Vector3 offset = smap.transform.position;
            for( int k = 0; k < v.Length; k++ )
            {
              v[k] -= offset;
            }
            tri.vertices = v;
            combineInstance.mesh = tri;
            /*Matrix4x4 mat = new Matrix4x4();
            mat.SetTRS( -smap.transform.position, Quaternion.identity, Vector3.one );
            combineInstance.transform = mat;*/
            comb.Add( combineInstance );
          }
        }
      }

      Mesh mesh_mod = new Mesh();
      mesh_mod.indexFormat = IndexFormat.UInt32;
      mesh_mod.CombineMeshes( comb.ToArray(), false, false );
      
      GameObject meshmod = Instantiate( NavMeshModifierPrefab, nmb.transform );
      meshmod.name = NavMeshBoxName + "mod";
      meshmod.transform.localPosition = Vector3.back * 0.1f;
      MeshCollider mmcld = meshmod.GetComponent<MeshCollider>();
      mmcld.sharedMesh = mesh_mod;
#if UNITY_EDITOR
      meshmod.AddComponent<MeshFilter>().sharedMesh = mesh_mod;
      meshmod.AddComponent<MeshRenderer>();
#endif

      mesh.SetVertices( verts.ToArray() );
      mesh.SetIndices( indices, MeshTopology.Triangles, 0 );
      meshCollider.sharedMesh = mesh;
      //debug
#if UNITY_EDITOR
      nmb.AddComponent<MeshFilter>().sharedMesh = mesh;
      nmb.AddComponent<MeshRenderer>(); //.enabled = false;
#endif
      navMeshSurface.BuildNavMesh();
      nmb.SetActive( false );
    }
  }


  /*public void GenerateAllLayerNavSurfaces( Vector2 center, Vector2 size )
  {
    for( int i = 0; i < transform.childCount; i++ )
    {
      Transform tns = transform.GetChild( i );
      if( tns.name.StartsWith( NavMeshBoxName ) )
        Util.Destroy( tns.gameObject );
    }
    for( int i = 0; i < MaxLayerCount; i++ )
    {
      GameObject nmb;
      if( layerNavmeshBox.Length <= i || layerNavmeshBox[i] == null )
      {
        nmb = new GameObject( NavMeshBoxName + i );
        layerNavmeshBox[i] = nmb.AddComponent<BoxCollider>();
      }
      else
        nmb = layerNavmeshBox[i].gameObject;
      nmb.transform.position = center + (Vector2.down * Global.GameplayLayerOffset * i);
      nmb.transform.rotation = Quaternion.Euler( -90, 0, 0 );
      nmb.transform.parent = transform;
      layerNavmeshBox[i].size = new Vector3( size.x, 0, size.y );
      layerNavmeshBox[i].transform.position = center + (Vector2.down * Global.GameplayLayerOffset * i);
    }
  }*/

  //#if UNITY_EDITOR

  [ExposeMethod]
  public void GenerateNavSurface()
  {
    GenerateNavSurfaces();
    EditorUtility.SetDirty( this );
  }

  /*[ExposeMethod]
  public void GenerateNavSurface_Size( Vector2 center, Vector2 size )
  {
    GenerateAllLayerNavSurfaces( center, size );
    EditorUtility.SetDirty( this );
  }*/
#endif


  /*[SerializeField] GenerationVariant[] generationVariants;
  [ExposeMethod] void CollectVariants() { generationVariants = Object.FindObjectsOfType<GenerationVariant>(); }*/


#region Boss

#if false
  [Header( "Boss" )]
  public BossBattleConfig[] BossConfig;
  Timer bossintroTimer = new Timer();

  [System.Serializable]
  public struct BossBattleConfig
  {
    public Boss Boss;
    public AudioLoop BossIntroMusic;
    public AudioLoop BattleMusic;
    public float bosswait;
    public bool FadeIn;
    public bool DialogueBeforeBattleEnabled;
    public string DialogueBeforeBattle;
    public UnityEvent OnBattleStart;
    public UnityEvent OnBossDeath;
  }

  public void StartBossIntro( int index )
  {
    BossBattleConfig bc = BossConfig[index];
    // queue the next boss, for gauntlet style battles
    if( BossConfig.Length > index + 1 )
      bc.Boss.EventDeath.AddListener( () => { StartBossIntro( index + 1 ); } );
    else
      bc.Boss.EventDeath.AddListener( () => { Global.instance.MusicCrossFadeTo( music, 3 ); } );

    // start all boss logic disabled
    bc.Boss.enabled = false;
    bc.Boss.gameObject.SetActive( true );

    if( bc.DialogueBeforeBattleEnabled && Global.instance.StartConversation(
      new[] {bc.Boss.GetComponent<ITalker>(), Global.instance.CurrentPlayer.GetComponent<ITalker>()},
      bc.DialogueBeforeBattle,
      new Global.ConversationOptions()
      {
        ShowBars = true,
        CancelIfBeyondRange = false,
        AllowPlayerMovement = false,
        LerpEnabled = true,
        LerpToPosition = bc.Boss.transform.position,
        OnComplete = () => { StartBossBattle( bc ); }
      } ) )
    {
      Global.instance.MusicCrossFadeTo( bc.BossIntroMusic, 1 );
      SpriteRenderer[] srs = bc.Boss.GetComponentsInChildren<SpriteRenderer>();
      if( bc.FadeIn )
      {
        // fade in all renderers
        foreach( var sr in srs )
          sr.color = new Color( 1, 1, 1, 0 );
      }
      TimerParams timerParams = new TimerParams()
      {
        duration = bc.bosswait,
        unscaledTime = true,
        UpdateDelegate = ( timer ) =>
        {
          if( bc.FadeIn )
          {
            foreach( var sr in srs )
              sr.color = new Color( 1, 1, 1, timer.ProgressNormalized );
          }
        },
        CompleteDelegate = () =>
        {
          Global.instance.MusicCrossFadeTo( null, 2 );
          if( bc.FadeIn )
            foreach( var sr in srs )
              sr.color = new Color( 1, 1, 1, 1 );
        }
      };
      bossintroTimer.Start( timerParams );
    }
    else
    {
      ((PlayerBiped) Global.instance.CurrentPlayer).PostConversation();
      StartBossBattle( bc );
    }
  }

  void StartBossBattle( BossBattleConfig bc )
  {
    bc.Boss.EventDeath.AddListener( () => { bc.OnBossDeath.Invoke(); } );
    Global.instance.Controls.BipedActions.Aim.Enable();
    Global.instance.Controls.BipedActions.NextAbility.Enable();
    Global.instance.Controls.BipedActions.NextWeapon.Enable();
    bossintroTimer.Start( 2, null, () =>
    {
      Global.instance.Controls.BipedActions.Enable();
      bc.Boss.enabled = true;
      Global.instance.PlayMusic( bc.BattleMusic );
      bc.OnBattleStart.Invoke();
    } );
  }
#endif

#endregion

  public void PlayMusic( AudioLoop audio ) { Global.instance.MusicCrossFadeTo( audio, Global.instance.MusicTransitionDuration ); }
  public void FadeOutMusic( float duration ) { Global.instance.MusicCrossFadeTo( null, duration ); }

  public void TurnOffDynamicSpawn()
  {
    // this function is for scene trigger convenience
    DynamicSpawnEnabled = false;
  }

  // used by Unity Events serialized into scenes.
  public void SetProgressFlag( string flag ) { Global.instance.SetProgressFlag( flag ); }
}