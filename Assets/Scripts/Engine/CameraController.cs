﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class CameraController : MonoBehaviour
{
  public Camera cam;
  public Controller LookTarget;

  public bool UseVerticalRange = true;
  [FormerlySerializedAs( "yHalfWidth" )]
  public float VerticalRangeYHalfWidth = 2;
  public float VerticalRangeYOffset = 0;
  public bool UseSpeedOffset;
  public float SpeedOffset = 5;
  public float SpeedMax = 10;
  public float SpeedOffsetLerp = 0.5f;
  public bool CursorInfluence;
  public float cursorAlpha = 0.5f;
  /*public float lerpAlpha = 50;*/
  public float zOffset;
  public float orthoTarget = 1;
  public float orthoSpeed = 1;
  [SerializeField] bool Snap;
  [SerializeField] int SnapPPU = 16;

  [Header( "Camera Zones" )]
  [SerializeField] CameraZone ActiveCameraZone;
  // If the typical behavior is overriden by another camera zone.
  public bool CameraZoneOverride;

  public bool ZoneTransitionFlag;
  public float ZoneTransitionDuration = 5;
  Timer ZoneTransitionTimer = new Timer();

  /*public Vector2 CameraPosition { get { return cameraPosition; } }*/
  public Vector2 cameraPosition;
  Vector2 aimOffset;
  Timer lerpTimer = new Timer();

  public float PerspectiveAngleLimit = 30;
  bool perspectiveExperiment;

  public bool PerspectiveExperiment
  {
    get { return perspectiveExperiment; }
    set
    {
      perspectiveExperiment = value;
      cam.orthographic = !value;
      cam.transform.rotation = Quaternion.identity;
    }
  }

  public void PreSceneTransition() { }
  public void PostSceneTransition() { }

  public void AssignOverrideCameraZone( CameraZone zone )
  {
    ActiveCameraZone = zone;
    CameraZoneOverride = !zone;
  }

  public CameraZone GetActiveZone() { return ActiveCameraZone; }

  // Use camera target values
  public void LerpAuto( Vector3 targetPosition, float duration, System.Action whenDone = null )
  {
    Vector2 pos = targetPosition;
    float ortho = cam.orthographicSize;
    GetCameraTargetValues( ref pos, ref ortho, targetPosition );
    Lerp( pos, ortho, duration, whenDone );
  }

  // set a target for ortho
  public void LerpAdjustTarget( Vector3 targetPosition, float targetOrtho, float duration, System.Action whenDone = null )
  {
    Vector2 pos = targetPosition;
    float ortho = targetOrtho;
    GetCameraTargetValues( ref pos, ref ortho, targetPosition );
    Lerp( pos, ortho, duration, whenDone );
  }

  public void Lerp( Vector3 targetPosition, float targetOrtho, float duration, System.Action whenDone = null )
  {
    float startOrtho = cam.orthographicSize;
    Vector2 startPos = transform.position;

    TimerParams tp = new TimerParams()
    {
      unscaledTime = false,
      duration = duration,
      CompleteDelegate = () =>
      {
        cameraPosition = targetPosition;
        transform.position = targetPosition;
        whenDone?.Invoke();
      },
      UpdateDelegate = ( Timer timer ) =>
      {
        Vector3 lerp = Vector3.Lerp( startPos, targetPosition, timer.ProgressNormalized );
        lerp.z = zOffset;
        cameraPosition = lerp;
        transform.position = lerp;
        cam.orthographicSize = Mathf.Lerp( startOrtho, targetOrtho, timer.ProgressNormalized );
      }
    };
    lerpTimer.Start( tp );
  }

  public void StopLerp( bool callOnComplete ) { lerpTimer.Stop( callOnComplete ); }

  public void TeleportToPosition( Vector2 pos )
  {
    Vector2 tpos = pos;
    float ortho = cam.orthographicSize;
    GetCameraTargetValues( ref tpos, ref ortho, pos );
    cam.orthographicSize = ortho;
    cameraPosition = new Vector3( tpos.x, tpos.y, zOffset );
    transform.position = cameraPosition;
  }

  public void TeleportToLookTarget( bool UseCameraZones = true )
  {
    if( LookTarget != null && LookTarget.pawn != null )
    {
      cameraPosition = LookTarget.pawn.transform.position;
      float ortho = cam.orthographicSize;
      if( UseCameraZones )
        GetCameraTargetValuesPawn( ref cameraPosition, ref ortho, cameraPosition );
      cam.orthographicSize = ortho;
    }
    else
    {
      cameraPosition = Vector3.zero;
    }
    transform.position = new Vector3( cameraPosition.x, cameraPosition.y, zOffset );
  }

  float speedOffset = 0;

  public void CameraLateUpdate()
  {
    if( !enabled || lerpTimer.IsActive )
      return;

    float DT = Mathf.Min( 0.015f, Time.unscaledDeltaTime );

    if( PerspectiveExperiment && LookTarget == Global.instance.PlayerController )
    {
      // TODO limit rotation or distance for perspective cameras
      // take desired rotation and project four points onto gameplay plane
      // clip points to bounds
      // IF new bounds is larger than desired then: derive focal point and rotation from clipped bounds
      // ELSE set rotation to zero and calculate Z distance

      Vector3 lookTarget = LookTarget.pawn.transform.position;
      if( CursorInfluence )
      {
        /*aimOffset = Vector2.Lerp( aimOffset, LookTarget.pawn.inputAim * cursorAlpha, lerpAlpha * Time.unscaledDeltaTime );*/
        aimOffset = LookTarget.pawn.inputAim;
        lookTarget = (Vector2)LookTarget.pawn.transform.position + LookTarget.pawn.inputAim * cursorAlpha;
      }
      /*float ignored = 0;
      GetCameraTargetValuesPawn( ref cameraPosition, ref ignored, lookTarget );*/
      lookTarget.z = zOffset;
      transform.position = lookTarget;

      Vector3 angles = Quaternion.LookRotation( (LookTarget.pawn.transform.position + (Vector3)aimOffset - transform.position).normalized, Vector3.up ).eulerAngles;
      angles.x = Mathf.Clamp( Util.NormalizeAngle( angles.x ), -PerspectiveAngleLimit, PerspectiveAngleLimit );
      angles.y = Mathf.Clamp( Util.NormalizeAngle( angles.y ), -PerspectiveAngleLimit, PerspectiveAngleLimit );
      cam.transform.rotation = Quaternion.Euler( angles );
    }
    else
    {
      Vector2 pos = cameraPosition;
      float ortho = orthoTarget;

      if( LookTarget != null && LookTarget.pawn != null )
      {
        if( UseSpeedOffset )
        {
          if( LookTarget.pawn.velocity.x <= 0 )
            speedOffset = Mathf.Lerp( speedOffset, 0, SpeedOffsetLerp * Time.deltaTime );
          else
            speedOffset = SpeedOffset * (Mathf.Min( SpeedMax, LookTarget.pawn.velocity.x ) / SpeedMax);
          pos.x = LookTarget.pawn.transform.position.x + speedOffset;
        }

        if( LookTarget.pawn.IgnoreCameraZones )
          pos = LookTarget.pawn.transform.position;
        else
        {
          /*if( CursorInfluence )
          {
            /*aimOffset = Vector2.Lerp( aimOffset, pawn.inputAim * cursorAlpha, lerpAlpha * Time.unscaledDeltaTime );#1#
            aimOffset = pawn.inputAim;
            lookTarget = (Vector2)pawn.transform.position + pawn.inputAim * cursorAlpha;
          }*/
          GetCameraTargetValuesPawn( ref pos, ref ortho, LookTarget.pawn.transform.position );
        }
      }
      else if( CameraZoneOverride )
      {
        // no look target
        GetCameraTargetValues( ref pos, ref ortho, pos );
      }

      if( ZoneTransitionFlag )
      {
        ZoneTransitionFlag = false;
        ZoneTransitionTimer.Stop( false );
        ZoneTransitionTimer.Start( ZoneTransitionDuration );
      }

      if( ZoneTransitionTimer.IsActive )
      {
        if( LookTarget != null && LookTarget.pawn != null )
        {
          /*cameraPosition.y = Vector2.Lerp( cameraPosition, pos, ZoneTransitionTimer.ProgressNormalized ).y;
          cameraPosition.x = pos.x;*/

          cameraPosition = Vector2.Lerp( cameraPosition, pos, ZoneTransitionTimer.ProgressNormalized );
          if( ActiveCameraZone )
          {
            if( !ActiveCameraZone.AffectY )
              cameraPosition.y = pos.y;
            if( !ActiveCameraZone.AffectX )
              cameraPosition.x = pos.x;
          }
        }
        else
          ZoneTransitionTimer.Stop( false );
      }
      else
      {
        cameraPosition = pos;
      }

      cam.orthographicSize = Mathf.Lerp( cam.orthographicSize, ortho, orthoSpeed * DT );

      Vector3 convert;
      if( Snap )
        convert = Util.Snap( cameraPosition, SnapPPU );
      else
        convert = cameraPosition;
      convert.z = zOffset;
      transform.position = convert;
    }
  }

  void GetCameraTargetValuesPawn( ref Vector2 pos, ref float ortho, Vector2 targetPos )
  {
    Vector2 lookTarget = targetPos;
    /*pos.x = lookTarget.x;*/
    if( UseVerticalRange )
    {
      if( lookTarget.y + VerticalRangeYOffset > pos.y + VerticalRangeYHalfWidth )
        pos.y = lookTarget.y + VerticalRangeYOffset - VerticalRangeYHalfWidth;
      if( lookTarget.y + VerticalRangeYOffset < pos.y - VerticalRangeYHalfWidth )
        pos.y = lookTarget.y + VerticalRangeYOffset + VerticalRangeYHalfWidth;
    }
    else
    {
      pos.y = lookTarget.y;
    }
    GetCameraTargetValues( ref pos, ref ortho, targetPos );
  }

  CameraZone GetZone( Vector2 pos )
  {
    CameraZone zone = null;
    CameraZone.DoesOverlapAnyZone( pos, ref zone );
    return zone;
  }


  void GetCameraTargetValues( ref Vector2 pos, ref float ortho, Vector2 origin )
  {
    // auto-switch to zone the player enters (respects the ignore flag on the zone)
    if( !CameraZoneOverride )
    {
      CameraZone zone = null;
      CameraZone.DoesOverlapAnyZone( origin, ref zone );
      if( ActiveCameraZone != zone )
        ZoneTransitionFlag = true;
      ActiveCameraZone = zone;
    }

    if( ActiveCameraZone != null )
    {
      Vector2 debug = pos;
      float hh, hw, xangle, yangle;

      if( ActiveCameraZone.SetOrtho )
        ortho = ActiveCameraZone.orthoTarget;

      if( cam.orthographic )
      {
        hh = ortho;
        hw = ortho * cam.aspect;
      }
      else
      {
        // draw gray lines showing camera rectangle
        yangle = Mathf.Deg2Rad * cam.fieldOfView * 0.5f;
        hh = Mathf.Tan( yangle ) * -transform.position.z;
        xangle = yangle * ((float)cam.pixelWidth / (float)cam.pixelHeight);
        hw = Mathf.Tan( xangle ) * -transform.position.z;
      }

      if( ActiveCameraZone.EncompassBounds )
      {
        Bounds bounds = new Bounds();
        Vector2 center = Vector2.zero;
        for( int i = 0; i < ActiveCameraZone.colliders.Length; i++ )
          center += (Vector2)ActiveCameraZone.colliders[i].transform.position + ActiveCameraZone.colliders[i].offset;
        center /= ActiveCameraZone.colliders.Length;
        bounds.center = center;
        for( int i = 0; i < ActiveCameraZone.colliders.Length; i++ )
          bounds.Encapsulate( ActiveCameraZone.colliders[i].bounds );

        pos.x = bounds.center.x;
        pos.y = bounds.center.y;

        if( cam.orthographic )
        {
          if( bounds.extents.y > bounds.extents.x )
            ortho = bounds.extents.y;
          else
            ortho = bounds.extents.x / cam.aspect;
        }
        else
        {
          if( bounds.extents.y > bounds.extents.x )
            ortho = bounds.extents.y;
          else
            ortho = bounds.extents.x / cam.aspect;
          //outPos.z = -Mathf.Max( bounds.extents.x / Mathf.Tan( 0.5f * cam.fieldOfView ), bounds.extents.y / Mathf.Tan( 0.5f * cam.fieldOfView / cam.aspect ) );
        }
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        Popcron.Gizmos.Square( bounds.center, bounds.size, Color.yellow );
#endif
      }
      else
      {
        if( CameraZoneOverride )
        {
          // if the origin(lookTarget) is outside of all the colliders, then clip to inside.
          Collider2D overlap = null;
          List<Vector2> points = new List<Vector2>();
          foreach( var cld in ActiveCameraZone.colliders )
          {
            if( cld.OverlapPoint( origin ) )
            {
              overlap = cld;
              break;
            }
            else
            {
              points.Add( cld.ClosestPoint( origin ) );
            }
          }
          if( overlap == null )
          {
            Vector2 neworigin = Util.FindClosest( origin, points.ToArray() );
            origin = neworigin + (neworigin - origin).normalized;
          }
        }

        if( ActiveCameraZone.FollowEdges )
        {
          List<Vector2> pots = new List<Vector2>();
          foreach( var cld in ActiveCameraZone.colliders )
          {
            if( cld is EdgeCollider2D )
              pots.Add( (cld as EdgeCollider2D).ClosestPoint( pos ) );
          }
          pos = Util.FindClosest( pos, pots.ToArray() );
        }
        else if( ActiveCameraZone.UsePolygonUnion )
        {
#region POLYGON_UNION

          // prevent multiple colliders within same zone from overriding clip points of one another by using a union poly.

          Vector2[] poly = ActiveCameraZone.union;
          Vector2 UL = (Vector2)pos + Vector2.left * hw + Vector2.up * hh;
          if( ClipToInsidePolygon( poly, ref UL, origin ) )
          {
            if( pos.y > UL.y - hh )
              pos.y = UL.y - hh;
            if( pos.x < UL.x + hw )
              pos.x = UL.x + hw;
          }

          Vector2 UR = (Vector2)pos + Vector2.right * hw + Vector2.up * hh;
          if( ClipToInsidePolygon( poly, ref UR, origin ) )
          {
            if( pos.y > UR.y - hh )
              pos.y = UR.y - hh;
            if( pos.x > UR.x - hw )
              pos.x = UR.x - hw;
          }

          Vector2 LL = (Vector2)pos + Vector2.left * hw + Vector2.down * hh;
          if( ClipToInsidePolygon( poly, ref LL, origin ) )
          {
            if( pos.y < LL.y + hh )
              pos.y = LL.y + hh;
            if( pos.x < LL.x + hw )
              pos.x = LL.x + hw;
          }

          Vector2 LR = (Vector2)pos + Vector2.right * hw + Vector2.down * hh;
          if( ClipToInsidePolygon( poly, ref LR, origin ) )
          {
            if( pos.y < LR.y + hh )
              pos.y = LR.y + hh;
            if( pos.x > LR.x - hw )
              pos.x = LR.x - hw;
          }

#endregion
        }
        else
        {
          foreach( var cld in ActiveCameraZone.colliders )
          {
            if( !CameraZoneOverride && !cld.OverlapPoint( origin ) )
              continue;
            Vector2 UL = (Vector2)pos + Vector2.left * hw + Vector2.up * hh;
            if( ClipToInsideCollider2D( cld, ref UL, origin ) )
            {
              if( ActiveCameraZone.AffectY && pos.y > UL.y - hh )
                pos.y = UL.y - hh;
              if( ActiveCameraZone.AffectX && pos.x < UL.x + hw )
                pos.x = UL.x + hw;
            }

            Vector2 UR = (Vector2)pos + Vector2.right * hw + Vector2.up * hh;
            if( ClipToInsideCollider2D( cld, ref UR, origin ) )
            {
              if( ActiveCameraZone.AffectY && pos.y > UR.y - hh )
                pos.y = UR.y - hh;
              if( ActiveCameraZone.AffectX && pos.x > UR.x - hw )
                pos.x = UR.x - hw;
            }

            Vector2 LL = (Vector2)pos + Vector2.left * hw + Vector2.down * hh;
            if( ClipToInsideCollider2D( cld, ref LL, origin ) )
            {
              if( ActiveCameraZone.AffectY && pos.y < LL.y + hh )
                pos.y = LL.y + hh;
              if( ActiveCameraZone.AffectX && pos.x < LL.x + hw )
                pos.x = LL.x + hw;
            }

            Vector2 LR = (Vector2)pos + Vector2.right * hw + Vector2.down * hh;
            if( ClipToInsideCollider2D( cld, ref LR, origin ) )
            {
              if( ActiveCameraZone.AffectY && pos.y < LR.y + hh )
                pos.y = LR.y + hh;
              if( ActiveCameraZone.AffectX && pos.x > LR.x - hw )
                pos.x = LR.x - hw;
            }
          }
        }

#if UNITY_EDITOR || DEVELOPMENT_BUILD
        Popcron.Gizmos.Square( pos, new Vector3( hw * 2, hh * 2, 0 ), Color.green );
        Popcron.Gizmos.Square( debug, new Vector3( hw * 2, hh * 2, 0 ), Color.green );
        if( cam.orthographic )
        {
          Bounds bounds = new Bounds( transform.position, new Vector3( cam.orthographicSize, cam.orthographicSize * cam.aspect, 0 ) );
          Popcron.Gizmos.Square( bounds.center, bounds.size, Color.green );
        }
#endif
      }
    }
  }

  bool ClipToInsideCollider2D( Collider2D cld, ref Vector2 cp, Vector2 origin )
  {
    if( !cld.OverlapPoint( cp ) )
    {
      Vector2[] points = null;
      if( cld is PolygonCollider2D )
        points = (cld as PolygonCollider2D).points;
      else if( cld is BoxCollider2D )
      {
        BoxCollider2D box = cld as BoxCollider2D;
        points = new Vector2[4];
        points[0] = box.offset + (Vector2.left * box.size.x * 0.5f) + (Vector2.down * box.size.y * 0.5f);
        points[1] = box.offset + (Vector2.right * box.size.x * 0.5f) + (Vector2.down * box.size.y * 0.5f);
        points[2] = box.offset + (Vector2.right * box.size.x * 0.5f) + (Vector2.up * box.size.y * 0.5f);
        points[3] = box.offset + (Vector2.left * box.size.x * 0.5f) + (Vector2.up * box.size.y * 0.5f);
      }
      List<Vector2> pots = new List<Vector2>();
      // add transform position for world space
      for( int i = 0; i < points.Length; i++ )
        points[i] += (Vector2)cld.transform.position;
      for( int i = 0; i < points.Length; i++ )
      {
        int next = (i + 1) % points.Length;
        /*Vector2 intersection = cp;
        if( Util.LineSegmentsIntersectionWithPrecisonControl( points[i], points[next], origin, cp, ref intersection ) )
        {
          pots.Add( intersection );
          #if UNITY_EDITOR || DEVELOPMENT_BUILD
          Popcron.Gizmos.Line( origin, intersection, Color.red );
          #endif
        }*/
        Vector2 segment = points[next] - points[i];
        if( !Util.DoLinesIntersect( points[i].x, points[i].y, points[next].x, points[next].y, origin.x, origin.y, cp.x, cp.y ) )
          continue;
        Vector2 perp = new Vector2( -segment.y, segment.x );
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        Popcron.Gizmos.Line( points[i], points[i] + perp, Color.blue );
#endif
        Vector2 projectionPerp = Util.Project2D( (cp - points[i]), perp.normalized );
        if( Vector2.Dot( perp.normalized, projectionPerp.normalized ) < 0 )
        {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
          Popcron.Gizmos.Line( points[i], points[i] + projectionPerp, Color.red );
#endif
          Vector2 adjust = cp - projectionPerp;
          if( Vector2.Dot( segment.normalized, (adjust - points[i]).normalized ) > 0 )
          {
            if( (adjust - points[i]).magnitude > segment.magnitude )
              adjust = points[next];
          }
          else
            adjust = points[i];
          pots.Add( adjust );
        }
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        else
          Popcron.Gizmos.Line( points[i], points[i] + projectionPerp, Color.magenta );
#endif
      }

      float distance = Mathf.Infinity;
      Vector2 closest = cp;
      foreach( var p in pots )
      {
        float dist = Vector2.Distance( p, origin );
        if( dist < distance )
        {
          closest = p;
          distance = dist;
        }
      }
      cp = closest;

      return true;
    }
    return false;
  }

  bool ClipToInsidePolygon( Vector2[] poly, ref Vector2 cp, Vector2 origin )
  {
    if( !Util.IsPointInPolygon( poly, cp ) )
    {
      Vector2[] points = poly;
      List<Vector2> pots = new List<Vector2>();
      for( int i = 0; i < points.Length; i++ )
      {
        int next = (i + 1) % points.Length;
        Vector2 segment = points[next] - points[i];
        if( !Util.DoLinesIntersect( points[i].x, points[i].y, points[next].x, points[next].y, origin.x, origin.y, cp.x, cp.y ) )
          continue;
        Vector2 perp = new Vector2( -segment.y, segment.x );
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        Popcron.Gizmos.Line( points[i], points[i] + perp, Color.blue );
#endif
        Vector2 projectionPerp = Util.Project2D( (cp - points[i]), perp.normalized );
        if( Vector2.Dot( perp.normalized, projectionPerp.normalized ) < 0 )
        {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
          Popcron.Gizmos.Line( points[i], points[i] + projectionPerp, Color.red );
#endif
          Vector2 adjust = cp - projectionPerp;
          if( Vector2.Dot( segment.normalized, (adjust - points[i]).normalized ) > 0 )
          {
            if( (adjust - points[i]).magnitude > segment.magnitude )
              adjust = points[next];
          }
          else
            adjust = points[i];
          pots.Add( adjust );
        }
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        else
          Popcron.Gizmos.Line( points[i], points[i] + projectionPerp, Color.magenta );
#endif
      }

      float distance = Mathf.Infinity;
      Vector2 closest = cp;
      foreach( var p in pots )
      {
        float dist = Vector2.Distance( p, origin );
        if( dist < distance )
        {
          closest = p;
          distance = dist;
        }
      }
      cp = closest;

      return true;
    }
    return false;
  }
}