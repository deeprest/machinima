﻿#if UNITY_EDITOR

using System.Collections.Generic;
using System.IO;
using SuperTiled2Unity;
using TMPro;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEngine.AI;
using UnityEditor.Build.Reporting;
using UnityEditor.SceneManagement;
using UnityEditor.U2D.Aseprite;
using UnityEngine.Tilemaps;
using Object = UnityEngine.Object;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

public static class StaticUtility
{
  [MenuItem( "Assets/ShowGUID", true )]
  static bool CanExecute() { return Selection.assetGUIDs.Length > 0; }

  [MenuItem( "Assets/ShowGUID" )]
  static void Execute()
  {
    for( int i = 0; i < Selection.objects.Length; i++ )
      Debug.Log( Selection.objects[i].GetType().ToString() + " " + Selection.objects[i].name + " " + Selection.assetGUIDs[i] );
  }

  /* Util.cs
  public static string GetCurrentAssetDirectory()
  {
    foreach( var obj in UnityEditor.Selection.GetFiltered<Object>( UnityEditor.SelectionMode.Assets ) )
    {
      var path = UnityEditor.AssetDatabase.GetAssetPath( obj );
      if( string.IsNullOrEmpty( path ) )
        continue;

      if( System.IO.Directory.Exists( path ) )
        return path;
      else if( System.IO.File.Exists( path ) )
        return System.IO.Path.GetDirectoryName( path );
    }

    return "Assets";
  }

  public static Object[] GetAssetsFromSelectedFolder( string search = "t:prefab" )
  {
    List<Object> objects = new List<Object>();
    string[] guids = AssetDatabase.FindAssets( search, new string[] { GetCurrentAssetDirectory() } );
    foreach( string guid in guids )
      objects.Add( AssetDatabase.LoadAssetAtPath<Object>( AssetDatabase.GUIDToAssetPath( guid ) ) );
    return objects.ToArray();
  }
  */

  public static void AutoFixSprite( SpriteRenderer spr )
  {
    SuperTag superTag = spr.GetComponent<SuperTag>();
    BoxCollider2D BoxCollider2D = spr.GetComponent<BoxCollider2D>();

    Vector2 pivot = new Vector2( spr.sprite.pivot.x / spr.sprite.rect.width, spr.sprite.pivot.y / spr.sprite.rect.height );

    if( !superTag || !superTag.IgnoreBoxCollider2D )
    {
      if( BoxCollider2D != null )
      {
        if( spr.gameObject.layer == LayerMask.NameToLayer( "twowayPlatform" ) )
        {
          BoxCollider2D.autoTiling = false;
          const float onewayPlatformThickness = 0.01f;
          BoxCollider2D.size = new Vector2( spr.size.x, onewayPlatformThickness );
          BoxCollider2D.offset = new Vector2( (0.5f - pivot.x) * BoxCollider2D.size.x, (0.5f - pivot.y) * BoxCollider2D.size.y );
        }
        else if( ((1 << spr.gameObject.layer) & LayerMask.GetMask( "entity", "trigger" )) == 0 )
        {
          Vector2 autoTileSize = new Vector2( spr.sprite.textureRect.width / spr.sprite.pixelsPerUnit, spr.sprite.textureRect.height / spr.sprite.pixelsPerUnit );

          BoxCollider2D.size = BoxCollider2D.autoTiling ? autoTileSize : spr.size;
          BoxCollider2D.offset = new Vector2( (0.5f - pivot.x) * BoxCollider2D.size.x, (0.5f - pivot.y) * BoxCollider2D.size.y );
        }
      }
    }

    NavMeshObstacle NavMeshObstacle = spr.GetComponent<NavMeshObstacle>();
    if( NavMeshObstacle != null )
    {
      //NavMeshObstacle.carving = true;
      NavMeshObstacle.size = new Vector3( spr.size.x, spr.size.y, 1f );
      NavMeshObstacle.center = new Vector2( (0.5f - pivot.x) * NavMeshObstacle.size.x, (0.5f - pivot.y) * NavMeshObstacle.size.y );
    }

    /*ShadowCaster2D shadowCaster = spr.GetComponent<ShadowCaster2D>();
    if( BoxCollider2D && shadowCaster )
    {
      Vector3 relOffset = BoxCollider2D.offset;
      Vector2 shadowBox = BoxCollider2D.size;
      if( BoxCollider2D.autoTiling )
        shadowBox = spr.size;

      Vector3[] m_ShapePath = new Vector3[]
      {
        relOffset + new Vector3( -shadowBox.x * 0.5f, -shadowBox.y * 0.5f ),
        relOffset + new Vector3( shadowBox.x * 0.5f, -shadowBox.y * 0.5f ),
        relOffset + new Vector3( shadowBox.x * 0.5f, shadowBox.y * 0.5f ),
        relOffset + new Vector3( -shadowBox.x * 0.5f, shadowBox.y * 0.5f )
      };

      System.Reflection.FieldInfo meshFI = shadowCaster.GetType().GetField( "m_ShapePath", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic );
      if( meshFI != null )
        meshFI.SetValue( shadowCaster, m_ShapePath );
      System.Reflection.FieldInfo meshHashFI = shadowCaster.GetType().GetField( "m_ShapePathHash", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic );
      if( meshHashFI != null )
        meshHashFI.SetValue( shadowCaster, Random.Range( 0, int.MaxValue ) );
    }*/
  }
}


public class CustomUtility : EditorWindow
{
  // This will make sure there is always a GLOBAL object when playing a scene in the editor
  [RuntimeInitializeOnLoadMethod]
  static void OnLoadMethod()
  {
    if( EditorPrefs.GetBool( "CreateGlobalOnStart" ) )
    {
      for( int i = 0; i < SceneManager.sceneCount; i++ )
      {
        Scene scene = SceneManager.GetSceneAt( i );
        if( scene.name == "GLOBAL" )
          return;
      }
      GameObject go = GameObject.Instantiate( Resources.Load<GameObject>( "GLOBAL" ) );
      if( EditorPrefs.GetBool( "SimulatePlayerOnStart" ) )
        go.GetComponent<Global>().SimulatePlayer = true;
    }
  }

  [MenuItem( "Tools/Custom Utility", false, 2 )]
  static void Init()
  {
    // Get existing open window or if none, make a new one:
    CustomUtility window = (CustomUtility)EditorWindow.GetWindow( typeof(CustomUtility) );
    window.Show();
  }

  void OnEnable() { ReadPrefs(); }

  void ReadPrefs()
  {
    if( EditorPrefs.HasKey( "BuildConfig" ) )
    {
      string guid = EditorPrefs.GetString( "BuildConfig" );
      string path = AssetDatabase.GUIDToAssetPath( guid );
      BuildConfig = (BuildConfig)AssetDatabase.LoadAssetAtPath( path, typeof(BuildConfig) );
    }
    if( EditorPrefs.HasKey( "BuildMacOS" ) )
      buildMacOS = EditorPrefs.GetBool( "BuildMacOS" );
    if( EditorPrefs.HasKey( "BuildLinux" ) )
      buildLinux = EditorPrefs.GetBool( "BuildLinux" );
    if( EditorPrefs.HasKey( "BuildWebGL" ) )
      buildWebGL = EditorPrefs.GetBool( "BuildWebGL" );
    if( EditorPrefs.HasKey( "BuildWindows" ) )
      buildWindows = EditorPrefs.GetBool( "BuildWindows" );
  }

  static BuildConfig BuildConfig;
  static bool buildMacOS;
  static bool buildLinux;
  static bool buildWebGL;
  static bool buildWindows;

  string progressMessage = "";
  float progress;
  bool processing;
  System.Action ProgressUpdate;
  System.Action ProgressDone;
  int index;
  int length;

  // AUDIO
  int audioDoppler;
  int audioDistanceMin = 1;
  int audioDistanceMax = 30;
  AudioRolloffMode audioRolloff = AudioRolloffMode.Logarithmic;


  List<GameObject> gos;
  /*List<Object> objects;*/
  /*List<Light2D> lights;*/
  List<string> assetPaths;
  int layer;

  GameObject replacementPrefab;
  bool ProcessChildObjects;

  bool ReplaceTextWithTMP;
  System.Type ReplaceComponentType;
  string FilterComponentName = "";
  TMP_FontAsset rcFont;
  int rcSize;
  string rcText;
  
  bool ReplacePrefab;
  bool ReplaceSprite;
  bool ReplaceMaterial;
  bool ChangeSortingLayer;
  /*int AssignUnknownSortingLayer;
  int AssignUnknownSortingLayerSelection;*/
  bool ModifyAudioSourceSettings;
  int FromLayerSelection;
  int FromLayer;
  int ToLayerSelection;
  int ToLayer;
  int AddToOrder;
  bool AutoFixSprite;
  bool ModifyTransforms;
  float AddTransformRot;
  Vector2 AddTransformPos;

  enum OperationContext
  {
    SELECTED = 0,
    SCENE,
    PROJECT
  }

  OperationContext operationContext;

  string FilterObjectName = "";
  string FilterSubObjectName = "";
  Sprite replacementSprite;
  Material replacementMaterial;
  string[] guids;

  // font output
  Texture2D fontImage;

  // SpriteLibrary
  string category;
  UnityEngine.U2D.Animation.SpriteLibraryAsset sla;
  /*Sprite[] sprites;*/

  Vector2 scrollPosition = Vector2.zero;
  bool foldTodo = true;
  bool foldSettings;
  bool foldBuild;
  bool foldGeneric;
  bool foldFont;
  bool foldMultiTool;
  bool foldSprite;
  int fontAdvance = 16;

  // Tilemap
  bool foldTilemap;
  Texture2D tilemap;
  Texture2D palette;
  Tilemap tilemapInScene;
  Texture2D tileset;
  Dictionary<string, int> paletteIndex;
  Vector2Int tileSize = new Vector2Int( 16, 16 );

  void StartJob( string message, int count, System.Action update, System.Action done )
  {
    progressMessage = message;
    processing = true;
    index = 0;
    progress = 0;
    length = count;
    ProgressUpdate = update;
    ProgressDone = done;
  }

  void OnGUI()
  {
    if( !Application.isPlaying && EditorPrefs.GetBool( "ConstantEditorRepaint" ) )
    {
      EditorApplication.QueuePlayerLoopUpdate();
      SceneView.RepaintAll();
      Repaint();
    }

    if( processing )
    {
      if( index == length )
      {
        processing = false;
        progress = 1;
        ProgressUpdate = null;
        if( ProgressDone != null )
          ProgressDone();
        progressMessage = "Done";
      }
      else
      {
        ProgressUpdate();
        progress = (float)index++ / (float)length;
      }
      // this call forces OnGUI to be called repeatedly instead of on-demand
      Repaint();
    }

    /*
    if( GUI.Button( EditorGUILayout.GetControlRect( false, 30 ), "NEW ACTION" ) )
    {
      int count = 0;
      if( Selection.objects.Length > 0 )
      {
        objects = new List<Object>( Selection.objects );
        count = objects.Count;
      }
      if( count > 0 )
      {
        StartJob( "Searching...", count, delegate ()
        {

            //objects[index]

        }, null );
      }
    }
    */

    EditorGUILayout.Space();
    EditorGUI.ProgressBar( EditorGUILayout.GetControlRect( false, 30 ), progress, progressMessage );

    scrollPosition = EditorGUILayout.BeginScrollView( scrollPosition, false, true );
    foldSettings = EditorGUILayout.Foldout( foldSettings, "Settings" );
    if( foldSettings )
    {
      EditorPrefs.SetBool( "ConstantEditorRepaint", EditorGUILayout.Toggle( "ConstantEditorRepaint", EditorPrefs.GetBool( "ConstantEditorRepaint", true ) ) );
      EditorPrefs.SetBool( "CreateGlobalOnStart", EditorGUILayout.Toggle( "CreatGlobalOnStart", EditorPrefs.GetBool( "CreateGlobalOnStart", true ) ) );
      EditorPrefs.SetBool( "SimulatePlayerOnStart", EditorGUILayout.Toggle( "SimulatePlayerOnStart", EditorPrefs.GetBool( "SimulatePlayerOnStart", false ) ) );
      Popcron.Gizmos.Enabled = EditorGUILayout.Toggle( "Gizmos.Enabled", Popcron.Gizmos.Enabled );
    }
    EditorGUILayout.Space();
    foldBuild = EditorGUILayout.Foldout( foldBuild, "Build" );
    if( foldBuild )
    {
      /*if( GUI.Button( EditorGUILayout.GetControlRect( false, 30 ), "Run WebGL Build" ) )
      {
        string path = EditorUtility.OpenFolderPanel( "Select WebGL build folder", "", "" );
        if( path.Length != 0 )
        {
          Debug.Log( path );
          Util.Execute( new Util.Command { cmd = "exec /usr/local/bin/static", args = "", dir = path }, true );
        }
      }*/
      EditorGUILayout.BeginHorizontal();
      buildMacOS = EditorGUILayout.ToggleLeft( "MacOS", buildMacOS, GUILayout.MaxWidth( 60 ) );
      buildLinux = EditorGUILayout.ToggleLeft( "Linux", buildLinux, GUILayout.MaxWidth( 60 ) );
      buildWindows = EditorGUILayout.ToggleLeft( "Windows", buildWindows, GUILayout.MaxWidth( 60 ) );
      buildWebGL = EditorGUILayout.ToggleLeft( "WebGL", buildWebGL, GUILayout.MaxWidth( 60 ) );
      EditorGUILayout.EndHorizontal();
      /*buildAutoSceneRefs = EditorGUILayout.Toggle( "Auto Add Scene Refs", buildAutoSceneRefs );*/
      BuildConfig buildConfig = (BuildConfig)EditorGUILayout.ObjectField( "Build Scenes", BuildConfig, typeof(BuildConfig), false );
      if( buildConfig != BuildConfig )
      {
        EditorPrefs.SetString( "BuildConfig", AssetDatabase.AssetPathToGUID( AssetDatabase.GetAssetPath( buildConfig ) ) );
        BuildConfig = buildConfig;
      }
      EditorGUILayout.BeginHorizontal();
      if( GUI.Button( EditorGUILayout.GetControlRect( false, 30 ), "Build Dev" ) )
      {
        if( BuildPipeline.isBuildingPlayer )
          return;
        BuildPlayerOptions bpo = new BuildPlayerOptions();
        bpo.options |= BuildOptions.Development;
        bpo.options |= BuildOptions.AllowDebugging;
        bpo.options |= BuildOptions.ConnectWithProfiler;
        Build( bpo );
      }
      if( GUI.Button( EditorGUILayout.GetControlRect( false, 30 ), "Build Release" ) )
      {
        if( BuildPipeline.isBuildingPlayer )
          return;
        BuildPlayerOptions bpo = new BuildPlayerOptions();
        bpo.options |= BuildOptions.CompressWithLz4;
        Build( bpo );
      }
      EditorGUILayout.EndHorizontal();
    }

    GUILayout.Space( 10 );
    foldGeneric = EditorGUILayout.Foldout( foldGeneric, "Custom Utils" );
    if( foldGeneric )
    {
      EditorGUILayout.BeginHorizontal();

      if( GUI.Button( EditorGUILayout.GetControlRect( false, 20 ), "Auto Parent to Zones" ) )
      {
        // gather all root objects with Area tag
        GameObject[] zones = GameObject.FindGameObjectsWithTag( "Zone" );
        // check all root objects 
        GameObject[] roots = SceneManager.GetActiveScene().GetRootGameObjects();
        foreach( var zone in zones )
        {
          for( int i = 0; i < zone.transform.childCount; i++ )
          {
            ArrayUtility.Add( ref roots, zone.transform.GetChild( i ).gameObject );
          }
        }
        //Undo.RecordObjects( roots, "Parent to Root Areas" );
        foreach( var go in roots )
        {
          if( !go.CompareTag( "Zone" ) && !go.CompareTag( "Scene" ) )
          {
            foreach( var zone in zones )
            {
              if( zone.GetComponent<Collider2D>().OverlapPoint( go.transform.position ) )
              {
                go.transform.parent = zone.transform;
                break;
              }
            }
          }
        }
        EditorSceneManager.MarkSceneDirty( SceneManager.GetActiveScene() );
      }

      if( GUI.Button( EditorGUILayout.GetControlRect( false, 20 ), "Auto Parent to Rooms" ) )
      {
        GameObject[] rooms = GameObject.FindGameObjectsWithTag( "Room" );
        if( rooms.Length > 0 )
        {
          Debug.Log( "Room count: " + rooms.Length );
          gos = new List<GameObject>( Object.FindObjectsOfType<GameObject>() );
          List<GameObject> gathered = new List<GameObject>();
          StartJob( "Gathering...", gos.Count, delegate()
          {
            GameObject go = gos[index];
            // Do not move anything under the SCENE node, or with certain tags.
            if( !go.transform.root.CompareTag( "Scene" ) && !go.CompareTag( "Zone" ) && !go.CompareTag( "Room" ) && !go.CompareTag( "Parallax" ) )
            {
              Transform top = go.transform;
              while( top.parent != null && !top.parent.CompareTag( "Zone" ) && !top.parent.CompareTag( "Room" ) && !top.parent.CompareTag( "Scene" ) )
                top = top.parent;
              gathered.Add( top.gameObject );
            }
          }, delegate()
          {
            Debug.Log( "Gathered Count: " + gathered.Count );
            StartJob( "Reparenting...", rooms.Length, delegate()
            {
              GameObject room = rooms[index];
              BoxCollider2D box = room.GetComponent<BoxCollider2D>();
              if( box != null )
                for( int i = 0; i < gathered.Count; i++ )
                  if( box.OverlapPoint( gathered[i].transform.position ) )
                    gathered[i].transform.SetParent( room.transform, false );
            }, delegate() { EditorSceneManager.MarkSceneDirty( SceneManager.GetActiveScene() ); } );
          } );
        }
        else
        {
          Debug.Log( "Must have an object tagged 'Room' with a box collider" );
        }
      }

      EditorGUILayout.EndHorizontal();

      /*GUILayout.Label( "Character", EditorStyles.boldLabel );*/
      if( GUI.Button( EditorGUILayout.GetControlRect( false, 20 ), "Select Player Character" ) )
        if( Application.isPlaying )
          Selection.activeGameObject = Global.instance.CurrentPlayer.gameObject;

      /*EditorGUILayout.BeginHorizontal();

      if( GUI.Button( EditorGUILayout.GetControlRect( false, 20 ), "Move Selection Up (Offset)" ) )
      {
        GameObject[] gos = Selection.GetFiltered<GameObject>( SelectionMode.TopLevel );
        Undo.SetCurrentGroupName( "TEST UP" );
        Undo.RecordObjects( gos, "Move Up (Offset)" );
        foreach( var obj in gos )
          obj.transform.position = obj.transform.position + Vector3.up * Global.GameplayLayerOffset;
      }

      if( GUI.Button( EditorGUILayout.GetControlRect( false, 20 ), "Move Selection Down (Offset)" ) )
      {
        GameObject[] gos = Selection.GetFiltered<GameObject>( SelectionMode.TopLevel );
        Undo.RecordObjects( gos, "Move Down (Offset)" );
        Undo.SetCurrentGroupName( "TEST Down" );
        foreach( var obj in gos )
          obj.transform.position = obj.transform.position + Vector3.down * Global.GameplayLayerOffset;
      }

      EditorGUILayout.EndHorizontal();*/

      EditorGUILayout.BeginHorizontal();

      layer = (LayerMask)EditorGUILayout.IntField( layer, GUILayout.MaxWidth( 40 ) );
      if( GUI.Button( EditorGUILayout.GetControlRect( false, 20 ), "Find GameObjects by Layer" ) )
      {
        gos = new List<GameObject>( Resources.FindObjectsOfTypeAll<GameObject>() );
        List<GameObject> found = new List<GameObject>();
        StartJob( "Searching...", gos.Count, delegate()
        {
          GameObject go = gos[index];
          if( go.layer == layer )
            found.Add( go );
        }, delegate()
        {
          foreach( var go in found )
          {
            Debug.Log( "Found: " + go.name, go );
          }
        } );
      }
      EditorGUILayout.EndHorizontal();
#if LIGHTING_TOGGLE
      if( GUI.Button( EditorGUILayout.GetControlRect( false, 30 ), "Gather Light Components" ) )
      {
        lights = new List<Light2D>( Resources.FindObjectsOfTypeAll<Light2D>() );
        List<Light2D> found = new List<Light2D>();
        StartJob( "Searching...", lights.Count, delegate() { found.Add( lights[index] ); }, delegate()
        {
          List<Global.LightData> assetLights = new List<Global.LightData>();
          foreach( var light in found )
          {
            string assetpath = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot( light );
            assetLights.Add( new Global.LightData() {EnabledInPrefab = light.enabled, light = light} );
            if( light.enabled )
              Debug.Log( "Path: " + assetpath + " Object: " + light.name + " in scene " + light.gameObject.scene.name, light );
            else
              Debug.LogWarning( "[DISABLED] Path: " + assetpath + " Object: " + light.name + " in scene " + light.gameObject.scene.name, light );
          }
          const string globalObject = "Assets/Resources/Global.prefab";
          GameObject go = PrefabUtility.LoadPrefabContents( globalObject );
          Global gbl = go.GetComponent<Global>();
          if( gbl.lightComponents == null )
            gbl.lightComponents = new Global.LightData[0];
          else
            ArrayUtility.Clear( ref gbl.lightComponents );
          ArrayUtility.AddRange( ref gbl.lightComponents, assetLights.ToArray() );
          PrefabUtility.SaveAsPrefabAsset( go, globalObject );
          AssetDatabase.SaveAssets();
        } );
      }
#endif
      if( GUI.Button( EditorGUILayout.GetControlRect( false, 20 ), "Show GUID of selected assets" ) )
      {
        for( int i = 0; i < Selection.assetGUIDs.Length; i++ )
          Debug.Log( Selection.objects[i].GetType().ToString() + " " + Selection.objects[i].name + " " + Selection.assetGUIDs[i] );
      }

#if false
      if( GUI.Button( EditorGUILayout.GetControlRect( false, 20 ), "Generate Edge Collider Nav Obstacles" ) )
      {
        GameObject go = new GameObject( "Nav Obstacle" );
        go.transform.position = Vector3.back;
        go.layer = LayerMask.NameToLayer( "Ignore Raycast" );
        //go.AddComponent<MeshRenderer>();
        //MeshFilter mf = go.AddComponent<MeshFilter>();

        List<CombineInstance> combine = new List<CombineInstance>();
        EdgeCollider2D[] edges = FindObjectsOfType<EdgeCollider2D>();
        for( int e = 0; e < edges.Length; e++ )
        {
          EdgeCollider2D edge = edges[e];
          /*
          Vector3[] v = new Vector3[(edge.points.Length - 1) * 4];
          int[] indices = new int[(edge.points.Length) * 4];
          Vector3[] n = new Vector3[v.Length];
          for( int i = 0; i < edge.points.Length - 1; i++ )
          {
            Vector2 a = edge.points[i];
            Vector2 b = edge.points[i + 1];
            Vector2 segment = b - a;
            Vector2 fudge = segment.normalized * fudgeFactor;
            Vector2 cross = (new Vector2( -segment.y, segment.x )).normalized * (edge.edgeRadius + fudgeFactor);
            v[i * 4] = a + cross - fudge;
            v[i * 4 + 1] = a + cross + segment + fudge;
            v[i * 4 + 2] = a - cross + segment + fudge;
            v[i * 4 + 3] = a - cross - fudge;
            n[i * 4] = Vector3.back;
            n[i * 4 + 1] = Vector3.back;
            n[i * 4 + 2] = Vector3.back;
            n[i * 4 + 3] = Vector3.back;
            indices[i * 4] = i * 4;
            indices[i * 4 + 1] = i * 4 + 1;
            indices[i * 4 + 2] = i * 4 + 2;
            indices[i * 4 + 3] = i * 4 + 3;
          }
          */
          CombineInstance ci = new CombineInstance();
          ci.transform = edge.transform.localToWorldMatrix;
          //ci.mesh = new Mesh();
          ci.mesh = edge.CreateMesh( false, false );
          //ci.mesh.vertices = v;
          //ci.mesh.normals = n;
          //ci.mesh.SetIndices( indices, MeshTopology.Quads, 0 );
          combine.Add( ci );
        }

        Mesh mesh = new Mesh();
        if( combine.Count == 1 )
        {
          mesh = combine[0].mesh;
          //go.transform.position = (Vector3)combine[0].transform.GetColumn( 3 );
        }
        else
          mesh.CombineMeshes( combine.ToArray(), false, false );

        MeshCollider mc = go.AddComponent<MeshCollider>();
        mc.sharedMesh = mesh;
        //mf.sharedMesh = mesh;
      }

      if( GUI.Button( EditorGUILayout.GetControlRect( false, 20 ), "Zip Persistent Files" ) )
      {
        List<string> persistentFilenames = new List<string>();
        foreach( string pfn in Global.persistentFilenames )
          persistentFilenames.Add( Application.persistentDataPath + "/" + pfn );
        ZipUtil.Zip( Application.dataPath + "/Resources/persistent.bytes", persistentFilenames.ToArray() );
      }

      string DialogueDirectory = Path.Combine( Application.dataPath, "def", "dialogue" );
      string LocaleDirectory = Path.Combine( DialogueDirectory, "locale" );
      if( GUI.Button( EditorGUILayout.GetControlRect( false, 30 ), "Export Localization Files" ) )
      {
        /*Object[] files = Selection.GetFiltered( typeof(TextAsset), SelectionMode.Assets );
        objects = new List<Object>();
        foreach( var file in files )
        {
          string extension = Path.GetExtension( AssetDatabase.GetAssetPath( file ) );
          if( extension != ".yaml" )
            continue;
          objects.Add( file );
        }*/
        Object[] textAssets = AssetDatabase.LoadAllAssetsAtPath( DialogueDirectory );
        objects = new List<Object>( textAssets );
        Directory.CreateDirectory( LocaleDirectory );
        for( int i = 0; i < objects.Count; i++ )
        {
          TextAsset obj = objects[i] as TextAsset;
          Debug.Log( "Exporting " + Path.GetFileName( AssetDatabase.GetAssetPath( obj ) ) + " to " + Directory.GetParent( AssetDatabase.GetAssetPath( obj ) ).FullName );
          const string token = "say:";
          int lineIndex = 0;
          List<string> lines = new List<string>();
          foreach( var line in obj.text.Split( '\n' ) )
          {
            if( line.IndexOf( token ) > 0 )
            {
              int tokenIndex = line.IndexOf( token ) + token.Length;
              string parsed = line.Substring( tokenIndex, line.Length - tokenIndex );
              lines.Add( string.Join( " ", lineIndex.ToString(), tokenIndex.ToString(), (line.Length - tokenIndex).ToString(), parsed.Trim() ) );
            }
            lineIndex++;
          }
          File.WriteAllLines( Path.Combine( LocaleDirectory, Path.GetFileName( AssetDatabase.GetAssetPath( obj ) ), ".txt" ), lines );
        }
      }

      if( GUI.Button( EditorGUILayout.GetControlRect( false, 30 ), "Import Localization Files" ) )
      {
        Object[] files = Selection.GetFiltered( typeof(TextAsset), SelectionMode.Assets );
        objects = new List<Object>();
        foreach( var file in files )
        {
          string extension = Path.GetExtension( AssetDatabase.GetAssetPath( file ) );
          if( extension != ".yaml" )
            continue;
          objects.Add( file );
        }
        Object[] textAssets = AssetDatabase.LoadAllAssetsAtPath( LocaleDirectory );
        objects = new List<Object>( textAssets );
        
      }
#endif

      GUILayout.Label( "Quad Mesh", EditorStyles.boldLabel );
      md = EditorGUILayout.Vector2Field( "Quad Dimensions", md );
      if( GUI.Button( EditorGUILayout.GetControlRect(), "Generate Quad Mesh" ) )
      {
        var mesh = new Mesh();
        var vertices = new Vector3[4];
        vertices[0] = new Vector3( -md.x * 0.5f, +md.y * 0.5f, 0 );
        vertices[1] = new Vector3( +md.x * 0.5f, +md.y * 0.5f, 0 );
        vertices[2] = new Vector3( -md.x * 0.5f, -md.y * 0.5f, 0 );
        vertices[3] = new Vector3( +md.x * 0.5f, -md.y * 0.5f, 0 );
        mesh.vertices = vertices;
        var uvs = new Vector2[4];
        uvs[0] = new Vector2( 0, 1 );
        uvs[1] = new Vector2( 1, 1 );
        uvs[2] = new Vector2( 0, 0 );
        uvs[3] = new Vector2( 1, 0 );
        mesh.uv = uvs;
        var indices = new[] { 0, 1, 2, 3, 2, 1 };
        mesh.SetIndices( indices, MeshTopology.Triangles, 0 );
        mesh.RecalculateNormals();
        AssetDatabase.CreateAsset( mesh, Path.Combine( Util.GetCurrentAssetDirectory(), "quad " + md.x + "x" + md.y + ".asset" ) );
        EditorUtility.SetDirty( mesh );
        AssetDatabase.Refresh();
      }

      // if( GUI.Button( EditorGUILayout.GetControlRect(), "DO IT" ) )
      // {
      // string path = Path.Combine( Path.GetDirectoryName( Application.dataPath ), "ProjectSettings", "ProjectSettings.asset" );
      // string PlayerSettingsFile = File.ReadAllText( path );
      // string output = Regex.Replace( PlayerSettingsFile, @"(bundleVersion\:).*", "$1 "+"8.0.0" );
      // File.WriteAllText( path, output ); 
      // AssetDatabase.Refresh();
      // }
    }

    EditorGUILayout.Space( 10 );
    foldMultiTool = EditorGUILayout.Foldout( foldMultiTool, "Multi Tool" );
    if( foldMultiTool )
    {
      operationContext = (OperationContext)EditorGUILayout.EnumPopup( "Context", operationContext );

      FilterObjectName = EditorGUILayout.TextField( "Filter by Name", FilterObjectName );

      ProcessChildObjects = EditorGUILayout.Toggle( "Process ChildObjects", ProcessChildObjects );
      if( ProcessChildObjects )
        FilterSubObjectName = EditorGUILayout.TextField( "Child Name", FilterSubObjectName );

      ReplaceTextWithTMP = EditorGUILayout.Toggle( "ReplaceComponents", ReplaceTextWithTMP );
      if( ReplaceTextWithTMP )
      {
        FilterComponentName = EditorGUILayout.TextField( "Component Name", FilterComponentName );
        ReplaceComponentType = typeof(UnityEngine.UI.Text); //(Behaviour)EditorGUILayout.ObjectField( "Component Type", ReplaceComponentType, typeof(UnityEngine.UI.Text), true );
        rcFont = (TMP_FontAsset)EditorGUILayout.ObjectField( "TMP Font", rcFont, typeof(TMP_FontAsset), true );
      }

      ReplacePrefab = EditorGUILayout.Toggle( "ReplacePrefab", ReplacePrefab );
      if( ReplacePrefab )
        replacementPrefab = (GameObject)EditorGUILayout.ObjectField( "Replacement Prefab", replacementPrefab, typeof(GameObject), false, GUILayout.MinWidth( 50 ) );

      ReplaceSprite = EditorGUILayout.Toggle( "ReplaceSprite", ReplaceSprite );
      if( ReplaceSprite )
        replacementSprite = (Sprite)EditorGUILayout.ObjectField( "Replacement Sprite", replacementSprite, typeof(Sprite), false );

      ReplaceMaterial = EditorGUILayout.Toggle( "Replace Material", ReplaceMaterial );
      if( ReplaceMaterial )
        replacementMaterial = (Material)EditorGUILayout.ObjectField( "Replacement Material", replacementMaterial, typeof(Material), false );

      ChangeSortingLayer = EditorGUILayout.Toggle( "Change Sorting Layer", ChangeSortingLayer );
      if( ChangeSortingLayer )
      {
        string[] sortingLayerNames = new string[0];
        foreach( var sortingLayer in SortingLayer.layers )
          ArrayUtility.Add( ref sortingLayerNames, sortingLayer.name );

        FromLayerSelection = EditorGUILayout.Popup( "From", FromLayerSelection, sortingLayerNames );
        FromLayer = SortingLayer.NameToID( sortingLayerNames[FromLayerSelection] );
        ToLayerSelection = EditorGUILayout.Popup( "To", ToLayerSelection, sortingLayerNames );
        ToLayer = SortingLayer.NameToID( sortingLayerNames[ToLayerSelection] );
        /*AssignUnknownSortingLayerSelection = EditorGUILayout.Popup( "Assign this to Unknown Layer", AssignUnknownSortingLayerSelection, sortingLayerNames );
        AssignUnknownSortingLayer = SortingLayer.NameToID( sortingLayerNames[AssignUnknownSortingLayerSelection] );*/
        AddToOrder = EditorGUILayout.IntField( "Add To Order", AddToOrder );
      }

      ModifyAudioSourceSettings = EditorGUILayout.Toggle( "Modify AudioSource", ModifyAudioSourceSettings );
      if( ModifyAudioSourceSettings )
      {
        audioDistanceMin = EditorGUILayout.IntField( "audioDistanceMin", audioDistanceMin );
        audioDistanceMax = EditorGUILayout.IntField( "audioDistanceMax", audioDistanceMax );
        audioRolloff = (AudioRolloffMode)EditorGUILayout.EnumPopup( "rolloff", audioRolloff );
        audioDoppler = EditorGUILayout.IntField( "doppler", audioDoppler );
      }

      ModifyTransforms = EditorGUILayout.Toggle( "Modify Transforms", ModifyTransforms );
      if( ModifyTransforms )
      {
        AddTransformPos = EditorGUILayout.Vector2Field( "Move", AddTransformPos );
        AddTransformRot = EditorGUILayout.FloatField( "Add Rot", AddTransformRot );
      }

      AutoFixSprite = EditorGUILayout.Toggle( "Auto Fix Sprite", AutoFixSprite );

      PrefabStage prefabStage = PrefabStageUtility.GetCurrentPrefabStage();

      if( GUI.Button( EditorGUILayout.GetControlRect( false, 30 ), "Process Objects" ) )
      {
        int count = 0;
        switch( operationContext )
        {
          case OperationContext.SELECTED:
            gos = new List<GameObject>( Selection.gameObjects );
            if( FilterObjectName.Length > 0 )
              for( int i = 0; i < gos.Count; i++ )
                if( !gos[i].name.StartsWith( FilterObjectName ) )
                  gos.RemoveAt( i-- );
            count = gos.Count;
            break;

          case OperationContext.SCENE:
            if( prefabStage != null )
            {
              ProcessChildObjects = true;
              gos = new List<GameObject>();
              gos.Add( prefabStage.prefabContentsRoot );
              assetPaths = new List<string>();
              assetPaths.Add( prefabStage.prefabAssetPath );
              count = 1;
            }
            else
            {
              gos = new List<GameObject>( FindObjectsOfType<GameObject>() );
              if( FilterObjectName.Length > 0 )
                for( int i = 0; i < gos.Count; i++ )
                  if( !gos[i].name.StartsWith( FilterObjectName ) )
                    gos.RemoveAt( i-- );
              count = gos.Count;
            }
            break;

          case OperationContext.PROJECT:
            // NOTE do not allow replacing prefabs with other prefab instances
            ReplacePrefab = false;
            assetPaths = new List<string>();
            guids = AssetDatabase.FindAssets( FilterObjectName + " t:prefab" );
            foreach( string guid in guids )
              assetPaths.Add( AssetDatabase.GUIDToAssetPath( guid ) );
            count = assetPaths.Count;
            break;
        }
        Undo.SetCurrentGroupName( "Process Objects" );
        StartJob( "Processing...", count, delegate()
        {
          GameObject go = null;

          switch( operationContext )
          {
            case OperationContext.SELECTED:
            case OperationContext.SCENE:
              go = gos[index];
              break;

            case OperationContext.PROJECT:
              go = PrefabUtility.LoadPrefabContents( assetPaths[index] );
              break;
          }

          Undo.RecordObject( go, go.name + " processed" );

          if( ProcessChildObjects )
          {
            Transform[] tfs = go.GetComponentsInChildren<Transform>();
            for( int i = 0; i < tfs.Length; i++ )
            {
              Transform tf = tfs[i];
              if( string.IsNullOrWhiteSpace( FilterSubObjectName ) || tf.name.StartsWith( FilterSubObjectName ) )
                ProcessSingle( tf.gameObject );
            }
          }
          else
          {
            ProcessSingle( go );
          }

          if( operationContext == OperationContext.PROJECT )
          {
            PrefabUtility.SaveAsPrefabAsset( go, assetPaths[index] );
            PrefabUtility.UnloadPrefabContents( go );
          }

          if( prefabStage != null )
            EditorSceneManager.MarkSceneDirty( prefabStage.scene );
        }, () =>
        {
          Undo.FlushUndoRecordObjects();
          AssetDatabase.SaveAssets();
        } );
      }
    }

    /*EditorGUILayout.Space( 10 );
    foldFont = EditorGUILayout.Foldout( foldFont, "Pixel Font" );
    if( foldFont )
    {
      fontAdvance = EditorGUILayout.IntField( "Character Advance", fontAdvance );
      // tested on Unity 2019.2.6f1
      fontImage = (Texture2D) EditorGUILayout.ObjectField( "Pixel Font Image", fontImage, typeof(Texture2D), false );
      if( GUI.Button( EditorGUILayout.GetControlRect( false, 30 ), "Create Pixel Font" ) )
      {
        string letter = " !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
        const int imageSize = 256;
        const int cols = 16;
        const int charWith = imageSize / cols;
        string output = "info font=\"Base 5\" size=32 bold=0 italic=0 charset=\"\" unicode=0 stretchH=100 smooth=1 aa=1 padding=0,0,0,0 spacing=2,2\n" + "common lineHeight=10 base=12 scaleW=" +
          imageSize + " scaleH=" + imageSize + " pages=1 packed=0\n" + "page id=0 file=\"" + fontImage.name + ".png\"\nchars count=" + letter.Length + "\n";
        for( int i = 0; i < letter.Length; i++ )
          output += "char id=" + i + " x=" + (i % cols) * charWith + " y=" + (i / cols) * charWith + " width=14 height=14 xoffset=1 yoffset=1 xadvance=" + fontAdvance + " page=0 chnl=0 letter=\"" +
            letter[i] + "\"\n";
        output += "kernings count=0";
        File.WriteAllText( Application.dataPath + "/font/" + fontImage.name + ".fnt", output );
        AssetDatabase.SaveAssets();

        string fntpath = Path.ChangeExtension( AssetDatabase.GetAssetPath( fontImage ), "fnt" );
        Debug.Log( fntpath );
        AssetDatabase.ImportAsset( fntpath, ImportAssetOptions.ForceUpdate | ImportAssetOptions.ForceSynchronousImport );
        AssetDatabase.Refresh();
        litefeel.BFImporter.DoImportBitmapFont( fntpath );

        // set default font values
        string fontsettings = Path.ChangeExtension( AssetDatabase.GetAssetPath( fontImage ), "fontsettings" );
        Font font = AssetDatabase.LoadAssetAtPath<Font>( fontsettings );
        SerializedObject so = new SerializedObject( font );
        so.Update();
        so.FindProperty( "m_AsciiStartOffset" ).intValue = 32;
        so.FindProperty( "m_Tracking" ).floatValue = 0.5f;
        so.FindProperty( "m_CharacterRects" ).GetArrayElementAtIndex( 0 ).FindPropertyRelative( "advance" ).floatValue = fontAdvance;
        so.ApplyModifiedProperties();
        so.SetIsDifferentCacheDirty();
        so.Update();
      }
    }*/

    EditorGUILayout.Space( 10 );
    foldSprite = EditorGUILayout.Foldout( foldSprite, "Sprite Library" );
    if( foldSprite )
    {
      sla = (UnityEngine.U2D.Animation.SpriteLibraryAsset)EditorGUILayout.ObjectField( "sprite library", sla, typeof(UnityEngine.U2D.Animation.SpriteLibraryAsset), false );
      category = EditorGUILayout.TextField( "category", category );
      if( GUI.Button( EditorGUILayout.GetControlRect( false, 30 ), "Add Selected Sprites" ) )
      {
        SortedList<string, Sprite> list = new SortedList<string, Sprite>();
        Object[] objects = Selection.GetFiltered<Object>( SelectionMode.Unfiltered );
        for( int i = 0; i < objects.Length; i++ )
        {
          int underscore = objects[i].name.LastIndexOf( '_' ) + 1;
          string index = objects[i].name.Substring( underscore, objects[i].name.Length - underscore );
          list.Add( index, (Sprite)objects[i] );
        }
        foreach( var pair in list )
          sla.AddCategoryLabel( pair.Value, category, pair.Key );

        EditorUtility.SetDirty( sla );
      }
    }
    EditorGUILayout.Space( 10 );

    foldTilemap = EditorGUILayout.Foldout( foldTilemap, "Aseprite Tilemap" );
    if( foldTilemap )
    {
      tileSize = EditorGUILayout.Vector2IntField( "Tile Size", tileSize );
      tilemapInScene = (Tilemap)EditorGUILayout.ObjectField( "tilemap in scene", tilemapInScene, typeof(Tilemap), true );
      tilemap = (Texture2D)EditorGUILayout.ObjectField( "tilemap", tilemap, typeof(Texture2D), false );
      tileset = (Texture2D)EditorGUILayout.ObjectField( "tileset", tileset, typeof(Texture2D), false );
      palette = (Texture2D)EditorGUILayout.ObjectField( "palette map", palette, typeof(Texture2D), false );

      // Prerequisites: Aseprite has the ability to export images for this script to work. 
      // Align the level to a grid (16x16 for SNES games)
      // Run the script "Pack Similar Tiles" in the Aseprite File menu. This creates two files, a tilemap and a tileset.
      // The palette for the tilemap must have unique values for this script to work, so next run "Random Palette" on the tilemap.
      // DO NOT edit or "Remap" the palette. 
      // The palette image is necessary because this script does not import and read indexed images.
      // In the palette menu, select "Save Palette" and save to a PNG.
      // Now save all three files into your Unity project.

      if( GUI.Button( EditorGUILayout.GetControlRect( false, 30 ), "Create Scene from Aseprite Tilemap" ) )
      {
        /*
        // Make tilemap readable
        {
          string path = AssetDatabase.GetAssetPath( tilemap );
          UnityEditor.U2D.Aseprite.AsepriteImporter ti0 = AssetImporter.GetAtPath( path ) as UnityEditor.U2D.Aseprite.AsepriteImporter;
          // ti0.isReadable = true;
          AssetDatabase.ImportAsset( path, ImportAssetOptions.ForceUpdate );
          EditorUtility.SetDirty( tilemap );
        }
        // Make palette readable
        {
          string path = AssetDatabase.GetAssetPath( palette );
          TextureImporter ti1 = AssetImporter.GetAtPath( path ) as TextureImporter;
          ti1.isReadable = true;
          AssetDatabase.ImportAsset( path, ImportAssetOptions.ForceUpdate );
          EditorUtility.SetDirty( palette );
        }*/

        paletteIndex = new Dictionary<string, int>();
        Color[] ppix = palette.GetPixels();
        int i = 0;
        for( int y = 0; y < palette.height; y++ )
        for( int x = 0; x < palette.width; x++, i++ )
        {
          paletteIndex[ColorUtility.ToHtmlStringRGB( ppix[x + (palette.height - 1 - y) * palette.width] )] = i;
        }

        string tilesetPath = AssetDatabase.GetAssetPath( tileset );
        AssetDatabase.ImportAsset( tilesetPath, ImportAssetOptions.ForceUpdate );
        /*EditorUtility.SetDirty( tileset );*/
        string dir = Path.Join( Path.GetDirectoryName( tilesetPath ), tileset.name );
        Directory.CreateDirectory( dir );

        Object[] spriteTiles = AssetDatabase.LoadAllAssetsAtPath( AssetDatabase.GetAssetPath( tileset ) );
        Dictionary<string, Tile> tiles = new Dictionary<string, Tile>();
        Vector3Int pos = Vector3Int.zero;
        int index = 0;
        for( int x = 0; x < tilemap.width; x++ )
        for( int y = 0; y < tilemap.height; y++ )
        {
          pos.x = x;
          pos.y = y;
          string idx = ColorUtility.ToHtmlStringRGB( tilemap.GetPixel( x, y ) );
          Sprite newSprite = (Sprite)spriteTiles[paletteIndex[idx]];
          Tile tile;
          if( tiles.ContainsKey( idx ) )
            tile = tiles[idx];
          else
          {
            tile = ScriptableObject.CreateInstance<Tile>();
            tile.sprite = newSprite;
            tile.colliderType = Tile.ColliderType.None;
            tiles.Add( idx, tile );
            AssetDatabase.CreateAsset( tile, Path.Join( dir, string.Format( "tile_{0}.asset", index ) ) );
            index++;
          }
          tilemapInScene.SetTile( pos, tile );
        }
      }
    }

    EditorGUILayout.EndScrollView();
  }

  // quad mesh dimensions
  Vector2 md;

  GameObject ReplaceObjectWithPrefabInstance( GameObject replaceThis, GameObject prefab )
  {
    GameObject go = (GameObject)PrefabUtility.InstantiatePrefab( prefab, replaceThis.transform.parent );
    go.transform.position = replaceThis.transform.position;
    go.transform.rotation = replaceThis.transform.rotation;
    SpriteRenderer sr = go.GetComponent<SpriteRenderer>();
    SpriteRenderer tfsr = replaceThis.GetComponent<SpriteRenderer>();
    if( sr != null && tfsr != null )
    {
      sr.size = tfsr.size;
      PrefabUtility.RecordPrefabInstancePropertyModifications( sr );
    }
    NavMeshObstacle gonob = go.GetComponent<NavMeshObstacle>();
    NavMeshObstacle nob = replaceThis.GetComponent<NavMeshObstacle>();
    if( gonob != null && nob != null )
    {
      gonob.size = nob.size;
      gonob.carving = nob.carving;
    }
    PrefabUtility.RecordPrefabInstancePropertyModifications( go.transform );
    DestroyImmediate( replaceThis.gameObject );
    return go;
  }


  void ProcessSingle( GameObject go )
  {
    if( ReplacePrefab && replacementPrefab != null )
      go = ReplaceObjectWithPrefabInstance( go, replacementPrefab );

    SpriteRenderer spr = go.GetComponent<SpriteRenderer>();
    if( spr != null )
    {
      if( ReplaceSprite && replacementSprite != null )
        spr.sprite = replacementSprite;
      if( replacementMaterial && replacementMaterial != null )
        spr.material = replacementMaterial;
      if( ChangeSortingLayer && spr.sortingLayerID == FromLayer )
        spr.sortingLayerID = ToLayer;
      /*if( spr.sortingLayerID == 0 )
        spr.sortingLayerID = AssignUnknownSortingLayer;*/
      // check this because assigning the sortingOrder might make prefab instance dirty.
      if( AddToOrder != 0 )
        spr.sortingOrder += AddToOrder;
      if( AutoFixSprite && spr.sprite != null && !spr.gameObject.name.Contains( "glyph" ) )
        StaticUtility.AutoFixSprite( spr );
    }
    if( ModifyAudioSourceSettings )
    {
      AudioSource ass = go.GetComponent<AudioSource>();
      if( ass )
      {
        ass.minDistance = audioDistanceMin;
        ass.maxDistance = audioDistanceMax;
        ass.rolloffMode = audioRolloff;
        ass.dopplerLevel = audioDoppler;
      }
    }
    if( ModifyTransforms )
    {
      go.transform.Translate( AddTransformPos );
      go.transform.Rotate( 0, 0, AddTransformRot );
    }
    if( ReplaceTextWithTMP )
    {
      Component[] cmps = go.GetComponents( ReplaceComponentType );
      for( int i = 0; i < cmps.Length; i++ )
      {
        rcSize = ((UnityEngine.UI.Text)cmps[i]).fontSize;
        rcText = ((UnityEngine.UI.Text)cmps[i]).text;
        DestroyImmediate( cmps[i] );
        TMP_Text tmpText = go.AddComponent<TextMeshProUGUI>();
        tmpText.fontSize = rcSize;
        tmpText.font = rcFont;
        tmpText.text = rcText;
      }
    }

    EditorUtility.SetDirty( go );
  }

  // Called from build script. DO NOT REMOVE.
  static void Build_MacOS()
  {
    buildMacOS = true;
    buildLinux = buildWindows = false;
    BuildPlayerOptions bpo = new BuildPlayerOptions();
    /*bpo.options |= BuildOptions.Development;*/
    Build( bpo );
  }

  static void Build( BuildPlayerOptions bpo )
  {
    /*
     * From the docs:
     * Building Addressables on Player Build requires Unity 2021.2+.
     * In earlier versions of Unity, you must build your Addressables content as a separate step.
     */

    /*AddressablesDataBuilderInput input = new AddressablesDataBuilderInput();
    UnityEditor.AddressableAssets.Build.DataBuilders.BuildScriptFastMode.BuildData<TResult>( input );*/

    EditorPrefs.SetBool( "BuildMacOS", buildMacOS );
    EditorPrefs.SetBool( "BuildLinux", buildLinux );
    EditorPrefs.SetBool( "BuildWindows", buildWindows );
    EditorPrefs.SetBool( "BuildWindows", buildWebGL );

    // Either use the given build config, or use the one assign to the GLOBAL prefab.
    const string globalObject = "Assets/Resources/GLOBAL.prefab";
    GameObject go = PrefabUtility.LoadPrefabContents( globalObject );
    Global gbl = go.GetComponent<Global>();
    if( BuildConfig )
    {
      gbl.BuildConfig = BuildConfig;
      PrefabUtility.SaveAsPrefabAsset( go, globalObject );
    }
    else
      BuildConfig = gbl.BuildConfig;

    EditorBuildSettingsScene[] newscenelist = new EditorBuildSettingsScene[0];
    foreach( var sref in BuildConfig.sceneRefs )
      ArrayUtility.Add( ref newscenelist, new EditorBuildSettingsScene() { enabled = true, path = sref.ScenePath } );
    EditorBuildSettings.scenes = newscenelist;
    /*for( int i = 0; i < EditorBuildSettings.scenes.Length; i++ )
      Debug.Log( EditorBuildSettings.scenes[i].path );*/

#if !DEVELOPMENT_BUILD
    System.Version v = System.Version.Parse( PlayerSettings.bundleVersion );
    PlayerSettings.bundleVersion = (new System.Version( v.Major, v.Minor, v.Build + 1 )).ToString();
#endif

    List<string> buildpaths = new List<string>();
    for( int i = 0; i < EditorBuildSettings.scenes.Length; i++ )
      if( EditorBuildSettings.scenes[i].enabled )
        buildpaths.Add( EditorBuildSettings.scenes[i].path );
    bpo.scenes = buildpaths.ToArray();
    // YAML/text assets can be stripped out of a build when stripping is enabled, so disable it here.
    PlayerSettings.SetManagedStrippingLevel( BuildTargetGroup.Standalone, ManagedStrippingLevel.Disabled );

    if( buildLinux )
    {
      bpo.targetGroup = BuildTargetGroup.Standalone;
      bpo.target = BuildTarget.StandaloneLinux64;
      string outDir = Directory.GetParent( Application.dataPath ).FullName + "/build/Linux";
      outDir += "/"+ BuildConfig.AppName + "." + Util.Timestamp();
      Directory.CreateDirectory( outDir );
      bpo.locationPathName = outDir + "/" + BuildConfig.AppName + ".x86_64";
      BuildPipeline.BuildPlayer( bpo );
      Debug.Log( bpo.locationPathName );
      // copy to shared folder
      /*string shareDir = System.Environment.GetFolderPath( System.Environment.SpecialFolder.UserProfile ) + "/SHARE";
      Util.DirectoryCopy( outDir, Path.Combine( shareDir, BuildConfig.AppName+"." + Util.Timestamp() ) );*/
    }
    if( buildWindows )
    {
      
      bpo.targetGroup = BuildTargetGroup.Standalone;
      bpo.target = BuildTarget.StandaloneWindows64;
      string outDir = Directory.GetParent( Application.dataPath ).FullName + "/build/Windows";
      outDir += "/"+ BuildConfig.AppName +"." + Util.Timestamp();
      Directory.CreateDirectory( outDir );
      bpo.locationPathName = outDir + "/" + BuildConfig.AppName + ".exe";
      BuildReport report = BuildPipeline.BuildPlayer( bpo );
      Debug.Log( bpo.locationPathName );
    }
    if( buildWebGL )
    {
      bpo.targetGroup = BuildTargetGroup.WebGL;
      bpo.target = BuildTarget.WebGL;
      string outDir = Directory.GetParent( Application.dataPath ).FullName + "/build/WebGL";
      Directory.CreateDirectory( outDir );
      bpo.locationPathName = outDir + "/" + BuildConfig.AppName + "." + Util.Timestamp();
      BuildPipeline.BuildPlayer( bpo );
      Debug.Log( bpo.locationPathName );
    }
    if( buildMacOS )
    {
      bpo.targetGroup = BuildTargetGroup.Standalone;
      bpo.target = BuildTarget.StandaloneOSX;
      /*PlayerSettings.SetScriptingBackend( BuildTargetGroup.Standalone, ScriptingImplementation.IL2CPP );*/
      string outDir = Directory.GetParent( Application.dataPath ).FullName + "/build/MacOS/";
      Directory.CreateDirectory( outDir );
      if( (bpo.options & BuildOptions.Development) > 0 )
      {
        bpo.options |= BuildOptions.AllowDebugging;
        // the extension is replaced with ".app" by Unity
        bpo.locationPathName = outDir + "DEV" + "." + Util.Timestamp() + ".extension";
      }
      else
        bpo.locationPathName = outDir + BuildConfig.AppName + "." + Util.Timestamp() + ".extension";
      BuildReport report = BuildPipeline.BuildPlayer( bpo );
      if( report.summary.result == BuildResult.Succeeded )
        Debug.Log( bpo.locationPathName );
      System.Diagnostics.Process.Start( "open", bpo.locationPathName );
    }
  }
}


#if false
void ClearGroundImages()
{
  string[] dirs = Directory.GetDirectories( Application.persistentDataPath );
  foreach( var dir in dirs )
  {
    Debug.Log( dir );
    string[] files = Directory.GetFiles( dir, "*.png" );
    foreach( var f in files )
    {
      File.Delete( f );
    }
  }
}

void ClearGroundOverlayImages()
{
  string[] dirs = Directory.GetDirectories( Application.persistentDataPath );
  foreach( var dir in dirs )
  {
    Debug.Log( dir );
    string[] files = Directory.GetFiles( dir, "*-dirt.png" );
    foreach( var f in files )
    {
      File.Delete( f );
    }
  }
}



// I wrote this for someone on the Unity forums
public class ProgressUpdateExample : EditorWindow
{
  [MenuItem("Tool/ProgressUpdateExample")]
  static void Init()
  {
    ProgressUpdateExample window = (ProgressUpdateExample)EditorWindow.GetWindow(typeof(ProgressUpdateExample));
    window.Show();
  }

  System.Action ProgressUpdate;
  bool processing = false;
  float progress = 0;
  int index = 0;
  int length = 0;

  List<GameObject> gos = new List<GameObject>();

  void OnGUI()
  {
    if( processing )
    {
      if( index == length )
      {
        processing = false;
        progress = 1;
        ProgressUpdate = null;
      }
      else
      {
        ProgressUpdate();
        progress = (float)index++ / (float)length;
      }
      // IMPORTANT: while processing, this call "drives" OnGUI to be called repeatedly instead of on-demand.
      Repaint();
    }

    EditorGUI.ProgressBar( EditorGUILayout.GetControlRect( false, 30 ), progress, "progress" );

    if( GUI.Button( EditorGUILayout.GetControlRect( false, 30 ), "List all Prefabs" ) )
    {
      // gather prefabs into list
      gos.Clear();
      string[] guids = AssetDatabase.FindAssets("t:prefab");
      foreach (string guid in guids)
      {
        GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>( AssetDatabase.GUIDToAssetPath( guid ) );
        gos.Add( prefab );
      }

      // initialize progress update
      length = gos.Count;
      index = 0;
      progress = 0;
      processing = true;
      ProgressUpdate = delegate() {
        GameObject go = gos[index];
        Debug.Log("prefab: " + go.name, go );
      };
    }
  }
}
#endif


#endif