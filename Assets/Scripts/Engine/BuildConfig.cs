﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu]
public class BuildConfig : ScriptableObject
{
  public string AppName = "App";
  public SceneReference[] sceneRefs;

  public SceneReference InitialScene;
  public SceneReference ReturnToThisSceneAfterBoss;
  /*public UniversalRenderPipelineAsset RenderPipelineAsset;*/

#if UNITY_EDITOR
  [ExposeMethod]
  public void AssignAllScenesInProject()
  {
    string[] guids = AssetDatabase.FindAssets( "t:scene", new string[] {"Assets"} );
    foreach( string guid in guids )
    {
      SceneReference sceneReference = new SceneReference();
      sceneReference.ScenePath = AssetDatabase.GUIDToAssetPath( guid );
      ArrayUtility.Add( ref sceneRefs, sceneReference );
      /*sceneRefs.Add(sceneReference);*/
    }
    EditorUtility.SetDirty( this );
  }
#endif
}