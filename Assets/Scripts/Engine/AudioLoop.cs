﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/*using UnityEngine.Localization;*/
[System.Serializable]
public struct AudioEvent
{
  public AudioLoop Audio;
  public float Time;
  public UnityEvent Event;
}

[CreateAssetMenu]
public class AudioLoop : ScriptableObject
{
  public AudioClip OneShotClip;
  public AudioClip loop;
  /*public float introDelay;*/


  static List<Timer> EventTimers = new List<Timer>();

  public MusicAlbum Album;
  public string TrackTitle;
  /*public LocalizedString TrackTitle;*/
  [Tooltip( "Optional: assign this if album has multiple artists" )]
  public string OptionArtist;
  /*public LocalizedString OptionArtist;*/

  public void Play( /*AudioSource introSource,*/ AudioSource loopSource )
  {
    loopSource.clip = loop;
    loopSource.Play();

    foreach( var evt in EventTimers )
      evt.Stop( false );
    EventTimers.Clear();
    if( Global.instance.AudioEvents != null )
      foreach( var evt in Global.instance.AudioEvents )
        if( evt.Audio == this )
          EventTimers.Add( new Timer( evt.Time, null, () => evt.Event.Invoke() ) );

    /*float skip = 0;
    /*if( intro )
    {
      introSource.playOnAwake = false;
      introSource.priority = loopSource.priority;
      introSource.volume = loopSource.volume;
      introSource.loop = false;
      introSource.clip = intro;
      introSource.PlayScheduled( AudioSettings.dspTime + introDelay );
      skip = intro.length;
    }
    else
    {
      introSource.Stop();
    }#1#

    if( loop )
    {
      loopSource.loop = true;
      loopSource.clip = loop;
      loopSource.PlayScheduled( AudioSettings.dspTime + introDelay + skip );
    }
    else
    {
      /*loopSource.Stop();#1#

      // experimental
      if( OneShotClip )
      {
        loopSource.playOnAwake = false;
        loopSource.priority = loopSource.priority;
        loopSource.volume = loopSource.volume;
        loopSource.loop = false;
        loopSource.clip = OneShotClip;
        loopSource.Play();
      }
    }*/
  }
}