﻿using UnityEngine;
using System.Collections.Generic;

// NOTE The object that "owns" a timer must stop the timer in OnDestroy() to avoid
// latent callbacks to/from objects that have been destroyed due to a scene change.
// I've attempted a few times to automate timer autodestruction. One problem is that
// there's no way to detect a dangling reference to an owner object.  If 'owner' was
// a UnityEngine.Object, the bool conversion operator might work, but subclassing Object
// is problematic... For instance, the 'new' keyword returns a null reference. It doesn't
// seem like we're supposed to subclass Unity.Object.

public struct TimerParams
{
  public bool unscaledTime;
  public float duration;

  public bool repeat;
  public int loops;
  public float interval;

  public System.Action<Timer> UpdateDelegate;
  public System.Action<Timer> IntervalDelegate;
  public System.Action CompleteDelegate;
}

public class Timer
{
  // New timers are held until the next frame, so that adding timers within the callback
  // of another timer does not modify the ActiveTimers collection while iterating.
  public static List<Timer> NewTimers = new List<Timer>();
  public static List<Timer> ActiveTimers = new List<Timer>();
  public static List<Timer> RemoveTimers = new List<Timer>();

  /*public static void OnSceneChange()
  {
    for( int i = 0; i < ActiveTimers.Count; i++ )
    {
      try
      {
        if( ReferenceEquals( ActiveTimers[i].owner, null ) )
          ActiveTimers[i].Stop( false );
      }
      catch( MissingReferenceException exp )
      {
        Debug.LogWarning( "Timer owner reference missing: " + exp.Message );
        ActiveTimers[i].Stop( false );
      }
    }
    // remove immediately, before next update.
    foreach( var timer in RemoveTimers )
      ActiveTimers.Remove( timer );
    RemoveTimers.Clear();
  }*/

  public static void UpdateTimers()
  {
    // remove first so that timers in both new and remove lists still get added.
    foreach( var timer in RemoveTimers )
      ActiveTimers.Remove( timer );
    RemoveTimers.Clear();
    ActiveTimers.AddRange( NewTimers );
    NewTimers.Clear();
    for( int i = 0; i < ActiveTimers.Count; i++ )
    {
      if( ActiveTimers[i].IsActive /*&& ActiveTimers[i].owner != null*/ )
        ActiveTimers[i].Update();
      else
      {
        ActiveTimers[i].active = false;
        RemoveTimers.Add( ActiveTimers[i] );
      }
    }
  }

  public Timer()
  {
    active = false;
    repeat = false;
  }

  public Timer( float duration, System.Action<Timer> UpdateDelegate, System.Action CompleteDelegate )
  {
    /*this.owner = owner;*/
    Start( duration, UpdateDelegate, CompleteDelegate );
  }

  public Timer( int loops, float interval, System.Action<Timer> IntervalDelegate, System.Action CompleteDelegate )
  {
    /*this.owner = owner;*/
    Start( loops, interval, IntervalDelegate, CompleteDelegate );
  }

  public void Start( TimerParams param )
  {
    /*this.owner = owner;*/
    if( !active )
      NewTimers.Add( this );
    active = true;
    unscaledTime = param.unscaledTime;
    StartTime = time;
    if( param.repeat )
    {
      repeat = true;
      Interval = param.interval;
      IntervalStartTime = StartTime;
      Duration = param.interval * param.loops;
      OnInterval = param.IntervalDelegate;
    }
    else
    {
      Duration = param.duration;
    }
    OnUpdate = param.UpdateDelegate;
    OnComplete = param.CompleteDelegate;
  }

  public void Start( float duration )
  {
    /*this.owner = owner;*/
    if( !active )
      NewTimers.Add( this );
    active = true;
    StartTime = time;
    Duration = duration;
    repeat = false;
    OnUpdate = null;
    OnInterval = null;
    OnComplete = null;
  }

  public void Start( float duration, System.Action<Timer> UpdateDelegate, System.Action CompleteDelegate )
  {
    /*this.owner = owner;*/
    if( !active )
      NewTimers.Add( this );
    active = true;
    StartTime = time;
    Duration = duration;
    repeat = false;
    OnUpdate = UpdateDelegate;
    OnInterval = null;
    OnComplete = CompleteDelegate;
  }

  public void Start( int loops, float interval, System.Action<Timer> IntervalDelegate, System.Action CompleteDelegate )
  {
    /*this.owner = owner;*/
    if( !active )
      NewTimers.Add( this );
    active = true;
    StartTime = time;
    Interval = interval;
    IntervalStartTime = StartTime;
    Duration = interval * loops;
    repeat = true;
    OnInterval = IntervalDelegate;
    OnUpdate = null;
    OnComplete = CompleteDelegate;
  }

  public void Stop( bool callOnComplete )
  {
    active = false;
    RemoveTimers.Add( this );
    if( callOnComplete && OnComplete != null )
      OnComplete();
  }

  public void Update()
  {
    if( active )
    {
      if( time - StartTime > Duration )
      {
        // ensure the last repeat interval update is called before stopping.
        if( repeat && OnInterval != null )
          OnInterval( this );
        Stop( true );
      }
      else if( repeat )
      {
        if( time - IntervalStartTime > Interval )
        {
          IntervalStartTime = time;
          if( OnInterval != null )
            OnInterval( this );
        }
      }
      else
      {
        if( OnUpdate != null )
          OnUpdate( this );
      }
    }
  }

  /*public void SetProgress( float progressSeconds ) { StartTime = time - progressSeconds; }*/
  public bool IsActive { get { return active; } }
  public float ProgressSeconds { get { return time - StartTime; } }
  public float ProgressNormalized { get { return Mathf.Clamp( (time - StartTime) / Duration, 0, 1 ); } }
  float time { get { return unscaledTime ? Time.unscaledTime : Time.time; } }

  // Check if owner is null when updating and remove if so.
  // This SHOULD HAVE avoided the need to manually remove the timer when the owner object is destroyed, but fails because
  // Unity holds on to the C# reference even if the C++ pointer is invalid after a scene change. Instead of showing the 
  // reference as null (we could have detected this and removed the timer on scene change),
  // Unity decides to *throw* a MissingReferenceException when you try to access... great. 
  /*object owner;*/
  
  
  bool active;
  public bool unscaledTime;
  float StartTime;
  float Duration;
  float Interval;
  float IntervalStartTime;
  bool repeat;
  System.Action<Timer> OnUpdate;
  System.Action<Timer> OnInterval;
  System.Action OnComplete;
}