﻿using UnityEngine;

public interface IVelocity
{
  Vector2 GetVelocityAtPoint( Vector2 point );
}

public interface IDamage
{
  DamageResult CalculateDamageResult( Damage damage );
  void TakeDamage( Damage damage );
  Vector2 GetExplosionDetectionPoint();
}

public interface ITrigger
{
  void Trigger( RaycastHit2D hit, Transform instigator );
}

public interface IWorldSelectable
{
  void Highlight();
  void Unhighlight();
  void Select();
  void Select( Entity instigator );
  void Unselect();
  Vector2 GetSelectablePosition();
}


public struct TalkerCameraInfo
{
  public Vector2 focus;
  public float orthoSize;
}


/*
public interface ITalker
{
  CharacterIdentity GetIdentity();
  void OnSay( string say, AudioClip voiceOver = null );
  void OnSpeakEnd();
  TalkerCameraInfo GetCameraInfo();

  UnityEngine.Events.UnityEvent GetDestructionEvent();
  void PreConversation( Global.ConversationOptions options );
  void PostConversation();
}
*/
