﻿using UnityEngine;

public class GhostPawn : Pawn
{
  [SerializeField] public float MoveSpeed = 3;
  // [SerializeField] public float Deceleration = 1;

  public override void EntityUpdate()
  {
    velocity = Vector2.zero;
    // velocity -= velocity.normalized * Mathf.Max( 1f, Deceleration * Time.deltaTime);
    if( input.MoveUp )
      velocity += Vector2.up * MoveSpeed;
    if( input.MoveDown )
      velocity += Vector2.down * MoveSpeed;
    if( input.MoveRight )
      velocity += Vector2.right * MoveSpeed;
    if( input.MoveLeft )
      velocity += Vector2.left * MoveSpeed;
    transform.position = transform.position + (Vector3) velocity * Time.deltaTime;
    ResetInput();
  }
}