﻿using System.Collections.Generic;
using UnityEngine;
using ClipperLib;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor( typeof(CameraZone) )]
[CanEditMultipleObjects]
public class CameraZoneEdtitor : MonoBehaviourInspector
{
  /*public override void OnInspectorGUI()
  {
    base.OnInspectorGUI();

    CameraZone zone = target as CameraZone;
    if( zone.UsePolygonUnion )
    {
      for( int i = 0; i < zone.union.Length; i++ )
      {
        int prev;
        if( i ==0)
          prev = zone.union.Length - 1;
        else
         prev = (i + 1) % zone.union.Length;
         #if UNITY_EDITOR || DEVELOPMENT_BUILD
        Popcron.Gizmos.Line( zone.union[prev], zone.union[i], Color.red );
        #endif
      }
    }
  }*/

  void OnSceneGUI()
  {
    Handles.color = Color.yellow;
    CameraZone zone = target as CameraZone;
    if( zone.UsePolygonUnion )
    {
      for( int i = 0; i < zone.union.Length; i++ )
      {
        int prev;
        if( i == 0 )
          prev = zone.union.Length - 1;
        else
          prev = (i + 1) % zone.union.Length;
        Handles.DrawLine( zone.union[prev], zone.union[i] );
      }
    }
  }
}
#endif


public class CameraZone : MonoBehaviour
{
  public static List<CameraZone> All = new List<CameraZone>();

  [Tooltip( "Higher priority zones will take precedence over lesser priorities." )]
  public int priority;
  public bool AffectY = true;
  public bool AffectX = false;
  [Tooltip( "The camera will increase its size to view all colliders. Useful for rooms." )]
  public bool EncompassBounds;
  public bool FollowEdges;
  public bool UsePolygonUnion;
  [Tooltip( "Ignore setting to set active camera zone to this when player enters zone." )]
  public bool IgnoreAutoSwitch;
  [Tooltip( "Set the camera zoom when this zone is the active zone." )]
  public bool SetOrtho;
  [Tooltip( "If SetOrtho is true, this is the camera zoom value." )]
  public float orthoTarget;
  [Tooltip( "Optional camera zone trigger collider." )]
  public Collider2D alternateTrigger;
  [Tooltip( "The camera will stay within the shapes of these colliders when the zone is active." )]
  public Collider2D[] colliders;

  public Bounds CameraBounds;

  private void Awake()
  {
    All.Add( this );
    UpdateBounds();
    CalculateUnion();
  }

  private void OnDestroy() { All.Remove( this ); }

  void UpdateBounds()
  {
    foreach( var cld in colliders )
    {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
      if( cld.isTrigger == false )
        Debug.LogWarning( "\"" + cld.gameObject.name + "\"" + " should be a trigger.", cld.gameObject );
      cld.isTrigger = true;
#endif

      if( cld is PolygonCollider2D )
      {
        PolygonCollider2D poly = cld as PolygonCollider2D;
        // camera poly bounds points are local to polygon
        CameraBounds = new Bounds();
        foreach( var p in poly.points )
          CameraBounds.Encapsulate( p );
      }
      else if( cld is BoxCollider2D )
      {
        CameraBounds = (cld as BoxCollider2D).bounds;
      }
    }
  }

  private static bool Overlaps( Vector2 point, CameraZone cz, Collider2D collider, ref CameraZone zone ) { return collider.OverlapPoint( point ) && !cz.IgnoreAutoSwitch && (zone == null || cz.priority > zone.priority); }

  public static bool DoesOverlapAnyZone( Vector2 point, ref CameraZone active )
  {
    CameraZone zone = null;
    for( int z = 0; z < All.Count; z++ )
    {
      if( All[z].alternateTrigger )
      {
        if( Overlaps( point, All[z], All[z].alternateTrigger, ref zone ) )
        {
          zone = All[z];
          break;
        }
      }
      else
        for( int i = 0; i < All[z].colliders.Length; i++ )
          if( Overlaps( point, All[z], All[z].colliders[i], ref zone ) )
          {
            zone = All[z];
            break;
          }
    }
    active = zone;
    return zone;
  }


#region POLYGON_UNION

  public Vector2[] union;

  [ExposeMethod]
  public void CalculateUnion()
  {
    const float PRECISION = 16;
    // WARNING no idea if the PolygonCOllider2D has consistent CW or CCW winding!!!
    //precondition: all your polygons have the same orientation 
    //(ie either clockwise or counter clockwise)

    List<List<IntPoint>> polys = new List<List<IntPoint>>();

    foreach( var cld in colliders )
    {
      if( cld is BoxCollider2D )
      {
        BoxCollider2D box = cld as BoxCollider2D;
        Vector2 bt = box.transform.position;
        Quaternion br = box.transform.rotation;
        Vector2 bsize = box.size;
        List<IntPoint> poly = new List<IntPoint>();
        Vector2 temp = bt + (Vector2) (br * (box.offset - (box.size * 0.5f)));
        poly.Add( new IntPoint( temp.x * PRECISION, temp.y * PRECISION ) );
        temp = bt + (Vector2) (br * (box.offset + new Vector2( -bsize.x, bsize.y ) * 0.5f));
        poly.Add( new IntPoint( temp.x * PRECISION, temp.y * PRECISION ) );
        temp = bt + (Vector2) (br * (box.offset + (box.size * 0.5f)));
        poly.Add( new IntPoint( temp.x * PRECISION, temp.y * PRECISION ) );
        temp = bt + (Vector2) (br * (box.offset + new Vector2( bsize.x, -bsize.y ) * 0.5f));
        poly.Add( new IntPoint( temp.x * PRECISION, temp.y * PRECISION ) );
        polys.Add( poly );
      }
      else if( cld is PolygonCollider2D )
      {
        List<IntPoint> poly = new List<IntPoint>();
        foreach( var point in (cld as PolygonCollider2D).points )
          poly.Add( new IntPoint( point.x * PRECISION, point.y * PRECISION ) );
        polys.Add( poly );
      }
    }

    List<List<IntPoint>> solution = new List<List<IntPoint>>();
    Clipper c = new Clipper();
    c.AddPaths( polys, PolyType.ptSubject, true );
    c.Execute( ClipType.ctUnion, solution, PolyFillType.pftNonZero, PolyFillType.pftNonZero );

    List<Vector2> output = new List<Vector2>();
    foreach( var ply in solution )
    foreach( var point in ply )
      output.Add( new Vector2( ((float) point.X) / PRECISION, ((float) point.Y) / PRECISION ) );
    union = output.ToArray();
  }

#endregion
}