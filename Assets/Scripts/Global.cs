using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Dungbeetle;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.AI;
using Unity.AI.Navigation;
using TMPro;
using UnityEngine.Tilemaps;
using LitJson;
using SuperTiled2Unity;
using UnityEngine.Rendering;
using UnityEngine.Serialization;
// using Ionic.Zip;
// using UnityEngine.Localization;
// using UnityEngine.Localization.Components;
// using UnityEngine.Localization.Settings;
using JsonUtil = deeprest.JsonUtil;
using Random = UnityEngine.Random;
// using SerializedObject = deeprest.SerializedObject;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class Global : MonoBehaviour
{
#region Header

  public static Global instance;
  public static bool IsQuiting;

  public static RaycastHit2D[] RaycastHits = new RaycastHit2D[16];
  public static Collider2D[] ColliderResults = new Collider2D[32];

  [Header( "Global Settings" )]
  public BuildConfig BuildConfig;

  [Tooltip( "Pretend this is a build we're running" )]
  public bool SimulatePlayer;
  public int Seed;
  const int MAX_SEED = 20;
  public float UpdateCullDistance = 10;
  public int MaxUpdateCount = 100;
  public int MaxConsideredForUpdate = 10000;
  public const float PixelDensity = 16;
  public static float Gravity = 16;
  public const float MaxVelocity = 60;

  // timescale when in slo-mo
  [SerializeField] float slowtime = 0.2f;
  Timer fadeTimer = new Timer();
  public float RepathInterval = 0.5f;

  public static string[] persistentFilenames = new string[]
  {
    "settings.json"
  };

  [Header( "Settings" )]
  public GameObject ToggleTemplate;
  public GameObject SliderTemplate;
  public GameObject StringTemplate;
  public GameObject DividerTemplate;
  public GameObject ReadOnlyStringTemplate;

  string settingsPath { get { return Application.persistentDataPath + "/" + "settings.json"; } }

  public Dictionary<string, FloatValue> FloatSetting = new Dictionary<string, FloatValue>( System.StringComparer.OrdinalIgnoreCase );
  public Dictionary<string, BoolValue> BoolSetting = new Dictionary<string, BoolValue>( System.StringComparer.OrdinalIgnoreCase );
  public Dictionary<string, StringValue> StringSetting = new Dictionary<string, StringValue>( System.StringComparer.OrdinalIgnoreCase );
  // screen settings
  public GameObject ScreenSettingsPrompt;
  public TMP_Text ScreenSettingsCountdown;
  Timer ScreenSettingsCountdownTimer = new Timer();

  [Header( "References" )]
  public CameraController CameraController;
  [SerializeField] AudioClip VolumeChangeNoise;
  [SerializeField] CreditsScroller CreditsScroller;

  [Header( "Prefabs" )]
  public GameObject audioOneShotPrefab;
  public GameObject AvatarPrefab;
  public GameObject oneshotSpritePrefab;

  [Header( "Transient (Assigned at runtime)" )]
  public bool Updating;
  [ReadOnly] public SceneScript sceneScript;
  [ReadOnly] public Pawn CurrentPlayer;
  [FormerlySerializedAs( "FallbackPlayerControllerDef" ), SerializeField]
  ControllerDefinition PlayerControllerDef;
  [ReadOnly] public Controller PlayerController;

  public Dictionary<string, int> AgentType = new Dictionary<string, int>();
  NavMeshSurface[] meshSurfaces;

  [Header( "UI" )]
  public GameObject UI;
  [SerializeField] private DevConsole.DevConsoleUI devConsole;
  [SerializeField] TMP_Text BuildVersion;
  [SerializeField] TMP_Text BuildTimestamp;
  [SerializeField] TMP_Text CommitHash;
  /*LocalizeStringEvent QualityName;*/

  public GameObject PauseMenu;
  [SerializeField] UIScreen SceneList;
  public GameObject SceneListElementTemplate;
  [SerializeField] UIScreen MusicList;
  [SerializeField] BugReportUI BugReport;
  public GameObject MusicListElementTemplate;
  [SerializeField] GameObject HUD;
  /*[SerializeField] CanvasScaler CanvasScaler;*/
  /*DiegeticUI ActiveDiegetic;*/

  bool MenuShowing { get { return PauseMenu.activeInHierarchy; } }

  public GameObject LoadingScreen;
  [SerializeField] Image fader;
  public SpriteRenderer bossfade;
  [SerializeField] Image RecordingIndicator;
  // cursor
  public float CursorOuterOrtho = 1;
  public float CursorOuterPerspective = 4;
  public float CursorSensitivity = 1;
  public bool AimSnap;
  public bool AutoAim;
  public bool ShowAimPath;
  public bool ShowInputDisplay;
  // status
  public Image weaponIcon;
  public Image abilityIcon;
  // settings
  public GameObject SettingsParent;
  [SerializeField] Selectable previousNavSelectable;
  public UIScreen ConfirmDialog;
  public bool HealthAudioPitch;
  public bool HealthTimescale;

  [Header( "Input" )]
  public Controls Controls;
  public bool UsingGamepad;
  public System.Action OnGameControllerChanged;
  Dictionary<string, string> ReplaceControlNames = new Dictionary<string, string>();
  // input display
  /*[SerializeField] InputResponse InputDisplayOnCamera;*/

  [Header( "Debug" )]
  [SerializeField] GameObject debugParent;
  [SerializeField] TMP_Text debugFPS;
  [SerializeField] TMP_Text debugText1;
  [SerializeField] TMP_Text debugText2;
  [SerializeField] TMP_Text debugText3;
  [SerializeField] TMP_Text debugText4;
  [SerializeField] TMP_Text debugText5;

  [Header( "Misc" )]
  // color shift
  public Color shiftyColor = Color.red;
  [SerializeField] float colorShiftSpeed = 1;
  [SerializeField] Image shifty;
  Timer killAfterSongTimer = new Timer();

  /*[SerializeField] Team[] Teams;*/
  public DamageDefinition CrushDamageDefinition;

  // loading screen
  public bool LoadingScene;
  float prog = 0;
  [SerializeField] Image progress;
  Timer fpsTimer = new Timer();
  int frames;
  float zoomDelta = 0;

  // Resppawn
  Timer RespawnTimer = new Timer();
  [SerializeField] float RespawnCheckInterval = 5;
  int spawnCycleIndex = 0;

  [Header( "Audio" )]
  public UnityEngine.Audio.AudioMixer mixer;
  public UnityEngine.Audio.AudioMixerSnapshot snapSlowmo;
  public UnityEngine.Audio.AudioMixerSnapshot snapNormal;

  [Header( "Music" )]
  public AudioLoop[] Music;
  const float AudioFloor = -30;
  public float MusicTransitionDuration = 1f;
  // For audio mixer snapshots
  public float AudioFadeDuration = 0.1f;
  // Reserved for intro clips (Will replace with a decent plugin eventually "IntroClip" on asset store)
  /*[SerializeField] AudioSource musicSourceIntro;*/
  // These are used to crossfade between audio clips.
  [SerializeField] AudioSource musicSource0;
  [SerializeField] AudioSource musicSource1;
  AudioSource activeMusicSource;
  Timer musicCrossFadeTimer = new Timer();

  [Header( "Common / Global Refs" )]
  public GameObject explosion;
  public AudioClip SoundHit;
  /*public AudioLoop Victory;
  public AudioClip AwesomeSting;
  public AudioClip SoundReflect;
  public AudioClip SoundConfirm;
  public AudioClip SoundSelect;
  public AudioClip SoundDenied;
  public AudioClip SoundWeaponFail;*/

  /*[Header( "Minimap" )]
  [SerializeField] Camera MinimapCamera;
  [SerializeField] GameObject Minimap;
  [SerializeField] float MinimapScrollerScale = 4;
  RenderTexture rt;
  RenderTexture rt2;
  [SerializeField] int pixelsPerUnit = 1;
  [SerializeField] Material bigsheetMaterial;
  [SerializeField] Material backgroundMaterial;
  [SerializeField] GameObject MinimapScroller;
  [SerializeField] float mmScrollSpeed = 800;
  [Header( "Minimap Render" )]
  [SerializeField] Shader mmShader;
  [SerializeField] Color[] mmColor;
  [SerializeField] RectTransform mmPlayer;
  [SerializeField] GameObject mmCharacterLayer;*/

#endregion

  // This will make sure there is always a GLOBAL object when playing a scene in the editor
  [RuntimeInitializeOnLoadMethod]
  static void OnLoadMethod()
  {
    Application.wantsToQuit += WantsToQuit;
#if UNITY_EDITOR
    EditorApplication.playModeStateChanged += OnPlayModeChange;
#endif
  }
#if UNITY_EDITOR
  static void OnPlayModeChange( PlayModeStateChange pmcs )
  {
    /*if( pmcs == PlayModeStateChange.ExitingPlayMode )
    {
      Global.instance.SetSkin( 0 );
    }*/
  }
#endif

  static bool WantsToQuit()
  {
    IsQuiting = true;
    // do pre-quit stuff here
    if( Global.instance != null )
      Global.instance.WriteSettings();

#if UNITY_EDITOR
    EditorApplication.playModeStateChanged -= OnPlayModeChange;
#endif
    return true;
  }

  public static void CustomLogger( string condition, string stackTrace, LogType type )
  {
    if( Global.instance == null )
      return;
    switch( type )
    {
      case LogType.Error:
        Global.instance.devConsole.LogError( condition + "\n" + stackTrace );
        break;

      case LogType.Exception:
        Global.instance.devConsole.LogError( condition + "\n" + stackTrace );
        break;

      case LogType.Assert:
        Global.instance.devConsole.LogError( condition + "\n" + stackTrace );
        break;

      case LogType.Warning:
        Global.instance.devConsole.LogWarning( condition ); /*+"\n"+ stackTrace );*/
        break;

      case LogType.Log:
        Global.instance.devConsole.Log( condition );
        break;
    }
  }

  void OnApplicationFocus( bool hasFocus )
  {
    if( hasFocus )
      Cursor.lockState = /*ActiveDiegetic ||*/ Paused ? CursorLockMode.None : CursorLockMode.Locked;
    else
      Cursor.lockState = CursorLockMode.None;
    Cursor.visible = (Cursor.lockState == CursorLockMode.None);
  }

  // moved these to be closer to their definitions
  public static int CharacterCollideLayers;
  public static int CharacterCollideLayersNoTwoWayPlatform;
  public static int CharacterDamageLayers;
  public static int CrushDamageLayers;
  public static int TriggerLayers;
  public static int WorldSelectableLayers;
  public static int ProjectileNoShootLayers;
  public static int DefaultProjectileCollideLayers;
  public static int FlameProjectileCollideLayers;
  public static int DamageCollideLayers;
  public static int StickyBombCollideLayers;
  public static int EnemyInterestLayers;
  public static int SightObstructionLayers;

  void Awake()
  {
    if( instance != null )
    {
      Destroy( gameObject );
      return;
    }
    instance = this;
    DontDestroyOnLoad( gameObject );

    // note: allowing characters to collide with each other introduces the risk of being forced into a corner.
    CharacterCollideLayers = LayerMask.GetMask( "Default", "pit", "destructible" ); //, "triggerAndCollision", "twowayPlatform", "entityCollisionOnly" );*/
    CharacterCollideLayersNoTwoWayPlatform = CharacterCollideLayers & ~LayerMask.GetMask( "twowayPlatform" );
    CharacterDamageLayers = LayerMask.GetMask( "entity", "destructible" );
    CrushDamageLayers = CharacterCollideLayersNoTwoWayPlatform | LayerMask.GetMask( "entity" );
    TriggerLayers = LayerMask.GetMask( "trigger", "triggerAndCollision", "pit" );
    WorldSelectableLayers = LayerMask.GetMask( "worldselect", "entity" );
    ProjectileNoShootLayers = LayerMask.GetMask( "Default" );
    DefaultProjectileCollideLayers = LayerMask.GetMask( "Default", "entity", "projectileCollide", "triggerAndCollision", "destructible" );
    FlameProjectileCollideLayers = LayerMask.GetMask( "Default", "entity", "triggerAndCollision", "destructible" );
    DamageCollideLayers = LayerMask.GetMask( "entity", "triggerAndCollision", "projectile", "destructible" );
    StickyBombCollideLayers = LayerMask.GetMask( "Default", "entity", "triggerAndCollision", "projectileCollide", "destructible" );
    EnemyInterestLayers = LayerMask.GetMask( "entity", "projectileCollide", "destructible" );
    SightObstructionLayers = LayerMask.GetMask( "Default", "triggerAndCollision" ); /*, "destructible"} );*/

    /*CanvasScaler = UI.GetComponent<CanvasScaler>();*/
    BuildVersion.text = GeneratedBuildInfo.BuildVersion;
    BuildTimestamp.text = GeneratedBuildInfo.BuildTimestamp;
    CommitHash.text = GeneratedBuildInfo.CommitHash;

    activeMusicSource = musicSource0;

    // intialize input before settings so that Controls exists for the Input Display objects
    InitializeInput();
    InitializeSettings();
    /*VerifyPersistentData();*/
    ReadSettings();
    UI.SetActive( true );
    ApplyScreenSettings();
    /*IntializeViewPortals();*/

    /*if( BoolSetting["RandomSeed"].Value )
      Seed = System.DateTime.Now.Second;
    else
      Seed = Mathf.RoundToInt( FloatSetting["Seed"].Value ) % MAX_SEED;*/
    Random.InitState( Seed );

    // SCRIPT EXECUTION ORDER Global.cs is first priority so that Awake called from scene load in editor respects the code below.
    //Entity.Limit.UpperLimit = 1000;
    Entity.Limit.EnforceUpper = false;

    GUI.enabled = false;
#if UNITY_STANDALONE
    Application.logMessageReceived += CustomLogger;
    // show unhandled excpetions on the dev console 
    System.AppDomain.CurrentDomain.UnhandledException += ( sender, args ) => devConsole.LogError( sender.ToString() + args.ToString() );
#endif
    SceneManager.sceneLoaded += delegate( Scene arg0, LoadSceneMode arg1 ) { Debug.Log( "scene loaded: " + arg0.name ); };
    /*SceneManager.activeSceneChanged += delegate( Scene arg0, Scene arg1 ) { Debug.Log( "active scene changed from " + arg0.name + " to " + arg1.name ); };*/

    meshSurfaces = FindObjectsOfType<NavMeshSurface>();
    foreach( var mesh in meshSurfaces )
      AgentType[NavMesh.GetSettingsNameFromID( mesh.agentTypeID )] = mesh.agentTypeID;

    TimerParams fpsTimerParams = new TimerParams();
    fpsTimerParams.unscaledTime = true;
    fpsTimerParams.repeat = true;
    fpsTimerParams.loops = int.MaxValue;
    fpsTimerParams.interval = 1;
    fpsTimerParams.IntervalDelegate = delegate( Timer tmr )
    {
      debugFPS.text = frames.ToString();
      frames = 0;
    };
    fpsTimer.Start( fpsTimerParams );

    RecordingIndicator.gameObject.SetActive( false );

    HideHUD();
    /*HideMinimap();*/
    PauseMenu.SetActive( false );
    HideLoadingScreen();

    /*DefaultConversationOptions = new ConversationOptions()
    {
      ShowBars = true,
      AllowPlayerMovement = true,
      CancelIfBeyondRange = true,
      CancelRange = 2,
      OnCancel = null,
      OnComplete = null,
      OnConclusion = null
    };
    SpeechBubble.SetActive( false );
    SpeechTextWorld.gameObject.SetActive( false );*/

    BugReport.gameObject.SetActive( false );
#if UNITY_EDITOR || DEVELOPMENT_BUILD
    Popcron.Gizmos.Enabled = false;
#endif
  }

  void Start()
  {
#if UNITY_EDITOR
    // workaround for Unity Editor_bug where AudioMixer.SetFloat() does not work in Awake()
    mixer.SetFloat( "MasterVolume", Util.DbFromNormalizedVolume( FloatSetting["MasterVolume"].Value, AudioFloor ) );
    mixer.SetFloat( "MusicVolume", Util.DbFromNormalizedVolume( FloatSetting["MusicVolume"].Value, AudioFloor ) );
    mixer.SetFloat( "SFXVolume", Util.DbFromNormalizedVolume( FloatSetting["SFXVolume"].Value, AudioFloor ) );
#endif

    if( Application.isEditor && !SimulatePlayer )
    {
      LoadingScreen.SetActive( false );
      fader.color = Color.clear;
      Updating = true;
      sceneScript = FindAnyObjectByType<SceneScript>();
      if( sceneScript != null )
        sceneScript.StartScene();
      else if( CurrentPlayer == null )
        SpawnPlayer();
      /*GenerateNavMesh();*/
    }
    else
    {
      LoadScene( BuildConfig.InitialScene,
        new LoadSceneOptions() { WaitForFadeIn = false, SpawnPlayer = false, FadeOut = true, ShowLoadingScreen = false },
        ( success ) =>
        {
          if( !success )
            Debug.LogError( "initial scene load failed!" );
        } );
    }
  }

  public string ReplaceFormattedStringWithControlNames( string source, bool colorize, Color controlNameColor )
  {
    // This is inefficient. DO NOT call this every frame.
    string outstr = "";
    string[] tokens = source.Split( '[' );
    foreach( var tok in tokens )
    {
      if( tok.Contains( "]" ) )
      {
        string[] ugh = tok.Split( ']' );
        if( ugh.Length > 2 )
          return "BAD FORMAT";
        InputAction ia = Controls.BipedActions.Get().FindAction( ugh[0] );
        if( ia == null )
          ia = Controls.MenuActions.Get().FindAction( ugh[0] );
        if( ia == null )
          ia = Controls.GlobalActions.Get().FindAction( ugh[0] );
        if( ia == null )
          ia = Controls.SpeechActions.Get().FindAction( ugh[0] );
        if( ia == null )
          ia = Controls.DevActions.Get().FindAction( ugh[0] );
        if( ia == null )
          return "(null): " + ugh[0];

        // NOTE
        // Some keys return an empty display name!!! (F1, tab)
        // Unity says they have fixed this, but they have not:
        // https://issuetracker.unity3d.com/issues/macos-input-system-getbindingdisplaystring-returns-empty-strings-for-some-mappings

        string controlName = ia.GetBindingDisplayString( group: "KeyboardScheme" );
        if( string.IsNullOrWhiteSpace( controlName ) || controlName[0] == 16 )
          controlName = GetBindingDisplayString( ia, InputBinding.MaskByGroups( "KeyboardScheme" ) );
        if( string.IsNullOrWhiteSpace( controlName ) || controlName[0] == 16 )
        {
          InputControl control = ia.controls.FirstOrDefault();
          controlName = control.shortDisplayName ?? control.name.ToUpper();
        }
        if( ReplaceControlNames.ContainsKey( controlName.ToUpper() ) )
          controlName = ReplaceControlNames[controlName.ToUpper()];

        if( colorize )
          outstr += "<color=#" + ColorUtility.ToHtmlStringRGB( controlNameColor ) + ">" + controlName + "</color>";
        else
          outstr += controlName;
        outstr += ugh[1];
      }
      else
        outstr += tok;
    }
    return outstr;
  }

  string GetBindingDisplayString( InputAction action, InputBinding bindingMask, InputBinding.DisplayStringOptions options = default )
  {
    var bindings = action.bindings;
    for( var i = 0; i < bindings.Count; ++i )
    {
      if( bindings[i].isPartOfComposite )
        continue;
      if( !bindingMask.Matches( bindings[i] ) )
        continue;
      var text = action.GetBindingDisplayString( i, out string _, out string controlPath, options );
      if( string.IsNullOrWhiteSpace( text ) )
        return controlPath.ToUpper();
      else
        return text;
    }
    return null;
  }

  void InitializeInput()
  {
    InputSystem.onDeviceChange += ( device, change ) =>
    {
      switch( change )
      {
        case InputDeviceChange.Added:
          Debug.Log( "Device added: " + device );
          break;

        case InputDeviceChange.Removed:
          Debug.Log( "Device removed: " + device );
          break;

        case InputDeviceChange.ConfigurationChanged:
          Debug.Log( "Device configuration changed: " + device );
          break;
      }
    };

    ReplaceControlNames.Add( "DELTA", "Mouse" );
    /*ReplaceControlNames.Add( "LMB", "Left Mouse Button" );
    ReplaceControlNames.Add( "RMB", "Right Mouse Button" );*/
    ReplaceControlNames.Add( "LB", "Left Bumper" );
    ReplaceControlNames.Add( "RB", "Right Bumper" );
    ReplaceControlNames.Add( "LT", "Left Trigger" );
    ReplaceControlNames.Add( "RT", "Right Trigger" );

    Controls = new Controls();
    Controls.Enable();
    Controls.MenuActions.Disable();
    Controls.SpeechActions.Disable();

    /*Controls.GlobalActions.Any.performed += ( obj ) =>
    {
      bool newvalue = obj.control.device.name.Contains( "Gamepad" );
      if( newvalue != UsingGamepad )
      {
        UsingGamepad = newvalue;
        OnGameControllerChanged.Invoke();
        OnboardingControls.GetComponent<OnboardingControls>().UpdateText();
      }
    };*/

    Controls.GlobalActions.Quit.performed += ( obj ) => Application.Quit();
    Controls.GlobalActions.Menu.performed += ( obj ) => TogglePauseMenu();
    Controls.DevActions.DEVPause.performed += ( obj ) =>
    {
      if( Paused )
        Unpause();
      else
        Pause();
    };

    Controls.GlobalActions.ConsoleToggle.performed += context =>
    {
      if( !devConsole.gameObject.activeSelf )
      {
        devConsole.Activate();
        Controls.BipedActions.Disable();
        Controls.MenuActions.Disable();
        Controls.DevActions.Disable();
      }
      else
      {
        devConsole.Deactivate();
        Controls.BipedActions.Enable();
        Controls.MenuActions.Enable();
        Controls.DevActions.Enable();
      }
    };
    Controls.GlobalActions.Screenshot.performed += ( obj ) => Util.Screenshot();

    /*Controls.DevActions.DEVRecordToggle.performed += ( obj ) =>
    {
      PlayerController.RecordToggle();
      RecordingIndicator.gameObject.SetActive( PlayerController.IsRecording() );
    };

    Controls.DevActions.DEVRecordPlayback.performed += ( obj ) =>
    {
      Controller controller = Controller.All.Find( x => x is AIController );
      if( controller != null )
        (controller as AIController).PlaybackBegin( PlayerController.recordHeader, PlayerController.recordFrameBuffer );
      /*PlayerController.PlaybackToggle();#1#
      RecordingIndicator.gameObject.SetActive( PlayerController.IsRecording() );
    };*/

    Controls.MenuActions.Back.performed += ( obj ) =>
    {
      if( !MenuShowing && /*ActiveDiegetic != null &&*/ CurrentPlayer != null )
        CurrentPlayer.UnselectWorldSelection();
    };

    // DEVELOPMENT
    Controls.DevActions.DEVGizmos.performed += ( obj ) =>
    {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
      Popcron.Gizmos.Enabled = !Popcron.Gizmos.Enabled;
#endif
    };
    Controls.DevActions.DEVRespawn.performed += ( obj ) => { Respawn(); };
    Controls.DevActions.DEVGhostToggle.performed += ( obj ) =>
    {
      if( ghostModeCachedPawn )
        DisableGhostMode();
      else
        EnableGhostMode();
    };
    Controls.DevActions.DEVSlow.performed += ( obj ) =>
    {
      if( Slowed )
        NoSlow();
      else
        Slow();
    };

    Controls.DevActions.DEVSave.performed += ( obj ) => { SaveGame( "SAVETEST" ); };
    Controls.DevActions.DEVLoad.performed += ( obj ) => { LoadGame( "SAVETEST" ); };

#if UNITY_WEBGL
    Controls.GlobalActions.Menu.AddBinding( "<Keyboard>/tab" );
#elif UNITY_STANDALONE
      InputAction DevCursorUnlock = new InputAction( "CursorUnlock", InputActionType.Button, "<Keyboard>/tab" );
      DevCursorUnlock.Enable();
      DevCursorUnlock.performed += context =>
      {
        /*if( Cursor.lockState != CursorLockMode.None )
        {*/
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        /*}
        else
        {
          Cursor.lockState = CursorLockMode.Locked;
          Cursor.visible = false;
        }*/
      };
    
      Controls.GlobalActions.Menu.AddBinding( "<Keyboard>/escape" );
      Controls.GlobalActions.Menu.AddBinding( "<Keyboard>/tab" );
      Controls.GlobalActions.BugReport.performed += context => { ShowBugReport(); };
#elif UNITY_EDITOR
    InputAction DevCursorUnlock = new InputAction( "CursorUnlock", InputActionType.Button, "<Keyboard>/escape" );
      DevCursorUnlock.Enable();
      DevCursorUnlock.performed += context =>
      {
        if( Cursor.lockState != CursorLockMode.None )
        {
          Cursor.lockState = CursorLockMode.None;
          Cursor.visible = true;
        }
        else
        {
          Cursor.lockState = CursorLockMode.Locked;
          Cursor.visible = false;
        }
      };
      Controls.GlobalActions.Menu.AddBinding( "<Keyboard>/tab" );
    Controls.GlobalActions.BugReport.performed += context => { ShowBugReport(); };
#endif

    /*Controls.SpeechActions.Continue.performed += context => { SpeakContinue(); };*/
    /*Controls.SpeechActions.End.performed += context => { SpeakCancel(); };*/
  }

  public class LoadSceneOptions
  {
    public bool WaitForFadeIn = true;
    public bool SpawnPlayer = false;
    public bool FadeOut = true;
    public bool ShowLoadingScreen = true;
  }

  public delegate void LoadSceneCallback( bool success );


  public void LoadScene( SceneReference scene, LoadSceneOptions options, LoadSceneCallback callback ) { StartCoroutine( LoadSceneRoutine( scene.GetSceneName(), options, callback ) ); }

  public void LoadScene( string scene, LoadSceneOptions options, LoadSceneCallback callback ) { StartCoroutine( LoadSceneRoutine( scene, options, callback ) ); }

  IEnumerator LoadSceneRoutine( string scene, LoadSceneOptions options, LoadSceneCallback callback )
  {
    Updating = false;
    if( options == null )
      options = new LoadSceneOptions();
    if( options.FadeOut )
    {
      FadeBlack();
      while( fadeTimer.IsActive )
        yield return null;
    }
    else
    {
      fader.color = Color.black;
      fader.gameObject.SetActive( true );
      yield return null;
    }

    Pause();
    HideHUD();
    if( options.ShowLoadingScreen )
      yield return ShowLoadingScreenRoutine( "Loading... " + scene );

    // pre scene transition
    if( CurrentPlayer != null )
    {
      CurrentPlayer.PreSceneTransition();
      SceneManager.MoveGameObjectToScene( CurrentPlayer.gameObject, gameObject.scene );
      // prevent portal cameras from being destroyed along with WarpDoors.
      /*foreach( var portal in portals )
        DoneWithViewPortal( portal );*/
    }
    // reaffirm current camera settings in case they were left in an undesired state
    /*CameraController.PerspectiveExperiment = BoolSetting["PerspectiveExperiment"].Value;
    CameraController.orthoTarget = FloatSetting["Zoom"].Value;*/
    CameraController.cam.orthographicSize = CameraController.orthoTarget;

    CameraController.PreSceneTransition();
    for( int i = 0; i < Controller.All.Count; i++ )
      Controller.All[i].PreSceneTransition();

    /*SpeechTimer.Stop( true );
    SpeechContinueTimer.Stop( false );*/

    LoadingScene = true;
    progress.fillAmount = 0;
    AsyncOperation asyncOperation = SceneManager.LoadSceneAsync( scene, LoadSceneMode.Single );
    if( asyncOperation != null )
    {
      while( !asyncOperation.isDone )
      {
        progress.fillAmount = asyncOperation.progress;
        yield return null;
      }

      for( int i = 0; i < Controller.All.Count; i++ )
        Controller.All[i].PostSceneTransition();

      if( CurrentPlayer != null )
      {
        CurrentPlayer.transform.position = FindBestSpawnPosition();
        CurrentPlayer.PostSceneTransition();
        ShowHUD();
      }
      else if( options.SpawnPlayer )
      {
        SpawnPlayer();
        ShowHUD();
      }

      // Scene Script should start it's music.
      sceneScript = FindFirstObjectByType<SceneScript>();
      if( sceneScript )
        sceneScript.StartScene();

      /*GenerateNavMesh();*/
      callback?.Invoke( true );
    }
    else
    {
      Debug.LogError( "Scene failed to load: " + scene );
      callback?.Invoke( false );
    }

    LoadingScene = false;
    // if scene load fails, allow normal play to continue
    progress.fillAmount = 1;
    yield return null;
    Unpause();
    if( options.ShowLoadingScreen )
      HideLoadingScreen();
    FadeClear();
    if( options.WaitForFadeIn )
      while( fadeTimer.IsActive )
        yield return null;
    Updating = true;
  }

#region NavMesh

  /*public GameObject GetNavMeshObject() { return meshSurfaces[0].gameObject; }*/

  /*[ExposeMethod]
  public void GenerateNavMesh()
  {
    if( Application.isEditor && !Application.isPlaying )
    {
      meshSurfaces = FindObjectsOfType<NavMeshSurface>();
      foreach( var mesh in meshSurfaces )
        AgentType[NavMesh.GetSettingsNameFromID( mesh.agentTypeID )] = mesh.agentTypeID;
    }
    GameObject generatedMeshCollider = Util.GenerateNavMeshForEdgeColliders();
    foreach( var mesh in meshSurfaces )
      mesh.BuildNavMesh();
    Util.Destroy( generatedMeshCollider );
  }*/


  /*[ExposeMethod]
  public void GenerateNavMesh()
  {
    if( Application.isEditor && !Application.isPlaying )
    {
      meshSurfaces = FindObjectsOfType<NavMeshSurface>();
      /*foreach( var mesh in meshSurfaces )
        AgentType[NavMesh.GetSettingsNameFromID( mesh.agentTypeID )] = mesh.agentTypeID;#1#
    }
    if( sceneScript == null )
      sceneScript = FindObjectOfType<SceneScript>();
    if( sceneScript != null )
      sceneScript.GenerateNavSurface();

    /*GameObject generatedMeshCollider = Util.GenerateNavMeshForEdgeColliders();#1#
    /*foreach( var mesh in meshSurfaces )
      mesh.BuildNavMesh();#1#
    /*Util.Destroy( generatedMeshCollider );#1#
  }*/


  /*void GenerateNavmeshForTilemap( List<NavMeshBuildSource> sources, Tilemap tilemap )
  {
    // the sources carve out the navmesh
    if( tilemap != null )
    {
      NavMeshBuildSource navMeshBuildSource = new NavMeshBuildSource();
      navMeshBuildSource.size = Vector3.one;
      tilemap.CompressBounds();
      for( int x = tilemap.cellBounds.xMin; x < tilemap.cellBounds.xMax; x++ )
      {
        for( int y = tilemap.cellBounds.yMin; y < tilemap.cellBounds.yMax; y++ )
        {
          Vector3Int posint = new Vector3Int( x, y, 0 );
          bool valid = tilemap.HasTile( posint );
          if( valid )
          {
            TileBase tilebase = tilemap.GetTile( posint );
            SuperTiled2Unity.SuperTile tile = tilebase as SuperTiled2Unity.SuperTile;
            if( tile == null)
              valid = false;
            else
            {
              SuperTiled2Unity.CustomProperty prop = tile.m_CustomProperties.Find( ugh => ugh.m_Name == "ground" );
              if( prop == null )
              {
                valid = false;
              }
            }
          }

          if( valid )
          {
            //TileData tiledata = new TileData(); tilebase.GetTileData( posint, null, ref tiledata );
            navMeshBuildSource.component = tilemap;
            navMeshBuildSource.shape = NavMeshBuildSourceShape.Box;
            navMeshBuildSource.size = Vector3.one * (tilemap.cellSize.x + 0.05f);
            navMeshBuildSource.transform = GetCellTransformMatrix( tilemap, Vector3.one, posint );
            sources.Add( navMeshBuildSource );
          }
        }
      }
    }
  }*/

#endregion

  public static Matrix4x4 GetCellTransformMatrix( Tilemap tilemap, Vector3 scale, Vector3Int vec3int ) { return Matrix4x4.TRS( Vector3.Scale( tilemap.GetCellCenterWorld( vec3int ), scale ) - tilemap.layoutGrid.cellGap, tilemap.transform.rotation, tilemap.transform.lossyScale ) * tilemap.orientationMatrix * tilemap.GetTransformMatrix( vec3int ); }

  void LateUpdate()
  {
    frames++;
    Timer.UpdateTimers();
    if( Camera.main )
      debugText1.text = Camera.main.orthographicSize.ToString( "##.#" );
    debugText2.text = "Active Timers: " + Timer.ActiveTimers.Count;
    debugText3.text = "Remove Timers: " + Timer.RemoveTimers.Count;
    debugText4.text = "New Timers: " + Timer.NewTimers.Count;

    if( !Updating )
      return;

    if( Paused )
    {
      float H;
      Color.RGBToHSV( shiftyColor, out H, out _, out _ );
      H += colorShiftSpeed * Time.unscaledDeltaTime;
      shiftyColor = Color.HSVToRGB( H, 1, 1 );
      shifty.color = shiftyColor;
    }
    else
    {
      if( sceneScript != null )
        sceneScript.UpdateScene();

      for( int i = 0; i < Controller.All.Count; i++ )
        Controller.All[i].Update();

      // update all entities within the cull radius
      Vector3 ppos = Camera.main.transform.position;
      Entity reference;
      int updatedCount = 0;
      int entityLimitCount = Mathf.Min( Entity.Limit.All.Count, MaxConsideredForUpdate );

      // todo try this after static objects in level have loaded.
      /*if( BoolSetting["ExperimentalEntitySort"].Value )
      {
        Profiler.BeginSample( "update SORT" );
        Entity.Limit.All.Sort( ( a, b ) => { return a.transform.position.y < b.transform.position.y ? -1 : 1; } );
        Profiler.EndSample();
      }*/

      float sqrUpdateCullDistance = UpdateCullDistance * UpdateCullDistance;
      for( int i = 0; updatedCount < MaxUpdateCount && i < entityLimitCount; i++ )
      {
        reference = Entity.Limit.All[i];
        reference.culled = Vector3.SqrMagnitude( reference.transform.position - ppos ) > sqrUpdateCullDistance;
        if( (reference.IgnoreCull || !reference.culled) && reference.gameObject.activeInHierarchy && reference.enabled )
        {
          reference.EntityUpdate();
          updatedCount++;
        }
      }

      debugText5.text = updatedCount.ToString();

      /*WarpDoor warpDoor;
      for( int i = 0; i < WarpDoor.All.Count; i++ )
      {
        warpDoor = WarpDoor.All[i];
        warpDoor.culled = Vector3.SqrMagnitude( warpDoor.transform.position - ppos ) > UpdateCullDistance * UpdateCullDistance;
        if( (warpDoor.IgnoreCull || !warpDoor.culled) && warpDoor.gameObject.activeInHierarchy && warpDoor.enabled )
          warpDoor.WarpDoorUpdate();
      }*/

      /*IViewWindow viewWindow;
      for( int i = 0; i < ViewWindows.Count; i++ )
      {
        viewWindow = ViewWindows[i];
        viewWindow.culled = Vector2.SqrMagnitude( viewWindow.position - (Vector2)ppos ) > UpdateCullDistance * UpdateCullDistance;
        if( (viewWindow.IgnoreCull || !viewWindow.culled) )
          viewWindow.UpdateViewWindow();
      }*/

      if( !CurrentPlayer )
      {
        if( !RespawnTimer.IsActive )
          RespawnTimer.Start( RespawnCheckInterval, null, () =>
          {
            // double check to avoid spawning two players.
            if( !CurrentPlayer )
              Global.instance.SpawnPlayer();
          } );
      }
    }

    CameraController.CameraLateUpdate();

    /*if( Minimap.activeInHierarchy )
    {
      MinimapScroller.transform.position += (Vector3) (-Controls.MenuActions.Move.ReadValue<Vector2>() * mmScrollSpeed * Time.unscaledDeltaTime);
      mmPlayer.anchoredPosition = pixelsPerUnit * MinimapCamera.worldToCameraMatrix.MultiplyPoint( PlayerController.pawn.transform.position );
    }*/
  }

  /*void LateUpdate() { CameraController.CameraLateUpdate(); }*/

  public void SpawnPlayer() { SpawnPlayer( FindBestSpawnPosition() ); }

  public void SpawnPlayer( Vector3 position )
  {
    GameObject go = Spawn( AvatarPrefab, position, Quaternion.identity, null, false );
    AssignCurrentPlayerAs( go.GetComponent<Pawn>() );
  }

  public void AssignCurrentPlayerAs( Pawn pawn )
  {
    if( FinalKillSequence )
      EndFinalKillSequence();

    // todo give current pawn it's previous controller back

    ShowHUD();
    if( PlayerController == null )
      PlayerController = PlayerControllerDef.GetNewController();
    PlayerController.AssignPawn( pawn );
    // convenience
    CurrentPlayer = pawn;
  }

  [ExposeMethod]
  public void Respawn()
  {
    if( PlayerController.pawn )
    {
      if( !sceneScript )
        RespawnAt( sceneScript.FindSpawnPosition() );
      else
        RespawnAt( CycleSpawnPosition() );
    }
    else
    {
      SpawnPlayer( CycleSpawnPosition() );
    }
  }

  public void RespawnAt( Vector2 pos )
  {
    if( PlayerController.pawn )
    {
      PlayerController.pawn.transform.position = pos;
      AssignCurrentPlayerAs( PlayerController.pawn );
    }
    else
      Respawn();
  }

  public Vector3 FindBestSpawnPosition()
  {
    GameObject go = null;
    go = GameObject.FindGameObjectWithTag( "FirstSpawn" );
    if( go == null )
      go = GameObject.FindGameObjectWithTag( "Respawn" );
    if( go != null )
      return go.transform.position;
    return Vector3.zero;
  }

  public Vector3 CycleSpawnPosition()
  {
    // cycle through random spawn points
    GameObject go = null;
    List<GameObject> gos = new List<GameObject>();
    gos.AddRange( GameObject.FindGameObjectsWithTag( "Respawn" ) );
    gos.AddRange( GameObject.FindGameObjectsWithTag( "FirstSpawn" ) );
    GameObject[] spawns = gos.ToArray();
    if( spawns.Length > 0 )
    {
      spawnCycleIndex %= spawns.Length;
      go = spawns[spawnCycleIndex++];
    }
    if( go != null )
    {
      Vector3 pos = go.transform.position;
      pos.z = 0;
      return pos;
    }
    return Vector3.zero;
  }

#region Time

  [Header( "Time" )]
  public static bool Paused;
  public static bool Slowed;
  public float CurrentTimescale = 1;

  public void Slow()
  {
    Slowed = true;
    Time.timeScale = slowtime;
    Time.fixedDeltaTime = 0.02f * Time.timeScale;
    mixer.TransitionToSnapshots( new UnityEngine.Audio.AudioMixerSnapshot[] { snapNormal, snapSlowmo }, new float[] { 0, 1 }, AudioFadeDuration );
  }

  public void NoSlow()
  {
    Slowed = false;
    Time.timeScale = CurrentTimescale;
    Time.fixedDeltaTime = 0.02f * Time.timeScale;
    mixer.TransitionToSnapshots( new UnityEngine.Audio.AudioMixerSnapshot[] { snapNormal, snapSlowmo }, new float[] { 1, 0 }, AudioFadeDuration );
  }

  public void Pause()
  {
    Paused = true;
    Time.timeScale = 0;
  }

  public void Unpause()
  {
    Paused = false;
    Time.timeScale = CurrentTimescale;
  }

#endregion

  public void ShowHUD() { HUD.SetActive( true ); }

  public void HideHUD() { HUD.SetActive( false ); }

#region BugReport

  public void ShowBugReport()
  {
    Pause();
    Controls.DevActions.Disable();
    Controls.MenuActions.Disable();
    Controls.BipedActions.Disable();
    Cursor.lockState = CursorLockMode.None;
    Cursor.visible = true;
    EnableRaycaster( true );
    StartCoroutine( BeginBugReport() );
  }

  IEnumerator BeginBugReport()
  {
    var attachments = new Dungbeetle.AttachmentCollection();
    attachments.AddString( "Example attachment (could be a saved game, settings etc.)." );
    attachments.AddString( "Yet another example attachment..." );

    ScreenshotSource screenshotSource = new ScreenshotSource();
    yield return screenshotSource.Wait( 5f ); // Wait for screenshot to be written to file, or time out.
    if( screenshotSource.Ready )
      yield return attachments.Wait(); // Wait for log to be read from file (don't time out, the length is capped anyway).

    if( !screenshotSource.Ready )
    {
      Debug.LogError( "The screenshot couldn't be read from file." );
      yield break;
    }
    else if( !attachments.Ready )
    {
      Debug.LogError( "The attachments couldn't be read from file." );
      yield break;
    }
    BugReport.Launch( SceneManager.GetActiveScene().name, Dungbeetle.BuildNameArk.BuildName, attachments, screenshotSource );
  }

  public void HideBugReport()
  {
    BugReport.Unselect();
    Unpause();
    // NOTE copied from HidePauseMenu
    EnableRaycaster( false );
    /*if( ActiveDiegetic != null )
    {
      ActiveDiegetic.InteractableOn();
      Cursor.lockState = CursorLockMode.None;
      Cursor.visible = true;
    }
    else*/
    {
      Global.instance.Controls.BipedActions.Enable();
      Controls.MenuActions.Disable();
      Cursor.lockState = CursorLockMode.Locked;
      Cursor.visible = false;
    }
    if( CurrentPlayer != null )
      Controls.BipedActions.Enable();
    Controls.DevActions.Enable();
  }

#endregion

  void ShowPauseMenu()
  {
    if( ScreenSettingsCountdownTimer.IsActive )
      return;

    // Anything not in the pause menu, deactivate here 
    BugReport.gameObject.SetActive( false );
    /*Minimap.SetActive( false );*/

    Pause();
    mixer.SetFloat( "MusicVolume", Util.DbFromNormalizedVolume( FloatSetting["MusicVolume"].Value * 0.8f, AudioFloor ) );
    mixer.SetFloat( "SFXVolume", Util.DbFromNormalizedVolume( FloatSetting["SFXVolume"].Value * 0.8f, AudioFloor ) );
    /*if( ActiveDiegetic )
    {
      ActiveDiegetic.InteractableOff();
    }
    else*/
    {
      Controls.MenuActions.Enable();
    }
    PauseMenu.SetActive( true );
    HideHUD();
    Cursor.lockState = CursorLockMode.None;
    Cursor.visible = true;
    EnableRaycaster( true );

    if( BoolSetting["EnablePauseSilence"].Value )
    {
      activeMusicSource.Pause();
      musicSource0.Pause();
      musicSource1.Pause();
    }
  }

  void HidePauseMenu()
  {
    if( ScreenSettingsCountdownTimer.IsActive )
      return;
    /*if( !conversationActive || !CurrentConversationOptions.Pause )*/
    Unpause();
    PauseMenu.SetActive( false );
    // Without this check, the intro scene shows the hud after loading.
    if( CurrentPlayer )
      ShowHUD();
    EnableRaycaster( false );
    mixer.SetFloat( "MusicVolume", Util.DbFromNormalizedVolume( FloatSetting["MusicVolume"].Value, AudioFloor ) );
    mixer.SetFloat( "SFXVolume", Util.DbFromNormalizedVolume( FloatSetting["SFXVolume"].Value, AudioFloor ) );

    /*if( ActiveDiegetic )
    {
      ActiveDiegetic.InteractableOn();
      Controls.MenuActions.Enable();
      Cursor.lockState = CursorLockMode.None;
      Cursor.visible = true;
    }
    else*/
    {
      Controls.MenuActions.Disable();
      Cursor.lockState = CursorLockMode.Locked;
      Cursor.visible = false;
    }
#if UNITY_WEBGL && !UNITY_EDITOR
      // cannot write settings on exit in webgl builds, so write them here
      WriteSettings();
#endif

    if( BoolSetting["EnablePauseSilence"].Value )
    {
      activeMusicSource.UnPause();
      musicSource0.UnPause();
      musicSource1.UnPause();
    }
  }

  void TogglePauseMenu()
  {
    if( MenuShowing )
      HidePauseMenu();
    else
      ShowPauseMenu();
  }

#region Diegetic

#if false
  public void DiegeticMenuOn( DiegeticUI dui )
  {
    ActiveDiegetic = dui;
    Controls.BipedActions.Disable();
    Controls.MenuActions.Enable();
    OverrideCameraZone( dui.CameraZone );
    Cursor.lockState = CursorLockMode.None;
    Cursor.visible = true;
  }

  public void DiegeticMenuOff()
  {
    ActiveDiegetic = null;
    Controls.MenuActions.Disable();
    Controls.BipedActions.Enable();
    OverrideCameraZone( null );
    Cursor.lockState = CursorLockMode.Locked;
    Cursor.visible = false;
  }
#endif

#endregion

#region Minimap

#if false
  public void ToggleMinimap()
  {
    if( Minimap.activeInHierarchy )
      HideMinimap();
    else
      ShowMinimap();
  }

  public void ShowMinimap()
  {
    // TEMP
    MinimapRender( sceneScript.bounds );
    //

    Minimap.SetActive( true );
    Controls.BipedActions.Disable();
    // exclusion to turn minimap off
    Controls.BipedActions.Minimap.Enable();
    Controls.MenuActions.Enable();

    mmPlayer.anchoredPosition = MinimapCamera.worldToCameraMatrix.MultiplyPoint( PlayerController.pawn.transform.position );
    MinimapScroller.transform.localPosition = -mmPlayer.anchoredPosition * MinimapScroller.transform.localScale.x;
  }

  public void HideMinimap()
  {
    Minimap.SetActive( false );
    Controls.BipedActions.Enable();
    Controls.MenuActions.Disable();
  }

  public void MinimapRender( Bounds bounds )
  {
    if( bounds.size.magnitude < 1 )
    {
      Debug.LogWarning( "Scene bounds has not been set." );
      return;
    }

    Vector2 size = bounds.size * pixelsPerUnit;
    rt = new RenderTexture( Mathf.FloorToInt( size.x ), Mathf.FloorToInt( size.y ), 32, UnityEngine.Experimental.Rendering.GraphicsFormat.R8G8B8A8_UNorm );
    rt.filterMode = FilterMode.Point;
    rt.wrapMode = TextureWrapMode.Repeat;

    rt2 = new RenderTexture( Mathf.FloorToInt( size.x ), Mathf.FloorToInt( size.y ), 32, UnityEngine.Experimental.Rendering.GraphicsFormat.R8G8B8A8_UNorm );
    rt2.filterMode = FilterMode.Point;
    rt2.wrapMode = TextureWrapMode.Repeat;

    Shader cached = bigsheetMaterial.shader;
    Shader cached2 = backgroundMaterial.shader;
    bigsheetMaterial.shader = mmShader;
    backgroundMaterial.shader = mmShader;

    int cachedCullingMask = MinimapCamera.cullingMask;

    bigsheetMaterial.color = mmColor[0];
    backgroundMaterial.color = mmColor[1];
    MinimapCamera.transform.position = bounds.center;
    MinimapCamera.targetTexture = rt;
    MinimapCamera.clearFlags = CameraClearFlags.SolidColor;
    MinimapCamera.backgroundColor = Color.clear;
    MinimapCamera.orthographicSize = size.y * 0.5f;
    MinimapCamera.Render();

    MinimapScroller.GetComponent<RectTransform>().sizeDelta = size;
    MinimapScroller.GetComponent<RectTransform>().localScale = new Vector3( MinimapScrollerScale, MinimapScrollerScale, 1 );
    MinimapScroller.GetComponent<RawImage>().texture = rt;

    bigsheetMaterial.color = mmColor[2];
    MinimapCamera.cullingMask = LayerMask.GetMask( "entity" );
    MinimapCamera.targetTexture = rt2;
    MinimapCamera.clearFlags = CameraClearFlags.Color;
    MinimapCamera.backgroundColor = Color.clear;
    MinimapCamera.Render();

    mmCharacterLayer.GetComponent<RectTransform>().sizeDelta = size;
    mmCharacterLayer.GetComponent<RawImage>().texture = rt2;

    MinimapCamera.cullingMask = cachedCullingMask;
    bigsheetMaterial.shader = cached;
    backgroundMaterial.shader = cached2;
  }

  public Texture GetMinimapTexture() { return rt; }
#endif

#endregion

#region WarpDoor

  // The primary gameplay layer is always on top, so that it has infinite overhead clearance.
  // The other layers are offset downward.
  // DO NOT CHANGE THIS CONSTANT. Existing level content relies on this value;
  public const float GameplayLayerOffset = 2000;

#if false
  [Header( "WarpDoor" )]
  [SerializeField] RawImage transitionImage;
  public float WarpDoorTransitionDuration = 1;
  RenderTexture renderTexture;

  // WarpDoor
  public void DoorTransitionRender( Entity[] entities, Vector2 newPos )
  {
    transitionImage.texture = renderTexture;
    Camera.main.targetTexture = renderTexture;
    Camera.main.Render();
    Camera.main.targetTexture = null;

    transitionImage.enabled = true;
    Color blend = Color.white;

    bool firstUpdate = true;
    new Timer( WarpDoorTransitionDuration, delegate( Timer timer )
    {
      if( firstUpdate )
      {
        firstUpdate = false;
        foreach( var ent in entities )
        {
          ent.Teleport( newPos );
          if( ent == CurrentPlayer )
            CameraController.TeleportToLookTarget();
        }
      }
      blend.a = 1f - timer.ProgressNormalized;
      transitionImage.color = blend;

      //CurrentPlayer.input.Jump = false;
    }, delegate { transitionImage.enabled = false; } );
  }
#endif

#endregion

  public void EnableRaycaster( bool enable = true ) { UI.GetComponent<UnityEngine.UI.GraphicRaycaster>().enabled = enable; }

  void ShowLoadingScreen( string message = "Loading.." )
  {
    LoadingScreen.SetActive( true );
    TMP_Text txt = LoadingScreen.GetComponentInChildren<TMP_Text>();
    txt.text = message;
  }

  void HideLoadingScreen() { LoadingScreen.SetActive( false ); }

  IEnumerator ShowLoadingScreenRoutine( string message = "Loading.." )
  {
    // This is a coroutine simply to wait a single frame after activating the loading screen.
    // Otherwise the screen will not show!
    ShowLoadingScreen( message );
    yield return null;
  }

  public GameObject Spawn( string resourceName, Vector3 position, Quaternion rotation, Transform parent = null, bool limit = true, bool initialize = true )
  {
    // THIS IS COMMENTED OUT UNTIL WE KNOW IF WE"LL USE IT
    // allow the lookup to check the name replacement table
    /*GameObject prefab = null;
    if( ResourceLookup.ContainsKey( resourceName ) )
    {
      prefab = ResourceLookup[resourceName];
    }
    else if( replacements.ContainsKey( resourceName ) )
    {
      if( ResourceLookup.ContainsKey( replacements[resourceName] ) )
        prefab = ResourceLookup[replacements[resourceName]];
    }
    if( prefab != null )
      return Spawn( prefab, position, rotation, parent, limit, initialize );*/
    return null;
  }

  public GameObject Spawn( GameObject prefab, Vector3 position, Quaternion rotation, Transform parent = null, bool limit = true, bool initialize = true )
  {
    if( limit )
    {
      ILimit[] limits = prefab.GetComponentsInChildren<ILimit>();
      foreach( var cmp in limits )
      {
        if( !cmp.IsUnderLimit() )
          return null;
      }
    }

    GameObject go = Instantiate( prefab, position, rotation, parent );
    go.name = prefab.name;

    if( initialize )
    {
      /*deeprest.SerializedComponent[] scs = go.GetComponentsInChildren<deeprest.SerializedComponent>();
      foreach( var sc in scs )
        sc.AfterDeserialize();*/
    }
    return go;
  }

  public void AudioOneShot( AudioClip clip, Vector3 position )
  {
    // independent, temporary positional sound object
    GameObject go = Instantiate( audioOneShotPrefab, position, Quaternion.identity );
    AudioSource source = go.GetComponent<AudioSource>();
    source.PlayOneShot( clip );
    new Timer( clip.length, null, delegate { Destroy( go ); } );
  }

  public void SpriteOneShot( Sprite sprite, Vector3 position, float duration )
  {
    GameObject go = Instantiate( oneshotSpritePrefab, position, Quaternion.identity );
    SpriteRenderer sr = go.GetComponent<SpriteRenderer>();
    sr.sprite = sprite;
    new Timer( duration, null, delegate { Destroy( go ); } );
  }

  public void SpriteOneShotBlinking( Sprite sprite, Vector3 position, int blinks, float interval )
  {
    bool flip = false;
    GameObject go = Instantiate( oneshotSpritePrefab, position, Quaternion.identity );
    SpriteRenderer sr = go.GetComponent<SpriteRenderer>();
    sr.sprite = sprite;
    new Timer( blinks * 2, interval, timer =>
    {
      go.SetActive( flip );
      flip = !flip;
    }, () =>
    {
      go.SetActive( false );
      Destroy( go );
    } );
  }

  public void FadeBlack()
  {
    fader.color = new Color( fader.color.r, fader.color.g, fader.color.b, 0 );
    fadeTimer.Stop( true );
    // Set gameObject active after Stop() because FadeClear() CompleteDelegate makes the gameObject inactive.
    fader.gameObject.SetActive( true );
    TimerParams tp = new TimerParams
    {
      unscaledTime = true,
      repeat = false,
      duration = 1,
      UpdateDelegate = delegate( Timer t )
      {
        Color fc = fader.color;
        fc.a = t.ProgressNormalized;
        fader.color = fc;
      },
      CompleteDelegate = delegate { fader.color = Color.black; }
    };
    fadeTimer.Start( tp );
  }

  public void FadeClear()
  {
    fadeTimer.Stop( true );
    TimerParams tp = new TimerParams
    {
      unscaledTime = true,
      repeat = false,
      duration = 1,
      UpdateDelegate = delegate( Timer t )
      {
        Color fc = fader.color;
        fc.a = 1 - t.ProgressNormalized;
        fader.color = fc;
      },
      CompleteDelegate = delegate
      {
        fader.color = Color.clear;
        fader.gameObject.SetActive( false );
      }
    };
    fadeTimer.Start( tp );
  }

#region Speech

#if false
  [Header( "Speech" )]
  public bool UseSpeechBubble = true;
  public bool UseSpeechFace = true;
  public bool UseSpeechCamera = true;
  public GameObject SpeechBubble;
  public GameObject SpeechIcon;
  public Image SpeechFaceRight;
  public Image SpeechFaceLeft;
  public TextMeshProUGUI SpeechCharacterName;
  public TextMeshProUGUI SpeechText;
  public TextMeshPro SpeechTextWorld;
  [SerializeField] Camera SpeechIconCamera;
  CharacterIdentity SpeechCharacter;
  PriorityEnum SpeechPriority = 0;
  // used to update the position of the speech camera.
  public Timer SpeechTimer = new Timer();
  [SerializeField] Animation SpeechNextArrow;
  [SerializeField] LocalizeStringEvent SpeechBubbleHelperText;
  const string Text_ContinueSpeech = "[Continue] Continue";
  const string Text_EndSpeech = "[End] Close";

  [Header( "Conversation" )]
  bool conversationActive;
  // used to wait for input during a conversation.
  Timer SpeechContinueTimer = new Timer();
  public float minInterval = 1;
  ITalker speakTalkerCurrent;
  ITalker[] talkers;
  public Animator CameraSpaceAnimator;
  Timer conversationRangeMonitor = new Timer();

  public class PreConversationState
  {
    public bool IgnoreZones;
    public bool CursorInfluence;
    public float OrthoTarget;
    public bool Vertical;
  }

  PreConversationState preconvostate;

  public class ConversationOptions
  {
    public bool Pause = false;
    // show the "cinematic" bars.
    public bool ShowBars = true;
    // allow the player to move around while engaged in conversation.
    public bool AllowPlayerMovement = true;
    // automatically cancel the face-to-face conversation because the player moved too far away.
    public bool CancelIfBeyondRange = true;
    public float CancelRange = 5;
    // called when the all lines of dialogue have been successfully uttered.
    public System.Action OnConclusion = null;
    // called when the conversation has ceased to be, either concluded or cancelled by the player.
    public System.Action OnComplete = null;
    // called when the player cancels the conversation, including when they walked away.
    public System.Action OnCancel = null;

    public bool LerpEnabled = true;
    // Vector2.zero is a special value that means calculate a default position.
    public Vector2 LerpToPosition = Vector2.zero;
    public float LerpDuration = 2;
    public float LerpZoom = 2;
  }

  ConversationOptions DefaultConversationOptions;
  ConversationOptions CurrentConversationOptions;

  public bool StartConversation( ITalker[] talkers, string convoID, ConversationOptions option = null )
  {
    // Speak() interaction can happen outside of a conversation (one-off comments, etc)
    if( conversationActive || SpeechContinueTimer.IsActive )
      return false;

    ConversationDefinition doc = talkers[0].GetIdentity().Conversation;
    for( int i = 0; i < doc.conversations.Length; i++ )
    {
      if( doc.conversations[i].id == convoID )
      {
        conversationActive = true;
        CurrentConversationOptions = option ?? DefaultConversationOptions;
        if( CurrentConversationOptions.CancelIfBeyondRange )
        {
          conversationRangeMonitor.Start( int.MaxValue, 1f, ( timer ) =>
          {
            try
            {
              if( talkers[0] == null || talkers[1] == null || Vector2.SqrMagnitude( talkers[0].GetCameraInfo().focus - talkers[1].GetCameraInfo().focus ) > CurrentConversationOptions.CancelRange )
              {
                SpeakCancel();
              }
            }
            catch( UnityEngine.MissingReferenceException )
            {
              SpeakCancel();
            }
          }, null );
        }

        preconvostate = new PreConversationState();
        preconvostate.IgnoreZones = CameraController.IgnoreCameraZones;
        preconvostate.CursorInfluence = CameraController.CursorInfluence;
        preconvostate.OrthoTarget = CameraController.orthoTarget;
        preconvostate.Vertical = CameraController.UseVerticalRange;

        this.talkers = talkers;
        foreach( var talker in talkers )
          talker.PreConversation( CurrentConversationOptions );

        if( CurrentConversationOptions.ShowBars )
          CameraSpaceAnimator.Play( "speechBarsEnable" );
        CameraController.CursorInfluence = false;
        SpeechBubbleHelperText.gameObject.SetActive( true );
        SpeechNextArrow.gameObject.SetActive( false );

        // unscaled time: jabber, animator, UI animations
        if( CurrentConversationOptions.Pause )
          Pause();

        if( CurrentConversationOptions.LerpEnabled )
        {
          Vector2 LerpPosition = CurrentConversationOptions.LerpToPosition;
          // calculate an average position for a default lerp target
          if( CurrentConversationOptions.LerpToPosition == Vector2.zero )
          {
            LerpPosition = Vector2.zero;
            foreach( var talker in talkers )
              LerpPosition += talker.GetCameraInfo().focus;
            LerpPosition /= (float) talkers.Length;
          }
          CameraController.LerpAdjustTarget( LerpPosition, CurrentConversationOptions.LerpZoom, CurrentConversationOptions.LerpDuration, () =>
          {
            CameraController.IgnoreCameraZones = true;
            SayLine( talkers, doc.conversations[i], 0, PriorityEnum.Conversation );
          } );
          return true;
        }
        else
          return SayLine( talkers, doc.conversations[i], 0, PriorityEnum.Conversation );
      }
    }
    Debug.LogWarning( "Conversation id not found: " + convoID );
    return false;
  }

  void EndConversation()
  {
    // unscaled time: jabber, animator, UI animations
    if( CurrentConversationOptions.Pause )
      Unpause();

    EndSpeak();
    conversationActive = false;

    CameraController.ZoneTransitionFlag = true;
    CameraController.StopLerp( false );

    if( CurrentConversationOptions.ShowBars )
      CameraSpaceAnimator.Play( "speechBarsDisable" );

    if( preconvostate != null )
    {
      CameraController.IgnoreCameraZones = preconvostate.IgnoreZones;
      CameraController.CursorInfluence = preconvostate.CursorInfluence;
      CameraController.orthoTarget = preconvostate.OrthoTarget;
      CameraController.UseVerticalRange = preconvostate.Vertical;
      preconvostate = null;
    }

    conversationRangeMonitor.Stop( false );

    foreach( var talker in talkers )
      if( (Object) talker != null )
        talker.PostConversation();

    CurrentConversationOptions.OnComplete?.Invoke();
  }

  bool SayLine( ITalker[] talkers, Conversation conversation, int i, PriorityEnum priority ) { return SpeakWait( conversation.dialogue[i].entry.GetLocalizedString(), talkers, conversation, i, priority ); }

  bool SpeakWait( string say, ITalker[] talkers, Conversation conversation, int i, PriorityEnum priority, int page = 1 )
  {
    ITalker talker = talkers[conversation.dialogue[i].id];
    /*float interval = Mathf.Max( minInterval, (float) say.Length / conversation.lettersPerSecond );
    if( !Mathf.Approximately( conversation.dialogue[i].wait, 0 ) )
      interval = conversation.dialogue[i].wait;*/

    if( Speak( talker, say, priority, float.MaxValue, conversation.dialogue[i].clip ) )
    {
      TMP_Text speechText;
      if( UseSpeechBubble )
        speechText = SpeechText;
      else
        speechText = SpeechTextWorld;

      speechText.pageToDisplay = page;
      /*TMP_TextInfo textInfo = speechText.GetTextInfo( say );
      string pageText = say.Substring( textInfo.pageInfo[page].firstCharacterIndex, textInfo.pageInfo[page].lastCharacterIndex - textInfo.pageInfo[page].firstCharacterIndex );*/

      if( i == conversation.dialogue.Length - 1 )
      {
        SpeechBubbleHelperText.SetEntry( "SPEECH_BUBBLE_END" );
        SpeechNextArrow.gameObject.SetActive( false );
      }
      else
      {
        SpeechBubbleHelperText.SetEntry( "SPEECH_BUBBLE_CONTINUE" );
        SpeechNextArrow.gameObject.SetActive( true );
        SpeechNextArrow.Play();
      }

      // wait for SpeakContinue to be called
      SpeechContinueTimer.Start( float.MaxValue, null, () =>
      {
        SpeechTimer.Stop( false );

        // todo test this
        // If any of the talkers has died or ceases to exist, end the conversation.
        foreach( var squawker in talkers )
          if( squawker == null || (squawker is Entity entity && entity.dead) )
          {
            SpeakCancel();
            return;
          }

        // if there are more pages, show the next.
        if( page + 1 <= speechText.GetTextInfo( say ).pageCount )
        {
          SpeakWait( say, talkers, conversation, i, priority, page + 1 );
          return;
        }
        talker.OnSpeakEnd();
        // continue to next dialogue line in the conversation.
        if( ++i < conversation.dialogue.Length )
          SayLine( talkers, conversation, i, priority );
        else if( conversationActive )
        {
          EndConversation();
          CurrentConversationOptions.OnConclusion?.Invoke();
        }
      } );
      return true;
    }
    return false;
  }

  void SpeakContinue()
  {
    if( SpeechContinueTimer.IsActive )
    {
      SpeechNextArrow.Stop();
      SpeechNextArrow.gameObject.SetActive( false );
      SpeechTimer.Stop( false );
      // When this timer is stopped, the OnComplete callback makes the conversation continue.
      SpeechContinueTimer.Stop( true );
    }
    else
      EndSpeak();
  }

  void SpeakCancel()
  {
    if( conversationActive )
    {
      EndConversation();
      CurrentConversationOptions.OnCancel?.Invoke();
    }
    else
      EndSpeak();
  }


  public bool Speak( ITalker talker, string text, PriorityEnum priority, float timeout = float.MaxValue, AudioClip voiceOver = null )
  {
    // Equal priority is allowed to override if it's a one-off remark.
    // Conversations should not stop for one another, though. 
    if( (!SpeechTimer.IsActive || ((priority < SpeechPriority) || (SpeechPriority == PriorityEnum.Remark))) )
    {
      if( BoolSetting["SpeechDisableControls"].Value )
        Controls.BipedActions.Disable();
      else
        Controls.BipedActions.Interact.Disable();
      Controls.SpeechActions.Enable();
      if( CurrentPlayer )
        CurrentPlayer.InteractIndicator.SetActive( false );

      SpeechTimer.Stop( false );

      speakTalkerCurrent = talker;
      speakTalkerCurrent.OnSay( text, voiceOver );

      SpeechCharacter = talker.GetIdentity();
      SpeechPriority = priority;

      SpeechBubble.SetActive( UseSpeechBubble );
      SpeechTextWorld.gameObject.SetActive( !UseSpeechBubble );
      if( UseSpeechBubble )
      {
        SpeechCharacterName.text = SpeechCharacter.CharacterName.GetLocalizedString();
        SpeechCharacterName.color = SpeechCharacter.SpeechTextColor;
        SpeechFaceRight.gameObject.SetActive( UseSpeechFace );
        SpeechFaceLeft.gameObject.SetActive( UseSpeechFace );
        if( CurrentPlayer != null && talker != CurrentPlayer.GetComponent<ITalker>() )
          SpeechFaceRight.sprite = SpeechCharacter.Face;

        // Using TextMeshPro page wrap now.
        /*SpeechText.horizontalOverflow = HorizontalWrapMode.Wrap;*/
        SpeechText.text = text;

        SpeechIconCamera.enabled = UseSpeechCamera;
        SpeechIcon.SetActive( UseSpeechCamera );

        speakTalkerCurrent.GetDestructionEvent().AddListener( EndSpeak );
        SpeechTimer.Start( float.MaxValue, timer =>
        {
          // interface is on a UnityEngine.Object, so it must be cast before checking validity.
          if( speakTalkerCurrent as MonoBehaviour == null )
          {
            // This might happen if the character is dead/destroyed while the speech UI is active.
            SpeakCancel();
            SpeechTimer.Stop( false );
            return;
          }
          TalkerCameraInfo info = speakTalkerCurrent.GetCameraInfo();
          SpeechIconCamera.transform.position = info.focus;
          SpeechIconCamera.orthographicSize = info.orthoSize;
        }, () => { speakTalkerCurrent?.GetDestructionEvent().RemoveListener( EndSpeak ); } );
        if( priority == PriorityEnum.Remark )
          SpeechBubbleHelperText.SetEntry( "SPEECH_BUBBLE_END" );
      }
      else
      {
        SpeechTextWorld.color = talker.GetIdentity().SpeechTextColor;
        SpeechTextWorld.text = text;

        Vector2 pos = talker.GetCameraInfo().focus + Vector2.up * 0.75f;
        Vector2 sizeDelta = SpeechTextWorld.GetComponent<RectTransform>().sizeDelta;
        Bounds bounds = new Bounds();
        Vector3 boundsCenter = CameraController.transform.position;
        boundsCenter.z = 0;
        bounds.center = boundsCenter;
        float widthRatio = ((float) Screen.width / (float) Screen.height);
        bounds.size = new Vector3( (2 * Camera.main.orthographicSize * widthRatio - sizeDelta.x), 2 * CameraController.orthoTarget - sizeDelta.y, 0 );
        SpeechTextWorld.transform.position = bounds.ClosestPoint( pos );

        if( speakTalkerCurrent is Entity entity )
          entity.EventDestroyed.AddListener( EndSpeak );
        SpeechTimer.Start( float.MaxValue, timer =>
        {
          if( speakTalkerCurrent == null )
          {
            // This might happen if the character is dead/destroyed while the speech UI is active.
            SpeakCancel();
            SpeechTimer.Stop( false );
            return;
          }
        }, () =>
        {
          if( speakTalkerCurrent is Entity entity2 )
            entity2.EventDestroyed.RemoveListener( EndSpeak );
        } );
      }
      return true;
    }
    return false;
  }

  void EndSpeak()
  {
    if( speakTalkerCurrent != null )
    {
      speakTalkerCurrent.OnSpeakEnd();
      speakTalkerCurrent = null;
    }
    // stop the timeout timer, which also tracks talker head positions.
    SpeechTimer.Stop( true );

    SpeechContinueTimer.Stop( false );
    if( UseSpeechBubble )
    {
      SpeechBubble.SetActive( false );
      SpeechCharacter = null;
      SpeechIconCamera.enabled = false;
    }
    else
    {
      SpeechTextWorld.gameObject.SetActive( false );
    }

    Controls.BipedActions.Enable();
    Controls.SpeechActions.Disable();
  }
#endif

#endregion

#region SerializePersistentData

#if false
  void VerifyPersistentData()
  {
    // Ensure that all persistent data files exist. If not, unpack them from the archive within the build.
    bool unpack = false;
    foreach( var filename in persistentFilenames )
      if( !File.Exists( Application.persistentDataPath + "/" + filename ) )
        unpack = true;
    if( unpack )
    {
      string zipPath = Application.temporaryCachePath + "/persistent.zip";
      TextAsset zipfile = Resources.Load( "persistent" ) as TextAsset;
      if( zipfile != null )
      {
        File.WriteAllBytes( zipPath, zipfile.bytes );
        Debug.Log( "Unzipping persistent: " + zipPath );
        using( ZipFile zip = ZipFile.Read( zipPath ) )
        {
          zip.ExtractAll( Application.persistentDataPath, ExtractExistingFileAction.OverwriteSilently );
        }
      }
      else
      {
        Debug.LogWarning( "no level directory or zip file in build: " + name );
      }
    }
  }
#endif

#endregion

#region Settings

  string[] resolutions = { "640x360", "640x400", "1280x720", "1280x800", "1920x1080", "1920x1200" };
  int ResolutionWidth;
  int ResolutionHeight;

  void InitializeSettings()
  {
    SettingUI[] settings = PauseMenu.GetComponentsInChildren<SettingUI>( true );
    // use existing UI objects, if they exist
    foreach( var s in settings )
    {
      if( s.isString )
      {
        s.stringValue.Init();
        StringSetting.Add( s.stringValue.name, s.stringValue );
      }
      if( s.isInteger )
      {
        s.intValue.Init();
        FloatSetting.Add( s.intValue.name, s.intValue );
      }
      if( s.isBool )
      {
        s.boolValue.Init();
        BoolSetting.Add( s.boolValue.name, s.boolValue );
      }
    }

    // screen settings are applied explicitly when user pushes button
    CreateBoolSetting( "Fullscreen", false, null );
    CreateStringSetting( "Resolution", "1280x800", null );
    CreateFloatSetting( "ResolutionSlider", 4, 0, resolutions.Length - 1, resolutions.Length - 1, delegate( float value )
    {
      string Resolution = resolutions[Mathf.FloorToInt( Mathf.Clamp( value, 0, resolutions.Length - 1 ) )];
      string[] tokens = Resolution.Split( 'x' );
      ResolutionWidth = int.Parse( tokens[0].Trim() );
      ResolutionHeight = int.Parse( tokens[1].Trim() );
      StringSetting["Resolution"].Value = ResolutionWidth.ToString() + "x" + ResolutionHeight.ToString();
    } );
    /*CreateFloatSetting( "UIScale", 1, 0.1f, 4, 20, null );*/
    CreateBoolSetting( "CropScreen", false, ( value ) =>
    {
      CameraController.cam.GetComponent<UnityEngine.U2D.PixelPerfectCamera>().cropFrameX = value;
      CameraController.cam.GetComponent<UnityEngine.U2D.PixelPerfectCamera>().cropFrameY = value;
    } );

    /*QualityName = (Instantiate( ReadOnlyStringTemplate, SettingsParent.transform )).GetComponentInChildren<LocalizeStringEvent>();
    CreateFloatSetting( "Quality", 1, 0, 1, 1, delegate( float value )
    {
      // This depends on the Quality names matching the Localization Keys!!!!
      QualityName.SetEntry( QualitySettings.names[Mathf.FloorToInt( value )] );
      QualitySettings.SetQualityLevel( (int) value );
    } );*/

    CreateFloatSetting( "MasterVolume", 1f, 0, 1, 20, delegate( float value )
    {
      if( Mathf.Approximately( value, 0f ) )
        mixer.SetFloat( "MasterVolume", -80 );
      else
        mixer.SetFloat( "MasterVolume", Util.DbFromNormalizedVolume( value, AudioFloor ) );
    } );
    CreateFloatSetting( "MusicVolume", 1f, 0, 1, 20, delegate( float value )
    {
      if( Mathf.Approximately( value, 0f ) )
        mixer.SetFloat( "MusicVolume", -80 );
      else
        mixer.SetFloat( "MusicVolume", Util.DbFromNormalizedVolume( value, AudioFloor ) );
    } );
    CreateFloatSetting( "SFXVolume", 0.7f, 0, 1, 20, delegate( float value )
    {
      if( Mathf.Approximately( value, 0f ) )
        mixer.SetFloat( "SFXVolume", -80 );
      else
        mixer.SetFloat( "SFXVolume", Util.DbFromNormalizedVolume( value, AudioFloor ) );
      if( Updating )
        AudioOneShot( VolumeChangeNoise, Camera.main.transform.position );
    } );

    CreateBoolSetting( "VSync", true, delegate( bool value ) { QualitySettings.vSyncCount = value ? 1 : 0; } );
    /*CreateFloatSetting( "CursorSensitivity", 1f, 0.01f, 2, 199, delegate( float value ) { CursorSensitivity = value; } );
    CreateFloatSetting( "Zoom", 3, 1, 5, 20, delegate( float value ) { CameraController.orthoTarget = value; } );
    CreateFloatSetting( "PlayerSpeedFactor", 0, 0, 1, 10, delegate( float value )
    {
      if( PlayerController != null )
        PlayerController.HACKSetSpeed( value );
    } );*/
    CreateBoolSetting( "EnablePauseSilence", true, delegate( bool value )
    {
      if( Paused )
      {
        if( value )
        {
          musicSource0.Pause();
          musicSource1.Pause();
          activeMusicSource.Pause();
        }
        else
        {
          musicSource0.UnPause();
          musicSource1.UnPause();
          activeMusicSource.UnPause();
        }
      }
    } );

    CreateDivider( "Developer Settings" );

    CreateBoolSetting( "ShowDevInfo", false, delegate( bool value ) { debugParent.SetActive( value ); } );
    /*CreateBoolSetting( "UseCameraVertical", true, delegate( bool value ) { CameraController.UseVerticalRange = value; } );
    CreateBoolSetting( "CursorInfluence", true, delegate( bool value )
    {
      if( CameraController != null )
        CameraController.CursorInfluence = value;
    } );
    CreateBoolSetting( "AimSnap", false, delegate( bool value ) { AimSnap = value; } );
    CreateBoolSetting( "AutoAim", false, delegate( bool value ) { AutoAim = value; } );
    CreateBoolSetting( "ShowAimPath", false, delegate( bool value ) { ShowAimPath = value; } );
    CreateBoolSetting( "AutoWallslide", true, null );*/

    /*CreateBoolSetting( "UseSpeechBubble", true, delegate( bool value ) { UseSpeechBubble = value; } );
    CreateBoolSetting( "UseSpeechFace", false, delegate( bool value ) { UseSpeechFace = value; } );
    CreateBoolSetting( "UseSpeechCamera", true, delegate( bool value ) { UseSpeechCamera = value; } );
    CreateBoolSetting( "SpeechDisableControls", false, null );*/

    /*CreateBoolSetting( "ShowInputDisplay", false, delegate( bool value )
    {
      ShowInputDisplay = value;
      InputDisplayOnCamera.gameObject.SetActive( ShowInputDisplay );
      if( ShowInputDisplay )
        InputDisplayOnCamera.Enable();
      else
        InputDisplayOnCamera.Disable();
    } );*/

    CreateDivider( "Experimental" );
    CreateBoolSetting( "KillScreenAtEndOfTrack", true, null );
    CreateBoolSetting( "KillScreenAtEndOfAlbum", true, null );
    CreateBoolSetting( "TestFinalKillScreen", false, ( value ) =>
    {
      // avoid infinite loop
      if( !value )
        return;
      HidePauseMenu();
      // assumes value == true
      FinalKillScreen();
      // reset the toggle to false
      BoolSetting["TestFinalKillScreen"].Value = false;
    } );
    /*CreateBoolSetting( "HealthAudioPitch", true, delegate( bool value )
    {
      HealthAudioPitch = value;
      if( value )
      {
        if( CurrentPlayer != null )
          OnPlayerHealthChange( ((PlayerBiped) CurrentPlayer).Health, ((PlayerBiped) CurrentPlayer).MaxHealth );
      }
      else
      {
        MusicPitch( 1 );
      }
    } );
    CreateBoolSetting( "HealthTimescale", false, delegate( bool value )
    {
      HealthTimescale = value;
      if( value )
      {
        if( CurrentPlayer != null )
          OnPlayerHealthChange( ((PlayerBiped) CurrentPlayer).Health, ((PlayerBiped) CurrentPlayer).MaxHealth );
      }
      if( !value )
      {
        CurrentTimescale = 1;
        Time.timeScale = 1;
      }
    } );*/

    /*CreateBoolSetting( "PerspectiveExperiment", false, delegate( bool value ) { CameraController.PerspectiveExperiment = value; } );*/
    /*CreateBoolSetting( "CrushMeansInstantDeath", false, null );*/
    /*CreateBoolSetting( "ShowImageUponDeath", true, null );*/
    CreateBoolSetting( "CreditsScroll", false, ( value ) =>
    {
      if( value )
        CreditsScroller.StartScroll();
      else
        CreditsScroller.StopScroll();
    } );
    /*CreateBoolSetting( "TeamColors", true, null );*/
    /*CreateBoolSetting( "ExperimentalEntitySort", true, null );*/

    // Mushroom
    /*for( int i = 0; i < Mushroom.MushSettings.Length; i++ )
    {
      InputAction Mush = new InputAction( "Mush"+i, InputActionType.Button, "<Keyboard>/" + i );
      Mush.Enable();
      Mush.performed += context => { Mushroom.LerpTimer( 3, Mushroom.MushSettings[i] ); };
    }
    CreateBoolSetting( "Mushroom", false, delegate( bool value )
    {
      if( Mushroom == null )
        return;
      if( value )
      {
      }
    } );*/

    CreateDivider( "Performance Settings" );

    CreateFloatSetting( "UpdateCullDistance", 50, 10, 100, 9, delegate( float value ) { UpdateCullDistance = value; } );
    CreateFloatSetting( "MaxUpdateCount", 500, 0, 1000, 100, delegate( float value ) { MaxUpdateCount = (int)value; } );
    CreateFloatSetting( "PickupLimit", 500, 50, 1000, 950, delegate( float value ) { Pickup.Limit.UpperLimit = (int)value; } );
    CreateFloatSetting( "EntityLimit", 5000, 100, 10000, 100, delegate( float value ) { Entity.Limit.UpperLimit = (int)value; } );

    /*CreateDivider( "Misc Settings" );*/
    /*CreateFloatSetting( "CursorOuter", 1, 0, 1, 20, delegate( float value ) { CursorOuter = value; } );
    CreateFloatSetting( "CameraLerpAlpha", 10, 0, 10, 100, delegate( float value ) { CameraController.lerpAlpha = value; } );*/
    /*CreateBoolSetting( "RandomSeed", false, null );
    CreateFloatSetting( "Seed", 0, 0, MAX_SEED, MAX_SEED, ( value ) => { Seed = Mathf.RoundToInt( value ) % MAX_SEED; } );*/

    foreach( var scene in BuildConfig.sceneRefs )
    {
      if( scene.GetSceneName() == "GLOBAL" )
        continue;
      GameObject go = Instantiate( SceneListElementTemplate, SceneListElementTemplate.transform.parent );
      go.GetComponentInChildren<TMP_Text>().text = scene.GetSceneName();
      go.GetComponentInChildren<Button>().onClick.AddListener( () =>
      {
        HidePauseMenu();
        LoadScene( scene.GetSceneName(), default, null );
      } );
      if( SceneList.InitiallySelected == null )
        SceneList.InitiallySelected = go;
    }
    Destroy( SceneListElementTemplate );

    // Add a button to cycle through music randomly.
    GameObject cycleButton = Instantiate( MusicListElementTemplate, MusicListElementTemplate.transform.parent );
    cycleButton.GetComponentInChildren<TMP_Text>().text = "Random Cycle";
    cycleButton.GetComponentInChildren<Button>().onClick.AddListener( PlayRandomSongFromAlbum );
    // Add a button for each entry in the music list.

    foreach( var audioLoop in Music )
    {
      GameObject go = Instantiate( MusicListElementTemplate, MusicListElementTemplate.transform.parent );
      go.GetComponentInChildren<TMP_Text>().text = audioLoop.name;
      go.GetComponentInChildren<Button>().onClick.AddListener( () => { StartMusicLoop( audioLoop ); } );
      if( MusicList.InitiallySelected == null )
        MusicList.InitiallySelected = go;
    }
    Destroy( MusicListElementTemplate );
  }

  // explicit UI navigation
  public void NextNav( Selectable selectable )
  {
    Navigation previousNav = previousNavSelectable.navigation;
    previousNav.selectOnDown = selectable;
    previousNavSelectable.navigation = previousNav;
    Navigation thisNav = selectable.navigation;
    thisNav.selectOnUp = previousNavSelectable;
    selectable.navigation = thisNav;
    previousNavSelectable = selectable;
  }

  void CreateBoolSetting( string key, bool value, System.Action<bool> onChange )
  {
    BoolValue bv;
    if( !BoolSetting.TryGetValue( key, out bv ) )
    {
      GameObject go = Instantiate( ToggleTemplate, SettingsParent.transform );
      SettingUI sss = go.GetComponent<SettingUI>();
      sss.isBool = true;
      bv = sss.boolValue;
      bv.name = string.Join( " ", Util.SplitCase.Split( key ) );
      go.GetComponentInChildren<TMP_Text>().text = key;
      /*go.GetComponentInChildren<LocalizeStringEvent>().SetEntry( key );*/
      bv.Init();
      BoolSetting.Add( key, bv );
    }
    bv.onValueChanged = onChange;
    bv.Value = value;
    NextNav( bv.toggle );
  }

  void CreateFloatSetting( string key, float value, float min, float max, int steps, System.Action<float> onChange )
  {
    FloatValue bv;
    if( !FloatSetting.TryGetValue( key, out bv ) )
    {
      GameObject go = Instantiate( SliderTemplate, SettingsParent.transform );
      SettingUI sss = go.GetComponent<SettingUI>();
      sss.isInteger = true;
      bv = sss.intValue;
      bv.name = string.Join( " ", Util.SplitCase.Split( key ) );
      go.GetComponentInChildren<TMP_Text>().text = key;
      /*go.GetComponentInChildren<LocalizeStringEvent>().SetEntry( key );*/
      bv.minValue = min;
      bv.maxValue = max;
      bv.steps = steps;
      bv.Init();
      FloatSetting.Add( key, bv );
    }
    bv.onValueChanged = onChange;
    bv.Value = value;
    NextNav( bv.slider );
  }

  void CreateStringSetting( string key, string value, System.Action<string> onChange )
  {
    StringValue val;
    if( !StringSetting.TryGetValue( key, out val ) )
    {
      GameObject go = Instantiate( StringTemplate, SettingsParent.transform );
      SettingUI sss = go.GetComponent<SettingUI>();
      sss.isString = true;
      val = sss.stringValue;
      val.name = string.Join( " ", Util.SplitCase.Split( key ) );
      go.GetComponentInChildren<TMP_Text>().text = key;
      // todo localize the value? How to we add/customize the localization table at runtime?
      /*go.GetComponentInChildren<LocalizeStringEvent>().SetEntry( key );*/
      val.Init();
      StringSetting.Add( key, val );
    }
    val.onValueChanged = onChange;
    val.Value = value;
    if( val.inputField != null )
      NextNav( val.inputField );
  }

  void CreateDivider( string key )
  {
    GameObject go = Instantiate( DividerTemplate, SettingsParent.transform );
    go.GetComponentInChildren<TMP_Text>().text = key;
    /*go.GetComponentInChildren<LocalizeStringEvent>().SetEntry( key );*/
  }

  void ReadSettings()
  {
    JsonData json = new JsonData();
    if( File.Exists( settingsPath ) )
    {
      string gameJson = File.ReadAllText( settingsPath );
      if( gameJson.Length > 0 )
      {
        JsonReader reader = new JsonReader( gameJson );
        json = JsonMapper.ToObject( reader );
      }
      foreach( var pair in BoolSetting )
        pair.Value.Value = JsonUtil.Read( pair.Value.Value, json, "settings", pair.Key );
      foreach( var pair in FloatSetting )
        pair.Value.Value = JsonUtil.Read( pair.Value.Value, json, "settings", pair.Key );
      foreach( var pair in StringSetting )
        pair.Value.Value = JsonUtil.Read( pair.Value.Value, json, "settings", pair.Key );
    }
  }

  void WriteSettings()
  {
    JsonWriter writer = new JsonWriter();
    writer.PrettyPrint = true;
    writer.WriteObjectStart(); // root

    writer.WritePropertyName( "settings" );
    writer.WriteObjectStart();
    foreach( var pair in StringSetting )
    {
      writer.WritePropertyName( pair.Key );
      writer.Write( pair.Value.Value );
    }
    foreach( var pair in FloatSetting )
    {
      writer.WritePropertyName( pair.Key );
      writer.Write( pair.Value.Value, "0.##" );
    }
    foreach( var pair in BoolSetting )
    {
      writer.WritePropertyName( pair.Key );
      writer.Write( pair.Value.Value );
    }
    writer.WriteObjectEnd();

    writer.WriteObjectEnd(); // root end
    //print( settingsPath );
    File.WriteAllText( settingsPath, writer.ToString() );

    // #if UNITY_WEBGL && !UNITY_EDITOR
    //     FileSync();
    //#endif
  }

  //#endif

  public void ApplyScreenSettings()
  {
    string[] tokens = StringSetting["Resolution"].Value.Split( new char[] { 'x' } );
    ResolutionWidth = int.Parse( tokens[0].Trim() );
    ResolutionHeight = int.Parse( tokens[1].Trim() );
#if !UNITY_WEBGL
    Screen.SetResolution( ResolutionWidth, ResolutionHeight, BoolSetting["Fullscreen"].Value );
#endif
    /*CanvasScaler.scaleFactor = FloatSetting["UIScale"].Value;*/
    ScreenSettingsCountdownTimer.Stop( false );
    ConfirmDialog.Unselect();

    // WarpDoor rendertexture for screen fade
    /*if( renderTexture != null )
      Destroy( renderTexture );
    renderTexture = new RenderTexture( ResolutionWidth, ResolutionHeight, 24, RenderTextureFormat.Default, 0 ); //GraphicsFormat.R8G8B8A8_UNorm, 0 );*/
  }

  struct ScreenSettings
  {
    public bool Fullscreen;
    public string Resolution;
    /*public float UIScale;*/
  }

  ScreenSettings CachedScreenSettings;

  public void ScreenChangePrompt()
  {
    CachedScreenSettings.Fullscreen = Screen.fullScreen;
    CachedScreenSettings.Resolution = Screen.width.ToString() + "x" + Screen.height.ToString();
    /*CachedScreenSettings.UIScale = CanvasScaler.scaleFactor;*/

    ConfirmDialog.Select();
    Screen.SetResolution( ResolutionWidth, ResolutionHeight, BoolSetting["Fullscreen"].Value );
    /*CanvasScaler.scaleFactor = FloatSetting["UIScale"].Value;*/

    TimerParams tp = new TimerParams
    {
      unscaledTime = true,
      repeat = false,
      duration = 10,
      UpdateDelegate = delegate( Timer obj ) { ScreenSettingsCountdown.text = "Accept changes or revert in <color=orange>" + (10 - obj.ProgressSeconds).ToString( "0" ) + "</color> seconds"; },
      CompleteDelegate = RevertScreenSettings
    };
    ScreenSettingsCountdownTimer.Start( tp );
  }

  public void RevertScreenSettings()
  {
    BoolSetting["Fullscreen"].Value = CachedScreenSettings.Fullscreen;
    StringSetting["Resolution"].Value = CachedScreenSettings.Resolution;
    /*FloatSetting["UIScale"].Value = CachedScreenSettings.UIScale;*/
    ApplyScreenSettings();
  }

#endregion

#region Music

  [SerializeField] MusicPlayer MenuMusicPlayer;
  [SerializeField] MusicPlayer HUDMusicPlayer;
  // Music Cycle
  /*public AudioLoop[] CycleMusic;*/
  Timer musicCycleTimer = new Timer();

  /*public void StartRandomMusicCycle()
  {
    int[] musicIndex = Util.ShuffledIndexList( Music.Length );
    int musicShuffleIndex = 0;
    Global.instance.PlayMusic( Music[musicIndex[musicShuffleIndex]] );
    musicCycleTimer.Start( 60, 6, ( timer ) =>
    {
      musicShuffleIndex = (musicShuffleIndex + 1) % Music.Length;
      Global.instance.MusicCrossFadeTo( Music[musicIndex[musicShuffleIndex]], 3 );
    }, null );
  }*/

  public void PlayRandomSongFromAlbum() { StartMusicLoop( Random.Range( 0, Music.Length ) ); }

  public void StartMusicLoop( AudioLoop audioLoop )
  {
    int index = System.Array.IndexOf( Music, audioLoop );
    StartMusicLoop( index );
  }

  public void StartMusicLoop( int musicIndex = 0 ) { MusicLoop( musicIndex, true ); }

  void MusicLoop( int musicIndex, bool playImmediate = false )
  {
    const float crossfadeDuration = 3;
    if( playImmediate )
      Global.instance.PlayMusic( Music[musicIndex] );
    else
      Global.instance.MusicCrossFadeTo( Music[musicIndex], crossfadeDuration );
    musicCycleTimer.Start( Music[musicIndex].loop.length /*+ crossfadeDuration*/, null, () =>
    {
      musicIndex = (musicIndex + 1) % Music.Length;
      MusicLoop( musicIndex, true );
    } );
  }

  public void StopMusicCycle()
  {
    StopMusic();
    musicCycleTimer.Stop( false );
  }

  public void PlayMusic( AudioLoop audioLoop )
  {
    StopMusic();
    audioLoop.Play( musicSource0 );
    activeMusicSource = musicSource0;
    activeMusicSource.volume = 1;

    MenuMusicPlayer.ShowMusicInfo( audioLoop );
    HUDMusicPlayer.ShowMusicInfo( audioLoop );
  }

  public void StopMusic()
  {
    musicSource0.Stop();
    musicSource1.Stop();
  }

  public void MusicCrossFadeTo( AudioLoop audioLoop, float duration )
  {
    // NOTE will not gracefully crossfade while an AudioLoop is playing it's intro clip.
    AudioSource previousMusicSource;
    if( activeMusicSource == musicSource0 )
    {
      activeMusicSource = musicSource1;
      previousMusicSource = musicSource0;
    }
    else
    {
      activeMusicSource = musicSource0;
      previousMusicSource = musicSource1;
    }

    if( audioLoop )
      audioLoop.Play( /*musicSourceIntro,*/ activeMusicSource );
    else
      activeMusicSource.clip = null;

    musicCrossFadeTimer.unscaledTime = true;
    musicCrossFadeTimer.Start( duration, delegate( Timer obj )
    {
      /*musicSourceIntro.volume = obj.ProgressNormalized;*/
      activeMusicSource.volume = obj.ProgressNormalized;
      previousMusicSource.volume = 1 - obj.ProgressNormalized;
    }, () =>
    {
      /*musicSourceIntro.volume = 1;*/
      activeMusicSource.volume = 1;
      previousMusicSource.volume = 0;
      previousMusicSource.Stop();

      MenuMusicPlayer.ShowMusicInfo( audioLoop );
      HUDMusicPlayer.ShowMusicInfo( audioLoop );
    } );
  }

  public void MusicPitch( float value )
  {
    musicSource0.pitch = value;
    musicSource1.pitch = value;
  }

#endregion

#region Boss_VictorySequence

#if false
  // allow only one boss at a time to potentially start the destruction/victory sequence
  public bool BossDestructionFlag;

  [DevConsole.Command( "StartVictorySequence" )]
  public static void StartVictorySequence_Command() { Global.instance.StartVictorySequenceDebug(); }

  [ExposeMethod]
  public void StartVictorySequenceDebug()
  {
    ((PlayerBiped) Global.instance.CurrentPlayer).VictorySequenceBegin();
    Global.instance.StartVictorySequence();
  }

  public void StartVictorySequence()
  {
    musicSource0.Stop();
    musicSource1.Stop();
    musicSource0.PlayOneShot( Victory.OneShotClip );
    musicSource0.volume = 1;
    musicSource0.pitch = 1;
    musicSource1.volume = 0;
    musicSource1.pitch = 1;
    CameraZone ActiveZone = CameraController.GetActiveZone();
    if( ActiveZone != null )
      ActiveZone.EncompassBounds = false;
    ((PlayerBiped) CurrentPlayer).VictoryMusicStart();
    CameraController.LerpAdjustTarget( CurrentPlayer.transform.position + Vector3.up * 0.25f, 0.8f, 5, () => { CameraController.enabled = false; } );

    Timer victoryTimer = new Timer();
    TimerParams tp = new TimerParams();
    tp.duration = Victory.ConsideredDoneDuration;
    tp.unscaledTime = true;
    tp.CompleteDelegate = delegate
    {
      // victory sting
      AudioOneShot( AwesomeSting, CameraController.CameraPosition );
      ((PlayerBiped) CurrentPlayer).VictoryStingBegin();
      new Timer( 1, null, () =>
      {
        musicSource0.volume = 0;
        LoadScene( BuildConfig.ReturnToThisSceneAfterBoss, default, null );
        new Timer( 1, null, () =>
        {
          CameraController.enabled = true;
          PlayerController.NormalInputMode();
          BossDestructionFlag = false;
          ((PlayerBiped) CurrentPlayer).VictorySequenceEnd();
        } );
      } );
    };
    victoryTimer.Start( tp );
  }
#endif

#endregion

#region ViewPortal

#if false
  /*public static List<IViewWindow> ViewWindows = new List<IViewWindow>();*/

  // DoorCamera, ViewPortal
  [SerializeField]
  GameObject PortalCameraPrefab;
  const int MaxViewPortals = 4;

  public class ViewPortal
  {
    public bool inUse;
    public Camera camera;
    public RenderTexture texture;
    public MonoBehaviour owner;
  }

  ViewPortal[] portals = new ViewPortal[MaxViewPortals];

  void IntializeViewPortals()
  {
    for( int i = 0; i < MaxViewPortals; i++ )
    {
      portals[i] = new ViewPortal();
      portals[i].inUse = false;
      /*portals[i].texture = new RenderTexture( 48, 48, 24, RenderTextureFormat.Default, 0 );
      portals[i].texture.filterMode = FilterMode.Point;*/
      portals[i].camera = (Instantiate( PortalCameraPrefab, transform )).GetComponent<Camera>();
      /*portals[i].camera.GetComponent<Camera>().targetTexture = portals[i].texture;*/
    }
  }

  int portalIndex;

  public bool RequestViewPortal( ref ViewPortal portal )
  {
    portal = null;
    // find an inactive portal
    for( int i = 0; i < MaxViewPortals; i++ )
    {
      if( portals[i].inUse == false )
      {
        portal = portals[i];
        portal.inUse = true;
        portalIndex = i;
        break;
      }
    }
    if( portal == null )
    {
      // if no inactive portal found, take one using a rolling index
      portalIndex = (portalIndex + 1 + MaxViewPortals) % MaxViewPortals;
      portal = portals[portalIndex];
      portal.inUse = true;
    }
    portal.camera.gameObject.SetActive( true );
    return true;
  }

  public void DoneWithViewPortal( ViewPortal portal )
  {
    portal.inUse = false;
    portal.camera.gameObject.SetActive( false );
    portal.camera.transform.parent = transform;
  }
#endif

#endregion

#region ProgressFlag

  public class ProgressBool
  {
    bool _value;
    bool _valueInitial;
    public System.Action<bool> onValueChanged;

    public bool Value
    {
      get { return _value; }
      set
      {
        if( value != _value || _valueInitial )
        {
          _value = value;
          _valueInitial = false;
          if( onValueChanged != null )
            onValueChanged.Invoke( _value );
        }
      }
    }
  }

  public Dictionary<string, ProgressBool> ProgressFlag = new Dictionary<string, ProgressBool>();
  // WARNING Enum values cannot be set in UnityEvents in the editor Inspector!!
  /*public Dictionary<ProgressBit, ProgressBool> ProgressBools = new Dictionary<ProgressBit, ProgressBool>();*/

  // call this from a script's Start method.
  public void RegisterProgressFlag( string flag, System.Action<bool> OnChange )
  {
    if( !ProgressFlag.ContainsKey( flag ) )
      ProgressFlag.Add( flag, new ProgressBool() );
    ProgressFlag[flag].onValueChanged += OnChange;
  }

  public bool SetProgressFlag( string flag )
  {
    // returns true if the value changed
    if( !ProgressFlag.ContainsKey( flag ) )
    {
      Debug.LogWarning( "Progress flag <" + flag + "> has not be registered." );
      return false;
    }
    bool value = ProgressFlag[flag].Value;
    ProgressFlag[flag].Value = true;
    return ProgressFlag[flag].Value != value;
  }

#endregion

#region DevConsole

  [DevConsole.Command( "clear" )]
  public static void Command_Clear() { DevConsole.DevConsoleUI.Instance.Clear(); }

  [DevConsole.Command( "help" )]
  public static void Command_Help()
  {
    DevConsole.DevConsoleUI.Instance.Log( "Command List:" );
    foreach( var pair in DevConsole.DevConsole.methodInfoCache )
    {
      System.Reflection.ParameterInfo[] pis = pair.Value.GetParameters();
      string parms = "";
      foreach( var parm in pis )
      {
        parms += string.Join( " ", "<" + parm.ParameterType.Name + ">", parm.Name );
      }
      DevConsole.DevConsoleUI.Instance.Log( pair.Key + " " + parms );
    }
  }

  [DevConsole.Command( "mush" )]
  public static void Command_Mushroom( int index )
  {
    if( !Global.instance.Mushroom )
      return;
    if( index == -1 )
    {
      Global.instance.Mushroom.DisableLerp( 4 );
    }
    else
    {
      Global.instance.Mushroom.Enable( index );
    }
  }

  public void Mushroom_Effect( int index ) { Global.instance.Mushroom.Enable( index ); }
  public void Mushroom_FadeInEffect( int index, float duration ) { Global.instance.Mushroom.EnableLerp( duration, index ); }


  [DevConsole.Command( "killscreen" ), DevConsole.Params]
  public static void Command_Killscreen( params string[] args )
  {
    int index;
    if( args.Length == 1 && int.TryParse( args[0], NumberStyles.Integer, CultureInfo.InvariantCulture, out index ) )
    {
      if( index == -1 )
        Global.instance.Mushroom.DisableLerp( 4 );
      else
        Global.instance.KillScreen( index );
    }
    else if( args.Length == 1 && args[0] == "off" )
    {
      Global.instance.Mushroom.Disable();
    }
    else if( args.Length == 0 || (args.Length == 1 && args[0] == "final") )
    {
      Global.instance.FinalKillScreen();
    }
  }

  /*
  public float killscreenTileInterval = 0.2f;
  public Timer kst = new Timer();
  public TileBase[] killTiles;

  void ShuffleTiles()
  {
    if( Global.instance.CurrentPlayer == null )
      return;
    // find tilemap
    Vector2 ppos = Global.instance.CurrentPlayer.transform.position;
    Vector2Int box = new Vector2Int( 26, 16 );
    GameObject[] zones = GameObject.FindGameObjectsWithTag( "Zone" );
    foreach( var zone in zones )
    {
      // todo check bounds around player. get bounds from collider and check bounds overlap.
      if( zone.GetComponent<Collider2D>().OverlapPoint( ppos ) )
      {
        Tilemap[] tmaps = zone.GetComponentsInChildren<Tilemap>();
        if( tmaps.Length == 0 )
          return;
        for( int i = 0; i < tmaps.Length; i++ )
        {
          Tilemap map = tmaps[i];
          BoundsInt bounds = new BoundsInt( new Vector3Int( (int)ppos.x - box.x / 2, (int)ppos.y - box.y / 2, 0 ), new Vector3Int( box.x, box.y, 1 ) );
          TileBase[] tiles = map.GetTilesBlock( bounds );
          Util.Shuffle( ref tiles );

          map.SetTilesBlock( bounds, tiles );
          map.RefreshAllTiles();
        }
      }
    }
    kst.Start( killscreenTileInterval, null, ShuffleTiles );
  }
  */

#if false
  [DevConsole.Command( "listscenes" )]
  public static void Command_ListScenes()
  {
    for( int i = 0; i < SceneManager.sceneCountInBuildSettings; i++ )
      DevConsole.DevConsoleUI.Instance.Log( SceneUtility.GetScenePathByBuildIndex( i ) );
  }

  [DevConsole.Command( "loadscene" )]
  public static void Command_LoadScene( string scene ) { Global.instance.LoadScene( scene, default, null ); }

  public GameObject DebugLoot;

  [DevConsole.Command( "loot" )]
  public static void Command_Loot()
  {
    Vector2 pos = Global.instance.CameraController.CameraPosition;
    Instantiate( Global.instance.DebugLoot, pos + Vector2.up, Quaternion.identity );
  }

#if UNITY_EDITOR
  [ExposeMethod]
  public void GatherAllPrefabs()
  {
    ArrayUtility.Clear( ref SpawnablePrefabs );
    string[] guids = AssetDatabase.FindAssets( "t:prefab", new[] {"Assets/prefab"} );
    foreach( string guid in guids )
    {
      GameObject go = AssetDatabase.LoadAssetAtPath<GameObject>( AssetDatabase.GUIDToAssetPath( guid ) );
      if( go != null && go.GetComponentsInChildren<Entity>().Length > 0 )
        ArrayUtility.Add( ref SpawnablePrefabs, go );
    }
    EditorUtility.SetDirty( this );
  }
#endif


  // assigned in editor
  public GameObject[] SpawnablePrefabs;
  /*Dictionary<string, GameObject> SpawnableLookup = new Dictionary<string, GameObject>();*/

  [DevConsole.Command( "spawn" ), DevConsole.Params]
  public static void Command_Spawn( params string[] args )
  {
    if( args.Length == 0 )
      return;
    if( args[0].Equals( "list", System.StringComparison.OrdinalIgnoreCase ) )
    {
      string output = "";
      foreach( var item in Global.instance.SpawnablePrefabs )
        if( item != null )
          output += item.name + "; ";
      for( int i = 0; i < output.Length; i += 80 )
        output.Insert( i, "\n" );
      DevConsole.DevConsoleUI.Instance.Log( output );
    }
    else
    {
      Vector2 pos = Global.instance.CameraController.CameraPosition;
      for( int i = 0; i < args.Length; i++ )
        foreach( var item in Global.instance.SpawnablePrefabs )
          if( item != null && item.name.Replace( ' ', '_' ).Equals( args[i].Replace( ' ', '_' ), System.StringComparison.OrdinalIgnoreCase ) )
          {
            Object.Instantiate( item, pos, Quaternion.identity );
            break;
          }
    }
  }

  [DevConsole.Command( "show" ), DevConsole.Params]
  public static void Command_Show2( params string[] args )
  {
    if( args[0].Equals( "setting", System.StringComparison.OrdinalIgnoreCase ) || args[0].Equals( "settings", System.StringComparison.OrdinalIgnoreCase ) )
    {
      if( args.Length == 1 )
      {
        string output = "";
        output += "[Bool]\n";
        foreach( var pair in instance.BoolSetting )
          output += pair.Key + " " + pair.Value.Value + "\n";
        output += "[Float]\n";
        foreach( var pair in instance.FloatSetting )
          output += pair.Key + " " + pair.Value.Value + "\n";
        output += "[String]\n";
        foreach( var pair in instance.StringSetting )
          output += pair.Key + " " + pair.Value.Value + "\n";
        DevConsole.DevConsoleUI.Instance.Log( output );
      }
      else
      {
        if( instance.BoolSetting.ContainsKey( args[1] ) )
          DevConsole.DevConsoleUI.Instance.Log( instance.BoolSetting[args[1]].Value.ToString() );
        else if( instance.FloatSetting.ContainsKey( args[1] ) )
          DevConsole.DevConsoleUI.Instance.Log( instance.FloatSetting[args[1]].Value.ToString() );
        else if( instance.StringSetting.ContainsKey( args[1] ) )
          DevConsole.DevConsoleUI.Instance.Log( instance.StringSetting[args[1]].Value.ToString() );
        else
          DevConsole.DevConsoleUI.Instance.LogError( "Setting not found." );
      }
    }
    else
      DevConsole.DevConsoleUI.Instance.LogError( "Key Not found" );
  }

  [DevConsole.Command( "set" )]
  public static void Command_Set( string key, string value )
  {
    if( instance.BoolSetting.ContainsKey( key ) )
    {
      bool result;
      if( bool.TryParse( value, out result ) )
        instance.BoolSetting[key].Value = result;
      else
        DevConsole.DevConsoleUI.Instance.LogError( "Cannot parse bool value" );
    }
    else if( instance.FloatSetting.ContainsKey( key ) )
    {
      float result;
      if( float.TryParse( value, out result ) )
        instance.FloatSetting[key].Value = result;
      else
        DevConsole.DevConsoleUI.Instance.LogError( "Cannot parse float value" );
    }
    else if( instance.StringSetting.ContainsKey( key ) )
    {
      instance.StringSetting[key].Value = key;
    }
    else
      DevConsole.DevConsoleUI.Instance.LogError( "Setting \"" + key + "\" not found." );
  }
#endif

#endregion

  /*public Team GetTeam( TeamFlags flags )
  {
    // check exact flag combo first
    for( int i = 0; i < Teams.Length; i++ )
      if( Teams[i].flags == flags )
        return Teams[i];
    // return first team with any common allegiance
    for( int i = 0; i < Teams.Length; i++ )
      if( (Teams[i].flags & flags) > 0 )
        return Teams[i];
    return null;
  }*/

  public void SaveGame( string saveStateName )
  {
    // SerializedObject.WriteScene( "SAVETEST" );
    // todo serialize progress flags
  }

  public void LoadGame( string saveStateName )
  {
    // todo Deserialize progress flags
  }

#region Locale

#if false
  [Header( "Locale" )]
  public Locale[] Locales;
  public int SelectedLocaleIndex;

  public void SelectLocale( int i )
  {
    LocalizationSettings.SelectedLocale = Locales[i];
    SelectedLocaleIndex = i;
  }
#endif

#endregion

  /*
  #region Skins

    [System.Serializable]
    public struct SkinUnion
    {
      public Material sharedMaterial;
      public Texture2D[] diffuse;
      public Texture2D[] emissive;
    }

    public SkinUnion[] skins;
    int skinIndex;
    public int skinCount;

    public void NextSkin()
    {
      skinIndex = (skinIndex + 1) % skinCount;
      SetSkin( skinIndex );
    }

    public void SetSkin( int index )
    {
      for( int i = 0; i < skins.Length; i++ )
      {
        // simply setting the texture does not work for some reason.
        //skins[i].sharedMaterial.SetTexture( "_MainTex", skins[i].skin[index] );
        Texture2D tex = ((Texture2D)skins[i].sharedMaterial.GetTexture( "_MainTex" ));
        tex.SetPixels32( skins[i].diffuse[index].GetPixels32() );
        tex.Apply();
        tex = ((Texture2D)skins[i].sharedMaterial.GetTexture( "_EmissiveTex" ));
        tex.SetPixels32( skins[i].emissive[index].GetPixels32() );
        tex.Apply();

      }
    }

  #endregion
  */

#region HealthBar

  [Header( "HealthBar" )]
  public Image healthbarBG_Image;
  public Image healthbar_Image;
  public AudioClip HealthPip;
  Timer healthPipTimer = new Timer();
  GameObject healthPipAudioGO;

  public void OnPlayerHealthChange( int Health, int MaxHealth )
  {
    // UI Image
    Vector2 bgRect = healthbarBG_Image.rectTransform.sizeDelta;
    bgRect.y = 7f + 2f * MaxHealth;
    healthbarBG_Image.rectTransform.sizeDelta = bgRect;
    Vector2 bar = healthbar_Image.rectTransform.sizeDelta;
    bar.y = 2f * Health;
    healthbar_Image.rectTransform.sizeDelta = bar;

    float healthNormalized = ((float)Health / (float)MaxHealth);
    if( HealthAudioPitch )
    {
      MusicPitch( 0.5f + 0.5f * healthNormalized );
    }
    if( HealthTimescale )
    {
      CurrentTimescale = 0.5f + 0.5f * healthNormalized;
      Time.timeScale = CurrentTimescale;
    }
  }

  public void StartHealthClimb( int healthStart, int healthEnd, int max )
  {
    Global.instance.Pause();

    healthPipAudioGO = Instantiate( audioOneShotPrefab );
    AudioSource source = healthPipAudioGO.GetComponent<AudioSource>();

    int healthCount = healthStart;
    TimerParams prms = new TimerParams() { unscaledTime = true, loops = healthEnd - healthStart, interval = 0.1f, repeat = true };
    prms.IntervalDelegate = ( timer ) =>
    {
      source.PlayOneShot( HealthPip );
      Global.instance.OnPlayerHealthChange( ++healthCount, max );
    };
    prms.CompleteDelegate = () =>
    {
      Global.instance.Unpause();
      Destroy( healthPipAudioGO );
    };
    healthPipTimer.Start( prms );
  }


  public void StartHealthMaxClimb( int max0, int max1, int health )
  {
    Global.instance.Pause();
    int maxCount = max0;
    TimerParams prms = new TimerParams() { unscaledTime = true, loops = max1 - max0, interval = 0.1f, repeat = true };
    prms.IntervalDelegate = ( timer ) =>
    {
      AudioOneShot( HealthPip, Vector3.zero );
      Global.instance.OnPlayerHealthChange( ++health, ++maxCount );
    };
    prms.CompleteDelegate = () => { Global.instance.Unpause(); };
    healthPipTimer.Start( prms );
  }

#if SHAQ
  public Sprite[] Shaq;
  Timer dtimer = new Timer();

  public void DeathScreen( float durWait )
  {
    fader.gameObject.SetActive( true );
    fader.color = new Color( 1, 1, 1, 0 );
    if( BoolSetting["ShowImageUponDeath"].Value )
      fader.sprite = Shaq[Random.Range( 0, Shaq.Length )];
    else
      fader.sprite = null;

    if( BoolSetting["CreditsScrollUponDeath"].Value )
      CreditsScroller.StartScroll();

    dtimer.Start( 1, null, () =>
    {
      dtimer.Start( .5f, ( timer ) => { fader.color = new Color( 1, 1, 1, timer.ProgressNormalized ); }, () =>
      {
        fader.color = Color.white;
        dtimer.Start( durWait, null, () =>
        {
          dtimer.Start( .5f, ( timer ) => { fader.color = new Color( 1, 1, 1, 1f - timer.ProgressNormalized ); }, () =>
          {
            fader.gameObject.SetActive( false );
            CreditsScroller.StopScroll();
          } );
        } );
      } );
    } );
  }
#endif

  [SerializeField] GameObject GhostPrefab;
  Pawn ghostPawn;
  Pawn ghostModeCachedPawn;

  public void EnableGhostMode()
  {
    Vector3 pos = Vector3.zero;
    ghostModeCachedPawn = PlayerController.pawn;
    if( ghostModeCachedPawn )
      pos = ghostModeCachedPawn.transform.position;
    if( ghostPawn == null )
    {
      GameObject go = Instantiate( GhostPrefab, pos, Quaternion.identity );
      ghostPawn = go.GetComponent<Pawn>();
    }
    ghostPawn.transform.position = pos;
    PlayerController.AssignPawn( ghostPawn );
    HideHUD();
  }

  public void DisableGhostMode()
  {
    if( ghostModeCachedPawn )
      AssignCurrentPlayerAs( ghostModeCachedPawn );
    else
      SpawnPlayer();
    ghostModeCachedPawn = null;
  }

#endregion

  public void OverrideCameraZone( CameraZone zone ) { CameraController.AssignOverrideCameraZone( zone ); }

  public Mushroom Mushroom;

  public AudioEvent[] AudioEvents;

  const float KillScreenDuration = 6;
  const float KillScreenTransitionDuration = 2;

  const float FinalKill_ShakeDuration = 20;
  public AnimationCurve FinalKill_ShakeCurve;
  const float FinalKill_Height = 40;
  const float FinalKill_CruiseDistance = 50;
  bool FinalKillSequence;

  // public float FinalKill_SkaterScale = 4;

  public void FinalKillScreen()
  {
    // check if this is the last song in the track list
    if( BoolSetting["KillScreenAtEndOfAlbum"].Value )
    {
      FinalKillSequence = true;

      if( sceneScript is CityWrap cw )
        cw.DynamicSpawnEnabled = false;

      // final kill screen => credits
      if( PlayerController.pawn is PawnSkateboarder skater )
      {
        // faceplant
        skater.Faceplant( () =>
        {
          skater.Rider.localPosition = Vector3.zero;

          // shake
          CameraShake shaker = Global.instance.CameraController.GetComponent<CameraShake>();
          shaker.amplitude = 0.5f;
          shaker.duration = FinalKill_ShakeDuration;
          shaker.rate = 100;
          shaker.intensityCurve = FinalKill_ShakeCurve;
          shaker.enabled = true;

          CameraController.enabled = false;

          skater.enabled = false;
          if( skater.TryGetComponent( out SortingGroup cmp ) )
          {
            // stop dynamic sort
            cmp.sortingOrder++;
          }

          // rise into the sky
          int stage = 0;
          const float stage0 = 0.1f;
          const float stage1 = 0.3f;
          const float stage2 = 0.5f;
          Mushroom.EnableLerp( FinalKill_ShakeDuration * stage1, 9 );
          Vector2 skater_startpos = skater.transform.position;
          CameraController.Lerp( skater_startpos + Vector2.up * FinalKill_Height * stage1, 6, FinalKill_ShakeDuration * stage1 );
          Timer riser = new Timer( FinalKill_ShakeDuration, ( timer ) =>
          {
            skater.transform.position = skater_startpos + Vector2.up * FinalKill_Height * timer.ProgressNormalized;
            if( timer.ProgressNormalized < stage2 )
            {
              /*skater.transform.localScale = Vector3.one * Mathf.Lerp( 1, FinalKill_SkaterScale, timer.ProgressNormalized );*/
              /*skater.Board.localScale = skater.Rider.localScale;*/
            }

            if( stage == 0 && timer.ProgressNormalized > stage0 )
            {
              stage++;
              skater.animator.Play( "fall" );
              CameraController.Lerp( CameraController.transform.position + Vector3.up * FinalKill_Height * (stage1 - stage0), 6, FinalKill_ShakeDuration * (stage1 - stage0) );
            }

            if( stage == 1 && timer.ProgressNormalized > stage1 )
            {
              stage++;
              skater.animator.Play( "fast" );
              Mushroom.EnableLerp( FinalKill_ShakeDuration * (stage2 - stage1), 10 );
              CameraController.Lerp( CameraController.transform.position + Vector3.up * FinalKill_Height * (stage2 - stage1), 6, FinalKill_ShakeDuration * (stage2 - stage1) );
            }

            if( stage == 2 && timer.ProgressNormalized > stage2 )
            {
              stage++;
              skater.animator.Play( "turn_toward" );
              skater.Board.SetParent(skater.transform, true );
              skater.Board.localScale = Vector3.one;
              Vector2 skateboard_startpos = skater.Board.localPosition;
              Vector3 mush_start = Mushroom.MushCamera.transform.localPosition;
              float rider_scale_start = skater.transform.localScale.x;
              Mushroom.EnableLerp( FinalKill_ShakeDuration * (1 - stage2), 11 );
              Timer skateboard_lerp = new Timer( FinalKill_ShakeDuration * (1 - stage2),
                timer1 =>
                {
                  // skater.transform.localScale = Vector3.one * Mathf.Lerp( rider_scale_start, FinalKill_SkaterScale, timer.ProgressNormalized );
                  skater.Board.localPosition = Vector2.Lerp( skateboard_startpos, Vector2.zero, timer1.ProgressNormalized );
                  Mushroom.MushCamera.transform.localPosition = Vector3.Lerp( mush_start, Vector3.back * 3f + Vector3.right * 0.2f, timer1.ProgressNormalized );
                },
                () =>
                {
                  // skater.transform.localScale = Vector3.one * FinalKill_SkaterScale;
                  skater.Board.localPosition = Vector3.zero;
                  Mushroom.MushCamera.transform.localPosition = Vector3.back * 3f + Vector3.right * 0.2f;
                } );
              CameraController.Lerp( skater_startpos + Vector2.up * FinalKill_Height + Vector2.right * 7f, 6, FinalKill_ShakeDuration * (1f - stage2) );
            }
          }, () =>
          {
            skater.animator.Play( "fast" );
            skater_startpos = skater.transform.position;
            const float cruise_duration = 6;
            new Timer( cruise_duration, ( timer ) => { skater.transform.position = skater_startpos + (Vector2.right * FinalKill_CruiseDistance + Vector2.up * FinalKill_Height) * timer.ProgressNormalized; }, CreditsScroller.StartScroll );
            CameraController.Lerp( skater_startpos + (Vector2.right * FinalKill_CruiseDistance + Vector2.up * FinalKill_Height) + Vector2.right * 7f, 6, cruise_duration );
          } );
        } );
      }
    }
  }

  void EndFinalKillSequence()
  {
    FinalKillSequence = false;

    if( sceneScript is CityWrap cw )
      cw.DynamicSpawnEnabled = true;

    CameraController.enabled = true;
    CameraController.StopLerp( true );
    Mushroom.MushCamera.transform.localPosition = Vector3.back * 3f;

    Mushroom.DisableLerp( 5 );

    if( PlayerController.pawn is PawnSkateboarder skater )
    {
      skater.enabled = true;
      if( skater.TryGetComponent( out SortingGroup cmp ) )
      {
        // re-enable dynamic sort
        cmp.sortingOrder = 10;
      }
    }
    CreditsScroller.StopScroll();
  }

  public void KillScreen( int index )
  {
    if( BoolSetting["KillScreenAtEndOfTrack"].Value )
    {
      if( PlayerController.pawn is PawnSkateboarder skater )
      {
        // faceplant
        skater.Faceplant( () =>
        {
          Mushroom.EnableLerp( KillScreenTransitionDuration, index );
          Timer killscreen = new Timer( KillScreenDuration, null, () =>
          {
            Mushroom.DisableLerp( 6 );
            musicCycleTimer.Stop( true );
            skater.RespawnWithGracePeriod( CycleSpawnPosition() );
          } );
        } );
      }
    }
  }
}