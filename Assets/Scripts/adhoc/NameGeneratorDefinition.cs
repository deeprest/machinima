﻿// Weird But Pronouncable Name Generator

using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[System.Serializable]
public class NameGenerator
{
  public string[] Separators = new[] {"_"};
  // source data replaces desired delimeter with this to have a readble debug char.
  const string delim = "_";
  public string DefaultTrainingData = "Abby_Abigail_Alexandria_Alice_Alicia_Allison_Alyssa_Andrea_Angelica_Angie_Anna_Annabelle_Anne_Ariel_Audrey_Barbara_Beatrice_Becky_Bernadette_Beth_Bethany_Betty_Betsy_Beverly_Bonnie_Brianna_Bridget_Brooke_Carly_Carol_Carrie_Catherine_Cathy_Cecilia_Celeste_Charlene_Charlotte_Christine_Christina_Cindy_Connie_Constance_Crystal_Cynthia_Daisy_Danielle_Deanna_Dee_Deirdre_Diana_Diane_Donna_Dora_Dorothy_Dot_Dotty_Edith_Edna_Elaine_Eliza_Elizabeth_Ellen_Emily_Emma_Erica_Erin_Erma_Ernestine_Esmerelda_Esther_Evelyn_Fanny_Fiona_Florence_Fran_Francis_Gabby_Gabriel_Gail_Georgette_Georgina_Gina_Ginger_Gloria_Grace_Gwen_Hannah_Harriet_Heather_Helen_Henrietta_Ida_Isabelle_Jackie_Jane_Janet_Janie_Jacqueline_Jasmine_Jeanette_Jennifer_Jenny_Jessica_Jo_Jody_Josie_Jolene_Josephine_Joyce_Judy_Judith_Julie_Karen_Katie_Kathy_Katherine_Kathleen_Kelly_Kimberly_Laura_Laurie_Leslie_Lila_Linda_Lisa_Lois_Lucille_Lucy_Lydia_Lynn_Mabel_Mavis_Mae_Mandy_Margaret_Marge_Maria_Martha_Martina_Mary_Maxine_Melanie_Melinda_Melissa_Melody_Meredith_Michelle_Millicent_Millie_Mindy_Minnie_Molly_Monica_Nadine_Nancy_Natalie_Nell_Nicole_Norma_Olga_Olive_Olivia_Pam_Pamela_Pat_Patricia_Patty_Paula_Pauline_Phyllis_Polly_Rachel_Rebecca_Regina_Renee_Rita_Rose_Rosie_Ruth_Sabrina_Sally_Samantha_Sandra_Sandy_Sarah_Sharon_Shelby_Sherrie_Sophia_Sophie_Stacey_Stephanie_Sue_Susan_Suzanne_Tabitha_Tammy_Tanya_Teresa_Thelma_Tina_Tracy_Trina_Tricia_Trish_Ursula_Valerie_Vanessa_Veronica_Vicky_Victoria_Viola_Violet_Wanda_Wendy_Yolanda_Zelda_Zoe_Aaron_Abraham_Adam_Alan_Alec_Alex_Alexander_Amos_Andrew_Anthony_Andy_Arnold_Arthur_Avery_Barry_Bart_Bartholomew_Benjamin_Bernard_Benny_Bill_Bob_Bobby_Brad_Bradley_Brent_Brett_Brian_Bruce_Burt_Byron_Caleb_Calvin_Carl_Chad_Chadwick_Chandler_Charles_Charlie_Chris_Christopher_Chuck_Clark_Cliff_Cole_Colin_Cory_Dale_Daniel_Darrel_Darren_Dave_David_Dennis_Derek_Devin_Dick_Dirk_Donald_Donny_Doug_Douglas_Drake_Drew_Dwain_Dwight_Edgar_Edward_Edwin_Elliot_Eric_Ernest_Ernie_Frank_Franklin_Fred_Frederick_Gary_George_Geoffrey_Glenn_Gordon_Graham_Grant_Greg_Gregory_Harold_Harry_Harvey_Heidi_Henry_Howard_Hunter_Ichabod_Isaac_Ivan_Jack_James_Jason_Jay_Jeff_Jeremy_Jerome_Jim_Jimmy_John_Jonathan_Jordan_Joe_Joel_Joseph_Joshua_Justin_Keith_Kareem_Kenneth_Kevin_Kirk_Kurt_Kyle_Lance_Larry_Lawrence_Lee_Leonard_Lester_Lionel_Llewellyn_Lou_Louis_Lyle_Mark_Mashall_Martin_Marty_Marvin_Matt_Matthew_Maurice_Michael_Nathan_Nathaniel_Ned_Neil_Nicholas_Nick_Nolan_Norbert_Norman_Oliver_Oscar_Oswald_Patrick_Paul_Peter_Phillip_Quincy_Ralph_Randy_Ray_Raymond_Reuben_Richard_Rick_Ricky_Robbie_Robert_Robin_Rodney_Roger_Ron_Ronald_Ronnie_Russell_Rusty_Ryan_Sam_Samuel_Scott_Shawn_Sheldon_Sidney_Simon_Spencer_Stanley_Steve_Steven_Stewart_Taylor_Ted_Theodore_Thomas_Tim_Timothy_Todd_Tom_Tommy_Tony_Travis_Trent_Trevor_Tyler_Ulysses_Victor_Wallace_Wally_Ward_Wayne_Wendall_William_Xavier_Yancy_Anderson_Andrews_Ashford_Atwood_Austen_Bacall_Baker_Ball_Barkley_Barnett_Barrett_Barrow_Bates_Baxter_Beamer_Bell_Birch_Bishop_Black_Blackstone_Blank_Brach_Brennan_Brenner_Brewer_Brightman_Brooks_Brown_Burns_Burnett_Butler_Byrd_Cane_Capes_Carpenter_Carroll_Carter_Cartwright_Cedar_Clark_Clay_Claymont_Clements_Cline_Coleman_Collins_Conley_Connelly_Connery_Conway_Crawford_Crowe_Daniels_Davenport_Davis_Dickens_Dickinson_Donaldson_Donovan_Draper_Drysdale_East_Elm_Fields_Fisher_Fletcher_Forrester_Foster_Francis_Franklin_Frederickson_Freeman_Gaines_Gates_Gatlin_George_Gordon_Gorman_Grant_Gray_Green_Griggs_Jackson_Jameson_Jennings_Jensen_Johnson_Jones_Jordan_Hampton_Harding_Harmon_Harper_Harris_Harrison_Hart_Higgins_Hodges_Holland_Hollinsworth_Holloway_Hopper_Howard_Kennedy_King_Kingsley_Kingston_Kirkpatrick_Lake_Lancaster_Lane_Lansford_Lee_Lewis_Lightfoot_Little_Long_Maltin_Mandrell_Maple_Marsh_Marshall_Martin_Martinelli_Masterson_May_McCoy_McGregor_Michaels_Miguez_Miller_Mills_Milne_Monty_Moore_Morris_Morrow_Nash_Nelson_Niles_Norman_North_Oak_Olsen_Parker_Parks_Pearl_Pearson_Perry_Peters_Peterson_Pine_Porter_Powell_Prescott_Presley_Preston_Price_Principal_Redford_Reeve_Reynolds_Richards_Richardson_Rivers_Roberts_Robins_Robinson_Rogers_Romano_Romero_Russell_Ryan_Sands_Sanders_Schmidt_Sheppard_Sherwood_Simon_Simpson_Smith_South_Spade_Spader_Sparks_Stanley_Steele_Stein_Steinberg_Stevens_Stewart_Stone_Summers_Swanson_Taylor_Tell_Teller_Thatcher_Thompson_Trent_Tyler_Vaughn_Verne_Wagner_Walsh_Ward_Warner_Washington_Watkins_Watson_Webber_Wells_Welsh_Wendall_West_Westmore_White_Williams_Wilson_Winston_Winters_Woodall_Woods_Worth_Wright_Wynne_Yates_Zane";
  public bool RandomSeed;
  public int Seed = 0;

  [Tooltip( "Normally, this script generates a weird name. This outputs a selected first name, too." )]
  public bool PrefixSelectedFirstName;

  public string InitialString;
  [Tooltip( "Select a random first letter from the training data, as opposed to a completely random letter." )]
  public bool SelectFirstCharFromData = true;

  public int MinLength = 4;
  public int MaxLength = 8;

  [Header( "Advanced" )]
  [Tooltip( "Randomly select from a weighted list (recommended)" )]
  public bool SelectWeighted = true;
  [Tooltip( "If SelectedWeighted is false, these apply:" )]
  public int SelectMostCommonCount = 3;
  [Tooltip( "This will gradually modify the model." )]
  public bool SampleOutput;

  // The model
  Dictionary<string, Dictionary<string, Dictionary<string, Datum>>> model = new Dictionary<string, Dictionary<string, Dictionary<string, Datum>>>();

  List<string> firstNames;

  public class Datum
  {
    public Datum( string a, string b, string c )
    {
      this.a = a;
      this.b = b;
      this.c = c;
      occurences = 1;
    }

    public string a;
    public string b;
    public string c;
    public int occurences;
  };

  public void SeedGenerator() { Random.InitState( Seed ); }

  public void Clear() { model.Clear(); }

  public void Dump()
  {
    string output = "";
    foreach( var a in model )
    foreach( var b in a.Value )
    foreach( var c in b.Value )
      output += a.Key + b.Key + "=" + c.Key + " (" + c.Value.occurences + ")\n";
    Debug.Log( output );
  }

  void AddSample( string a, string b, string c, int value = 1 )
  {
    if( model.ContainsKey( a ) )
    {
      if( model[a].ContainsKey( b ) )
      {
        if( model[a][b].ContainsKey( c ) )
          model[a][b][c].occurences += value;
        else
          model[a][b].Add( c, new Datum( a, b, c ) );
      }
      else
      {
        model[a].Add( b, new Dictionary<string, Datum>() );
        model[a][b].Add( c, new Datum( a, b, c ) );
      }
    }
    else
    {
      model.Add( a, new Dictionary<string, Dictionary<string, Datum>>() );
      model[a].Add( b, new Dictionary<string, Datum>() );
      model[a][b].Add( c, new Datum( a, b, c ) );
    }
  }

  public void TrainDefault( int multiplier = 1 ) { Train( DefaultTrainingData, multiplier ); }

  public void Train( string data, int multiplier = 1 )
  {
    // populate list to select a first name
    firstNames = new List<string>( DefaultTrainingData.Split( Separators, System.StringSplitOptions.None ) );

    // replace given delimiter with one that is guaranteed to be readable, for debugging.
    data = string.Join( delim, data.Split( Separators, System.StringSplitOptions.None ) ).ToLower();

    string a, b, c;
    for( int i = 0; i < data.Length; i++ )
    {
      if( i == 0 )
        a = delim;
      else
        a = data[i - 1].ToString();
      b = data[i].ToString();
      if( i < data.Length - 1 )
        c = data[i + 1].ToString();
      else
        c = delim;
      AddSample( a.ToLower(), b.ToLower(), c.ToLower(), multiplier );
    }
  }

  string GetNext( string a, string b )
  {
    string next = delim;
    if( model.ContainsKey( a ) )
    {
      if( model[a].ContainsKey( b ) )
      {
        Datum[] ray = new Datum[model[a][b].Keys.Count];
        model[a][b].Values.CopyTo( ray, 0 );
        List<Datum> list = new List<Datum>( ray );
        list.Sort( delegate( Datum x, Datum y )
        {
          if( x.occurences == y.occurences )
            return 0;
          else if( x.occurences > y.occurences )
            return -1;
          else
            return 1;
        } );

        if( SelectWeighted )
        {
          // randomly select from a weighted list
          int sum = 0;
          for( int i = 0; i < list.Count; i++ )
            sum += list[i].occurences;
          int randomIndex = Random.Range( 0, sum - 1 );
          int run = 0;
          for( int i = 0; i < list.Count; i++ )
          {
            run += list[i].occurences;
            if( run > randomIndex )
            {
              next = list[i].c;
              break;
            }
          }
        }
        else
        {
          // randomly select from values in the map
          next = list[Random.Range( 0, Mathf.Min( SelectMostCommonCount, list.Count ) )].c;
        }
      }
    }
    return next;
  }

  string RandomLetter()
  {
    const string alpha = "abcdefghijklmnopqrstuvwxyz";
    return alpha[Random.Range( 0, alpha.Length )].ToString();
  }

  string RandomNextLetter( string c )
  {
    const string vowel = "aeiou";
    const string consonant = "bcdfghjklmnpqrstvwxyz";
    if( consonant.Contains( c ) )
      return vowel[Random.Range( 0, vowel.Length )].ToString();
    return consonant[Random.Range( 0, consonant.Length )].ToString();
  }

  public string Generate()
  {
    if( model == null || model.Keys.Count == 0 )
      TrainDefault();

    string output = "";
    string a = delim;
    string b = "";
    string c = "";
    if( !string.IsNullOrEmpty( InitialString ) )
    {
      output = InitialString.Substring( 0, InitialString.Length - 1 );
      b = InitialString.Substring( InitialString.Length - 1 );
      if( InitialString.Length > 1 )
        a = InitialString.Substring( InitialString.Length - 2, 1 );
    }
    else if( SelectFirstCharFromData )
    {
      // choose random first letter from table
      int ri = Random.Range( 0, model[a].Keys.Count );
      var enu = model[a].Keys.GetEnumerator();
      enu.MoveNext();
      for( int i = 0; i < ri; i++ )
        enu.MoveNext();
      b = enu.Current;
    }
    else
    {
      // random first letter
      b = RandomLetter();
    }

    // initial letter
    output += b;
    for( int i = output.Length; i < MaxLength; i++ )
    {
      c = GetNext( a, b );
      if( c == delim )
      {
        if( i >= MinLength )
          break;
        // char not found
        c = RandomNextLetter( b );
        if( SampleOutput )
          AddSample( a, b, c );
      }
      else if( SampleOutput )
      {
        AddSample( a, b, c );
      }
      output += c;
      a = b;
      b = c;
    }
    // capitalize first letter
    output = output[0].ToString().ToUpper() + output.Substring( 1 );

    if( PrefixSelectedFirstName )
      output = output.Insert( 0, firstNames[Random.Range( 0, firstNames.Count )] + " " );

    return output;
  }
}