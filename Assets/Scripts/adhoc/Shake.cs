﻿using UnityEngine;
using System.Collections;

public class Shake : MonoBehaviour
{
  public Transform target;
  public float amplitude = 1;
  public bool EnableTimeout;
  public float duration = 3;
  public float rate = 32;
  public AnimationCurve intensityCurve;
  public Vector3 localShakeAmount;

  Vector3 localStart;
  float startTime;

  void OnEnable()
  {
    if( target )
      localStart = target.localPosition;
    startTime = Time.time;
  }

  void OnDisable() { target.localPosition = localStart; }

  void Update()
  {
    float localTime = (Time.time - startTime);
    Vector3 pos = Vector3.zero;
    pos.x = localShakeAmount.x * Mathf.Sin( localTime * rate ) * intensityCurve.Evaluate( localTime / duration );
    pos.y = localShakeAmount.y * Mathf.Cos( localTime * rate ) * intensityCurve.Evaluate( localTime / duration );
    pos.z = 0;
    target.localPosition = localStart + (pos * amplitude);
    if( EnableTimeout && localTime > duration )
      enabled = false;
  }
}