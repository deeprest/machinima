﻿using UnityEngine;

[ExecuteAlways]
public class ConstantRotation : MonoBehaviour /*, IVelocity*/
{
  public bool PausedInEditMode;
  [SerializeField] bool unscaledTime;
  [SerializeField] Transform target;
  // [SerializeField] Rigidbody2D body;
  public float speed = 1;
  float rot;

  /*public Vector2 Velocity { get { return Vector2.zero; } }

  public Vector2 GetVelocityAtPoint( Vector2 point )
  {
    if( body )
      return body.GetPointVelocity( point );
    return Vector2.zero;
  }*/

  void Update()
  {
    if( !Application.isEditor || !PausedInEditMode )
    {
      /*if( body )
        body.angularVelocity = speed;
      else */
      if( target != null )
      {
        rot += speed * (unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime);
        target.rotation = Quaternion.Euler( 0, 0, rot );
      }
    }
  }
}