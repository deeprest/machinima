﻿using UnityEngine;

// Act like a child without being one.
[ExecuteAlways]
public class TransformChild : MonoBehaviour
{
  [SerializeField] Transform parent;
  [SerializeField] Vector2 localPosition;
  [SerializeField] float localZRot;

  void LateUpdate()
  {
    if( parent )
    {
      transform.rotation = parent.rotation * Quaternion.Euler( 0, 0, localZRot );
      transform.position = parent.position + parent.rotation * localPosition;
    }
  }

  [ExposeMethod]
  void CopyTransform()
  {
    localPosition = transform.localPosition;
    localZRot = transform.rotation.eulerAngles.z;
  }
}