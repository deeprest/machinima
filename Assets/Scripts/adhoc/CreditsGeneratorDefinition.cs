using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

// This contains the format weights, and show how to use the name generator.
[CreateAssetMenu()]
public class CreditsGeneratorDefinition : ScriptableObject
{
  public List<CreditsGenerator.FormatWeight> formatWeights;

  [ExposeMethod]
  void Gen()
  {
    CreditsGenerator generator = new CreditsGenerator();
    generator.formatWeights = formatWeights;
    Debug.Log( generator.GenName() );
  }
}

[System.Serializable]
public class CreditsGenerator
{
  public List<FormatWeight> formatWeights;

  [System.NonSerialized] string[] Rank =
  {
    "Executive", "Senior", "Assistant", "Associate", "Consulting", "Junior"
  };
  [System.NonSerialized] string[] Title =
  {
    "Producer", "President", "Vice President", "Director", "Technical Director", "Head", "Coordinator"
  };

  [System.NonSerialized] string[] Department =
  {
    "Production", "Operations", "Development", "Art", "Engineering", "Design", "Management", "Animation", "Audio", "Sound",
    "Payroll", "Accounting", "Rigging", "Visualization", "FX", "Research", "Linguistics", "Catering", "Metrics", "Planning",
    "Human Resources", "Animal Resources", "Monetization", "Ergonomics", "Quality Assurance", "Legal", "Special Equipment",
    "Phlebotomy", "Courage", "Zen", "Biscuits", "Leisure", "Beans", "Ideas", "Incredulity", "Promotions", "Fabrication",
    "Marketing", "Sales", "Graphics Arts", "Optical Effects"
  };
  [System.NonSerialized] string[] DepartmentSuffix =
  {
    "Lead", "Director", "Supervisor", "Consultant", "Support", "Facilitator", "Stenographer", "Officer", "Coordinator",
    "Assistant", "Services"
  };

  [System.NonSerialized] string[] Position =
  {
    "Artist", "Concept Artist", "Modeler", "Texture Artist", "Animator", "Visual FX Artist",
    "Designer", "Producer", "Writer", "Story Editor",
    "Engineer", "Programmer", "Scripter", "Tester",
    "Recording Mixer", "Foley", "Sequencer", "Re-Recording Engineer",
    "Researcher", "Tenderizer", "Assistant",
    "Cook", "Chef", "Culinary Artist", "Sandwich Distributor",
    "Accountant", "Contrarian",
    "Grip", "Phlebotomist", "Driver",
    "Unit Publicist", "Color Timer", "Dolby Consultant"
  };

  /*[System.NonSerialized] string[] Food =
  {
    "Bean", "Noodle", "Cuisine", "Taco", "Burger", "Sandwich", "Pancake", "Pizza", "Lemonade", "Waffle", "Cracker",
    "Nugget", "Food", "Edible", "Stuff"
  };*/

  public enum FormatType
  {
    // Director of Design, Head of Art, Animation, Music, Sound, Marketing, Customer Service, Accounting
    TitleOfDepartment,
    // Associate Director of Design
    RankTitleOfDepartment,
    // Design Lead, Art Director
    DepartmentSuffix,
    // Artist, Designer, Engineer
    Position,
    // Consulting Researcher
    RankPosition,
  }

  [System.NonSerialized] FormatType format;

  [System.Serializable]
  public struct FormatWeight
  {
    public FormatType type;
    public float weight;
  }

  float totalWeight;

  int iRank;
  int[] rankIndex;
  int iTitle;
  int[] titleIndex;
  int iDepartment;
  int[] departmentIndex;
  int iDepartmentSuffix;
  int[] departmentSuffixIndex;
  int iPosition;
  int[] positionIndex;

  public CreditsGenerator()
  {
    if( formatWeights == null || formatWeights.Count != System.Enum.GetValues( typeof(FormatType) ).Length )
    {
      formatWeights = new List<FormatWeight>();
      FormatType[] types = (FormatType[]) System.Enum.GetValues( typeof(FormatType) );
      string[] names = System.Enum.GetNames( typeof(FormatType) );
      for( int i = 0; i < names.Length; i++ )
        formatWeights.Add( new FormatWeight() {type = types[i], weight = 1} );
    }
    CalculateFormatWeights();

    iRank = 0;
    rankIndex = Util.ShuffledIndexList( Rank.Length );
    iTitle = 0;
    titleIndex = Util.ShuffledIndexList( Title.Length );
    iDepartment = 0;
    departmentIndex = Util.ShuffledIndexList( Department.Length );
    iDepartmentSuffix = 0;
    departmentSuffixIndex = Util.ShuffledIndexList( DepartmentSuffix.Length );
    iPosition = 0;
    positionIndex = Util.ShuffledIndexList( Position.Length );
  }

  public string GenName()
  {
    format = GetRandomFormatWeighted();
    string rank = Rank[rankIndex[iRank % rankIndex.Length]];
    string title = Title[titleIndex[iTitle % titleIndex.Length]];
    string dept = Department[departmentIndex[iDepartment % departmentIndex.Length]];
    string deptSuffix = DepartmentSuffix[departmentSuffixIndex[iDepartmentSuffix % departmentSuffixIndex.Length]];
    string position = Position[positionIndex[iPosition % positionIndex.Length]];

    /*string foodPlural = food + "s";
    if( food.EndsWith( "ch" ) | food.EndsWith( "sh" ) | food.EndsWith( "s" ) | food.EndsWith( "x" ) |
      food.EndsWith( "z" ) )
      foodPlural = food + "es";*/

    string genname = "";
    if( format == FormatType.TitleOfDepartment )
    {
      genname = string.Join( " ", title, "of", dept );
      iTitle++;
      iDepartment++;
    }
    if( format == FormatType.RankTitleOfDepartment )
    {
      genname = string.Join( " ", rank, title, "of", dept );
      iRank++;
      iTitle++;
      iDepartment++;
    }
    if( format == FormatType.DepartmentSuffix )
    {
      genname = string.Join( " ", dept, deptSuffix );
      iDepartment++;
      iDepartmentSuffix++;
    }
    if( format == FormatType.Position )
    {
      genname = position;
      iPosition++;
    }
    if( format == FormatType.RankPosition )
    {
      genname = string.Join( " ", rank, position );
      iRank++;
      iPosition++;
    }

    return genname;
  }

  void CalculateFormatWeights()
  {
    totalWeight = 0;
    for( int i = 0; i < formatWeights.Count; i++ )
      totalWeight += formatWeights[i].weight;
  }

  FormatType GetRandomFormatWeighted()
  {
    float randy = Random.Range( 0, totalWeight );
    float runningTotal = 0;
    for( int i = 0; i < formatWeights.Count; i++ )
    {
      runningTotal += formatWeights[i].weight;
      if( runningTotal > randy )
        return formatWeights[i].type;
    }
    return formatWeights[0].type;
  }
}