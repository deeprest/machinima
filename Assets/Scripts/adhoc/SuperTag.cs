﻿using UnityEngine; 

#if UNITY_EDITOR
using UnityEditor;
#endif

public class SuperTag : MonoBehaviour
{
#if UNITY_EDITOR
  [ExposeMethod]
  public void AutoSizeMe() { StaticUtility.AutoFixSprite( GetComponent<SpriteRenderer>() );}
  public bool IgnoreBoxCollider2D;
#endif

  public bool NoNavObstacle;
}