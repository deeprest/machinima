﻿using UnityEngine;

public class GenerationVariant : MonoBehaviour
{
  public bool CITY_LOOP;
  public bool GenOnStart;
  public GameObject[] random;

  void Start()
  {
    if( GenOnStart )
      Generate();
  }

  public GameObject Generate()
  {
    if( random.Length > 0 )
    {
      GameObject prefab = random[Random.Range( 0, random.Length )];
      if( prefab != null )
      {
        GameObject go;
        if( Application.isPlaying )
          go = Global.instance.Spawn( prefab, transform.position, Quaternion.identity, transform.parent );
        else
          go = Instantiate( prefab, transform.position, Quaternion.identity, transform.parent );

        if( go && CITY_LOOP )
        {
          // For city loop/wrap support, make sure dynamically spawned objects have no parent.
          // If they do, and they wander away from the parent zone, when the zone relocates,
          // the child will pop outof/into view.
          Entity entity = go.GetComponentInChildren<Entity>();
          if( entity && !entity.IsStatic )
            go.transform.parent = null; 
        }
        return go;
      }
    }
    return null;
  }
}