﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using static System.String;

namespace DevConsole
{
  public class DevConsoleUI : MonoBehaviour
  {
    [SerializeField] int MaxCharacterCount = 10000;
    [SerializeField] TMPro.TMP_Text _output;
    [SerializeField] TMPro.TMP_InputField _input;
    private List<string> _history;
    private int _backlogIdx;

    public static DevConsoleUI Instance;

    public DevConsoleUI()
    {
      _history = new List<string>();
      DevConsole.RegisterCommands();
      Instance = this;
    }

    public void Activate()
    {
      gameObject.SetActive( true );
      _input.ActivateInputField();
    }

    public void Deactivate() { gameObject.SetActive( false ); }

    private void Update()
    {
      // hack: shim for broken enter key.
      if( Keyboard.current.leftMetaKey.isPressed && Keyboard.current.semicolonKey.wasPressedThisFrame )
        ExecuteCommand( _input.text );
      // /hack

      if( Keyboard.current.enterKey.wasPressedThisFrame )
        ExecuteCommand( _input.text );
      else if( Keyboard.current.upArrowKey.wasPressedThisFrame )
      {
        if( _backlogIdx < 0 )
          return;
        _input.text = _history[_backlogIdx];
        if( _backlogIdx > 0 )
          _backlogIdx--;
      }
      else if( Keyboard.current.downArrowKey.wasPressedThisFrame )
      {
        if( _backlogIdx > _history.Count - 1 )
          return;
        _input.text = _history[_backlogIdx];
        if( _backlogIdx < _history.Count - 1 )
          _backlogIdx++;
      }
    }

    public void Log( string message )
    {
      _output.text += $"{message}\n";
      TruncateOutputString();
    }

    public void LogWarning( string message )
    {
      _output.text += $"<color=yellow>{message}</color>\n";
      TruncateOutputString();
    }

    public void LogError( string message )
    {
      _output.text += $"<color=red>{message}</color>\n";
      TruncateOutputString();
    }

    void TruncateOutputString()
    {
      if( _output.text.Length < MaxCharacterCount )
        return;
      // avoid breaking the color tags
      string output = _output.text.Substring( Mathf.Max( 0, _output.text.Length - MaxCharacterCount ), Mathf.Min( _output.text.Length, MaxCharacterCount ) );
      int index = output.IndexOf( "</color>" ) + 1;
      output = output.Substring( index, output.Length - index );
      _output.text = output;
    }

    private void ExecuteCommand( string input )
    {
      var (command, args) = ParseCommand( input );
      _history.Add( input );
      _backlogIdx = _history.Count - 1;
      Log( $"> {input}" );
      DevConsole.ExecuteCommand( command, args );
      _input.text = Empty;
      _input.ActivateInputField();
    }

    private static (string, string[]) ParseCommand( string input )
    {
      var split = input.Split( ' ' );
      var command = split[0];
      var args = split.Skip( 1 ).ToArray();
      return (command, args);
    }

    public void Clear()
    {
      _history.Clear();
      _output.text = "";
    }
  }
}