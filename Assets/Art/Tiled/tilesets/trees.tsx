<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.2" name="trees" tilewidth="261" tileheight="200" tilecount="12" columns="0" objectalignment="bottom" fillmode="preserve-aspect-fit">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="187" height="160" source="trees/Tree1.png"/>
 </tile>
 <tile id="1">
  <image width="56" height="75" source="trees/Small Tree1.png"/>
 </tile>
 <tile id="2">
  <image width="160" height="160" source="trees/Tree2.png"/>
 </tile>
 <tile id="3">
  <image width="176" height="160" source="trees/Tree3.png"/>
 </tile>
 <tile id="4">
  <image width="133" height="200" source="trees/Tree4.png"/>
 </tile>
 <tile id="5">
  <image width="142" height="200" source="trees/Tree5.png"/>
 </tile>
 <tile id="6">
  <image width="155" height="160" source="trees/Tree6.png"/>
 </tile>
 <tile id="7">
  <image width="261" height="200" source="trees/Tree7.png"/>
 </tile>
 <tile id="8">
  <image width="163" height="150" source="trees/Tree8.png"/>
 </tile>
 <tile id="9">
  <image width="180" height="160" source="trees/Tree9.png"/>
 </tile>
 <tile id="10">
  <image width="177" height="160" source="trees/Tree10.png"/>
 </tile>
 <tile id="11">
  <image width="141" height="150" source="trees/Tree11.png"/>
 </tile>
</tileset>
