<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.2" name="street_stuff" tilewidth="16" tileheight="32" tilecount="12" columns="4">
 <grid orientation="orthogonal" width="16" height="16"/>
 <image source="../../various images/street_stuff.png" width="64" height="96"/>
 <tile id="2">
  <objectgroup draworder="index" id="2">
   <object id="1" x="2" y="22" width="11" height="6"/>
  </objectgroup>
 </tile>
 <tile id="3">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="28">
    <polygon points="0,2 5,6 15,6 15,3 11,0 0,0"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
