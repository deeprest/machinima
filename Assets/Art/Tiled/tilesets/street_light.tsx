<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.2" name="street_light" tilewidth="32" tileheight="128" tilecount="2" columns="2">
 <image source="../../various images/street_light.png" width="64" height="128"/>
 <tile id="0">
  <objectgroup draworder="index" id="2">
   <object id="3" x="18" y="125">
    <polygon points="2,0 9,3 14,0 9,-2"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1">
  <objectgroup draworder="index" id="2">
   <object id="2" x="-2" y="125">
    <polygon points="2,0 9,3 14,0 9,-2"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
