<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.2" name="buildings_0" tilewidth="800" tileheight="236" tilecount="4" columns="0">
 <grid orientation="orthogonal" width="16" height="16"/>
 <tile id="0">
  <image width="800" height="160" source="../../Aseprite/Dredzkas-s_shore-160pxH.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="31" y="21" width="539" height="139"/>
  </objectgroup>
 </tile>
 <tile id="1">
  <image width="552" height="160" source="../../Aseprite/Hibatchi.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="37" y="78" width="487" height="82"/>
  </objectgroup>
 </tile>
 <tile id="2">
  <image width="273" height="159" source="../../Aseprite/x-ray_arcade.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="19" y="41" width="246" height="118"/>
  </objectgroup>
 </tile>
 <tile id="3">
  <image width="694" height="236" source="../../various images/Dred-isotest.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="76" y="235">
    <polygon points="0,0 -43,-42 -44,-181 133,-180 179,-135 179,-78 285,-78 287,-128 397,-124 399,-80 533,-80 533,-1"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
