<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.2" name="tc_tileset" tilewidth="16" tileheight="16" tilecount="192" columns="16" objectalignment="bottomleft" fillmode="preserve-aspect-fit">
 <image source="../newset.png" width="256" height="192"/>
 <tile id="0">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="86">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="87">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="84">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="85">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="82">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="83">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="80">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="81">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="91">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="107">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="106">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="90">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="72">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="73">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="15">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="13">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="14">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="88">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="89">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="74">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="75">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="19">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="13" width="16" height="5"/>
  </objectgroup>
 </tile>
 <tile id="20">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="11" width="16" height="5"/>
  </objectgroup>
 </tile>
 <tile id="22"/>
 <tile id="23">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="11" width="16" height="7"/>
  </objectgroup>
 </tile>
 <tile id="24">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="11" width="16" height="7"/>
  </objectgroup>
 </tile>
 <tile id="25">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="12" width="16" height="6"/>
  </objectgroup>
 </tile>
 <tile id="26">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="12" width="16" height="6"/>
  </objectgroup>
 </tile>
 <tile id="27">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="28">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="3" y="4" width="10" height="12">
    <properties>
     <property name="unity:layer" value="pit"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="29">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="30">
  <properties>
   <property name="tile_collision" type="int" value="1"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16">
    <properties>
     <property name="tile_collision_shape" type="int" value="1"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="31"/>
 <tile id="32"/>
 <tile id="33"/>
 <tile id="34"/>
 <tile id="35"/>
 <tile id="36"/>
 <tile id="37">
  <properties>
   <property name="ground" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="38">
  <properties>
   <property name="ground" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="39">
  <properties>
   <property name="ground" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="40">
  <properties>
   <property name="ground" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="41">
  <properties>
   <property name="ground" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="42">
  <properties>
   <property name="ground" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="43">
  <objectgroup draworder="index" id="2">
   <object id="5" x="0" y="16">
    <polygon points="0,-1 0,-4 5,-4 16,2 16,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="44"/>
 <tile id="46">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="2">
    <polygon points="0,0 16,10 16,14 13,14 0,6"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="45"/>
 <tile id="47"/>
 <tile id="48"/>
 <tile id="49"/>
 <tile id="50"/>
 <tile id="51"/>
 <tile id="52"/>
 <tile id="53"/>
 <tile id="54"/>
 <tile id="55"/>
 <tile id="56"/>
 <tile id="57"/>
 <tile id="58"/>
 <tile id="59"/>
 <tile id="60"/>
 <tile id="61"/>
 <tile id="62">
  <animation>
   <frame tileid="59" duration="100"/>
   <frame tileid="60" duration="100"/>
   <frame tileid="61" duration="100"/>
   <frame tileid="62" duration="100"/>
  </animation>
 </tile>
 <tile id="67"/>
 <tile id="68"/>
 <tile id="69">
  <properties>
   <property name="ground" type="bool" value="false"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="6">
    <polygon points="0,0 -12,10 -16,10 -16,8 0,-6"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="70">
  <properties>
   <property name="ground" type="bool" value="false"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="4" x="0" y="0" width="16" height="7"/>
  </objectgroup>
 </tile>
 <tile id="71">
  <properties>
   <property name="ground" type="bool" value="false"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="4">
    <polygon points="0,0 0,-4 3,-4 16,10 16,12 11,12"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="76">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="12" width="16" height="6"/>
  </objectgroup>
 </tile>
 <tile id="77">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="78">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="79">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="92">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="93">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="94">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="95">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="96">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="97">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="98">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="99">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="100">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="101">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="102">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="103">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="104">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="105">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="108">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="109">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="110">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="111">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="113">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="114">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="115"/>
 <tile id="116"/>
 <tile id="117"/>
 <tile id="118"/>
 <tile id="119"/>
 <tile id="120">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="121">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="122">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="123">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="124">
  <properties>
   <property name="ground" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="125">
  <properties>
   <property name="ground" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="126">
  <properties>
   <property name="ground" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="127">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="130">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="131">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="132">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="133">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="134">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="63">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="135">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="136">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="11"/>
 <tile id="12">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="137"/>
 <tile id="138"/>
 <tile id="139"/>
 <tile id="140"/>
 <tile id="141"/>
 <tile id="142"/>
 <tile id="143"/>
 <tile id="144">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="148"/>
 <tile id="149"/>
 <tile id="150"/>
 <tile id="151"/>
 <tile id="152"/>
 <tile id="112">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="129">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="153"/>
 <tile id="154"/>
 <tile id="155"/>
 <tile id="156"/>
 <tile id="157"/>
 <tile id="158"/>
 <tile id="159"/>
 <tile id="160">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="161">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="162">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="163">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="164">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="165">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="166">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="167">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="168">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="145">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="128">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="147">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="169">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="170">
  <properties>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="171"/>
 <tile id="16">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="12" width="16" height="6"/>
  </objectgroup>
 </tile>
 <tile id="17">
  <objectgroup draworder="index" id="2">
   <object id="2" x="0" y="12" width="16" height="6"/>
  </objectgroup>
 </tile>
 <tile id="18">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="12" width="16" height="6"/>
  </objectgroup>
 </tile>
 <tile id="172"/>
 <tile id="64">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="65">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="66">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="173"/>
 <tile id="174"/>
 <tile id="175"/>
 <tile id="176"/>
 <tile id="177"/>
 <tile id="178"/>
 <tile id="179"/>
 <tile id="180"/>
 <tile id="181"/>
 <tile id="146"/>
 <tile id="182"/>
 <tile id="183"/>
 <tile id="184"/>
 <tile id="185"/>
 <tile id="186"/>
 <tile id="187"/>
 <tile id="188"/>
 <tile id="189"/>
 <tile id="190"/>
 <tile id="191"/>
 <tile id="21"/>
</tileset>
