<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.2" name="buildings" tilewidth="800" tileheight="320" tilecount="15" columns="0" fillmode="preserve-aspect-fit">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="238" height="160" source="../../Buildings/01_Circle-a.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="35" y="130" width="166" height="27"/>
  </objectgroup>
 </tile>
 <tile id="1">
  <image width="325" height="156" source="../../Buildings/02_Fuel-riverwest-radio.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="24" y="115" width="293" height="41"/>
  </objectgroup>
 </tile>
 <tile id="2">
  <image width="486" height="320" source="../../Buildings/03_Sunrise.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="23" y="312">
    <polygon points="0,0 322,1 422,-15 422,-39 351,-58 181,-58 -2,-7"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="3">
  <image width="261" height="155" source="../../Buildings/04_Mad-planet.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="12" y="107" width="208" height="48"/>
  </objectgroup>
 </tile>
 <tile id="4">
  <image width="628" height="160" source="../../Buildings/05_Four-seasons.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="38" y="116" width="578" height="40"/>
  </objectgroup>
 </tile>
 <tile id="5">
  <image width="289" height="158" source="../../Buildings/06_WMSE.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="39" y="106" width="207" height="51"/>
  </objectgroup>
 </tile>
 <tile id="6">
  <image width="237" height="160" source="../../Buildings/07_Last-rites.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="55" y="116" width="127" height="42"/>
  </objectgroup>
 </tile>
 <tile id="7">
  <image width="307" height="157" source="../../Buildings/08_Promises.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="28" y="120" width="255" height="35"/>
  </objectgroup>
 </tile>
 <tile id="8">
  <image width="379" height="160" source="../../Buildings/09_Odd-duck-top-shelf.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="26" y="114" width="331" height="43"/>
  </objectgroup>
 </tile>
 <tile id="9">
  <image width="258" height="160" source="../../Buildings/10_Sky-high.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="131" width="248" height="27"/>
  </objectgroup>
 </tile>
 <tile id="10">
  <image width="328" height="160" source="../../Buildings/11_Cactus-club.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="58" y="128" width="261" height="33"/>
  </objectgroup>
 </tile>
 <tile id="11">
  <image width="495" height="160" source="../../Buildings/12_MGR.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="18" y="113" width="465" height="42"/>
  </objectgroup>
 </tile>
 <tile id="12">
  <image width="800" height="160" source="../../Buildings/14_Dredzkas-s_shore-160pxH.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="30" y="135" width="539" height="25"/>
  </objectgroup>
 </tile>
 <tile id="13">
  <image width="552" height="160" source="../../Buildings/15_Hibatchi.png"/>
  <objectgroup draworder="index" id="2">
   <object id="3" x="40" y="132" width="485" height="27"/>
  </objectgroup>
 </tile>
 <tile id="14">
  <image width="273" height="159" source="../../Buildings/16_x-ray_arcade.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="19" y="131" width="245" height="28"/>
  </objectgroup>
 </tile>
</tileset>
