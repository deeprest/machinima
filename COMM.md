
Use aseprite files in project, or export to pngs?
Old Way: Export spritesheets and specify animation frames by index.
New Way: Use Ase Files in project: avoids export. Creates unity anims automatically. 
Old Way:  AnimSequence. Manage frames manually. Currently only uses a constant framerate.
New Way:  Unity animations. Can animate other things than sprites. 
Old Way: "Skins" swap texture. Creates duplicate textures (Unity sprites are BOUND to the texture! so must overwrite image data at runtime using duplicate textures...)
New Way: Sprite Resolver.  Must animate *strings*. **When using the SpriteResolver, animations cannot be created automatically from ASEprite files**
SOLUTION: 
For animations, use ASE files, and create an animation override controller and replace animations with those from alternate ASE file, just swap at runtime.



DEV.md is my todo list.



2) Clarify which systems to target..  
Desktop and Web
  : Box, or Dropbox, or Proton


3) Establish a workflow. 
  : Handoff art folder containing ase and Tiled projects


3) I think we should use Aseprite, and Tiled.

4) Tilemaps, level objects
  * I can edit the tilemap in Unity
  * We could use Tiled ...
  * For static tilemaps, I can use my adhoc importer (tiles should be pixel-perfect clones)
  

2) Do either one of you want to use the Unity editor?
: NO
  * Warning: Being a "developer" involves a lot of headaches (VCS, Unity) ...
  * This requires you to clone the repository, follow some specific initialization steps.
  * If so, I can write instructions or maybe make a video of my workflow (or possibly do a screensharing session?)
  
  
4) How many features of T&C do we want to implement? 
  * Movement, jumping, faceplant, fall down pits



# NOT SENT
Not to make things confusing, but...
In the Unity editor I can combine separate Tiled maps into a single chunk if needed. For instance, we can create a single background parallax layer and reuse that in Unity with different maps from Tiled. We don't necessarily need to have *all* layers in *every* Tiled map.
N maps -> chunk
N chunks -> area (loop of chunks)
N areas -> world





# New build
https://drive.proton.me/urls/TJAEE62WJC#ndptonDCPcFS

## @Steve
I'm glad you like it so far :) You can jump with the spacebar. I can change the controls to anything you like if spacebar to jump doesn't seem right.  If you hold the (forward) "D" key while jumping, the dude jumps off the board (useless for now!).  I haven't decided whether to show the player an image with the controls or little in-game animations that show which button to use in context.  The controls are supposed to be listed in the menu, but it's been a low priority to fix the list because it's tied to the control remapping functionality. 

Next up is obstacles!


## @Alex
I'd like to know more about this killscreen. ("Can you describe the ruckus?") And what is a "code explosion"? :D

If you haven't made changes to "dude BASE AB V1.ase" then here are some fixes I'll upload soon:
1. swapped "accel" frames so foot is on ground at first frame, to avoid visual delay.
2. minor pixel cleanup
3. brake frames were out of sync with dude, fixed.

You said to have 3 parallax layers, one of them being 1:3. I've made those layers 1:4 instead because right now the chunk size is 100 units and is not evenly divisible by 3. If we really want 1:3 and the parallax layers of separate chunks to line up, we'll need to use a multiple of 6 such as 120 units wide.

We absolutely can have groups of chunks that loop! As long as all chunks are the same width, I don't need to write new code. (If we do need to have varying widths, it's not too difficult to implement, but it would take time away from the fun stuff.)  So, when a condition is met, the game will add the new chunks to the active list, making the transition seamless. We can then remove the previous chunks when out of view, so when you enter a new neighborhood/area you loop within that group of chunks until the next condition is met. I assume that is what you had in mind.  So even though each chunk (Tiled map) is the same the size, each area-loop can have any number of chunks.


## Changes
1. Replaced the music with the new album tracks. The songs now loop like a playlist and you can pick any song from the menu as usual.  
2. Map changes. I'm experimenting to see what you guys think.
3. Obstacle tiles. Mostly basic static obstacles so far.  Some obstacles are not functional yet, such as the oil can, potholes, cliffs. 
4. Red barrels that explode when hit
5. Screen shake upon faceplant
6. Bugs, probably. Let me know!
    
## Known issues:
* Some buildings don't have collision yet.   
* Some pieces of the level look janky and overlap when they shouldn't, etc. Level design is fun but time-consuming and highly dependent on available gameplay features, so any maps (made by me) now will be changed later anyway.
* 16:10 aspect ratio does not show everything intended at the bottom of the screen. 16:9 shows more vertical space. This is not intentional but I'll figure it out.
* There might be an animation bug with the little music info box that slides down when a song changes; Do you like having something that shows the song title?  In the future I'd like to add a large psychedelic fullscreen overlay that shows the song title instead of a tiny box in the corner. Just an idea.

## Near Future
* obstacle interactions
* cash as player resource. When taking damage, lose some cash.
* carrying objects, such as beer
* placeholder sounds for many events. We can easily remove/replace them.
* By the way, you can always find my current TODO list here:


## Ideas and Future Stuff... if time permits

  * We could have "soft" obstacles that you can roll through like carboard boxes or cats :) The idea is when you hit them you think "oh crap" but keep rolling-- the goal being the feeling of relief. Maybe there are other solid obstacles that look similar or have the same color. They could block off secret areas, too.
  * Do we want cars to drive by?

I have a few things I've implemented in a different project that might be appropriate:

  * People walking around on preset paths or randomly. They can be obstacles or just decoration. If we're not limited by the performance of a WebGL build, we can have crowds of people walking around.
  * Restaurant name and sign generator.  
  * I also have a name generator that I use to generate fake credits, just for fun. You can see it right now by enabling the "Credits Scroll" checkbox in the settings menu. We could use this to generate names for random people walking around.
  * "Talkers" The ability to interact with people and have a limited conversation. The limitation is that there are no dialogue selections. That would require a branching/tree system that I do not want to implement. I would purchase a plugin for something like that.


###############


The kill screen -> song select is no problem. I can create a toggle setting called "kill-after-song mode" (or something better, suggestions?).

## Project Page ##
I started a project page on itch.io: 
https://deeprest.itch.io/lavish-waste-electric-beach  
It's restricted until published so use the password "woodwater" if you want to check it out in a week or so. It's bare-bones for now.  I'll be adding info, screenshots, gameplay videos within the next two weeks. Let me know if you want me to post the project there, and if you have any preferences about the content.


## Box Art ##
We could use "box art" for a few things:
1. Game icon. I'm using the face of the "dude" for now.
2. Cover Image for project page. From itch.io: "The cover image is used whenever itch.io wants to link to your project from another part of the site. Required (Minimum: 315x250, Recommended: 630x500)".   
Maybe we could use an image of the skateboard guy doing a hop, with a band photo? 
3. (possible) Splash screen. (Alex said this might not be needed/desired?). This would be shown upon game launch, and let the player decide when to "start the album".
Screenshots from the game might really help with this. You'll know what I mean when you see...

## The Killscreens ##
My attempts to make a killscreen look more modern/psychedelic and not retro... If these are not adequate tell me, and I'll try to make a more retro effect. My previous attempts involved switching tiles and colors randomly but they looked kind of lame.
So here's what I have so far. If you don't like them for the kill screens, we could use them as "intoxication effects".
This is a 500GB video- sorry about the size.
https://drive.proton.me/urls/NP0JY92BVG#2ZP3bkpCaYIN



## WebGL ##
The WebGL build looks great but the framerate lags around 25 for me.  Interestingly, the fullscreen mode uses a large WebGL canvas instead of simply scaling-up the lower resolution and this slows the game. BUT- if I "zoom" in the browser (Firefox, on my macbookpro) by using the trackpad and, uh, un-pinching... it scales up correctly and the framerate is glorious!!  WebGL games need a server to host, and itch.io is perfect for that, but we could deploy it on your site, too. I might try to upload to my website just to see if there's any difficulty or hidden requirements for that. I've never actually launched a web game, but since this game is entirely in-browser, I can't imagine it being too difficult because it doesn't need to actually communicate with the server.


## Maps / Level size
While I said earlier that we'd have 13 maps, I don't think that's realistic anymore for the remaining time before launch.  I'm now planning to have 2 large "chunks" with multiple paths through them.  They are bigger than the previous maps, and I think they'll have enough variation to be adequate for version 1 at launch.  The main reason my estimate was wrong before is because 1) I upgraded the art style to an isometric view, and 2) the way maps must be created in Tiled might be frustrating for you guys to jump in and start editing.  I hope this is okay with you guys, for the launch version.  I still want to create more maps and keep adding fun stuff after launch.


## About the Tiled maps ##
I'm happy to do the map editing myself, but just in case you want to try editing the maps, there's a few things to know.
The important layers in each map:
Ground Layer: For road, sidewalk, or anything lying "flat" on the surface.  I place the ground tiles first. We can add another ground layer for any decorative decals on the surface, instead of creating special one-use tiles.
Sort Layer: The "vertical" layer. For anything the skateboard dude can move in *front of* or *behind* goes on the sort layer. Things like guardrails, trees, barrels, streetlights, etc. (this layer is defined by the unity:sortingorder=10
Object Layer: where the camera bounding box and invisible collision shapes are placed.

Tiled map editor tips:
I don't know how much time you've spend with Tiled, but for a quick-start, these are the shortcut keys I use the most:
R = Rectangle Select. Use this to select a section of tiles. You can copy and paste sections for quick duplication. When an area is selected, you can ONLY modify the tiles within that selection.
Command+Shift+A = deselect
B = Brush Tool. Right-Click (or two-finger click on a laptop trackpad) to pick a tile directly from the map.
W = Magic Wand Select. Select all contiguous tiles of the same tile ID.
S = Select Same Tile. Non-contiguous select of the same tile ID in the entire map.
H = Highlight. This makes it easier to see only the tiles on the currently selected layer. Try selecting the "sort" layer and pressing H to see why this is useful.



#########

"Is it possible to make the camera a bit lower to the ground, so you get a bit more of the background and building height?"

This is an interesting question. The answer is yes, and it looks better. 
The trade-off is that depth perception for gameplay is more difficult. With a "more-compressed depth", the player might run into things without knowing exactly where the boundaries of objects are. But it might turn out okay.
[insert image]
I cannot simply rotate the camera without everything getting skewed like in the above image. We can only achieve this by changing the art to simulate a different perspective. This means redo-ing the tiles so they have more "angle" and modifying the map to have less rows of tiles.  I might be able to finish this by saturday morning.

## Step 1: Change the art to "lower the camera". 
But all this really accomplishes is that more stuff (within our simulated-depth) can fit on screen, because it requires fewer tiles (vertically) to show the same amount of world depth.  He's a quick mockup of what that can look like:
[image]

But there's still the problem of not seeing most of the building face. 
Step 2: we must shrink the buildings, or increase the camera "size".
[image]
[image]
This is a little better, but if we increase the camera size too much, the player character becomes too small.


Step 3: move the camera to see more of the buildings as the player goes by.  Two ways to do this:
1. "Squeeze"  Use camera boundaries in the map to push the camera towards the notable buildings when the player moves through a bottleneck.  This is level design task and can't be done for all buildings.
2. "Slide"   Dynamically slide the camera a bit towards the point of interest when the player gets close enough. Enabled with a trigger shape placed into the map. This can look really cool when done right, but can potentially pull the camera away from something the player wants to see. I could make the camera slide back to normal if the player moves far enough away. Many possibilities.

I'm already doing Step 1 and 2, but 3 might take too much time away from the other things I need to do within the next week.

Time to sleep!








