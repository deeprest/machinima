

# Controls
* Menu / Pause = Esc
* You can change the music from the menu, select different scenes, and eventually view/modify the controls.
* Respawn = F1
* Move = ASDW
* Jump = space
* Accel = D
* Brake = A
* Unlock the mouse cursor = Tab 
* Ghost Mode (for looking around) = F2
* Screenshot = F5  (writes image to "/Users/username/Library/Application Support/deeprest/woodwater")
* Slow motion = Alt+O  (Alt+P will pause)
* Dev console (not used yet) = tilde

# Note
1. Supported aspect ratios: 16:9 and 16:10
2. Screen size is ~20x12 units (16px=1unit)

# Done
+ Project initialization. Gitlab setup. Appropriate engine version. 
+ Import useful code from old project.  Removal of unecessary features, and some improvements.
+ level import utility upgraded to work with newer tilemap module.
+ endless looping level built from zones.
+ Make a better template level in Tiled. Needs to be a feature-complete map. Include in new build and send him the source map.
+ music album loop instead of scene music or random
+ during wipeout, have camera follow rider instead of stopping at contact point.
+ (dude V1.ase) reordered "accel" frames so foot is on ground at first frame.
+ added screen shake when rider collides with an obstacle
+ tiled: hide/remove grey cityscape layer
+ tiled: 3 layers; 1:1, 1:2, 1:4
+ add HitUpdate to pawn. make some boxes explode when hit. 
+ use Tab to release cursor lock in builds
+ dev info
+ mask size
+ crop camera option
+ background no asphault
+ Show Buildings...better. Larger view size; camera bound in level to show some buildings more; or full tile redo(!); iso rotation not possible without billboarding every vertical object... maybe a SuperTile customization could do an object replacement for this. [did tile redo]
+ longer levels, new buildings
  * runtime is 1307 seconds (22min). Max speed 10 u/s. Chunk width 1000.
  * [**TWO CHUNKS BEFORE LAUNCH** loop]
## AUTOPLAY.
  + generate navmesh from tile collision obtained from SuperTile custom property.
  + AI controller.
  + PlayerController goes automatic after non-input delay.
  + add another skateboarder using autoplay. Be sure to remove AI avatar if it exits any active chunk- or possibly hold it at the edge and parent to chunk?
+ fire hydrant collision
+ tab to open menu in web build
+ UPLOAD WEBGL BUILD TO ITCH
+ remove duplicated pseudo depth buildings, just use face
+ "kill-after-song mode”
  1. toggle setting
  2. faceplant upon song conclusion
  3. spawn at (specific?) respawn point
  + Kill screen during or at end of track "Lavender Tone"; use timer, be sure to stop timer upon song change.
1. hop state
2. renderer layer
3. camera shake; amp start at .05 and increase to .2-.5 over 30s duration?;
4. disable camera zones
5. raise into sky
6. whatever is on background layer should be only thing showing at the end; maybe have "Electric Beach" written in sand?
7. blur. Must have Default in cam render layers because the raw image renders on Default layer;
+ WebGL build
  * slow in browser fullscreen mode. pinch-zoom scales correctly.
  * Deploy on itch.io



## FIX

- game breaks easily after final kill screen. Showstopper. For instance, hit respawn or reload the scene. Needs cleanup function.
- ONGOING: fix any collision tunnelling (change shape to have a more shallow angle, or increase raycast distance)

### AI / controllers
- AI controller will stall intermittently. Just sits there. Looks like pathing fails. Could avoid with trailing spawn position that does a raycast first to guarantee forward-clearance.
- AI dudes fail to respawn. Possibly update-culled.
- ghost pawn enable latent velocity. Not apparent on MacOS build. Needs repro.
- 
### rendering / camera / mush
- !! Effect transitions are not interpolating correctly! During No Joke Mama every effect has a HARD CUT. Console command "mush X" does not interpolate, either.
- Cropping: fix camera rendering outside mushroom image. This happens when setting CropScreen is off. So either force cropping, or expand mush rect? Credits can render outside of cropped screen also because there is no mask.
- moving vertically at the start makes chunk_2 disappear. Is this a chunk/wrap problem or are the sprite renderers being culled?
- explosive barrel does not use sort layer. Look at dynamic sort. 
- Maps: fix collision for buildings. You can move behind them while in front.

### song timers
- song timer progress seems to be affected by pause menu / slomo / alt-pause... and it throws kill screen transitions off. 
- final kill screen: after album restarts and end-of-song killscreen is triggered, the fall animation is not correct... because it's being force




# TODO
- remove: 90 degree mush rotation.
- song 2 (panopticon) should have a mush=1 effect.

## Tech
- Editor: UghSettings is not reading settings file correctly.
- Apple Developer account, to remove dev warning on app launch. Send photo-ID. Must enroll a third time with government name on Apple ID.
- Try using Codecks for project progress
- bug report server


## Next Milestone
- Start screen
  1. options: play full album or select song. (spawn point for each song?)
- jump parabola. Currently it lerps up and then down. don't be so lazy
 
- pixel perfect camera resolution VS screen resolutions. Fewer options is better. Try to reduce options.
* 320x192
* 416x250
* 416x240
* 480x288

- Use cash/money as a game resource. Receive damage and lose money, gain enough and bonus features reveal themselves.
- Carry Objects; beverage/it[README.md](README.md)em.
  - when fall, lose beverage. UI indicator.
  - how to grab beverages? On contact, jump and hit mid-air, or highlight with indicator and button press?
  1. UI icon, level meter
  2. lose money when damaged. particle system, anim, flashing grace period
  3. position avatar near collision location. Use nav mesh?
  4. Placeholder: use a modified sprite when there's enough cash in pocket.


- (?) record/playback of input

## Stretch Goals / Maybe
- jump while moving vertically; possible to jump past collision boundaries if not large enough; must detect land position and do faceplant; new anims needed?
- scooter
- beach; surf area
- walkers
- talkers. talking interaction. Could use YAML for dialogue.
- indexed color shader; export process from aseprite is tedious. Maybe write import script that takes image+palette and generateds R-indexed image?


* Pawn movement.
    + accel
    + speed limit. speed minimum!
    + turns. initial delay before turn anim plays.
    + brake. (no move backwards. camera behavior. Maybe if there's time)
    + jump while holding board (hop)
    - jump off board. Speed remains constant while in air.  [Needs board collision]
    - grind on rail

* Interactions
    + board hits obstacle: normal => fly through air, faceplant. crash sprite. Camera follows rider.
    + fall into pit / fall off back of road. puff sprite. sound.
    - jump off ramp
    - stand-up animation
    - pickups
    ? rough terrain
    
* Active Obstacles
    * cars?
    * walkers. cat?



## Art
- tiled: grass or stone ground textures to anchor buildings to their depth
- "At some point I think the barriers/rails should be more like curbs or concrete. The barrier blocks have lots of neighborhood-specific potential: road rubble, construction barricades, cats, rats, drunks, trash, tires. Will be fun to go in later and swap these out."
 

### Loop Feature (Area loops)
- feature: if player avoids falling for duration -> add next chunk set and remove previous after entering new set.
- condition / trigger -> add new chunks.
- when previous area chunks are out of view, remove them.


