This game is a **playable music album** that has entirely optional gameplay. It's something to play (or watch) while you listen to the music.  "Electric Beach" is an album released by the band Lavish Waste. Lavish Waste is based in [Milwaukee, Wisconsin](https://www.youtube.com/watch?v=nRCTc6stICc "Everything I know about Milwaukee I learned from Alice Cooper in Wayne's World.").

Inspired by the original NES game [Town & Country Surf Designs: Wood & Water Rage](https://en.wikipedia.org/wiki/Town_%26_Country_Surf_Designs%3A_Wood_%26_Water_Rage), the game has an isometric view, and includes pixelated photos of actual Milwaukee landmarks, as one might see while skating through town.

This is still in development, so we're aware of all the horrible bugs... and will destroy them. There are many planned improvements for the next update.

## [Listen to the album on Bandcamp](https://lavishwaste.bandcamp.com/album/electric-beach)


* ### [Play the WebGL build on itch.io](https://deeprest.itch.io/lavish-waste-electric-beach)

* ### [MacOS build is available for download.](https://deeprest.itch.io/lavish-waste-electric-beach)
* ### Linux and Windows builds will be released <em>soon</em>.
![soon](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fy.yarn.co%2F763468a7-252d-48ba-9a20-14629796200a_text.gif&f=1&nofb=1&ipt=8f11d80b97cff671e3d32b5dd9f3fb70fe251b70452d86d7312488570da57535&ipo=images)






